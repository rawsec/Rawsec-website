---
title: Domains
layout: "page"
---
Official domain names of Rawsec services.

The blog:

URL                                     | Description
----------------------------------------|----------------------------
https://blog.raw.pm/                    | Main domain
hxxps://rawsec[.]ml/                    | Backup domain, taken over
hxxps://rawsec[.]gq/                    | Backup domain, unavailable
hxxps://rawsec[.]cf/                    | Backup domain, unavailable
hxxps://rawsec[.]tk/                    | Backup domain, unavailable
hxxps://rawsec[.]ga/                    | Backup domain, unavailable
hxxps://xn--en8h[.]cf/                  | Backup domain, unavailable
https://rawsec-blog.netlify.app/        | Dev domain
https://rawsec.gitlab.io/Rawsec-website | Gitlab domain, broken paths

The inventory:

URL                                                 | Description
----------------------------------------------------|----------------------------
https://inventory.raw.pm/                           | Main domain
hxxps://inventory[.]rawsec[.]ml/                    | Backup domain, taken over
https://rawsec-cybersecurity-inventory.netlify.app/ | Dev domain
https://rawsec.gitlab.io/rawsec-cybersecurity-list/ | Gitlab domain, broken paths

The write-up factory:

URL                                       | Description
------------------------------------------|----------------------------
https://writeup.raw.pm/                   | Main domain
https://writeup-factory.netlify.app/      | Dev domain
https://rawsec.gitlab.io/writeup-factory/ | Gitlab domain
