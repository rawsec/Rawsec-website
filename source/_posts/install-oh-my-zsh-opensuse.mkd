---
layout: post
title: "Install Oh-my-zsh on openSUSE"
lang: en
categories:
  - linux
  - opensuse
tags:
  - linux
  - opensuse
date: 2014-10-24 00:00:00
thumbnail: /images/opensuse.svg
authorId: noraj
---

+ Install **zsh** `zypper in zsh` and **git** `zypper in git`.
+ Then install **oh-my-zsh** `wget --no-check-certificate http://install.ohmyz.sh -O - | sh`.
+ Then define **zsh** as your default shell: `chsh -s /bin/zsh`.
