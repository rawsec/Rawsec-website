---
layout: post
title: "IceCTF - 60 - Blue Monday - Misc"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - misc
date: 2016/08/25
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : IceCTF 2016
- **Website** : [https://icec.tf/](https://icec.tf/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/319)

### Description

Those who came before me lived through their vocations From the past until completion, they'll turn away no more And still I find it so hard to say what I need to say But I'm quite sure that you'll tell me just how I should feel today. [blue_monday](https://play.icec.tf/problem-static/blue_monday_ff0973317ee7c2df4225f994ad49bb4075546b9f20eb22bbc636be910f628bfd)

## Solution

1. Let's see what this file is!
```
file blue_monday
blue_monday: Standard MIDI data (format 1) using 1 track at 1/220
```
2. It's some MIDI file, we've to convert it into a text format. I used [this online service][midi2txt]. That gave me:
```
MFile 0 1 220
MTrk
0 On ch=1 n=73 v=100
220 Off ch=1 n=73 v=0
220 On ch=1 n=99 v=100
440 Off ch=1 n=99 v=0
440 On ch=1 n=101 v=100
660 Off ch=1 n=101 v=0
660 On ch=1 n=67 v=100
880 Off ch=1 n=67 v=0
880 On ch=1 n=84 v=100
1100 Off ch=1 n=84 v=0
1100 On ch=1 n=70 v=100
1320 Off ch=1 n=70 v=0
1320 On ch=1 n=123 v=100
1540 Off ch=1 n=123 v=0
1540 On ch=1 n=72 v=100
1760 Off ch=1 n=72 v=0
1760 On ch=1 n=65 v=100
1980 Off ch=1 n=65 v=0
1980 On ch=1 n=99 v=100
2200 Off ch=1 n=99 v=0
2200 On ch=1 n=107 v=100
2420 Off ch=1 n=107 v=0
2420 On ch=1 n=49 v=100
2640 Off ch=1 n=49 v=0
2640 On ch=1 n=110 v=100
2860 Off ch=1 n=110 v=0
2860 On ch=1 n=57 v=100
3080 Off ch=1 n=57 v=0
3080 On ch=1 n=95 v=100
3300 Off ch=1 n=95 v=0
3300 On ch=1 n=109 v=100
3520 Off ch=1 n=109 v=0
3520 On ch=1 n=85 v=100
3740 Off ch=1 n=85 v=0
3740 On ch=1 n=53 v=100
3960 Off ch=1 n=53 v=0
3960 On ch=1 n=73 v=100
4180 Off ch=1 n=73 v=0
4180 On ch=1 n=99 v=100
4400 Off ch=1 n=99 v=0
4400 On ch=1 n=95 v=100
4620 Off ch=1 n=95 v=0
4620 On ch=1 n=87 v=100
4840 Off ch=1 n=87 v=0
4840 On ch=1 n=49 v=100
5060 Off ch=1 n=49 v=0
5060 On ch=1 n=55 v=100
5280 Off ch=1 n=55 v=0
5280 On ch=1 n=104 v=100
5500 Off ch=1 n=104 v=0
5500 On ch=1 n=95 v=100
5720 Off ch=1 n=95 v=0
5720 On ch=1 n=109 v=100
5940 Off ch=1 n=109 v=0
5940 On ch=1 n=73 v=100
6160 Off ch=1 n=73 v=0
6160 On ch=1 n=68 v=100
6380 Off ch=1 n=68 v=0
6380 On ch=1 n=49 v=100
6600 Off ch=1 n=49 v=0
6600 On ch=1 n=53 v=100
6820 Off ch=1 n=53 v=0
6820 On ch=1 n=95 v=100
7040 Off ch=1 n=95 v=0
7040 On ch=1 n=76 v=100
7260 Off ch=1 n=76 v=0
7260 On ch=1 n=51 v=100
7480 Off ch=1 n=51 v=0
7480 On ch=1 n=116 v=100
7700 Off ch=1 n=116 v=0
7700 On ch=1 n=53 v=100
7920 Off ch=1 n=53 v=0
7920 On ch=1 n=95 v=100
8140 Off ch=1 n=95 v=0
8140 On ch=1 n=72 v=100
8360 Off ch=1 n=72 v=0
8360 On ch=1 n=52 v=100
8580 Off ch=1 n=52 v=0
8580 On ch=1 n=118 v=100
8800 Off ch=1 n=118 v=0
8800 On ch=1 n=69 v=100
9020 Off ch=1 n=69 v=0
9020 On ch=1 n=95 v=100
9240 Off ch=1 n=95 v=0
9240 On ch=1 n=97 v=100
9460 Off ch=1 n=97 v=0
9460 On ch=1 n=95 v=100
9680 Off ch=1 n=95 v=0
9680 On ch=1 n=114 v=100
9900 Off ch=1 n=114 v=0
9900 On ch=1 n=52 v=100
10120 Off ch=1 n=52 v=0
10120 On ch=1 n=118 v=100
10340 Off ch=1 n=118 v=0
10340 On ch=1 n=51 v=100
10560 Off ch=1 n=51 v=0
10560 On ch=1 n=125 v=100
10780 Off ch=1 n=125 v=0
11780 Meta TrkEnd
TrkEnd
```
3. Alternatively you can use [midicsv][midicsv] that convert MIDI to CSV. It's only up to you which file you prefer to parse.
```
0, 0, Header, 1, 1, 220
1, 0, Start_track
1, 0, Note_on_c, 0, 73, 100
1, 220, Note_off_c, 0, 73, 0
1, 220, Note_on_c, 0, 99, 100
1, 440, Note_off_c, 0, 99, 0
1, 440, Note_on_c, 0, 101, 100
1, 660, Note_off_c, 0, 101, 0
1, 660, Note_on_c, 0, 67, 100
1, 880, Note_off_c, 0, 67, 0
1, 880, Note_on_c, 0, 84, 100
1, 1100, Note_off_c, 0, 84, 0
1, 1100, Note_on_c, 0, 70, 100
1, 1320, Note_off_c, 0, 70, 0
1, 1320, Note_on_c, 0, 123, 100
1, 1540, Note_off_c, 0, 123, 0
1, 1540, Note_on_c, 0, 72, 100
1, 1760, Note_off_c, 0, 72, 0
1, 1760, Note_on_c, 0, 65, 100
1, 1980, Note_off_c, 0, 65, 0
1, 1980, Note_on_c, 0, 99, 100
1, 2200, Note_off_c, 0, 99, 0
1, 2200, Note_on_c, 0, 107, 100
1, 2420, Note_off_c, 0, 107, 0
1, 2420, Note_on_c, 0, 49, 100
1, 2640, Note_off_c, 0, 49, 0
1, 2640, Note_on_c, 0, 110, 100
1, 2860, Note_off_c, 0, 110, 0
1, 2860, Note_on_c, 0, 57, 100
1, 3080, Note_off_c, 0, 57, 0
1, 3080, Note_on_c, 0, 95, 100
1, 3300, Note_off_c, 0, 95, 0
1, 3300, Note_on_c, 0, 109, 100
1, 3520, Note_off_c, 0, 109, 0
1, 3520, Note_on_c, 0, 85, 100
1, 3740, Note_off_c, 0, 85, 0
1, 3740, Note_on_c, 0, 53, 100
1, 3960, Note_off_c, 0, 53, 0
1, 3960, Note_on_c, 0, 73, 100
1, 4180, Note_off_c, 0, 73, 0
1, 4180, Note_on_c, 0, 99, 100
1, 4400, Note_off_c, 0, 99, 0
1, 4400, Note_on_c, 0, 95, 100
1, 4620, Note_off_c, 0, 95, 0
1, 4620, Note_on_c, 0, 87, 100
1, 4840, Note_off_c, 0, 87, 0
1, 4840, Note_on_c, 0, 49, 100
1, 5060, Note_off_c, 0, 49, 0
1, 5060, Note_on_c, 0, 55, 100
1, 5280, Note_off_c, 0, 55, 0
1, 5280, Note_on_c, 0, 104, 100
1, 5500, Note_off_c, 0, 104, 0
1, 5500, Note_on_c, 0, 95, 100
1, 5720, Note_off_c, 0, 95, 0
1, 5720, Note_on_c, 0, 109, 100
1, 5940, Note_off_c, 0, 109, 0
1, 5940, Note_on_c, 0, 73, 100
1, 6160, Note_off_c, 0, 73, 0
1, 6160, Note_on_c, 0, 68, 100
1, 6380, Note_off_c, 0, 68, 0
1, 6380, Note_on_c, 0, 49, 100
1, 6600, Note_off_c, 0, 49, 0
1, 6600, Note_on_c, 0, 53, 100
1, 6820, Note_off_c, 0, 53, 0
1, 6820, Note_on_c, 0, 95, 100
1, 7040, Note_off_c, 0, 95, 0
1, 7040, Note_on_c, 0, 76, 100
1, 7260, Note_off_c, 0, 76, 0
1, 7260, Note_on_c, 0, 51, 100
1, 7480, Note_off_c, 0, 51, 0
1, 7480, Note_on_c, 0, 116, 100
1, 7700, Note_off_c, 0, 116, 0
1, 7700, Note_on_c, 0, 53, 100
1, 7920, Note_off_c, 0, 53, 0
1, 7920, Note_on_c, 0, 95, 100
1, 8140, Note_off_c, 0, 95, 0
1, 8140, Note_on_c, 0, 72, 100
1, 8360, Note_off_c, 0, 72, 0
1, 8360, Note_on_c, 0, 52, 100
1, 8580, Note_off_c, 0, 52, 0
1, 8580, Note_on_c, 0, 118, 100
1, 8800, Note_off_c, 0, 118, 0
1, 8800, Note_on_c, 0, 69, 100
1, 9020, Note_off_c, 0, 69, 0
1, 9020, Note_on_c, 0, 95, 100
1, 9240, Note_off_c, 0, 95, 0
1, 9240, Note_on_c, 0, 97, 100
1, 9460, Note_off_c, 0, 97, 0
1, 9460, Note_on_c, 0, 95, 100
1, 9680, Note_off_c, 0, 95, 0
1, 9680, Note_on_c, 0, 114, 100
1, 9900, Note_off_c, 0, 114, 0
1, 9900, Note_on_c, 0, 52, 100
1, 10120, Note_off_c, 0, 52, 0
1, 10120, Note_on_c, 0, 118, 100
1, 10340, Note_off_c, 0, 118, 0
1, 10340, Note_on_c, 0, 51, 100
1, 10560, Note_off_c, 0, 51, 0
1, 10560, Note_on_c, 0, 125, 100
1, 10780, Note_off_c, 0, 125, 0
1, 11780, End_track
0, 0, End_of_file
```
4. I used the first one because I found it's was easier to parse.
5. To get the flag, we need to the value of `n` into ASCII only for `On`/`Note_on_c` lines (or only `Off`/`Note_off_c,`) because each value is in double.
6. Here's my script:
```python
import re

filename = "blue_monday_alt.txt"

binary = ""
text = ""

with open(filename, 'r') as fh:
    for line in fh:
        #print(line.rstrip('\n'))
        if 'On' in line:
            regex_result = re.compile(r'n=(.*?)\sv=').search(line)
            if regex_result:
                text += chr(int(regex_result.group(1)))

print(text)
```
7. And the result:
```
python3 blue_monday.py
IceCTF{HAck1n9_mU5Ic_W17h_mID15_L3t5_H4vE_a_r4v3}
```

[midi2txt]:http://flashmusicgames.com/midi/mid2txt.php "Binary MIDI file to text (MF2T/T2MF format) conversion"
[midicsv]:https://www.fourmilab.ch/webtools/midicsv/
