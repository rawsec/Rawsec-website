---
layout: post
title: "Generate background pattern in Ruby"
date: 2020/03/19
lang: en
categories:
- programming
- ruby
tags:
- programming
- ruby
thumbnail: /images/programming.jpg
authorId: noraj
---
We will see how to generate a SVG background with triangle patterns in ruby to
have something that look likes this:

![](https://sendeyo.com/up/d/e4d48f0e7a)

At first, install the [TrianglePattern](https://github.com/hendrixfan/triangle) gem:

```
$ gem install triangle_pattern
```

Then we only need a very short script with some simple options:

- an array of color (a gradient preferably) that we can generate with a website like https://cssgradient.io/
- the width and height of the image
- `cell_size`: size of the mesh used to generate triangles
- `variance`: amount of randomness used when generating triangles
- `seed`: seed of the random generator

```ruby
require 'triangle_pattern'

pattern = TrianglePattern.generate(
  colors: ['#020024', '#6c1678', '#ff6400'],
  width: 1920,
  height: 1080,
  cell_size: 45,
  variance: 0.80,
  seed: 1337
)

File.open('background.svg', 'w') { |f| f.write(pattern.to_svg) }
```


