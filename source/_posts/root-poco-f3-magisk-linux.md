---
layout: post
title: "Root POCO F3 using Magisk from Linux"
date: 2022/05/30 23:00:00
updated: 2022/08/04 13:37
lang: en
categories:
- linux
- android
tags:
- linux
- android
- root
thumbnail: /images/android-2995824.svg
authorId: noraj
toc: true
---
**Disclaimer**: for advanced power user only. If you don't understand what your are doing, then don't do it.

## Prerequisites

- Phone unlocked (OEM unlock / Mi unlock)
- USB debug enabled

## Rooting with Magisk

Download the [latest release][magrel] of [Magisk][magisk] on your phone and
install it. For example `Magisk-v24.3.apk` as of Mar 10, 2022.

Then, on your PC, [download the stable ROM][ROMF3] of MIUI corresponding to your region and the version installed on your phone.
For example [V13.0.4.0.SKHEUXM - European version](https://bigota.d.miui.com/V13.0.4.0.SKHEUXM/miui_ALIOTHEEAGlobal_V13.0.4.0.SKHEUXM_17e135935d_12.0.zip).

Note: the same steps can be followed if you are running [LineageOS](https://blog.raw.pm/en/install-lineageos-poco-f3/), just by using [LineageOS installation package](https://download.lineageos.org/alioth) instead.

Plug your phone to your PC with an USB cable.

Run `adb devices` to pair your PC with your phone (accept the popup on the phone).

Extract the ROM to a folder (eg. `miui`): `7z x miui_ALIOTHEEAGlobal_V13.0.4.0.SKHEUXM_17e135935d_12.0.zip -omiui`.

As we can see on Magisk app, the POCO F3 has a ramdisk, so as
explained [in Magisk documentation][maginstall] we need to fetch a copy of
`boot.img`. But as we can see the ROM image contains only a big `payload.bin` file.

```
$ 7z l miui_ALIOTHEEAGlobal_V13.0.4.0.SKHEUXM_17e135935d_12.0.zip

7-Zip [64] 17.04 : Copyright (c) 1999-2021 Igor Pavlov : 2017-08-28
p7zip Version 17.04 (locale=fr_FR.UTF-8,Utf16=on,HugeFiles=on,64 bits,12 CPUs x64)

Scanning the drive for archives:
1 file, 3439948000 bytes (3281 MiB)

Listing archive: miui_ALIOTHEEAGlobal_V13.0.4.0.SKHEUXM_17e135935d_12.0.zip

--
Path = miui_ALIOTHEEAGlobal_V13.0.4.0.SKHEUXM_17e135935d_12.0.zip
Type = zip
Physical Size = 3439948000
Comment = signed by SignApk

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2009-01-01 00:00:00 .....          630          630  META-INF/com/android/metadata
2009-01-01 00:00:00 .....         1039         1039  META-INF/com/android/metadata.pb
2009-01-01 00:00:00 .....          633          633  apex_info.pb
2009-01-01 00:00:00 .....          345          345  care_map.pb
2009-01-01 00:00:00 .....   3439941522   3439941522  payload.bin
2009-01-01 00:00:00 .....          156          156  payload_properties.txt
2009-01-01 00:00:00 .....         1594         1077  META-INF/com/android/otacert
------------------- ----- ------------ ------------  ------------------------
2009-01-01 00:00:00         3439945919   3439945402  7 files
```

We'll have to extract `boot.img` from `payload.bin`. For this we'll use
[payload dumper](https://github.com/vm03/payload_dumper) (see [Android OTA payload dumping / extraction: 4 tools review](https://blog.raw.pm/en/android-OTA-payload-dumping/) to see how to do it with `ssut/payload-dumper-go ` which is much faster).

```
$ git clone https://github.com/vm03/payload_dumper
$ cd payload_dumper
```

Install the dependencies, for me on ArchLinux: `pacman -S python-protobuf python-six --needed --asdeps && pikaur -S python-bsdiff4`,
else you can still create a virtual environment and pip install `requirements.txt` or use the docker image.

Then launch the extraction:

```
$ python payload_dumper.py ../miui/payload.bin
```

The extracted boot image is stored in `output/boot.img`.

We have a separate vbmeta partition because `vbmeta.img` is extracted from the firmware
package and we can also confirm by running `adb shell ls -l /dev/block/by-name`
that shows `vbmeta_a` and `vbmeta_b`.

We can copy the boot image we extracted to the phone with the following
command:

```
$ adb push output/boot.img storage/self/primary/Download/
```

Now we can click `Install` on the Magisk app. You will have to select `boot.img`
from the download folder to select the image to patch.

Then we'll download the patched image to our PC (the path is written in Magisk logs).

```
$ adb pull /storage/emulated/0/Download/magisk_patched-[ramdom].img
```

[Treble Check](https://play.google.com/store/apps/details?id=com.kevintresuelo.treble) app
show we have a A/B partition system, so this will change the flash method.

Reboot the phone in bootloader/fastboot mode:

```
$ adb reboot bootloader
```

In fastboot mode flash the boot image:

```
$ fastboot flash boot_ab magisk_patched-[ramdom].img
```

Now reboot the phone.

We can launch the Magisk app to check the root was successful.

[magisk]:https://github.com/topjohnwu/Magisk/
[magrel]:https://github.com/topjohnwu/Magisk/releases
[ROMF3]:https://c.mi.com/oc/miuidownload/detail?device=1900394
[maginstall]:https://topjohnwu.github.io/Magisk/install.html
