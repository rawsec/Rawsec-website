---
layout: post
title: "RCTF 2017 - intoU - Misc"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - misc
date: 2017/05/23
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : RCTF 2017
- **Website** : [ctf.teamrois.cn/](http://ctf.teamrois.cn/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/450)

### Description

> I'm so into you I can barely breath.And all I wanna do is to fall in deep
>
> https://static2017.teamrois.cn/misc_745cfbae1f7e94f0a49e9f81a69d72e4/intoU_71725c59e10e615f74d3ba94af441f5c.zip

For this challenge we have a wav file `Ariana Grande - Into You (Samurah & DVZE Remix).wav`. I didn't listen to it so I can't tell you how the music feels but I can tell you this must be a steganography challenge.

So using `Audacity` to visualize the file, I switched from **Waveform** to **Spectrogram**. There I went to the end of the file an see something unusual: there is a blank cut and then something that doesn't look like music.

![](https://i.imgur.com/Tcy2wrK.png)

Let's zoom.

![](https://i.imgur.com/d8QyCZQ.png)

We are able to read somethind ending with `_wav`, this must be part of the flag.

Now let's change some spectrogram settings.

+ Scale: `Linear` => `Mel`
+ Maximum frequency: `8000 Hz` => `22050 Hz`
+ Window size: `256` => `2048`
+ Check `Show the spectrum using grayscale colors`

This is much readable now:

![](https://i.imgur.com/YzyPZWP.png)

Flag is: `RCTF{bmp_file_in_wav}`.
