---
layout: post
title: "How to enhance BurpSuite (or any other Java app) font rendering"
date: 2020/09/26 20:45:00
lang: en
categories:
- security
tags:
- burp
thumbnail: /images/security-265130_640.jpg
authorId: noraj
toc: false
---
If we read *Java - Better font rendering*
on ArchLinux wiki we can read this:

> Both closed source and open source implementations of Java are known to have improperly implemented anti-aliasing of fonts. This can be fixed with the following options: `-Dawt.useSystemAAFontSettings=on`, `-Dswing.aatext=true`

So to fix font rendering in BurpSuite Pro for example, we just have to edit
`~/BurpSuitePro/BurpSuitePro.vmoptions` into:

```
# Enter one VM parameter per line
# For example, to adjust the maximum memory usage to 512 MB, uncomment the following line:
# -Xmx512m
# To include another file, uncomment the following line:
# -include-options [path to other .vmoption file]

-XX:MaxRAMPercentage=50
-Dawt.useSystemAAFontSettings=on
-Dswing.aatext=true
```

A note about anti-aliasing availability:

> Anti-aliasing of fonts is available with Oracle Java 1.6 and OpenJDK on Linux.

For any other Java app call your Java app like this:

```
$ _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true' executable
```

## References

- [Java - Better font rendering](https://wiki.archlinux.org/index.php/Java#Better_font_rendering)
- [Java Runtime Environment fonts](https://wiki.archlinux.org/index.php/Java_Runtime_Environment_fonts)
