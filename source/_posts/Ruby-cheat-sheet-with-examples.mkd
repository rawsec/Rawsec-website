---
layout: post
title: 'Ruby cheat sheet with examples'
date: 2017/02/20
lang: en
categories:
- programming
- ruby
tags:
- linux
- programming
- ruby
thumbnail: /images/programming.jpg
authorId: noraj
---
Take it ~~easy~~ Ruby!

## Basic concepts

### Hello World

```ruby
puts "Hello World!"
```

### Comments

```ruby
# single line comment

=begin
  Multi-lines
  comment
=end
```

### Variables

```ruby
my_var = 42
My_constant_var = 31337
```

### Shorthand assignment

```ruby
a += b # a = a + b
a -= b # a = a - b
a *= b # a = a * b
a /= b # a = a / b
a %= b # a = a % b
a **= b # a = a**b
```

### Parallel assignment

```ruby
a, b, c = 11, 22, 33
```

### Input

```ruby
input = gets
```

## Control structures

### Equality

```ruby
irb(main):001:0> 42 == 42.0 # equal values
=> true
irb(main):002:0> 42.eql?(42.0) # equal values and same type
=> false
```

### Subsumption

Be cautious!

```ruby
irb(main):004:0> 42 === (10..100)
=> false
irb(main):005:0> 42 === (50..100)
=> false
irb(main):006:0> (10..100) === 42
=> true
irb(main):007:0> (50..100) === 42
=> false
irb(main):008:0> Integer === 42
=> true
irb(main):009:0> Integer === 'string'
=> false
irb(main):010:0> 42 === Integer
=> false
irb(main):011:0> /at/ === 'Hi mate'
=> true
irb(main):012:0> /at/ === 'Hello'
=> false
irb(main):013:0> 'Hi mate' === /at/
=> false
```

### if/elsif/else statement

```ruby
val = 1337
if val == 42
  puts "That's always true"
elsif val == 1000
  puts "Nearly right"
elsif val == 1300
  puts "Even closer"
else
  puts "Nah!!"
end
# ouput: Nah!
```

### unless statement

Opposite of `if`

```ruby
val = 1337
unless val == 1337
  puts "Your're the elite!"
else
  puts "Master!"
end
# output: Master!
```

### Logical operators

Symbol  | Word
---     | ---
`&&`    | `and`
`||`    | `or`
`!`     | `not`

### case statement

```ruby
a = 10
case a
when 1
  puts 'One'
when 2
  puts 'Two'
when 5
  puts 'Five'
when 10
  puts 'Ten'
when 42, 1337, 31337
  puts 'Master!'
else
  puts 'Nah!'
end
# output: Ten
```

### while loop

```ruby
a = 0
while a <= 5
  puts a
  a += 1
end
```

### until loop

```ruby
a = 0
until a > 5
  puts a
  a += 1
end
```

### Ranges

```ruby
irb(main):043:0> (41..43).to_a
=> [41, 42, 43]
irb(main):044:0> (41...43).to_a
=> [41, 42]
irb(main):045:0> ('m'..'o').to_a
=> ["m", "n", "o"]
irb(main):046:0> ('y'..'b').to_a
=> []
```

### for loop

```ruby
for i in (1..10)
  puts "i: #{i}"
end
```

### break

```ruby
for i in (1..10)
  break if i > 5
  puts "i: #{i}"
end
```

### next

```ruby
for i in (1..10)
  next if i % 2 == 0
  puts "i: #{i}"
end
```

### loop do

Like a `do while` in others languages.

```ruby
a = 0
loop do
  puts a
  a += 1
  break if a > 10
end
```

## Collections

### Arrays

```ruby
# Basics
irb(main):047:0> peoples = ["Alice", "Bob", "Eve"]
=> ["Alice", "Bob", "Eve"]
irb(main):048:0> peoples[0]
=> "Alice"
irb(main):049:0> peoples[1]
=> "Bob"
irb(main):050:0> peoples[-1]
=> "Eve"
# Adding elements
irb(main):051:0> foo = [42, "Cindy", 0.02, true]
=> [42, "Cindy", 0.02, true]
irb(main):052:0> foo << 'd'
=> [42, "Cindy", 0.02, true, "d"]
irb(main):053:0> foo.push(1337)
=> [42, "Cindy", 0.02, true, "d", 1337]
irb(main):055:0> foo.insert(2, 0.05)
=> [42, "Cindy", 0.05, 0.02, true, "d", 1337]
irb(main):056:0> foo.pop
=> 1337
irb(main):057:0> foo
=> [42, "Cindy", 0.05, 0.02, true, "d"]
irb(main):060:0> foo.delete_at(2)
=> 0.05
irb(main):061:0> foo
=> [42, "Cindy", 0.02, true, "d"]
# Removing elements
irb(main):065:0* puts foo
42
Cindy
0.02
true
d
=> nil
irb(main):066:0> print foo
[42, "Cindy", 0.02, true, "d"]=> nil
# Array Ranges
irb(main):068:0* alph = ['a', 'c', 'z', 'e']
=> ["a", "c", "z", "e"]
irb(main):069:0> alph[1..3]
=> ["c", "z", "e"]
```

### Array manipulations

```ruby
# Combining arrays
irb(main):072:0> x = [41, 53]
=> [41, 53]
irb(main):073:0> y = [72, 16, 133]
=> [72, 16, 133]
irb(main):074:0> res = x + y
=> [41, 53, 72, 16, 133]
irb(main):075:0> res = y + x
=> [72, 16, 133, 41, 53]
irb(main):078:0> z = [16, 41, 55]
=> [16, 41, 55]
irb(main):079:0> res - z
=> [72, 133, 53]
# Boolean operations
irb(main):081:0> x
=> [41, 53]
irb(main):082:0> z
=> [16, 41, 55]
irb(main):083:0> x & z
=> [41]
irb(main):085:0> x | z
=> [41, 53, 16, 55]
# Moving elements
irb(main):086:0> z.reverse
=> [55, 41, 16]
irb(main):087:0> z
=> [16, 41, 55]
irb(main):088:0> z.reverse!
=> [55, 41, 16]
irb(main):089:0> z
=> [55, 41, 16]
# Array methods
irb(main):090:0> z.length
=> 3
irb(main):091:0> z.size
=> 3
irb(main):092:0> z.sort
=> [16, 41, 55]
irb(main):093:0> c = [42, 42, 75, 42]
=> [42, 42, 75, 42]
irb(main):094:0> c.uniq
=> [42, 75]
irb(main):095:0> z.freeze # prevent from being modified, there is no unfreeze method
=> [55, 41, 16]
irb(main):096:0> z.pop
RuntimeError: can't modify frozen Array
	from (irb):96:in 'pop'
	from (irb):96
	from /usr/bin/irb:11:in `<main>'
irb(main):102:0> z.frozen?
=> true
irb(main):103:0> z.include?(55)
=> true
irb(main):104:0> z.include?(56)
=> false
irb(main):105:0> z.min
=> 16
irb(main):106:0> z.max
=> 55
```

### Hashes and symbols

`||`

a     | b
---   | ---
`||`  | `pipe pipe`
`|`   | `pipe`

