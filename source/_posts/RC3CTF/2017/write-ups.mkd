---
layout: post
title: "RC3 CTF 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
date: 2017/11/20
thumbnail: /images/ctf.png
authorId: noraj
---

## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : RC3 CTF 2017
- **Website** : [rc3ctf.rc3.club](https://rc3ctf.rc3.club/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/529)

## 100 - This is where I'd put my Home Page.. IF I HAD ONE - Web

> This page won't seem to load.. what is happening???????? - Joker
13.59.6.98:80
>
> Note: Flag is not in RC3-2017{} format

This seems like a loop of pages redirecting one to the next.

```
$ curl http://13.59.6.98/
<meta http-equiv="refresh" content="0; url=C.html" /> <p hidden>R</p>
```

So I made a short ruby script to collect the hidden elements.

```ruby
require 'net/http'

page = ''
hidden_string = []

loop do
  uri = URI('http://13.59.6.98/' + page)
  html = Net::HTTP.get(uri)
  page, hidden = html.match(/<meta http-equiv="refresh" content="0; url=([a-zA-Z0-9]+\.html)" \/> <p hidden>(.*)<\/p>/).captures
  break if page == 'C.html' && !hidden_string.empty?
  hidden_string.push(hidden)
end

puts hidden_string.join
```

By running my script I got the flag: `RC3W3MUS7G0D33P3RR3D1R3C777`.
