---
layout: post
title: "Setup an ArchLinux package caching proxy on Raspberry Pi 4"
date: 2021/01/23 21:50:00
lang: en
categories:
- linux
- archlinux
tags:
- linux
- system
- proxy
- archlinux
- cache
- pacman
- raspberry-pi
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
## Introduction

I installed the unofficial ArchLinux ARM on my [Raspberry Pi 4](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4).
Hosting the caching proxy can be done on any OS and any architecture, so here
it doesn't matter that the server will be ARMv7 based when the proxy client
will be x86_64 ones.

## Purpose

I have several machines using ArchLinux on my local networks (desktop, laptop,
2 VMs) so every day when running an update with `pacman -Syu` I fetch all the
packages from Internet (WAN) several times, which is useless, is a waste of
bandwidth (in my case not really an issue as I have a 1 Gbps fiber connection)
but the main bottleneck is the mirrors upstream bandwidth that can be quite low
sometimes.

So here we will configure an ArchLinux package caching proxy on my Raspberry Pi 4
and all my ArchLinux machines will use it as a pacman mirror, so the 1st
ArchLinux machine will still requires to fetch packages from the WAN but all
other ArchLinux machines will be able to fetch the same packages from LAN at
the speed of light as the packages would have previously been cached.

Pros:

- Save WAN downstream bandwidth
- Download ArchLinux packages from LAN at high speed
- Way more efficient for home usage than setting up a full pacman mirror
- Easier to setup and smarter than a cache with a classical web server like nginx or darkhttpd (see [Read-only cache](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Read-only_cache) or [Dynamic reverse proxy cache using nginx](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Dynamic_reverse_proxy_cache_using_nginx))
- 24/7 available thanks to the client/server architecture rather than a [Distributed cache](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Distributed_read-only_cache) between clients
- Embedded automatic purge timer (pacoloco) rather than having to manually setup purge service using paccache (flexo)

## Install

As a caching proxy for pacman I chose [pacoloco](https://github.com/anatol/pacoloco)
which is available on the [AUR](https://aur.archlinux.org/packages/pacoloco-git/)
so we can install it with a [Pacman wrapper](https://wiki.archlinux.org/index.php/AUR_helpers#Pacman_wrappers).

Example:

```
$ pikaur -S pacoloco-git
```

## Configuration

### Caching server

Edit `/etc/pacoloco.yaml`:

```yaml
cache_dir: /var/cache/pacoloco
port: 9129
download_timeout: 200 # stuck downloads will timeout after 200 sec
purge_files_after: 604800 # purge file after 7 days
repos:
  archlinux:
    urls:
      - http://archlinux.polymorf.fr
      - http://mirror.archlinux.ikoula.com/archlinux
      - http://mirror.lastmikoi.net/archlinux
      - https://mirrors.celianvdb.fr/archlinux
      - http://archlinux.mirrors.ovh.net/archlinux
  blackarch:
    urls:
      - https://www.blackarch.org/blackarch
      - https://blackarch.tamcore.eu/blackarch
      - https://mirror.adversec.com/blackarch
      - https://ftp.halifax.rwth-aachen.de/blackarch
```

As we are running a Raspberry Pi with a 32 GB MicroSD card it's important
to set `purge_files_after` with a low value so the caching server disk will not
get full. For other options see [pacoloco - Configure](https://github.com/anatol/pacoloco#configure).

Of course it's recommended you use mirrors near you for performance, to do that
you can use [Reflector](https://wiki.archlinux.org/index.php/Reflector).

pacoloco is totally capable of caching [Unofficial user repositories](https://wiki.archlinux.org/index.php/Unofficial_user_repositories)
or overlay repositories for ArchLinux like [BlackArch](https://blackarch.org/).

### ArchLinux clients

For ArchLinux official repositories, edit `/etc/pacman.d/mirrorlist` and put
the following line on top of all other mirror servers:

```
Server = http://<your_caching_host>:9129/repo/archlinux/$repo/os/$arch
```

For the BlackArch repository, edit `/etc/pacman.d/blackarch-mirrorlist` and put
the following line on top of all other mirror servers:

```
Server = http://<your_caching_host>:9129/repo/blackarch/$repo/os/$arch
```

## Running

Then we just start the service:

```
# systemctl start pacoloco
```

Then all is automatic, the first ArchLinux client using `pacman -Syu` will
ask the caching proxy to fetch the databases & package for it, but as soon as
the second client will ask for the same packages they will be served from
the caching proxy on the LAN rather than fetched from the Internet (WAN).
