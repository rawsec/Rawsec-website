---
layout: post
title: "ArchLinux - Save face (and your system)"
date: 2024/03/14 22:52:00
lang: en
categories:
- linux
- archlinux
tags:
- linux
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
Whatever boot issue you're facing or system breakage you encounter, that would be neat to be able to fix you system rather than reinstalling it, isn't it?

I'll give you a few hints to do that, especially if you have a [setup similar to mine](https://blog.raw.pm/en/unusual-archlinux-installation/).

I hope you have an AL [USB key](https://wiki.archlinux.org/title/USB_flash_installation_medium) ready to go.

Unless you have an English qwerty keyboard, don't forget to change your keyboard layout for a good start.

```zsh
loadkeys fr
```

You can use `lsblk -f` to identify your ESP and your root partition.

In case of an encrypted partition, unlock it (eg. LVM on LUKS).

```zsh
cryptsetup open /dev/sdc2 cryptlvm
```

Mount the root partition then the ESP (and swap if you have one).

```zsh
mount /dev/myvg2/root /mnt
swapon /dev/myvg2/swap
mount /dev/sdc1 /mnt/efi
```

Let's change root into the system to be saved:

```zsh
arch-chroot /mnt
```

That may not be a bad idea to drop privileges to normal user to prevent accidents. _With great power comes great responsibility_.

```zsh
su noraj
```

If planning to fix boot images and having a bind mount, one doesn't want to forget to mount it before generating initramfs images or something.

```zsh
sudo mount --bind /efi/EFI/arch /boot
```

Finally, please try anything in your power to save that poor system.
