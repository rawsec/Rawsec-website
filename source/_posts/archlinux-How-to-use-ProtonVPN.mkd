---
layout: post
title: "How to use ProtonVPN on Arch Linux"
date: 2017/06/25
lang: en
categories:
- linux
- archlinux
tags:
- linux
- vpn
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
---
## Install the necessary packages

Install [`openvpn`](https://www.archlinux.org/packages/core/x86_64/openvpn/) if needed:

```
# pacman -S openvpn --needed
```

To prevent DNS leakage you'll need an additional package, for more info see [Arch Linux Wiki](https://wiki.archlinux.org/index.php/OpenVPN#DNS).

### Using systemd < 229

Install [`openvpn-update-resolv-conf`](https://aur.archlinux.org/packages/openvpn-update-resolv-conf/) or [`openvpn-update-resolv-conf-git`](https://aur.archlinux.org/packages/openvpn-update-resolv-conf-git/):

```
$ yaourt -S openvpn-update-resolv-conf-git --needed
```

but you will *may* need to change all ProtonVPN config files from:

```
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
```

to:

```
setenv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
down-pre
```

or alternatively if you don't want to edit your client configuration, you can add the following options to your openvpn command:

```
--setenv PATH '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' --up /etc/openvpn/update-resolv-conf --/etc/openvpn/update-resolv-conf --down-pre
```

### Using systemd >= 229

Install [`openvpn-update-systemd-resolved`](https://aur.archlinux.org/packages/openvpn-update-systemd-resolved/):

```
$ yaourt -S openvpn-update-systemd-resolved --needed
```

but you will need to change all ProtonVPN config files from:

```
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
```

to:

```
setenv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
script-security 2
up /etc/openvpn/update-systemd-resolved
down /etc/openvpn/update-systemd-resolved
down-pre
```

or alternatively if you don't want to edit your client configuration, you can add the following options to your openvpn command:

```
--setenv PATH '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' --down-pre
```

## Get the ProtonVPN config files

Create an account or log in [account.protonvpn.com](https://account.protonvpn.com/).

Go to the download menu an download the config file you need (choose country and protocol).

**Note**: if using a free account only `Connect to server` and a restricted number of country and server are available:

```
jp-01.protonvpn.com.tcp443.ovpn
jp-01.protonvpn.com.udp1194.ovpn
jp-02.protonvpn.com.tcp443.ovpn
jp-02.protonvpn.com.udp1194.ovpn
jp-03.protonvpn.com.tcp443.ovpn
jp-03.protonvpn.com.udp1194.ovpn
nl-05.protonvpn.com.tcp443.ovpn
nl-05.protonvpn.com.udp1194.ovpn
nl-06.protonvpn.com.tcp443.ovpn
nl-06.protonvpn.com.udp1194.ovpn
nl-07.protonvpn.com.tcp443.ovpn
nl-07.protonvpn.com.udp1194.ovpn
nl-08.protonvpn.com.tcp443.ovpn
nl-08.protonvpn.com.udp1194.ovpn
us-13.protonvpn.com.tcp443.ovpn
us-13.protonvpn.com.udp1194.ovpn
us-14.protonvpn.com.tcp443.ovpn
us-14.protonvpn.com.udp1194.ovpn
us-15.protonvpn.com.tcp443.ovpn
us-15.protonvpn.com.udp1194.ovpn
us-16.protonvpn.com.tcp443.ovpn
us-16.protonvpn.com.udp1194.ovpn
```

Find your OpenVPN credentials in the account menu. You man need to init the OpenVPN password.

## Connecting using the command line interface (CLI)

Launch `openvpn` with privileges:

```
# openvpn nl-07.protonvpn.com.udp1194.ovpn
```

**Note**: don't forget to use the additionnal arguments that we saw during the install if you didn't change the config files. For  example I'm using `openvpn --setenv PATH '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' --up /etc/openvpn/update-resolv-conf --/etc/openvpn/update-resolv-conf --down-pre --config nl-07.protonvpn.com.udp1194.ovpn`, please not that when using arguments you need to use `--config` else you'll have an error message like `Options error: Unrecognized option or missing or extra parameter(s) in [CMD-LINE]:1: /etc/openvpn/update-resolv-conf (2.4.2)`.

When seeing `Initialization Sequence Completed` you are succesfully connected.

## Be carefull with remote connections

Launching OpenVPN remotly (ex: via SSH) on a server like a VPS or a dedicated server can be dangerous. Using a VPN will change you IP address and default gateway so your remote connection will drop and you won't be able to connect to your server anymore. To recover you'll need an alternative net rescue mode or a hard reboot.

In order not to break your active remote connection you will need to set up appropriate route before lauching openvpn.

See this answer on [serverfault.com](https://serverfault.com/questions/659955/allowing-ssh-on-a-server-with-an-active-openvpn-client#answer-793700):

> It uses iptables and ip (iproute2). Below, it is assumed that the default gateway interface before OpenVPN is started is "eth0". The idea is to ensure that when a connection to eth0 is made, even if eth0 is not the default gateway interface anymore, response packets for the connection go back on eth0 again.
>
> You could use the same number for the connection mark, firewall mark and routing table. I used distinct numbers to make the diffences between them more apparent.
>
> ```
> # set "connection" mark of connection from eth0 when first packet of connection arrives
> sudo iptables -t mangle -A PREROUTING -i eth0 -m conntrack --ctstate NEW -j CONNMARK --set-mark 1234
>
> # set "firewall" mark for response packets in connection with our connection mark
> sudo iptables -t mangle -A OUTPUT -m connmark --mark 1234 -j MARK --set-mark 4321
>
> # our routing table with eth0 as gateway interface
> sudo ip route add default dev eth0 table 3412
>
> # route packets with our firewall mark using our routing table
> sudo ip rule add fwmark 4321 table 3412
> ```
