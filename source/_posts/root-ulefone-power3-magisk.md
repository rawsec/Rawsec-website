---
layout: post
title: "Root Ulefone Power 3 using Magisk from Linux"
date: 2023/02/20 01:12:00
lang: en
categories:
- linux
- android
tags:
- linux
- android
- root
thumbnail: /images/android-2995824.svg
authorId: noraj
toc: true
---
**Disclaimer**: for advanced power user only. If you don't understand what your are doing, then don't do it.

## Prerequisites

- Phone unlocked (OEM unlock)
- USB debug enabled
- Apply all OTA updates
  - Android version: 7.1.1
  - Android security patch level: May 5, 2018
  - Kernel version: `4.4.22+release@release32 #2 Fri Nov 24 19:25:19 CST 2017`
  - Build Number: Power_3_G03

To unlock the bootloader, check `OEM unlock` in the developer menu, then reboot to bootloader mode (`adb reboot bootloader`), then perform the unlock `fastboot oem unlock`. **THIS WILL WIPE ALL DATA**.

## Rooting with Magisk

Download and install [this build](https://github.com/canyie/Magisk/actions/runs/4150249379) (or a more recent one) because [latest release][magrel] of [Magisk][magisk] (v25.2) won't work for this device. In the future if v25.3 or higher is out, go for it as it will include the fix.

Then, on your PC, download the latest stock update of the official ROM. corresponding to your region and the version installed on your phone.
Looking on the official [Ulefone support software download page](https://www.ulefone.com/support/software-download.html), the update is available to download on their [Google Drive](https://drive.google.com/drive/folders/18VtvZh5qYfhiMTHWZMq0LkBvRRNN7e10). The file is named `GQ3056MF1_HCS986A_Ulefone_v40.gz`.

Plug your phone to your PC with an USB cable.

Run `adb devices` to pair your PC with your phone (accept the popup on the phone).

Extract the ROM: `tar xaf ./GQ3056MF1_HCS986A_Ulefone_v40.gz`. You'll get all extracted in a folder named `s986a_gq_gq3056_a2_66_512g48g_fhdp_bom3-n1_Power_3_G03_user`.

As we can see on Magisk app, the Ulefone Power 3 has a ramdisk, so as
explained [in Magisk documentation][maginstall] we need to fetch a copy of
`boot.img`.

We have do not have a separate vbmeta partition because no `vbmeta.img` is extracted from the firmware
package and we can also confirm by running `adb shell ls -l /dev/block/by-name`
that shows file not found.

We can copy the boot image we extracted to the phone with the following
command:

```
$ adb push s986a_gq_gq3056_a2_66_512g48g_fhdp_bom3-n1_Power_3_G03_user/boot.img storage/self/primary/Download/
```

Now we can click `Install` on the Magisk app. Before selecting the image we have to check `Patch vbmeta in boot image` option.
You will have to select `boot.img` from the download folder to select the image to patch.

Then we'll download the patched image to our PC (the path is written in Magisk logs).

```
$ adb pull /storage/emulated/0/Download/magisk_patched-[ramdom].img
```

Reboot the phone in bootloader/fastboot mode:

```
$ adb reboot bootloader
```

In fastboot mode flash the boot image:

```
$ fastboot flash boot magisk_patched-[ramdom].img
```

Now reboot the phone.

We can launch the Magisk app to check the root was successful.

[magisk]:https://github.com/topjohnwu/Magisk/
[magrel]:https://github.com/topjohnwu/Magisk/releases
[maginstall]:https://topjohnwu.github.io/Magisk/install.html
