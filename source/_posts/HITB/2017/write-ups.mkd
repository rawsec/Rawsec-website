---
layout: post
title: "HITB CTF Singapore 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - mobile
  - misc
date: 2017/08/24
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By        | Version | Comment
| ---       | ---     | ---
| noraj     | 1.0     | Creation

### CTF

- **Name** : HITB CTF Singapore 2017
- **Website** : [hitb.xctf.org.cn](https://hitb.xctf.org.cn/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/498)

## Cephalopod - Misc

>  We've found some strange networktraffic, we suspect it contains a flag.
>
> [*Download Attachments*](https://hitb.xctf.org.cn/media/task/2a9c1cdd-2ac0-4b2a-828d-269c6e04ebbb.pcap)

`binwalk` always help:

```
$ binwalk 2a9c1cdd-2ac0-4b2a-828d-269c6e04ebbb.pcap

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
26441         0x6749          PNG image, 1754 x 2480, 8-bit/color RGBA, non-interlaced
26577         0x67D1          Zlib compressed data, best compression
```

So there is probably an image with the flag.

Let's open wireshark:

```
$ wireshark-gtk 2a9c1cdd-2ac0-4b2a-828d-269c6e04ebbb.pcap
```

Let's check [Wikipedia](https://en.wikipedia.org/wiki/List_of_file_signatures), PNG file signature begins with `89 50 4E 47 0D 0A`.

Press `CTRL + F`, select **Hex value** as **Display filter**.

That lead us to frame n°126. (Note: if you search `PNG` as string you'll find a request for `flag.png` so we are on the good way)

Right click on the frame, click on **Follow TCP Stream**, select **Raw** representation, donc select **Entire conversation** but **10.0.2.7:39618 -> 10.0.2.10:6800 (2412380 bytes)** and then save it as a file.

We can see there is some ceph stuff before the PNG file signature so extract the PNG:

```
$ foremost ceph_and_png
```

Now we can see the flag:

```
$ display output/png/flag.png
```

Flag is `HITB{95700d8aefdc1648b90a92f3a8460a2c}`.

![](https://i.imgur.com/GdKzg8A.jpg)

Note: imgur converted the picture into jpeg.

## Prime - Mobile

> Do you know prime?
>
> [*Download Attachments*](https://hitb.xctf.org.cn/media/task/ab436242-a5c7-4dd8-b88d-1982be05b3bd.apk)

Unpack the application.apk file with assets, resources, compiled code, etc...

```
$ apktool d -r -s ab436242-a5c7-4dd8-b88d-1982be05b3bd.apk
```

Convert Dex to java class:

```
$ d2j-dex2jar ab436242-a5c7-4dd8-b88d-1982be05b3bd/classes.dex
dex2jar ab436242-a5c7-4dd8-b88d-1982be05b3bd/classes.dex -> ./classes-dex2jar.jar
```

Now take a look at the source:

```
$ jd-gui classes-dex2jar.jar
```

Or we can also use `jadx-gui` that give us:

```java
package com.iromise.prime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static long N = ((long) Math.pow(10.0d, 16.0d));

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        Button start = (Button) findViewById(R.id.start);
        Log.i("Number", String.valueOf(N));
        start.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "HITB{" + MainActivity.this.CalcNumber(MainActivity.N) + "}", 0).show();
            }
        });
    }

    private Boolean isOk(long n) {
        if (n == 1) {
            return Boolean.FALSE;
        }
        if (n == 2) {
            return Boolean.TRUE;
        }
        for (long i = 2; i * i < n; i++) {
            if (n % i == 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    private long CalcNumber(long n) {
        long number = 0;
        for (long i = 1; i <= n; i++) {
            if (isOk(i).booleanValue()) {
                number++;
            }
        }
        return number;
    }
}
```

This is calculating the number of prime numbers up to 10000000000000000 (10 quadrillion = 10 million billion = 1 × 10^16). So this will take a while before displaying the toast.

I prefer to use a search engine: [Prime number theorem](https://en.wikipedia.org/wiki/Prime_number_theorem) and [OEIS](https://oeis.org/A006880/list).

![](https://i.imgur.com/e3OnTMG.png)

So the flag appears to be `HITB{279238341033925}` but it wasn't valid.

**Update**: *Shinji Hirako* point me the following:

> The java code also counts all squares of primes less than or equal to `10000000000000000`. To calculate the number of squares of primes less than `10000000000000000`, we take the square root of `10000000000000000` which is `10**8` and count how many prime numbers until `10**8`. From the diagram you posted, this is `5761455`. So answer is `279238341033925 + 5761455` and flag is `HITB{279238346795380}`
>
> Edit: The reason squares of primes are included is because when it is a square of a prime, the code never enters the for loop. For example, when `4` is passed as the parameter to `isOk()`, the condition for the for loop is `l =2, l*l < n`. But `l*l = 4` and `4` is not less than 4, hence we never enter the for loop. Same logic for all other squares of primes, but not squares in general.
