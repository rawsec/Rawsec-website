---
layout: post
title: "Install LineageOS 19.1 on POCO F3 to replace MIUI 13.0.4"
date: 2022/05/30 23:15:00
lang: en
categories:
- linux
- android
tags:
- linux
- android
- lineageos
thumbnail: /images/android-2995824.svg
authorId: noraj
toc: true
---
**Disclaimer**: those are basically the steps described [on the LineageOS wiki](https://wiki.lineageos.org/devices/alioth/install). Check the wiki for updated information.

## Prerequisites

- Phone unlocked (OEM unlock / Mi unlock)
- USB debug enabled

## Flashing the vendor boot partition

Download vendor_boot file (`vendor_boot.img`) from [here](https://mirror.math.princeton.edu/pub/lineageos/full/alioth/)
from the directory named with the latest date. For example, https://mirror.math.princeton.edu/pub/lineageos/full/alioth/20220528/vendor_boot.img.

Reboot the phone in bootloader/fastboot mode:

```
$ adb reboot bootloader
```

Flash the vendor boot image:

```
$ fastboot flash vendor_boot vendor_boot.img
```

## Temporarily booting a custom recovery

Download [Lineage Recovery](https://download.lineageos.org/alioth) image. For
example, https://mirrorbits.lineageos.org/recovery/alioth/20220528/lineage-19.1-20220528-recovery-alioth.img.

Temporarily flash the recovery image on our device (we are already in fastboot mode from previous steps):

```
$ fastboot flash boot lineage-19.1-20220528-recovery-alioth.img
```

Note: In my case it flashed to `boot_b` partition.

Now reboot into recovery to verify the installation.

```
$ fastboot reboot-recovery
```

## Ensuring all firmware partitions are consistent

To avoid issues, we'll copy the contents of the active slot to the inactive slot.

Sideload the `copy-partitions-20210323_1922.zip`:

1. Download the `copy-partitions-20210323_1922.zip` file from [here](https://www.androidfilehost.com/?fid=2188818919693768129).
2. Sideload the `copy-partitions-20210323_1922.zip` package:
  - On the device, select _Apply Update_, then _Apply from ADB_ to begin sideload.
  - On the host machine, sideload the package using: `adb sideload copy-partitions-20210323_1922.zip`
  - Click on _Yes_ (install anyway) when it says _Signature verification failed_ (because it's made by LineageOS developers but is not signed with official keys)
3. Return to previous menu with the left arrow, then reboot to recovery by tapping _Advanced_, then _Reboot to recovery_.

## Installing LineageOS from recovery

1. Download the [LineageOS installation package](https://download.lineageos.org/alioth) that you would like to install. For example https://mirrorbits.lineageos.org/full/alioth/20220528/lineage-19.1-20220528-nightly-alioth-signed.zip.
2. Download Google Apps addon from [here](https://wiki.lineageos.org/gapps). For example, mobile version Lineage 19.1 (android 12.1) **nano** package arm64 architecture from MindTheGapps: `MindTheGapps-12.1.0-arm64-20220416_174313.zip`.
3. We already are in recovery mode from previous steps
4. Now tap _Factory Reset_, then _Format data / factory reset_ and continue with the formatting process. This will remove encryption and delete all files stored in the internal storage, as well as format your cache partition (if you have one).
5. Return to the main menu.
6. Sideload the LineageOS `.zip` package:
  - On the device, select _Apply Update_, then _Apply from ADB_ to begin sideload.
  - On the host machine, sideload the package using: `adb sideload lineage-19.1-20220528-nightly-alioth-signed.zip`.
7. To install Google Apps addon:
  - Return to the main menu
  - Click _Advanced_, then _Reboot to Recovery_
  - Then when your device reboots, click _Apply Update_, then _Apply from ADB_
  - Sideload Google Apps: `adb sideload MindTheGapps-12.1.0-arm64-20220416_174313.zip`
  - Click on _Yes_ (install anyway) when it says _Signature verification failed_ (because it's not made by LineageOS developers and so is not signed with official keys)
8. Once you have installed everything successfully, click the back arrow in the top left of the screen, then _Reboot system now_.
