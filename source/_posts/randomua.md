---
layout: post
title: 'randomua - Inject random user-agent in pentest CLI tools'
lang: en
categories:
  - security
tags:
  - security
  - 'user-agent'
  - web
  - pentest
date: 2020/11/28 20:51:00
thumbnail: /images/security-265130_640.jpg
authorId: noraj
toc: true
---
## Why?

A lot web-focused hacking/pentest tools have the ability to choose a user-agent (UA) to send
while fuzzing/scanning/enumerating or at least to send custom HTTP headers.

By default most of the tools will send a user-agent containing their name, eg.
sqlmap will send something like `User-agent: sqlmap/1.0-dev`.
The problem with that, is that WAF, IPS and other defense security tools can
very easily use a blacklist of such user-agents to block all hacking attempts.
The good news is that if the blocking is only based on the user-agent and not
on the behavior, it's also easy for offensive security professionals to send
a customized user-agent to mimic a legitimate Web browser, library, software.

But here are the difficulties we can still face:

- Very few tools (eg. sqlmap with `--random-agent`) have a native option to use
a randomly selected user-agent. Most of the time you need to paste a string yourself.
- Searching on internet or reading a list each time we want to select a user-agent is cumbersome.
- Even when a random agent option is available you can't choose between the user-agent categories natively. You may not need an exact user-agent but may wan to use a specific category, eg. cloud platform.

So I will show here how to use [randomua][randomua] CLI tool to easily and quickly inject
a custom user-agent randomly chosen from a specific category in your tools HTTP header.

## Usage

First of all, a quick display of the help message of [randomua][randomua] to see the categories available.

```
$ randomua -h
randomua (random user agent) | Generate a random user agent string.

Usage:
  randomua [options]
  randomua -h | --help
  randomua --version

Options:
  -c, --crawler            Gereate a random crawler user agent string
  -d, --desktop-browser    Gereate a random desktop browser user agent string
  -m, --mobile-browser     Gereate a random mobile browser user agent string
  -o, --console            Gereate a random console user agent string
  -f, --offline-browser    Gereate a random offline browser user agent string
  -e, --email-client       Gereate a random email client user agent string
  -l, --link-checker       Gereate a random link checker user agent string
  -a, --email-collector    Gereate a random email collector user agent string
  -v, --validator          Gereate a random validator user agent string
  -r, --feed-reader        Gereate a random feed reader user agent string
  -i, --library            Gereate a random library user agent string
  -u, --cloud-platform     Gereate a random cloud platform user agent string
  -t, --other              Gereate a random other user agent string
  -s, --version            Print version and exit
  -h, --help               Show this message

Examples:
  randomua -d
```

## Example

[randomua][randomua] is very straightforward, some basic examples of randomly generated user-agents from the (respectively) email collector, library and desktop browser categories.

~~~
$ randomua -e
Mozilla/5.0 (Windows NT 6.2; rv:17.0) Gecko/20130801 Thunderbird/17.0.8

$ randomua -i
curl/7.15.4 (i686-pc-linux-gnu) libcurl/7.15.4 OpenSSL/0.9.7e zlib/1.2.3

$ randomua -d
Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.8.0.3) Gecko/20060426 Firefox/1.5.0.3
~~~

Now let's see how you can inject the user-agent in some common web hackign tools: sqlmap, dirsearch, lulzbuster, ffuf, nikto.

~~~
$ sqlmap -u https://URL/?id=1 -p id --user-agent $(randomua -i)

$ dirsearch -u https://URL/ -e php --user-agent $(randomua -d)

$ lulzbuster -s https://URL/ -u $(randomua -d)

$ ffuf -u https://URL/FUZZ -c -v -w ~/WORDLIST -H "User-Agent: $(randomua -d)"

$ nikto -h https://URL/ -useragent $(randomua -m)

$ testssl -h "User-Agent: $(randomua -c)" https://URL/
~~~

[randomua]:https://github.com/picatz/randomua
