---
layout: post
title: "QEMU/KVM/Virt-Manager guest support on Debian"
date: 2022/06/10 02:28:00
lang: en
categories:
- linux
- debian
tags:
- linux
- debian
- system
- virtualization
thumbnail: /images/tux-293844_640.png
authorId: noraj
toc: true
---
I wrote a more detailed article [Install QEMU/KVM/Virt-Manager on ArchLinux](https://blog.raw.pm/en/install-qemu-kvm-virt-manager-archlinux) but here I will focus on installing guest _additions_ for QEMU/KVM on Debian.
By that I mean support for shared folder, shared clipboard, VirtIO devices.

VirtIO kernel modules could be auto-loaded automatically but in case it is not one
has to edit `/etc/initramfs-tools/modules` and add the following modules:

```
virtio
virtio_blk
virtio_pci
virtio_net
```

Then we can force rebuild the init ramdisk:

```
update-initramfs -u
```

Install QEMU guest additions:

```
apt install qemu-guest-agent
```

Since the VM will be using SPICE we can improve the user experience by installing
`spice-vdagent` to enable shared clipboard.

```
apt install spice-vdagent
```

Then to get shared folder working with `virtio-9p`, we can follow the following steps.

In virt-manager VM settings:

1. Hit the _Add Hardware_ button
2. Choose a _Filesystem_ device
3. Select _virtio-9p_ as Driver
4. Define the source path (folder to share from the host) and target path (a mount tag)

And in the guest, mount the share:

```
sudo mount -t 9p -o trans=virtio <mount-tag> <mount-point>
```

For example:

```
sudo mount -t 9p -o trans=virtio hostshare /home/noraj/Share
```

Note: it's possible to [tweak the msize option](https://wiki.qemu.org/Documentation/9psetup#Performance_Considerations_(msize)) to increase performance.

It's also possible to add it to `/etc/fstab` to mount it automatically.

```
# 9P shared folder
hostshare /home/noraj/Share 9p  trans=virtio,rw,_netdev 0 0
```
