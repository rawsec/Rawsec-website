---
layout: post
title: "ArchLinux - mkinitcpio v38 upgrade"
date: 2024/03/14 22:21:00
lang: en
categories:
- linux
- archlinux
tags:
- linux
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
With the release of mkinitcpio v38, [mkinitcpio hook migration and early microcode](https://archlinux.org/news/mkinitcpio-hook-migration-and-early-microcode/) has been announced (04/03/2024). But how to migrate?

For [a setup similar to mine](https://blog.raw.pm/en/unusual-archlinux-installation/), the steps are as follows.

Note: If it's already too late, and you can't boot anymore (blocked by a kernel panic), then [this article](https://blog.raw.pm/en/archlinux-system-rescue) will help you boot from an AL USB key and rescue your system.

First, upgrade the system. (it may fail at mkinitcpio stage, but we'll fix that)

```zsh
pacman -Syu
```

Then, remove all kernel mkinitcpio presets because they contain old directives that will make mkinitcpio fail when trying to build initramfs.

```zsh
rm /etc/mkinitcpio.d/linux*.preset
```

Then in order to re-generate new presets, we'll have to re-install kernel packages in order to trigger the pacman post-transaction hook that generates them.

```zsh
pacman -S linux linux-lts linux-zen
```

Now let's edit mkinitcpio configuration (`/etc/mkinitcpio.conf` or a drop-in `/etc/mkinitcpio.conf.d/hooks.conf`) to add the new `microcode` hook:


```diff
-HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 resume filesystems fsck)
+HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block encrypt lvm2 resume filesystems fsck)
```

Now we can edit the bootloader configuration, e.g. for rEFInd `/efi/EFI/arch/refind_linux.conf`, and remove the microcode `initrd` that is no longer required as it's now packed together with the main initramfs image:

```diff
-"Boot with default options"  "cryptdevice=UUID=2fe657fa-3e5d-46db-9f2e-54529f96150e:cryptlvm initrd=\EFI\arch\amd-ucode.img root=/dev/myvg2/root rw initrd=\EFI\arch\initramfs-%v.img resume=/dev/myvg2/swap"
+"Boot with default options"  "cryptdevice=UUID=2fe657fa-3e5d-46db-9f2e-54529f96150e:cryptlvm root=/dev/myvg2/root rw initrd=\EFI\arch\initramfs-%v.img resume=/dev/myvg2/swap"
```

Finally, we can rebuild all initramfs images at once:

```zsh
mkinitcpio -P
```

Reboot. You should have survived the [mkinitcpio v38 released](https://lists.archlinux.org/hyperkitty/list/arch-projects@lists.archlinux.org/thread/PZHWLVJLN5EPW6EVK2HEKVBDBNZWHREN/).
