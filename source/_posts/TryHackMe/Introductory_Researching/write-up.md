---
layout: post
title: "Introductory Researching - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
date: 2020/11/14 23:46:00
updated: 2020/12/17 19:16:00
thumbnail: /images/TryHackMe/introductory_researching.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Introductory Researching
- **Profile:** [tryhackme.com](https://tryhackme.com/room/introtoresearch)
- **Difficulty:** Easy
- **Description**: A brief introduction to research skills for pentesting.

![Introductory Researching](/images/TryHackMe/introductory_researching.png)

**Disclaimer**: the answers are either general culture, searching the keyword in
the question or looking for an option in the man page, it's very easy and obvious,
the room description already give it all so the write-up doesn't need details.

# Write-up

## Example Research Question

> In the Burp Suite Program that ships with Kali Linux, what mode would you use to manually send a request (often repeating a captured request numerous times)?

Answer: {% spoiler repeater %}

> What hash format are modern Windows login passwords stored in?

Answer: {% spoiler NTLM %}

> What are automated tasks called in Linux?

Answer: {% spoiler cron jobs %}

> What number base could you use as a shorthand for base 2 (binary)?

Answer: {% spoiler base 16 %}

> If a password hash starts with $6$, what format is it (Unix variant)?

Answer: {% spoiler sha512crypt %}

## Vulnerability Searching

> What is the CVE for the 2020 Cross-Site Scripting (XSS) vulnerability found in WPForms?

Answer: {% spoiler CVE-2020-10385 %}

> There was a Local Privilege Escalation vulnerability found in the Debian version of Apache Tomcat, back in 2016. What's the CVE for this vulnerability?

Answer: {% spoiler CVE-2016-1240 %}

> What is the very first CVE found in the VLC media player?

Answer: {% spoiler CVE-2007-0017 %}

> If I wanted to exploit a 2020 buffer overflow in the sudo program, which CVE would I use?

Answer: {% spoiler CVE-2019-18634 %}

## Manual Pages

> SCP is a tool used to copy files from one computer to another.
> What switch would you use to copy an entire directory?

Answer: {% spoiler `-r` %}

> fdisk is a command used to view and alter the partitioning scheme used on your hard drive.
> What switch would you use to list the current partitions?

Answer: {% spoiler `-l` %}

> nano is an easy-to-use text editor for Linux. There are arguably better editors (Vim, being the obvious choice); however, nano is a great one to start with.
> What switch would you use to make a backup when opening a file with nano?

Answer: {% spoiler `-B` %}

> Netcat is a basic tool used to manually send and receive network requests. 
> What command would you use to start netcat in listen mode, using port 12345?

Answer: {% spoiler `nc -l -p 12345` %}
