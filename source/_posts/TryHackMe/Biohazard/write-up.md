---
layout: post
title: "Biohazard - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - stegano
  - misc
date: 2021/05/05 20:23:00
thumbnail: /images/TryHackMe/biohazard.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Biohazard
- **Profile:** [tryhackme.com](https://tryhackme.com/room/biohazard)
- **Difficulty:** Medium
- **Description**: A CTF room based on the old-time survival horror game, Resident Evil. Can you survive until the end?

![Biohazard](/images/TryHackMe/biohazard.png)

# Write-up

## Overview

Warning: This is a mind game and not a security challenge.

## Network enumeration

Port and service scan with nmap:

```
# Nmap 7.91 scan initiated Tue May  4 10:16:27 2021 as: nmap -sSVC -p- -oA nmap_full -v 10.10.193.27
Nmap scan report for 10.10.193.27
Host is up (0.026s latency).
Not shown: 65532 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 c9:03:aa:aa:ea:a9:f1:f4:09:79:c0:47:41:16:f1:9b (RSA)
|   256 2e:1d:83:11:65:03:b4:78:e9:6d:94:d1:3b:db:f4:d6 (ECDSA)
|_  256 91:3d:e4:4f:ab:aa:e2:9e:44:af:d3:57:86:70:bc:39 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Beginning of the end
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue May  4 10:17:19 2021 -- 1 IP address (1 host up) scanned in 52.54 seconds
```

Let's add a domain:

```
$ grep biohazard /etc/hosts
10.10.193.27 biohazard.thm
```

## The Mansion

### emblem

At http://biohazard.thm/mansionmain/ page there is a HTML comment leaking
another page:

```html
<!-- It is in the /diningRoom/ -->
```

At http://biohazard.thm/diningRoom/ there is another HTML comment containing a
base64 string.

```html
<!-- SG93IGFib3V0IHRoZSAvdGVhUm9vbS8= -->
```

We can decode it:

```
$ printf %s SG93IGFib3V0IHRoZSAvdGVhUm9vbS8= | base64 -d
How about the /teaRoom/
```

But before to go there we can go at http://biohazard.thm/diningRoom/emblem.php
where we find the emblem flag.

### lock_pick

At http://biohazard.thm/teaRoom/ there is a link to http://biohazard.thm/teaRoom/master_of_unlock.html
which give us the lock_pick flag and a link to the next page http://biohazard.thm/artRoom/.

### music_sheet + map

The http://biohazard.thm/artRoom/ page only contains a link to http://biohazard.thm/artRoom/MansionMap.html
which contains list of endpoints:

```
Look like a map

Location:
/diningRoom/
/teaRoom/
/artRoom/
/barRoom/
/diningRoom2F/
/tigerStatusRoom/
/galleryRoom/
/studyRoom/
/armorRoom/
/attic/
```

At http://biohazard.thm/barRoom/ we can enter the lock_pick flag that send us to
http://biohazard.thm/barRoom357162e3db904857963e6e0b64b96ba7/.
There, there is a link to http://biohazard.thm/barRoom357162e3db904857963e6e0b64b96ba7/musicNote.html
where we can read the following message:

> Look like a music note
>
> NV2XG2LDL5ZWQZLFOR5TGNRSMQ3TEZDFMFTDMNLGGVRGIYZWGNSGCZLDMU3GCMLGGY3TMZL5

It looks like a base32 string.
By decoding it we obtain the music_sheet flag.

```
$ printf %s 'NV2XG2LDL5ZWQZLFOR5TGNRSMQ3TEZDFMFTDMNLGGVRGIYZWGNSGCZLDMU3GCMLGGY3TMZL5' | base32 -d
```

### gold_emblem

We can use the music_sheet flag at the previous secret bar room page that send us to
http://biohazard.thm/barRoom357162e3db904857963e6e0b64b96ba7/barRoomHidden.php.
The is a link to om357162e3db904857963e6e0b64b96ba7/gold_emblem.php where we can find
the gold_emblem flag.

### blue_jewel

At http://biohazard.thm/diningRoom2F/ there is a HTML comment:

```html
<!-- Lbh trg gur oyhr trz ol chfuvat gur fgnghf gb gur ybjre sybbe. Gur trz vf ba gur qvavatEbbz svefg sybbe. Ivfvg fnccuver.ugzy -->
```

Looks like a substitution cipher (eg. Caesar).

I used my own toolkit named [ctf-party](https://github.com/noraj/ctf-party/) to decrypt the
message. The tool can be either used as a CLI or a library:

```irb
$ ctf-party 'Lbh trg gur oyhr trz ol chfuvat gur fgnghf gb gur ybjre sybbe. Gur trz vf ba gur qvavatEbbz svefg sybbe. Ivfvg fnccuver.ugzy' rot13
You get the blue gem by pushing the status to the lower floor. The gem is on the diningRoom first floor. Visit sapphire.html

$ ctf_party_console
irb(main):001:0> message = 'Lbh trg gur oyhr trz ol chfuvat gur fgnghf gb gur ybjre sybbe. Gur trz vf ba gur qvavatEbbz svefg sybbe. Ivfvg fnccuver.ugzy'
=> "Lbh trg gur oyhr trz ol chfuvat gur fgnghf gb gur ybjre sybbe. Gur trz vf ba gur qvavatEbbz svefg sybbe. Ivfvg fnccuver.ugzy"
irb(main):002:0> message.rot13
=> "You get the blue gem by pushing the status to the lower floor. The gem is on the diningRoom first floor. Visit sapphire.html"
```

Then we can grab the blue_jewel flag at http://biohazard.thm/diningRoom/sapphire.html.

### crest 2

At http://biohazard.thm/galleryRoom/ there is a link to http://biohazard.thm/galleryRoom/note.txt
with the following content:

> crest 2:
>
> GVFWK5KHK5WTGTCILE4DKY3DNN4GQQRTM5AVCTKE
>
> Hint 1: Crest 2 has been encoded twice
>
> Hint 2: Crest 2 contains 18 letters
>
> Note: You need to collect all 4 crests, combine and decode to reveal another path
>
> The combination should be crest 1 + crest 2 + crest 3 + crest 4. Also, the combination is a type of encoded base and you need to decode it

It seems the first pass of encoding is base32:

```
$ printf %s GVFWK5KHK5WTGTCILE4DKY3DNN4GQQRTM5AVCTKE | base32 -d
5KeuGWm3LHY85cckxhB3gAQMD
```

Then using ctf-party I verified it was not a base64 string:

```
$ ctf_party_console
irb(main):005:0> '5KeuGWm3LHY85cckxhB3gAQMD'.b64?
=> false
```

Using CyberChef magic recipe I identified it was a base58 string:

- https://gchq.github.io/CyberChef/#recipe=Magic(3,false,false,'')&input=NUtldUdXbTNMSFk4NWNja3hoQjNnQVFNRA
- https://gchq.github.io/CyberChef/#recipe=From_Base58('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',true)&input=NUtldUdXbTNMSFk4NWNja3hoQjNnQVFNRA

Let's save crest 2 for now: h1bnRlciwgRlRQIHBh.

### crest 1

At http://biohazard.thm/tigerStatusRoom/ page it's asking for the blue gem/jewel
flag and so we are redirected to http://biohazard.thm/tigerStatusRoom/gem.php.

There is another crest:

> crest 1:
>
> S0pXRkVVS0pKQkxIVVdTWUpFM0VTUlk9
>
> Hint 1: Crest 1 has been encoded twice
>
> Hint 2: Crest 1 contains 14 letters
>
> Note: You need to collect all 4 crests, combine and decode to reveal another path
>
> The combination should be crest 1 + crest 2 + crest 3 + crest 4. Also, the combination is a type of encoded base and you need to decode it

This time is used the CyberChef magic recipe directly to find it's
base64 then base32.

https://gchq.github.io/CyberChef/#recipe=Magic(3,false,false,'')&input=UzBwWFJrVlZTMHBLUWt4SVZWZFRXVXBGTTBWVFVsazk

So crest 1 is: RlRQIHVzZXI6IG.

### rebecca?

At http://biohazard.thm/barRoom357162e3db904857963e6e0b64b96ba7/barRoomHidden.php page
it's asking for the emblem flag and redirect us to
http://biohazard.thm/barRoom357162e3db904857963e6e0b64b96ba7/emblem_slot.php
where there is only one word given: rebecca.

We don't know yet what it can be used for.

### shield

At http://biohazard.thm/diningRoom/ page it's asking for the emblem flag but it's
not working. In fact it expect the golden emblem flag.
We are redirected to http://biohazard.thm/diningRoom/emblem_slot.php with
the following content:

> klfvg ks r wimgnd biz mpuiui ulg fiemok tqod. Xii jvmc tbkg ks tempgf tyi_hvgct_jljinf_kvc

I used ctf-party again to check if it's a rot cipher but it seems not

```
$ ctf_party_console
irb(main):001:0> message = 'klfvg ks r wimgnd biz mpuiui ulg fiemok tqod. Xii jvmc tbkg ks tempgf tyi_hvgct_jljinf_kvc'
irb(main):004:0> (1..26).each.map { |n| message.rot(shift: n) }
=>
["lmgwh lt s xjnhoe cja nqvjvj vmh gjfnpl urpe. Yjj kwnd uclh lt ufnqhg uzj_iwhdu_kmkjog_lwd",
 "mnhxi mu t ykoipf dkb orwkwk wni hkgoqm vsqf. Zkk lxoe vdmi mu vgorih vak_jxiev_lnlkph_mxe",
 "noiyj nv u zlpjqg elc psxlxl xoj ilhprn wtrg. All mypf wenj nv whpsji wbl_kyjfw_momlqi_nyf",
 "opjzk ow v amqkrh fmd qtymym ypk jmiqso xush. Bmm nzqg xfok ow xiqtkj xcm_lzkgx_npnmrj_ozg",
 "pqkal px w bnrlsi gne ruznzn zql knjrtp yvti. Cnn oarh ygpl px yjrulk ydn_malhy_oqonsk_pah",
 "qrlbm qy x cosmtj hof svaoao arm loksuq zwuj. Doo pbsi zhqm qy zksvml zeo_nbmiz_prpotl_qbi",
 "rsmcn rz y dptnuk ipg twbpbp bsn mpltvr axvk. Epp qctj airn rz altwnm afp_ocnja_qsqpum_rcj",
 "stndo sa z equovl jqh uxcqcq cto nqmuws bywl. Fqq rduk bjso sa bmuxon bgq_pdokb_rtrqvn_sdk",
 "tuoep tb a frvpwm kri vydrdr dup ornvxt czxm. Grr sevl cktp tb cnvypo chr_qeplc_susrwo_tel",
 "uvpfq uc b gswqxn lsj wzeses evq psowyu dayn. Hss tfwm dluq uc dowzqp dis_rfqmd_tvtsxp_ufm",
 "vwqgr vd c htxryo mtk xaftft fwr qtpxzv ebzo. Itt ugxn emvr vd epxarq ejt_sgrne_uwutyq_vgn",
 "wxrhs we d iuyszp nul ybgugu gxs ruqyaw fcap. Juu vhyo fnws we fqybsr fku_thsof_vxvuzr_who",
 "xysit xf e jvztaq ovm zchvhv hyt svrzbx gdbq. Kvv wizp goxt xf grzcts glv_uitpg_wywvas_xip",
 "yztju yg f kwaubr pwn adiwiw izu twsacy hecr. Lww xjaq hpyu yg hsadut hmw_vjuqh_xzxwbt_yjq",
 "zaukv zh g lxbvcs qxo bejxjx jav uxtbdz ifds. Mxx ykbr iqzv zh itbevu inx_wkvri_yayxcu_zkr",
 "abvlw ai h mycwdt ryp cfkyky kbw vyucea jget. Nyy zlcs jraw ai jucfwv joy_xlwsj_zbzydv_als",
 "bcwmx bj i nzdxeu szq dglzlz lcx wzvdfb khfu. Ozz amdt ksbx bj kvdgxw kpz_ymxtk_acazew_bmt",
 "cdxny ck j oaeyfv tar ehmama mdy xawegc ligv. Paa bneu ltcy ck lwehyx lqa_znyul_bdbafx_cnu",
 "deyoz dl k pbfzgw ubs finbnb nez ybxfhd mjhw. Qbb cofv mudz dl mxfizy mrb_aozvm_cecbgy_dov",
 "efzpa em l qcgahx vct gjococ ofa zcygie nkix. Rcc dpgw nvea em nygjaz nsc_bpawn_dfdchz_epw",
 "fgaqb fn m rdhbiy wdu hkpdpd pgb adzhjf oljy. Sdd eqhx owfb fn ozhkba otd_cqbxo_egedia_fqx",
 "ghbrc go n seicjz xev ilqeqe qhc beaikg pmkz. Tee friy pxgc go pailcb pue_drcyp_fhfejb_gry",
 "hicsd hp o tfjdka yfw jmrfrf rid cfbjlh qnla. Uff gsjz qyhd hp qbjmdc qvf_esdzq_gigfkc_hsz",
 "ijdte iq p ugkelb zgx knsgsg sje dgckmi romb. Vgg htka rzie iq rckned rwg_ftear_hjhgld_ita",
 "jkeuf jr q vhlfmc ahy lothth tkf ehdlnj spnc. Whh iulb sajf jr sdlofe sxh_gufbs_ikihme_jub",
 "klfvg ks r wimgnd biz mpuiui ulg fiemok tqod. Xii jvmc tbkg ks tempgf tyi_hvgct_jljinf_kvc"]
```

But the only flag with are missing form the first page is the shield one and
the hint for this one is `Blaise de Vigenère` so it must be a vigenere cipher.

It requires a key so by pure guessing let's say it could be `rebecca` we obtained earlier.

https://gchq.github.io/CyberChef/#recipe=Vigen%C3%A8re_Decode('rebecca')&input=a2xmdmcga3MgciB3aW1nbmQgYml6IG1wdWl1aSB1bGcgZmllbW9rIHRxb2QuIFhpaSBqdm1jIHRia2cga3MgdGVtcGdmIHR5aV9odmdjdF9qbGppbmZfa3Zj

The decoded message:

> there is a shield key inside the dining room. The html page is called the_great_shield_key

At http://biohazard.thm/diningRoom/the_great_shield_key.html we get the shield flag.

### crest 3

At http://biohazard.thm/armorRoom/ the door is locked and asking for the
shield flag. It redirects us to http://biohazard.thm/armorRoom547845982c18936a25a9b37096b21fc1/
and a link to http://biohazard.thm/armorRoom547845982c18936a25a9b37096b21fc1/note.txt
with the content of crest 3:

> crest 3:
>
> MDAxMTAxMTAgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAxMDAgMDExMDAxMDAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDAgMDAxMDAwMDAgMDAxMTAxMTAgMDExMDAwMTEgMDAxMDAwMDAgMDAxMTAxMTEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAxMTAgMDAxMTAxMDAgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMTAgMDExMDAwMDEgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTAxMTEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAxMDEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMDAgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTEwMDAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMTAgMDAxMDAwMDAgMDAxMTAxMTAgMDAxMTEwMDA=
>
> Hint 1: Crest 3 has been encoded three times
>
> Hint 2: Crest 3 contains 19 letters
>
> Note: You need to collect all 4 crests, combine and decode to reveal another path
>
> The combination should be crest 1 + crest 2 + crest 3 + crest 4. Also, the combination is a type of encoded base and you need to decode it

It's encode with base64 then binary then hexadecimal. Let's use ctf-party to
decode it.

```
$ ctf_party_console
irb(main):001:0> message = 'MDAxMTAxMTAgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAxMDAgMDExMDAxMDAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDAgMDAx
MDAwMDAgMDAxMTAxMTAgMDExMDAwMTEgMDAxMDAwMDAgMDAxMTAxMTEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAxMTAgMDAxMTAxMDAgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMTAgMDExMDAwMDEgMDAxMDAwMDAgMDAxMTAxMDEg
MDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTAxMTEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAxMDEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMDAgMDAxMDAwMDAgMDAxMTAxMDEgMDAxMTEwMDAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMTAgMDAxMDAwMDAgMDAxMTAxMTAgMDAxMTEwMDA='
=> "MDAxMTAxMTAgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAwMTEgMDAxMDAwMDAgMDAxMTAxMDAgMDExMDAxMDAgMDAxMDAwMDAgMDAxMTAwMTEgMDAxMTAxMTAgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDEgMDAxMDAwMDAgMDAxMTAxMDAgMDAxMTEwMDAgMDAxMDAwMDAgMDAxMTAxMTA...
irb(main):004:0> message.from_b64.gsub(' ', '').from_bin.gsub(' ', '').from_hex
=> "c3M6IHlvdV9jYW50X2h"
```

### crest 4

At http://biohazard.thm/attic/ the door is locked and asking for the
shield flag. It redirects us to http://biohazard.thm/attic909447f184afdfb352af8b8a25ffff1d/
with a link to http://biohazard.thm/attic909447f184afdfb352af8b8a25ffff1d/note.txt
and crest 4 content:

> crest 4:
>
> gSUERauVpvKzRpyPpuYz66JDmRTbJubaoArM6CAQsnVwte6zF9J4GGYyun3k5qM9ma4s
>
> Hint 1: Crest 2 has been encoded twice
>
> Hint 2: Crest 2 contains 17 characters
>
> Note: You need to collect all 4 crests, combine and decode to reveal another path
>
> The combination should be crest 1 + crest 2 + crest 3 + crest 4. Also, the combination is a type of encoded base and you need to decode it

Here it's base58 + hexadecimal.

https://gchq.github.io/CyberChef/#recipe=From_Base58('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',false)From_Hex('Space')

Crest 4: `pZGVfZm9yZXZlcg==`

### crests

By joining the 4 crest parts we have the following base64 string:
`RlRQIHVzZXI6IGh1bnRlciwgRlRQIHBhc3M6IHlvdV9jYW50X2hpZGVfZm9yZXZlcg==`
which decodes to `FTP user: edited, FTP pass: edited`.

## The guard house

With the FTP account acquired in the previous steps we can get our hands on
some new files.

```
$ ftp biohazard.thm
Connected to biohazard.thm.
220 (vsFTPd 3.0.3)
Name (biohazard.thm:noraj): hunter
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 0        0            7994 Sep 19  2019 001-key.jpg
-rw-r--r--    1 0        0            2210 Sep 19  2019 002-key.jpg
-rw-r--r--    1 0        0            2146 Sep 19  2019 003-key.jpg
-rw-r--r--    1 0        0             121 Sep 19  2019 helmet_key.txt.gpg
-rw-r--r--    1 0        0             170 Sep 20  2019 important.txt
226 Directory send OK.
```

Each file can be downloaded with the `get` command.

### helmet

Read `important.txt`

> Jill,
>
> I think the helmet key is inside the text file, but I have no clue on decrypting stuff. Also, I come across a /EDITED/ door but it was locked.
>
> From,
> Barry

Nothing suspicious with the first image:

```
$ exiftool ftp/001-key.jpg
ExifTool Version Number         : 12.25
File Name                       : 001-key.jpg
Directory                       : ftp
File Size                       : 7.8 KiB
File Modification Date/Time     : 2021:05:05 09:22:44+02:00
File Access Date/Time           : 2021:05:05 09:25:32+02:00
File Inode Change Date/Time     : 2021:05:05 09:22:49+02:00
File Permissions                : -rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 400
Image Height                    : 320
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 400x320
Megapixels                      : 0.128
```

The second and third image have a comment:

```
$ exiftool -Comment ftp/002-key.jpg
Comment                         : 5fYmVfZGVzdHJveV9
$ exiftool -Comment ftp/003-key.jpg
Comment                         : Compressed by jpeg-recompress
```

The first image was hiding a file:

```
$ steghide extract -sf ftp/001-key.jpg
Enter passphrase:
wrote extracted data to "key-001.txt".

$ cat key-001.txt
cGxhbnQ0Ml9jYW
```

And the third is hiding files too:

```
$ binwalk -e ftp/003-key.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
1930          0x78A           Zip archive data, at least v2.0 to extract, uncompressed size: 14, name: key-003.txt
2124          0x84C           End of Zip archive, footer length: 22

$ cat ftp/_003-key.jpg-1.extracted/key-003.txt
3aXRoX3Zqb2x0
```

If we combine the keys we have a base64 string.

```
$ printf %s cGxhbnQ0Ml9jYW5fYmVfZGVzdHJveV93aXRoX3Zqb2x0 | base64 -d
<edited>
```

It must be the password for the gpg file:

```
$ gpg helmet_key.txt.gpg
gpg: WARNING: no command supplied.  Trying to guess what you mean ...
gpg: AES256.CFB encrypted data
gpg: encrypted with 1 passphrase
```

We have the helmet flag.

## The Revisit

### eagle

At http://biohazard.thm/studyRoom/ the door is locked and asking for the
helmet flag. It redirects to http://biohazard.thm/studyRoom28341c5e98c93b89258a6389fd608a3c/
where we can download http://biohazard.thm/studyRoom28341c5e98c93b89258a6389fd608a3c/doom.tar.gz.

Let's decompress the archive:

```
$ tar tvf doom.tar.gz
-rw-r--r-- root/root        25 2019-09-20 09:02 eagle_medal.txt

$ tar xaf doom.tar.gz

$ cat eagle_medal.txt 
SSH user: EDITED
```

### wolf

At http://biohazard.thm/hidden_closet/ the door is locked and asking for the
helmet flag. It redirects to http://biohazard.thm/hiddenCloset8997e740cb7f5cece994381b9477ec38/
where we can find http://biohazard.thm/hiddenCloset8997e740cb7f5cece994381b9477ec38/MO_DISK1.txt
and http://biohazard.thm/hiddenCloset8997e740cb7f5cece994381b9477ec38/wolf_medal.txt.

`MO_DISK1.txt`

```
wpbwbxr wpkzg pltwnhro, txrks_xfqsxrd_bvv_fy_rvmexa_ajk
```

`wolf_medal.txt`

```
SSH password: EDITED
```

We can't decrypt MO_DISK1 at this step.

## Underground laboratory

### chris

Connect to SSH:

```
$ ssh umbrella_guest@biohazard.thm
...
umbrella_guest@umbrella_corp:~$ id
uid=1001(umbrella_guest) gid=1001(umbrella) groups=1001(umbrella)
```

There is a suspicious hidden folder:

```
umbrella_guest@umbrella_corp:~$ ls -lhA
total 56K
-rw-r--r--  1 umbrella_guest umbrella  220 Sep 19  2019 .bash_logout
-rw-r--r--  1 umbrella_guest umbrella 3.7K Sep 19  2019 .bashrc
drwxrwxr-x  6 umbrella_guest umbrella 4.0K Sep 20  2019 .cache
drwxr-xr-x 11 umbrella_guest umbrella 4.0K Sep 19  2019 .config
-rw-r--r--  1 umbrella_guest umbrella   26 Sep 19  2019 .dmrc
drwx------  3 umbrella_guest umbrella 4.0K Sep 19  2019 .gnupg
-rw-------  1 umbrella_guest umbrella  346 Sep 19  2019 .ICEauthority
drwxr-xr-x  2 umbrella_guest umbrella 4.0K Sep 20  2019 .jailcell
drwxr-xr-x  3 umbrella_guest umbrella 4.0K Sep 19  2019 .local
-rw-r--r--  1 umbrella_guest umbrella  807 Sep 19  2019 .profile
drwx------  2 umbrella_guest umbrella 4.0K Sep 20  2019 .ssh
-rw-------  1 umbrella_guest umbrella  109 Sep 19  2019 .Xauthority
-rw-------  1 umbrella_guest umbrella 7.4K Sep 19  2019 .xsession-errors

$ umbrella_guest@umbrella_corp:~$ ls -lhA .jailcell/
total 4.0K
-rw-r--r-- 1 umbrella_guest umbrella 501 Sep 20  2019 chris.txt

umbrella_guest@umbrella_corp:~$ cat .jailcell/chris.txt
Jill: Chris, is that you?
Chris: Jill, you finally come. I was locked in the Jail cell for a while. It seem that weasker is behind all this.
Jil, What? Weasker? He is the traitor?
Chris: Yes, Jill. Unfortunately, he play us like a damn fiddle.
Jill: Let's get out of here first, I have contact brad for helicopter support.
Chris: Thanks Jill, here, take this MO Disk 2 with you. It look like the key to decipher something.
Jill: Alright, I will deal with him later.
Chris: see ya.

MO disk 2: albert
```

This is the vigenere decryption key for the disk 1.

https://gchq.github.io/CyberChef/#recipe=Vigen%C3%A8re_Decode('albert')&input=d3Bid2J4ciB3cGt6ZyBwbHR3bmhybywgdHhya3NfeGZxc3hyZF9idnZfZnlfcnZtZXhhX2Fqaw

```
weasker login password, EDITED
```

### traitor

Let's connect as the weasker:

```
umbrella_guest@umbrella_corp:~$ su weasker
Password: 
weasker@umbrella_corp:/home/umbrella_guest$ cd
weasker@umbrella_corp:~$ ls -lhA
total 72K
-rw-------  1 weasker weasker   18 Sep 20  2019 .bash_history
-rw-r--r--  1 weasker weasker  220 Sep 18  2019 .bash_logout
-rw-r--r--  1 weasker weasker 3.7K Sep 18  2019 .bashrc
drwxrwxr-x 10 weasker weasker 4.0K Sep 20  2019 .cache
drwxr-xr-x 11 weasker weasker 4.0K Sep 20  2019 .config
drwxr-xr-x  2 weasker weasker 4.0K Sep 19  2019 Desktop
drwx------  3 weasker weasker 4.0K Sep 19  2019 .gnupg
-rw-------  1 weasker weasker  346 Sep 20  2019 .ICEauthority
drwxr-xr-x  3 weasker weasker 4.0K Sep 19  2019 .local
drwx------  5 weasker weasker 4.0K Sep 19  2019 .mozilla
-rw-r--r--  1 weasker weasker  807 Sep 18  2019 .profile
drwx------  2 weasker weasker 4.0K Sep 19  2019 .ssh
-rw-r--r--  1 weasker weasker    0 Sep 20  2019 .sudo_as_admin_successful
-rw-r--r--  1 root    root     534 Sep 20  2019 weasker_note.txt
-rw-------  1 weasker weasker  109 Sep 20  2019 .Xauthority
-rw-------  1 weasker weasker 5.5K Sep 20  2019 .xsession-errors
-rw-------  1 weasker weasker 6.6K Sep 20  2019 .xsession-errors.old

weasker@umbrella_corp:~$ cat weasker_note.txt
Weaker: Finally, you are here, Jill.
Jill: Weasker! stop it, You are destroying the  mankind.
Weasker: Destroying the mankind? How about creating a 'new' mankind. A world, only the strong can survive.
Jill: This is insane.
Weasker: Let me show you the ultimate lifeform, the Tyrant.

(Tyrant jump out and kill Weasker instantly)
(Jill able to stun the tyrant will a few powerful magnum round)

Alarm: Warning! warning! Self-detruct sequence has been activated. All personal, please evacuate immediately. (Repeat)
Jill: Poor bastard
```

## root

There is no real EoP:

```
weasker@umbrella_corp:~$ id
uid=1000(weasker) gid=1000(weasker) groups=1000(weasker),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),118(lpadmin),126(sambashare)
weasker@umbrella_corp:~$ sudo -l
[sudo] password for weasker: 
Matching Defaults entries for weasker on umbrella_corp:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User weasker may run the following commands on umbrella_corp:
    (ALL : ALL) ALL
weasker@umbrella_corp:~$ sudo su -
root@umbrella_corp:~# pwd
/root
root@umbrella_corp:~# cat root.txt
In the state of emergency, Jill, Barry and Chris are reaching the helipad and awaiting for the helicopter support.

Suddenly, the Tyrant jump out from nowhere. After a tough fight, brad, throw a rocket launcher on the helipad. Without thinking twice, Jill pick up the launcher and fire at the Tyrant.

The Tyrant shredded into pieces and the Mansion was blowed. The survivor able to escape with the helicopter and prepare for their next fight.

The End

flag: edited
```
