---
layout: post
title: "Nessus - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - exploit
  - thm
  - nessus
date: 2020/09/01
updated: 2020/12/17 19:36:00
thumbnail: /images/TryHackMe/Nessus.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Nessus
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rpnessus)
- **Difficulty:** Easy
- **Description**: Part of the Red Primer series, learn how to set up and use Nessus

![Nessus](/images/TryHackMe/Nessus.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S nessus
```

**Disclaimer**: answers are very easy, obvious, given in the question or the
description or just requires to search the help page so I won't give details here.

## [Task 3] Nessus Quiz

### #1

> As we log into Nessus, we are greeted with a button to launch a scan, what is the name of this button?

Answer: {% spoiler new scan %}

### #2

> Nessus allows us to create custom templates that can be used during the scan selection as additional scan types, what is the name of the menu where we can set these?

Answer: {% spoiler Policies %}

### #3

> Nessus also allows us to change plugin properties such as hiding them or changing their severity, what menu allows us to change this?

Answer: {% spoiler Plugin Rules %}

### #4

> Nessus can also be run through multiple 'Scanners' where multiple installations can work together to complete scans or run scans on remote networks, what menu allows us to see all of these installations?

Answer: {% spoiler Scanners %}

### #5

> Let's move onto the scan types, what scan allows us to see simply what hosts are 'alive'?

Answer: {% spoiler Host Discovery %}

### #6

> One of the most useful scan types, which is considered to be 'suitable for any host'?

Answer: {% spoiler Basic Network Scan %}

### #7

> Following a few basic scans, it's often useful to run a scan wherein the scanner can authenticate to systems and evaluate their patching level. What scan allows you to do this?

Answer: {% spoiler Credentialed Patch Audit %}

### #8

> When performing Web App tests it's often useful to run which scan? This can be incredibly useful when also using nitko, zap, and burp to gain a full picture of an application.

Answer: {% spoiler Web Application Tests %}

## [Task 4] Scanning!

### #2

> Create a new 'Basic Network Scan' targeting the deployed VM. What option can we set under 'BASIC' to set a time for this scan to run? This can be very useful when network congestion is an issue.

Answer: {% spoiler Schedule %}

### #3

> Under discovery set the scan to cover ports 1-65535. What is this type called?

Answer: {% spoiler Port scan (all ports) %}

### #4

> As we are connected to the network via a VPN, it may be to our benefit to 'tone down' the scan a bit. What scan type can we change to under 'ADVANCED' for this lower bandwidth connection?

Answer: {% spoiler Scan low bandwidth links %}

### #6

> After the scan completes, which 'Vulnerability' can we view the details of to see the open ports on this host?

Answer: {% spoiler Nessus SYN scanner %}

### #7

> There seems to be a chat server running on this machine, what port is it on?

Answer: {% spoiler 6667 %}

```
IRC Daemon Version Detection

The IRC server version is : ngIRCd-21. IPv6+IRCPLUS+SSL+SYSLOG+ZLIB-i686/pc/linux-gnu
```

### #8

> Looks like we have a medium level vulnerability relating to SSH, what is this vulnerability named?

Answer: {% spoiler SSH Weak Algorithms Supported %}

### #9

> What web server type and version is reported by Nessus?

Answer: {% spoiler Apache/2.4.99 %}

The answer is in `HTTP Server Type and Version` which is grouped under
`HTTP (Multiple Issues)`: `Apache/2.4.7`.
But the `Apache HTTP Server Version` grouped under
`Apache HTTP Server (Multiple Issues)` reports `Apache/2.4.99`.

The room is expecting the wrong answer, obviously 2.4.99 is an invalid version
that doesn't exist and is a false positive or a bug, 2.4.7 should be the correct
answer.

## [Task 6] So you're telling me that's how you set up a web app...

### #2

> What is the plugin id of the plugin that determines the HTTP server type and version?

Answer: {% spoiler 10107 %}

`HTTP Server Type and Version` plugin.

### #3

> What authentication page is discovered by the scanner that transmits credentials in cleartext?

Answer: {% spoiler login.php %}

The room is outdated so `login.php` won't show up in your scan.

### #4

> What is the file extension of the config backup?

Answer: {% spoiler .bak %}

The room is outdated you can't find the answer by yourself.

### #5

> Which directory contains example documents? (This will be in a php directory)

Answer: {% spoiler /external/phpids/0.6/docs/examples %}

The room is outdated, the server serving this page is down so you can't find it.

### #6

> What vulnerability is this application susceptible to that is associated with X-Frame-Options?

Answer: {% spoiler clickjacking %}

The room is outdated, nessus can't find the vuln because the web server is not here anymore.

### #7

> What version of php is the server using?

Answer: {% spoiler 5.5.9-1ubuntu4.26 %}

The room is outdated, nessus find the version `5.5.9-1ubuntu4.14` but the room
is expecting `5.5.9-1ubuntu4.26`.
