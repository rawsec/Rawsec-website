---
layout: post
title: "Nessus - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - exploit
  - thm
  - nessus
date: 2021/01/05 18:59:00
thumbnail: /images/TryHackMe/Nessus.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Nessus
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rpnessusredux)
- **Difficulty:** Easy
- **Description**: Learn how to set up and use Nessus, a popular vulnerability scanner.

![Nessus](/images/TryHackMe/Nessus.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S nessus
```

## Navigation and Scans

> What is the name of the button which is used to launch a scan?

Answer: {% spoiler new scan %}

The blue button on the top right corner.

> What side menu option allows us to create custom templates?

Answer: {% spoiler policies %}

The first item in the _resources_ section.

> What menu allows us to change plugin properties such as hiding them or changing their severity?

Answer: {% spoiler plugin rules %}

The second item in the _resources_ section.

> In the 'Scan Templates' section after clicking on 'New Scan', what scan allows us to see simply what hosts are alive?

Answer: {% spoiler host discovery %}

Explicit name.

> One of the most useful scan types, which is considered to be 'suitable for any host'?

Answer: {% spoiler basic network scan %}

Not really the most useful, I'm using the advanced one every time.

> What scan allows you to 'Authenticate to hosts and enumerate missing updates'?

Answer: {% spoiler credentialed patch audit %}

Explicit name.

> What scan is specifically used for scanning Web Applications?

Answer: {% spoiler web application tests %}

Explicit name.

## Scanning!

> Create a new 'Basic Network Scan' targeting the deployed VM. What option can we set under 'BASIC' (on the left) to set a time for this scan to run? This can be very useful when network congestion is an issue.

Answer: {% spoiler schedule %}

Just do as told.

> Under 'DISCOVERY' (on the left) set the 'Scan Type' to cover ports 1-65535. What is this type called?

Answer: {% spoiler port scan (all ports) %}

Just do as told.

> What 'Scan Type' can we change to under 'ADVANCED' for lower bandwidth connection?

Answer: {% spoiler scan low bandwidth links %}

Just do as told.

> After the scan completes, which 'Vulnerability' in the 'Port scanners' family can we view the details of to see the open ports on this host?

Answer: {% spoiler Nessus SYN scanner %}

Same type of scan we always do with nmap.

> What Apache HTTP Server Version is reported by Nessus?

Answer: {% spoiler 2.4.99 %}

Check the Apache HTTP Server Version module.

## Scanning a Web Application!

> What is the plugin id of the plugin that determines the HTTP server type and version?

Answer: {% spoiler 10107 %}

id of the HTTP Server Type and Version plugin.

> What authentication page is discovered by the scanner that transmits credentials in cleartext?

Answer: {% spoiler `/login.php` %}

Check the Web Server Transmits Cleartext Credentials plugin.

> What is the file extension of the config backup?

Answer: {% spoiler .bak %}

Check the Backup Files Disclosure plugin.

> Which directory contains example documents? (This will be in a php directory)

Answer: {% spoiler `/external/phpids/0.6/docs/examples/` %}

Check the Browsable Web Directories plugin.

> What vulnerability is this application susceptible to that is associated with X-Frame-Options?

Answer: {% spoiler Clickjacking %}

Check the Web Application Potentially Vulnerable to Clickjacking module.
