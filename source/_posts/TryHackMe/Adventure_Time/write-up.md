---
layout: post
title: "Adventure Time - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - stegano
  - misc
date: 2021/05/05 20:27:00
thumbnail: /images/TryHackMe/adventuretime.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Adventure Time
- **Profile:** [tryhackme.com](https://tryhackme.com/room/adventuretime)
- **Difficulty:** Hard
- **Description**: A CTF based challenge to get your blood pumping...

![Adventure Time](/images/TryHackMe/adventuretime.jpeg)

# Write-up

## Overview

Warning: totally unrealistic and as little to do with security, it's more a mind game

## Network enumeration

Port and service scan:

```
# Nmap 7.91 scan initiated Wed May  5 10:52:11 2021 as: nmap -sSVC -p- -oA nmap_full -v 10.10.65.232
Nmap scan report for adventuretime.thm (10.10.65.232)
Host is up (0.080s latency).
Not shown: 65530 closed ports
PORT      STATE SERVICE  VERSION
21/tcp    open  ftp      vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -r--r--r--    1 ftp      ftp       1401357 Sep 21  2019 1.jpg
| -r--r--r--    1 ftp      ftp        233977 Sep 21  2019 2.jpg
| -r--r--r--    1 ftp      ftp        524615 Sep 21  2019 3.jpg
| -r--r--r--    1 ftp      ftp        771076 Sep 21  2019 4.jpg
| -r--r--r--    1 ftp      ftp       1644395 Sep 21  2019 5.jpg
|_-r--r--r--    1 ftp      ftp         40355 Sep 21  2019 6.jpg
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp    open  ssh      OpenSSH 7.6p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 58:d2:86:99:c2:62:2d:95:d0:75:9c:4e:83:b6:1b:ca (RSA)
|   256 db:87:9e:06:43:c7:6e:00:7b:c3:bc:a1:97:dd:5e:83 (ECDSA)
|_  256 6b:40:84:e6:9c:bc:1c:a8:de:b2:a1:8b:a3:6a:ef:f0 (ED25519)
80/tcp    open  http     Apache httpd 2.4.29
| http-methods:
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: 404 Not Found
443/tcp   open  ssl/http Apache httpd 2.4.29 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: 400 Bad Request
| ssl-cert: Subject: commonName=adventure-time.com/organizationName=Candy Corporate Inc./stateOrProvinceName=Candy Kingdom/countryName=CK
| Issuer: commonName=adventure-time.com/organizationName=Candy Corporate Inc./stateOrProvinceName=Candy Kingdom/countryName=CK
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2019-09-20T08:29:36
| Not valid after:  2020-09-19T08:29:36
| MD5:   fe38 d852 1fab ee33 b560 42ab 3e53 c129
|_SHA-1: 66ba 29fa 3a0e 26f6 d31b c61b ed83 61a1 609f e621
31337/tcp open  Elite?
| fingerprint-strings:
|   DNSStatusRequestTCP, RPCCheck, SSLSessionReq:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not
|   DNSVersionBindReqTCP:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not
|     version
|     bind
|   GenericLines, NULL:
|     Hello Princess Bubblegum. What is the magic word?
|   GetRequest:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not GET / HTTP/1.0
|   HTTPOptions:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not OPTIONS / HTTP/1.0
|   Help:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not HELP
|   RTSPRequest:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not OPTIONS / RTSP/1.0
|   SIPOptions:
|     Hello Princess Bubblegum. What is the magic word?
|     magic word is not OPTIONS sip:nm SIP/2.0
|     Via: SIP/2.0/TCP nm;branch=foo
|     From: <sip:nm@nm>;tag=root
|     <sip:nm2@nm2>
|     Call-ID: 50000
|     CSeq: 42 OPTIONS
|     Max-Forwards: 70
|     Content-Length: 0
|     Contact: <sip:nm@nm>
|_    Accept: application/sdp
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port31337-TCP:V=7.91%I=7%D=5/5%Time=60925D45%P=x86_64-unknown-linux-gnu
SF:%r(NULL,32,"Hello\x20Princess\x20Bubblegum\.\x20What\x20is\x20the\x20ma
SF:gic\x20word\?\n")%r(GetRequest,57,"Hello\x20Princess\x20Bubblegum\.\x20
SF:What\x20is\x20the\x20magic\x20word\?\nThe\x20magic\x20word\x20is\x20not
SF:\x20GET\x20/\x20HTTP/1\.0\n")%r(SIPOptions,124,"Hello\x20Princess\x20Bu
SF:bblegum\.\x20What\x20is\x20the\x20magic\x20word\?\nThe\x20magic\x20word
SF:\x20is\x20not\x20OPTIONS\x20sip:nm\x20SIP/2\.0\r\nVia:\x20SIP/2\.0/TCP\
SF:x20nm;branch=foo\r\nFrom:\x20<sip:nm@nm>;tag=root\r\nTo:\x20<sip:nm2@nm
SF:2>\r\nCall-ID:\x2050000\r\nCSeq:\x2042\x20OPTIONS\r\nMax-Forwards:\x207
SF:0\r\nContent-Length:\x200\r\nContact:\x20<sip:nm@nm>\r\nAccept:\x20appl
SF:ication/sdp\n")%r(GenericLines,32,"Hello\x20Princess\x20Bubblegum\.\x20
SF:What\x20is\x20the\x20magic\x20word\?\n")%r(HTTPOptions,5B,"Hello\x20Pri
SF:ncess\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20word\?\nThe\x20ma
SF:gic\x20word\x20is\x20not\x20OPTIONS\x20/\x20HTTP/1\.0\n")%r(RTSPRequest
SF:,5B,"Hello\x20Princess\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20
SF:word\?\nThe\x20magic\x20word\x20is\x20not\x20OPTIONS\x20/\x20RTSP/1\.0\
SF:n")%r(RPCCheck,75,"Hello\x20Princess\x20Bubblegum\.\x20What\x20is\x20th
SF:e\x20magic\x20word\?\nThe\x20magic\x20word\x20is\x20not\x20\x80\0\0\(r\
SF:xfe\x1d\x13\0\0\0\0\0\0\0\x02\0\x01\x86\xa0\0\x01\x97\|\0\0\0\0\0\0\0\0
SF:\0\0\0\0\0\0\0\0\0\0\0\0\n")%r(DNSVersionBindReqTCP,69,"Hello\x20Prince
SF:ss\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20word\?\nThe\x20magic
SF:\x20word\x20is\x20not\x20\0\x1e\0\x06\x01\0\0\x01\0\0\0\0\0\0\x07versio
SF:n\x04bind\0\0\x10\0\x03\n")%r(DNSStatusRequestTCP,57,"Hello\x20Princess
SF:\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20word\?\nThe\x20magic\x
SF:20word\x20is\x20not\x20\0\x0c\0\0\x10\0\0\0\0\0\0\0\0\0\n")%r(Help,4D,"
SF:Hello\x20Princess\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20word\
SF:?\nThe\x20magic\x20word\x20is\x20not\x20HELP\n")%r(SSLSessionReq,A1,"He
SF:llo\x20Princess\x20Bubblegum\.\x20What\x20is\x20the\x20magic\x20word\?\
SF:nThe\x20magic\x20word\x20is\x20not\x20\x16\x03\0\0S\x01\0\0O\x03\0\?G\x
SF:d7\xf7\xba,\xee\xea\xb2`~\xf3\0\xfd\x82{\xb9\xd5\x96\xc8w\x9b\xe6\xc4\x
SF:db<=\xdbo\xef\x10n\0\0\(\0\x16\0\x13\0\n\0f\0\x05\0\x04\0e\0d\0c\0b\0a\
SF:0`\0\x15\0\x12\0\t\0\x14\0\x11\0\x08\0\x06\0\x03\x01\0\n");
Service Info: Host: 127.0.1.1; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed May  5 10:57:27 2021 -- 1 IP address (1 host up) scanned in 316.60 seconds
```

Let's add a domain:

```
$ grep adventuretime /etc/hosts
10.10.65.232 adventuretime.thm
```

## Web enumeration part 1

Warning: This step is pure guessing and not real enumeration as the directory you are
supposed to find is `candybar` and is not in most wordlist, so using common
real-life wordlist won't help you, you have to find the only right one by chance.
Also the app is available over HTTPS but not HTTP.

```
$ ffuf -u https://adventuretime.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
...
nothing

$ ffuf -u https://adventuretime.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt
```

## Useless stuff part 1

When you find https://adventuretime.thm/candybar/ you realize the challenge is
a bit of a cancer because you have a whole base32 string to decode but it's given
to you as an image that you have to manually recopy as OCR doesn't work very
well with non-words.

But "hopefully" you can CTRL+U and have the string in a HTML comment:

```html
 <!-- KBQWY4DONAQHE53UOJ5CA2LXOQQEQSCBEBZHIZ3JPB2XQ4TQNF2CA5LEM4QHEYLKORUC4=== -->
```

```
$ printf %s 'KBQWY4DONAQHE53UOJ5CA2LXOQQEQSCBEBZHIZ3JPB2XQ4TQNF2CA5LEM4QHEYLKORUC4===' | base32 -d
Palpnh rwtrz iwt HHA rtgixuxrpit udg rajth.
```

We obtain a message that looks like encrypted with Caesar cipher.
It's not rot13 so I used [ctf-party](https://github.com/noraj/ctf-party/) to
brute-force all shift possibilities:

```ruby
$ ctf_party_console
irb(main):001:0> message = 'Palpnh rwtrz iwt HHA rtgixuxrpit udg rajth.'
=> "Palpnh rwtrz iwt HHA rtgixuxrpit udg rajth."
irb(main):002:0> (1..26).each.map { |n| message.rot(shift: n) }
=>
["Qbmqoi sxusa jxu IIB suhjyvysqju veh sbkui.",
 "Rcnrpj tyvtb kyv JJC tvikzwztrkv wfi tclvj.",
 "Sdosqk uzwuc lzw KKD uwjlaxauslw xgj udmwk.",
 "Teptrl vaxvd max LLE vxkmbybvtmx yhk venxl.",
 "Ufqusm wbywe nby MMF wylnczcwuny zil wfoym.",
 "Vgrvtn xczxf ocz NNG xzmodadxvoz ajm xgpzn.",
 "Whswuo ydayg pda OOH yanpebeywpa bkn yhqao.",
 "Xitxvp zebzh qeb PPI zboqfcfzxqb clo zirbp.",
 "Yjuywq afcai rfc QQJ acprgdgayrc dmp ajscq.",
 "Zkvzxr bgdbj sgd RRK bdqshehbzsd enq bktdr.",
 "Always check the SSL certificate for clues.",
 "Bmxbzt difdl uif TTM dfsujgjdbuf gps dmvft.",
 "Cnycau ejgem vjg UUN egtvkhkecvg hqt enwgu.",
 "Dozdbv fkhfn wkh VVO fhuwlilfdwh iru foxhv.",
 "Epaecw gligo xli WWP givxmjmgexi jsv gpyiw.",
 "Fqbfdx hmjhp ymj XXQ hjwynknhfyj ktw hqzjx.",
 "Grcgey inkiq znk YYR ikxzoloigzk lux iraky.",
 "Hsdhfz joljr aol ZZS jlyapmpjhal mvy jsblz.",
 "Iteiga kpmks bpm AAT kmzbqnqkibm nwz ktcma.",
 "Jufjhb lqnlt cqn BBU lnacrorljcn oxa ludnb.",
 "Kvgkic mromu dro CCV mobdspsmkdo pyb mveoc.",
 "Lwhljd nspnv esp DDW npcetqtnlep qzc nwfpd.",
 "Mximke otqow ftq EEX oqdfuruomfq rad oxgqe.",
 "Nyjnlf purpx gur FFY pregvsvpngr sbe pyhrf.",
 "Ozkomg qvsqy hvs GGZ qsfhwtwqohs tcf qzisg.",
 "Palpnh rwtrz iwt HHA rtgixuxrpit udg rajth."]
```

> Always check the SSL certificate for clues.

Seems we have to check the SSL cert.

Let's grab it:

```
$ openssl s_client -showcerts -connect adventuretime.thm:443 </dev/null
CONNECTED(00000003)
depth=0 C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com
verify error:num=18:self signed certificate
verify return:1
depth=0 C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com
verify error:num=10:certificate has expired
notAfter=Sep 19 08:29:36 2020 GMT
verify return:1
depth=0 C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com
notAfter=Sep 19 08:29:36 2020 GMT
verify return:1
---
Certificate chain
 0 s:C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com
   i:C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com
-----BEGIN CERTIFICATE-----
MIIEEzCCAvugAwIBAgIUbcVV6csztEzfPt+tkvEXDwDTNuEwDQYJKoZIhvcNAQEL
BQAwgZgxCzAJBgNVBAYTAkNLMRYwFAYDVQQIDA1DYW5keSBLaW5nZG9tMR0wGwYD
VQQKDBRDYW5keSBDb3Jwb3JhdGUgSW5jLjELMAkGA1UECwwCQ0MxGzAZBgNVBAMM
EmFkdmVudHVyZS10aW1lLmNvbTEoMCYGCSqGSIb3DQEJARYZYnViYmxlZ3VtQGxh
bmQtb2Ytb29vLmNvbTAeFw0xOTA5MjAwODI5MzZaFw0yMDA5MTkwODI5MzZaMIGY
MQswCQYDVQQGEwJDSzEWMBQGA1UECAwNQ2FuZHkgS2luZ2RvbTEdMBsGA1UECgwU
Q2FuZHkgQ29ycG9yYXRlIEluYy4xCzAJBgNVBAsMAkNDMRswGQYDVQQDDBJhZHZl
bnR1cmUtdGltZS5jb20xKDAmBgkqhkiG9w0BCQEWGWJ1YmJsZWd1bUBsYW5kLW9m
LW9vby5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIsPkc5wZq
3vwvIPUao4OrBkQ/qcRuykZ9BRiyVNqSiwolBlkBoV+Fro4kC5EFg0a05njaC+3K
hebtBRK8w3JpOP/wJV7vfPgBkjid/wyt1ejRrxtCVdquJAExkX8pAWC4N93fZV7f
LqED5A3mixWNS6c/hjzjeNfxhtxMF2sVYnvHCoysWtYPx3btpsxmBTGiO3Ae/ram
6XNMS62db71qarseenXJNWg/YVLnNxsK5Qh3Fyqq0yIMvAeePAgcOfnImgJesp12
01qzdF9nYLjatzaCs7UzjooY3DSJt13oAwuDi7xobQfixUylNotuhKUNIHtOmZ/5
HQiiK+hvGYJDAgMBAAGjUzBRMB0GA1UdDgQWBBROc7+FG51+kgTPa4tQsbciaZvn
KzAfBgNVHSMEGDAWgBROc7+FG51+kgTPa4tQsbciaZvnKzAPBgNVHRMBAf8EBTAD
AQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBfcgRIbZ6seeRNPzWGs5rOkQh77pFLUTKo
2ZU3oFBJ632AYHOs1UhjWYeKGslPXloK9qHbBoDdL6uJU5FFsD1B7bMa4PLN7nuM
0QcwXpqIlO7Fo6n00yY6IEey9jomTwLe8th+R8V8vNWd5gfQ9cey79WXnuekkez9
49UMv3z7jIgW3eWSw/AHV+Vdc8t9+SLSPxciCgfxYEp4Avl0u2qzSfoKJsmHDKMp
p89GR4+2BjnmsYtFs5RL8KNAr4P3xD+QT6/VZdTWnARV9r/7CFn+ZD2C9BQJ8OX2
n+oi/jgOP0wo6FEb7YMKaoUGlqKl+xTlgy5KF/zpQAovH01KngtW
-----END CERTIFICATE-----
---
Server certificate
subject=C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com

issuer=C = CK, ST = Candy Kingdom, O = Candy Corporate Inc., OU = CC, CN = adventure-time.com, emailAddress = bubblegum@land-of-ooo.com

---
No client certificate CA names sent
Peer signing digest: SHA256
Peer signature type: RSA-PSS
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1525 bytes and written 412 bytes
Verification error: certificate has expired
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: FA80A6DCCBEE65C17C7CCD2FF0886DE9A753701DCE871BC228C7B1454AFB594F
    Session-ID-ctx:
    Master-Key: B3AB0F074EFD7DC5B0EC0B4BAF4E182EE281838C36623FBE2A32362C773446B3658DB2AA3527CA6622336B280EFA5C24
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    Start Time: 1620205837
    Timeout   : 7200 (sec)
    Verify return code: 10 (certificate has expired)
    Extended master secret: yes
---
DONE
```

We find some information:

- email: `bubblegum@land-of-ooo.com`
- expected domain: `adventure-time.com`

So let's add those domains:

```
$ grep adventuretime /etc/hosts
10.10.65.232 adventuretime.thm land-of-ooo.com adventure-time.co
```

## Web enumeration part 2

Warning: This step is pure guessing and not real enumeration as the directory you are
supposed to find is `yellowdog` and is not in most wordlist, so using common
real-life wordlist won't help you, you have to find the only right one by chance.

```
$ ffuf -u https://land-of-ooo.com/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
...
nothing

$ ffuf -u https://land-of-ooo.com/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt
$ ffuf -u https://land-of-ooo.com/yellowdog/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt
```

Then unrealistic folder name again: `bananastock`.

## Useless stuff part 2

There is a HTML comment on this page:

```html
    <!-- _/..../.\_.../._/_./._/_./._/...\._/._./.\_/..../.\_..././.../_/_._.__/_._.__/_._.__ -->
```

It's morse code but with non-standard delimiters:

- letter delimiter: forward slash instead of space
- word delimiter: backslash instead of line feed

https://gchq.github.io/CyberChef/#recipe=From_Morse_Code('Forward%20slash','Backslash')&input=Xy8uLi4uLy5cXy4uLi8uXy9fLi8uXy9fLi8uXy8uLi5cLl8vLl8uLy5cXy8uLi4uLy5cXy4uLi8uLy4uLi9fL18uXy5fXy9fLl8uX18vXy5fLl9f

That give us this useless message:

> THE BANANAS ARE THE BEST!!!

That was a prank (=bad joke) to make us lose our time.

We have to enumerate more.

## Web enumeration part 3

Warning: This step is pure guessing and not real enumeration as the directory you are
supposed to find is `princess` and is not in most wordlist, so using common
real-life wordlist won't help you, you have to find the only right one by chance.

```
$ ffuf -u https://land-of-ooo.com/yellowdog/bananastock/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt
```

Then unrealistic folder name again: `princess`.

## Useless stuff part 2

Again a HTML comment:

```html
    <!--
    Secrettext = 0008f1a92d287b48dccb5079eac18ad2a0c59c22fbc7827295842f670cdb3cb645de3de794320af132ab341fe0d667a85368d0df5a3b731122ef97299acc3849cc9d8aac8c3acb647483103b5ee44166
    Key = my cool password
    IV = abcdefghijklmanopqrstuvwxyz
    Mode = CBC
    Input = hex
    Output = raw
    -->
```

Oh wait something related to security? It's AES but there is no challenge since
we have all parameters to decrypt it.

We can use CyberChef to decrypt the message with luck as key length cipher mode,
padding scheme where not provided.

https://gchq.github.io/CyberChef/#recipe=AES_Decrypt(%7B'option':'Latin1','string':'my%20cool%20password'%7D,%7B'option':'Latin1','string':'abcdefghijklmanopqrstuvwxyz'%7D,'CBC','Hex','Raw',%7B'option':'Hex','string':''%7D,%7B'option':'Hex','string':''%7D)&input=MDAwOGYxYTkyZDI4N2I0OGRjY2I1MDc5ZWFjMThhZDJhMGM1OWMyMmZiYzc4MjcyOTU4NDJmNjcwY2RiM2NiNjQ1ZGUzZGU3OTQzMjBhZjEzMmFiMzQxZmUwZDY2N2E4NTM2OGQwZGY1YTNiNzMxMTIyZWY5NzI5OWFjYzM4NDljYzlkOGFhYzhjM2FjYjY0NzQ4MzEwM2I1ZWU0NDE2Ng

Decoded message:

> the magic safe is accessible at port 31337. the magic word is: ricardio

We can connect to the unrealistic useless service and get a username.

```
$ pwncat adventure-time.com 31337
Hello Princess Bubblegum. What is the magic word?
ricardio
The new username is: apple-guards
```

Then you have to guess the username can be used over SSH and that the
previous useless message `THE BANANAS ARE THE BEST!!!` is the password.

```
$ ssh apple-guards@adventure-time.com
```

## flag1

Once connect we can read the 1st flag:

```
apple-guards@at:~$ id
ididuid=1009(apple-guards) gid=1009(apple-guards) groups=1009(apple-guards)

apple-guards@at:~$ ls -lhA
total 40K
-rw------- 1 apple-guards apple-guards   11 sep 23  2019 .bash_history
-rw-r--r-- 1 apple-guards apple-guards  220 apr  4  2018 .bash_logout
-rw-r--r-- 1 apple-guards apple-guards 3,7K apr  4  2018 .bashrc
drwx------ 2 apple-guards apple-guards 4,0K sep 20  2019 .cache
-rw-r----- 1 apple-guards apple-guards   30 sep 22  2019 flag1
-rw-r--r-- 1 apple-guards apple-guards   59 sep 20  2019 flag.txt
drwx------ 3 apple-guards apple-guards 4,0K sep 20  2019 .gnupg
-rw------- 1 apple-guards apple-guards  711 sep 21  2019 mbox
-rw-r--r-- 1 apple-guards apple-guards  807 apr  4  2018 .profile
-rw------- 1 apple-guards apple-guards  713 sep 21  2019 .viminfo

apple-guards@at:~$ cat /home/apple-guards/flag1
tryhackme{edited}
```

## flag2

There is an email:

```
apple-guards@at:~$ cat mbox
From marceline@at  Fri Sep 20 16:39:54 2019
Return-Path: <marceline@at>
X-Original-To: apple-guards@at
Delivered-To: apple-guards@at
Received: by at.localdomain (Postfix, from userid 1004)
        id 6737B24261C; Fri, 20 Sep 2019 16:39:54 +0200 (CEST)
Subject: Need help???
To: <apple-guards@at>
X-Mailer: mail (GNU Mailutils 3.4)
Message-Id: <20190920143954.6737B24261C@at.localdomain>
Date: Fri, 20 Sep 2019 16:39:54 +0200 (CEST)
From: marceline@at

Hi there bananaheads!!!
I heard Princess B revoked your access to the system. Bummer!
But I'll help you guys out.....doesn't cost you a thing.....well almost nothing.

I hid a file for you guys. If you get the answer right, you'll get better access.
Good luck!!!!
```

The sender is `marceline` which is a valid user on the machine, so lets find files
owned by her.

```
apple-guards@at:~$ cat /etc/passwd
...
finn:x:1001:1001::/home/finn:/bin/bash
jake:x:1002:1002::/home/jake:/bin/bash
bubblegum:x:1003:1003::/home/bubblegum:/bin/bash
marceline:x:1004:1004::/home/marceline:/bin/bash
peppermint-butler:x:1006:1006::/home/peppermint-butler:/bin/bash
gunter:x:1007:1007::/home/gunter:/bin/bash
fern:x:1008:1008::/home/fern:/bin/bash
mysql:x:124:128:MySQL Server,,,:/nonexistent:/bin/false
apple-guards:x:1009:1009::/home/apple-guards:/bin/bash
...

apple-guards@at:~$ find / -type f -user marceline 2>/dev/null
/etc/fonts/helper

apple-guards@at:~$ file /etc/fonts/helper
/etc/fonts/helper: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/l, BuildID[sha1]=6cee442f66f3fb132491368c671c1cf91fc28332, for GNU/Linux 3.2.0, not stripped
```

There is one executable. It's a Vigenere challenge. We can solve it with
CyberChef.

https://gchq.github.io/CyberChef/#recipe=Vigen%C3%A8re_Decode('gone')&input=R3BuaGtzZQ

`Abadeer`

```
apple-guards@at:~$ /etc/fonts/helper

======================================
      BananaHead Access Pass
       created by Marceline
======================================

Hi there bananaheads!!!
So you found my file?
But it won't help you if you can't answer this question correct.
What? I told you guys I would help and that it wouldn't cost you a thing....
Well I lied hahahaha

Ready for the question?

The key to solve this puzzle is gone
And you need the key to get this readable: Gpnhkse

Did you solve the puzzle? yes

What is the word I'm looking for? Abadeer

That's it!!!! You solved my puzzle
Don't tell princess B I helped you guys!!!
My password is 'My friend Finn'
```

We can connect as marceline.

```
apple-guards@at:~$ su marceline
Password:
marceline@at:/home/apple-guards$ cd
marceline@at:~$ cat /home/marceline/flag2
tryhackme{edited}
```

## flag3

Another useless mind game:

```
marceline@at:~$ cat I-got-a-secret.txt
Hello Finn,

I heard that you pulled a fast one over the banana guards.
B was very upset hahahahaha.
I also heard you guys are looking for BMO's resetcode.
You guys broke him again with those silly games?

You know I like you Finn, but I don't want to anger B too much.
So I will help you a little bit...

But you have to solve my little puzzle. Think you're up for it?
Hahahahaha....I know you are.

111111111100100010101011101011111110101111111111011011011011000001101001001011111111111111001010010111100101000000000000101001101111001010010010111111110010100000000000000000000000000000000000000010101111110010101100101000000000000000000000101001101100101001001011111111111111111111001010000000000000000000000000001010111001010000000000000000000000000000000000000000000001010011011001010010010111111111111111111111001010000000000000000000000000000000001010111111001010011011001010010111111111111100101001000000000000101001111110010100110010100100100000000000000000000010101110010100010100000000000000010100000000010101111100101001111001010011001010010000001010010100101011100101001101100101001011100101001010010100110110010101111111111111111111111111111111110010100100100000000000010100010100111110010100000000000000000000000010100111111111111111110010100101111001010000000000000001010
```

Somehow you have to guess it's a program in [spoon](https://www.dcode.fr/langage-spoon).

The output is: *The magic word you are looking for is ApplePie*.

You have to guess that it can be used on the useless netcat service since it's tagged
as a *magic word*.

```
$ pwncat adventure-time.com 31337
Hello Princess Bubblegum. What is the magic word?
ApplePie
The password of peppermint-butler is: That Black Magic
```

Connect as `peppermint-butler` and grab the flag:

```
marceline@at:~$ su peppermint-butler
Password:
peppermint-butler@at:/home/marceline$ cd
peppermint-butler@at:~$ cat /home/peppermint-butler/flag3
tryhackme{edited}
```

## flag4

There is an image `butler-1.jpg` with stego but we don't know what.

```
peppermint-butler@at:~$ ls -lhA
total 108K
-rw-r--r-- 1 peppermint-butler peppermint-butler  220 apr  4  2018 .bash_logout
-rw-r--r-- 1 peppermint-butler peppermint-butler 3,7K apr  4  2018 .bashrc
-rw------- 1 peppermint-butler peppermint-butler  84K sep 21  2019 butler-1.jpg
drwx------ 2 peppermint-butler peppermint-butler 4,0K sep 20  2019 .cache
-rw-r----- 1 peppermint-butler peppermint-butler   28 sep 22  2019 flag3
drwx------ 3 peppermint-butler peppermint-butler 4,0K sep 20  2019 .gnupg
-rw-r--r-- 1 peppermint-butler peppermint-butler  807 apr  4  2018 .profile
```

Let's try to find some hints:

```
peppermint-butler@at:~$ find / -type f -user peppermint-butler -name *.txt 2>/dev/null
/usr/share/xml/steg.txt
/etc/php/zip.txt

peppermint-butler@at:~$ cat /usr/share/xml/steg.txt
I need to keep my secrets safe.
There are people in this castle who can't be trusted.
Those banana guards are not the smartest of guards.
And that Marceline is a friend of princess Bubblegum,
but I don't trust her.

So I need to keep this safe.

The password of my secret file is 'ToKeepASecretSafe'

peppermint-butler@at:~$ cat /etc/php/zip.txt
I need to keep my secrets safe.
There are people in this castle who can't be trusted.
Those banana guards are not the smartest of guards.
And that Marceline is a friend of princess Bubblegum,
but I don't trust her.

So I need to keep this safe.

The password of my secret file is 'ThisIsReallySave'
```

Using `ToKeepASecretSafe` as password we can extract `secret.zip` from the image.

```
$ steghide extract -sf butler-1.jpg
Enter passphrase:
wrote extracted data to "secrets.zip".
```

`ThisIsReallySave` is the password required to extract the zip:

```
$ 7z x secrets.zip
$ cat secrets.txt
[0200 hours][upper stairs]
I was looking for my arch nemesis Peace Master,
but instead I saw that cowering little puppet from the Ice King.....gunter.
What was he up to, I don't know.
But I saw him sneaking in the secret lab of Princess Bubblegum.
To be able to see what he was doing I used my spell 'the evil eye' and saw him.
He was hacking the secret laptop with something small like a duck of rubber.
I had to look closely, but I think I saw him type in something.
It was unclear, but it was something like 'The Ice King s????'.
The last 4 letters where a blur.

Should I tell princess Bubblegum or see how this all plays out?
I don't know.......
```

The password of `gunter` is incomplete `The Ice King s????`.

For that we can extract all passwords of 5 chars beginning with an s:

```
$ grep -E '^s\w{4}$' /usr/share/wordlists/passwords/rockyou.txt
```

And then bruteforce the SSH auth:

```
$ hydra -l gunter -P s____.txt ssh://adventuretime.thm
...
[22][ssh] host: adventuretime.thm   login: gunter   password: The Ice King sucks
```

Connect as gunter:

```
peppermint-butler@at:~$ su gunter
Password:
gunter@at:/home/peppermint-butler$ cd
gunter@at:~$ cat /home/gunter/flag4
tryhackme{edited}
```

## flag5

Let's find some SUID binaries for EoP:

```
gunter@at:/home$ find / -user root -perm -u=s 2>/dev/null
/usr/sbin/pppd
/usr/sbin/exim4
...
```

exim is not usual and is in an old version:

```
gunter@at:/home$ exim4 --version
Exim version 4.90_1 #4 built 14-Feb-2018 16:01:14
```

There is a local EoP for this version:

```
$ searchsploit -p 46996
  Exploit: Exim 4.87 - 4.91 - Local Privilege Escalation
      URL: https://www.exploit-db.com/exploits/46996
     Path: /usr/share/exploitdb/exploits/linux/local/46996.sh
File Type: Bourne-Again shell script, ASCII text executable, with CRLF line terminators
```

it's possible to find the interface used in the config file:

```
gunter@at:/etc/exim4$ grep interface /etc/exim4/update-exim4.conf.conf
dc_local_interfaces='127.0.0.1.60000'
```

The bash exploit has a very low quality, let's find one that is easier to adapt.
https://github.com/AzizMea/CVE-2019-10149-privilege-escalation/blob/master/wizard.py

Then change the port and execute it to get root.

```
gunter@at:/tmp$ python wizard.py

root@at:/root# cat /home/bubblegum/Secrets/bmo.txt



░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░▄██████████████████████▄░░░░
░░░░█░░░░░░░░░░░░░░░░░░░░░░█░░░░
░░░░█░▄██████████████████▄░█░░░░
░░░░█░█░░░░░░░░░░░░░░░░░░█░█░░░░
░░░░█░█░░░░░░░░░░░░░░░░░░█░█░░░░
░░░░█░█░░█░░░░░░░░░░░░█░░█░█░░░░
░░░░█░█░░░░░▄▄▄▄▄▄▄▄░░░░░█░█░░░░
░░░░█░█░░░░░▀▄░░░░▄▀░░░░░█░█░░░░
░░░░█░█░░░░░░░▀▀▀▀░░░░░░░█░█░░░░
░░░░█░█░░░░░░░░░░░░░░░░░░█░█░░░░
░█▌░█░▀██████████████████▀░█░▐█░
░█░░█░░░░░░░░░░░░░░░░░░░░░░█░░█░
░█░░█░████████████░░░░░██░░█░░█░
░█░░█░░░░░░░░░░░░░░░░░░░░░░█░░█░
░█░░█░░░░░░░░░░░░░░░▄░░░░░░█░░█░
░▀█▄█░░░▐█▌░░░░░░░▄███▄░██░█▄█▀░
░░░▀█░░█████░░░░░░░░░░░░░░░█▀░░░
░░░░█░░░▐█▌░░░░░░░░░▄██▄░░░█░░░░
░░░░█░░░░░░░░░░░░░░▐████▌░░█░░░░
░░░░█░▄▄▄░▄▄▄░░░░░░░▀██▀░░░█░░░░
░░░░█░░░░░░░░░░░░░░░░░░░░░░█░░░░
░░░░▀██████████████████████▀░░░░
░░░░░░░░██░░░░░░░░░░░░██░░░░░░░░
░░░░░░░░██░░░░░░░░░░░░██░░░░░░░░
░░░░░░░░██░░░░░░░░░░░░██░░░░░░░░
░░░░░░░░██░░░░░░░░░░░░██░░░░░░░░
░░░░░░░▐██░░░░░░░░░░░░██▌░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░


Secret project number: 211243A
Name opbject: BMO
Rol object: Spy

In case of emergency use resetcode: tryhackme{edited}


-------

Good job on getting this code!!!!
You solved all the puzzles and tried harder to the max.
If you liked this CTF, give a shout out to @n0w4n.
```

## Conclusion

I finished it only for the points. One of the worst challenge I played in my
life and the worst recently. Looks like the room is designed to be painful and
it's not even an april fool. I'm not surprised the room is that low rated.

- Do we learn something? No
- Is it useful in real-life? No
- Have we discovered new security attacks or tools? No
- Have we lost our time guessing? Yes
- Have we lost our time finding improbable/unrealistic stuff? Yes
- Have we enjoyed the challenge? No
