---
layout: post
title: "TakeOver - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - web
  - dns
date: 2023/04/15 20:45:00
thumbnail: /images/TryHackMe/takeover.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** TakeOver
- **Profile:** [tryhackme.com](https://tryhackme.com/room/takeover)
- **Difficulty:** Easy
- **Description**: This challenge revolves around subdomain enumeration.

![TakeOver](/images/TryHackMe/takeover.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        1,
        0,
        0,
        3,
        0
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Preparation

Put the custom domain in your hosts file.

```
$ grep futurevera.thm /etc/hosts
10.10.48.46 futurevera.thm
```

## Sub-domain enumeration

As you can learn in my [ffuf room](https://tryhackme.com/room/ffuf) (_Task 6 - Finding vhosts and subdomains_), it's possible to use `ffuf` to enumerate sub-domains.

```
➜ ffuf -u https://10.10.48.46 -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -H 'Host: FUZZ.futurevera.thm' -fs 4605

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v2.0.0-dev
________________________________________________

 :: Method           : GET
 :: URL              : https://10.10.48.46
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt
 :: Header           : Host: FUZZ.futurevera.thm
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
 :: Filter           : Response size: 4605
________________________________________________

[Status: 200, Size: 1522, Words: 367, Lines: 34, Duration: 28ms]
    * FUZZ: support

[Status: 200, Size: 3838, Words: 1326, Lines: 81, Duration: 28ms]
    * FUZZ: blog

:: Progress: [4989/4989] :: Job [1/1] :: 873 req/sec :: Duration: [0:00:08] :: Errors: 0 :
```

Two sub-domains are identified: `support` and `blog`.

Now we can add them in the hosts file.

```
$ grep futurevera.thm /etc/hosts
10.10.48.46 futurevera.thm support.futurevera.thm blog.futurevera.thm
```

## Cert viewing

The command to view remote SSL/TLS certification with `OpenSSL` is a bit overcomplicated and we don't need all the details but just to fetch the Alternative name where there can be potential subdomains. We can use the `ssl-cert` script of `nmap` to do that:

```
➜ nmap -p 443 --script ssl-cert blog.futurevera.thm
Starting Nmap 7.93 ( https://nmap.org ) at 2023-04-15 19:56 CEST
Nmap scan report for blog.futurevera.thm (10.10.48.46)
Host is up (0.027s latency).
rDNS record for 10.10.48.46: futurevera.thm

PORT    STATE SERVICE
443/tcp open  https
| ssl-cert: Subject: commonName=blog.futurevera.thm/organizationName=Futurevera/stateOrProvinceName=Oregon/countryName=US
| Issuer: commonName=blog.futurevera.thm/organizationName=Futurevera/stateOrProvinceName=Oregon/countryName=US
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-03-13T10:22:57
| Not valid after:  2023-03-13T10:22:57
| MD5:   8df0656c3814dd46c6ed5371e007d0e9
|_SHA-1: 6641a3bdc9f787f0bc84171abce4897b3711d28e

Nmap done: 1 IP address (1 host up) scanned in 0.35 seconds

➜ nmap -p 443 --script ssl-cert support.futurevera.thm
Starting Nmap 7.93 ( https://nmap.org ) at 2023-04-15 19:58 CEST
Nmap scan report for support.futurevera.thm (10.10.48.46)
Host is up (0.027s latency).
rDNS record for 10.10.48.46: futurevera.thm

PORT    STATE SERVICE
443/tcp open  https
| ssl-cert: Subject: commonName=support.futurevera.thm/organizationName=Futurevera/stateOrProvinceName=Oregon/countryName=US
| Subject Alternative Name: DNS:secrethelpdesk934752.support.futurevera.thm
| Issuer: commonName=support.futurevera.thm/organizationName=Futurevera/stateOrProvinceName=Oregon/countryName=US
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-03-13T14:26:24
| Not valid after:  2024-03-12T14:26:24
| MD5:   aef3dd042e6ae9196b68ac30c2d1177a
|_SHA-1: d62ec5cadbe8c933359faa67f0adf6e7e4fee395

Nmap done: 1 IP address (1 host up) scanned in 0.31 seconds
```

So in `support.futurevera.thm` certificate, there is a secret alt name: `secrethelpdesk934752.support.futurevera.thm`.

Again, we should add it to our hosts file.

```
$ grep futurevera.thm /etc/hosts
10.10.48.46 futurevera.thm support.futurevera.thm blog.futurevera.thm secrethelpdesk934752.support.futurevera.thm
```

## Grab the flag

Going to the secret domain, we are redirected to the flag.

```
$ curl http://secrethelpdesk934752.support.futurevera.thm -I 
HTTP/1.1 302 Found
Date: Sat, 15 Apr 2023 18:03:13 GMT
Server: Apache/2.4.41 (Ubuntu)
Location: http://flag{edited}.s3-website-us-west-3.amazonaws.com/
Content-Type: text/html; charset=UTF-8
```
