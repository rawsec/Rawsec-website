---
layout: post
title: "Bash Scripting - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - bash
date: 2021/02/06 15:42:00
thumbnail: /images/TryHackMe/bashscripting.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Bash Scripting
- **Profile:** [tryhackme.com](https://tryhackme.com/room/bashscripting)
- **Difficulty:** Easy
- **Description**: A Walkthrough room to teach you the basics of bash scripting

![Bash Scripting](/images/TryHackMe/bashscripting.png)

# Write-up

## Our first simple bash scripts

> What piece of code can we insert at the start of a line to comment out our code?

Answer: {% spoiler `#` %}

It's the same character as in most languages but if you don't know you can read
the room material.

> What will the following script output to the screen, echo "BishBashBosh"

Answer: {% spoiler `BishBashBosh` %}

`echo` just print the string passed as argument.

## Variables

> What would this code return?

Answer: {% spoiler `Jammy is 21 years old` %}

You can write the script or evaluate the variables in your brain.

> How would you print out the city to the screen?

Answer: {% spoiler `echo $city` %}

Simply echo the variable.

> How would you print out the country to the screen?

Answer: {% spoiler `echo $country` %}

Simply echo the variable.

## Parameters

> How can we get the number of arguments supplied to a script?

Answer: {% spoiler `$#` %}

Check the [cheatsheet][cheatsheet].

> How can we get the filename of our current script(aka our first argument)?

Answer: {% spoiler `$0` %}

Check the [cheatsheet][cheatsheet].

> How can we get the 4th argument supplied to the script?

Answer: {% spoiler `$4` %}

Check the course material.

> If a script asks us for input how can we direct our input into a variable called 'test' using "read"

Answer: {% spoiler `read test` %}

The answer is in the question.

> What will the output of "echo $1 $3" if the script was ran with "./script.sh hello hola aloha"

Answer: {% spoiler `hello aloha` %}

It will display 1st and 3rd args.

## Arrays

> What would be the command to print audi to the screen using indexing.

Answer: {% spoiler `echo "${cars[1]}"` %}

The second string is at index one.

> If we wanted to remove tesla from the array how would we do so?

Answer: {% spoiler `unset cars[3]` %}

Use unset to remove an item.

> How could we insert a new value called toyota to replace tesla?

Answer: {% spoiler `cars[3]="toyota"` %}

Set a string at index 3.

## Conditionals

> What is the flag to check if we have read access to a file?

Answer: {% spoiler `-r` %}

The first letter of read.

> What is the flag to check to see if it's a directory?

Answer: {% spoiler `-d` %}

The first letter of directory.

[cheatsheet]:https://devhints.io/bash

