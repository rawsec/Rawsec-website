---
layout: post
title: "Git Happens - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - web
  - git
date: 2023/04/10 20:51:00
thumbnail: /images/TryHackMe/githappens.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Git Happens
- **Profile:** [tryhackme.com](https://tryhackme.com/room/githappens)
- **Difficulty:** Easy
- **Description**: Boss wanted me to create a prototype, so here it is! We even used something called "version control" that made deploying this really easy!

![Git Happens](/images/TryHackMe/githappens.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap gittools
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        2,
        1,
        0,
        0,
        3,
        0
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Network enumeration

Nmap service and port enumeration scan:

```
# Nmap 7.93 scan initiated Mon Apr 10 20:25:51 2023 as: nmap -sSVC -T4 -p- -v --open --reason -oA nmap 10.10.47.113
Nmap scan report for 10.10.47.113
Host is up, received reset ttl 63 (0.083s latency).
Not shown: 65534 closed tcp ports (reset)
PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 nginx 1.14.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD
| http-git: 
|   10.10.47.113:80/.git/
|     Git repository found!
|_    Repository description: Unnamed repository; edit this file 'description' to name the...
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: Super Awesome Site!
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Apr 10 20:26:25 2023 -- 1 IP address (1 host up) scanned in 34.60 seconds
```

There is only a web application.

## Web discovery

The nmap script `http-git` already found there is a git repository exposed.

We can dump it with `gittools`.

```
$ gittools-gitdumper http://10.10.47.113/.git/ git-repo
$ cd git-repo
$ git restore .
```

Then we can check the history of modifications if there is something juicy.

```
$ git log -p
```

There are two interesting commits were the password hash of the admin:

```
$ git log -p d954a99b96ff11c37a558a5d93ce52d0f3702a7d
$ git log -p bc8054d9d95854d278359a432b6d97c27e24061d 
```

There are also two with the password in cleartext:

```
$ git log -p e56eaa8e29b589976f33d76bc58a0c4dfb9315b1
$ git log -p 395e087334d613d5e423cdf8f7be27196a360459
```

And that's all, the room was just about finding the password.
