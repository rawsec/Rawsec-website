---
layout: post
title: "Google Dorking - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - osint
date: 2020/11/14 23:42:00
updated: 2020/12/17 19:17:00
thumbnail: /images/TryHackMe/google_dorking.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Google Dorking
- **Profile:** [tryhackme.com](https://tryhackme.com/room/googledorking)
- **Difficulty:** Easy
- **Description**: Explaining how Search Engines work and leveraging them into finding hidden content!

![Google Dorking](/images/TryHackMe/google_dorking.png)

# Write-up

**Disclaimer**: answer are very easy, obvious or given the in room description
so it doesn't require details.

## Let's Learn About Crawlers

> Name the key term of what a "Crawler" is used to do

Answer: {% spoiler index %}

> What is the name of the technique that "Search Engines" use to retrieve this information about websites?

Answer: {% spoiler crawling %}

> What is an example of the type of contents that could be gathered from a website?

Answer: {% spoiler keywords %}

## Enter: Search Engine Optimisation

> Using the SEO Site Checkup tool on "tryhackme.com", does TryHackMe pass the "Meta Title Test" (Yea / Nay)

Answer: {% spoiler yea %}

> How many pages use "flash"

Answer: {% spoiler 0 %}

> From a "rating score" perspective alone, what website would list first out of the following two?

Answer: {% spoiler googledorking.cmnatic.co.uk %}

## Beepboop - Robots.txt

> Where would "robots.txt" be located on the domain "ablog.com"

Answer: {% spoiler `ablog.com/robots.txt` %}

> If a website was to have a sitemap, where would that be located?

Answer: {% spoiler `/sitemap.xml` %}

> How would we only allow "Bingbot" to index the website?

Answer: {% spoiler `User-agent: "Bingbot"` %}

> How would we prevent a "Crawler" from indexing the directory "/dont-index-me/"?

Answer: {% spoiler `Diasallow: /dont-index-me/` %}

> What is the extension of a Unix/Linux system configuration file that we might want to hide from "Crawlers"?

Answer: {% spoiler `.conf` %}

## Sitemaps

> What is the typical file structure of a "Sitemap"?

Answer: {% spoiler `xml` %}

> What real life example can "Sitemaps" be compared to?

Answer: {% spoiler `map` %}

> Name the keyword for the path taken for content on a website

Answer: {% spoiler `route` %}

## What is Google Dorking?

> What would be the format used to query the site bbc.co.uk about flood defences

Answer: {% spoiler `site:bbc.co.uk flood defences` %}

> What term would you use to search by file type?

Answer: {% spoiler `filetype:` %}

> What term can we use to look for login pages?

Answer: {% spoiler `intitle: login` %}
