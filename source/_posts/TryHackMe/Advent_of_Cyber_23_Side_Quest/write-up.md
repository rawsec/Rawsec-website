---
layout: post
title: "Advent of Cyber '23 Side Quest - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
date: 2024/01/07 01:44:00
thumbnail: /images/TryHackMe/adventofcyber23sidequest.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Advent of Cyber '23 Side Quest
- **Profile:** [tryhackme.com](https://tryhackme.com/room/adventofcyber23sidequest)
- **Difficulty:** Info
- **Description**: Explore a series of advanced challenges alongside the core Advent of Cyber event!

![Advent of Cyber '23 Side Quest](/images/TryHackMe/adventofcyber23sidequest.png)

This is the steps to find the key for entering the [Advent of Cyber '23 Side Quests](https://tryhackme.com/room/adventofcyber23sidequest) (advanced bonus challenges alongside [Advent of Cyber 2023](https://tryhackme.com/room/adventofcyber2023)).

# Write-up

## Introduction

> ## What Is the Advent of Cyber Side Quest?
>
> The Advent of Cyber 2023 is an annual event hosted by TryHackMe in December. It's like an advent calendar but with exciting security challenges instead of chocolates! If you are unfamiliar with the event but you want to participate, join [this room](https://tryhackme.com/room/adventofcyber2023). While Advent of Cyber is a fully guided event accessible to users of all skill levels in cyber security, we wanted to prepare something extra for more advanced participants. That's how the Side Quest came to be!
>
> The Advent of Cyber 2023 Side Quest is a series of four connected challenges in which you'll play as the Bandit Yeti. Your goal will be to get a foothold into the Best Festival Company. These challenges will have no additional guidance and will range between "Hard" and "Insane" difficulty levels. Please note that completing the Side Quest is entirely optional; you don't need to do it to enjoy the main Advent of Cyber 2023 event.
>
> ## How To Find Side Quest Challenges
>
> The challenges are located in secret, unpublished rooms, and to find them, you need to search for keys in the main Advent of Cyber event, the event room, and some of our most recent social media posts. The keys are QR codes, leading you to secret challenge rooms.
>
> Getting the keys will not be easy: you will have to dig around and even solve some puzzles to find them. We will leave some breadcrumbs to get you started, but you're on your own after that!

## First Side Quest Challenge Key

> This key is divided into four parts. Three will be posted on our social media channels between Tuesday, 28th November and Thursday, 30th November. The final part of the key will be posted in this room, on December 1st, 4 pm GMT. Find them all, put them together, and uncover the link to the first secret challenge! All the links to our social media channels can be found in Task 3 of the main [Advent of Cyber](https://tryhackme.com/room/adventofcyber2023) room.

The four pieces of QR code grabbed on the social medias:

- **Piece 1** - **TryHackMe** [Advent of Cyber '23 Side Quest](https://tryhackme.com/room/adventofcyber23sidequest) room - Task 5 - https://assets.tryhackme.com/additional/aoc2023/6d156/50af2.png
- **Piece 2** - **Discord** - https://discord.com/channels/521382216299839518/1176552309707264041/1179095411420577943 - https://assets.tryhackme.com/additional/aoc2023/b3620/e94fa.png
- **Piece 3** - **Linkedin** - https://www.linkedin.com/posts/tryhackme_can-you-help-elf-mcskidy-and-her-team-tackle-activity-7135598321280188416-5wnQ - https://hubs.la/Q02bklp30 (https://assets.tryhackme.com/additional/aoc2023/5d60a/809cd.png)
- **Piece 4** - **Twitter** - https://twitter.com/RealTryHackMe/status/1730184898365767880 - https://hubs.la/Q02btlld0 (https://assets.tryhackme.com/additional/aoc2023/2f7f8/0f93a.png)

So assembling the pieces we get this message:

> Well done! You found all of the clues. The secret code is:
>
> https://tryhackme.com/jr/adv3nt0fdbopsjcap

The hardest piece to find was the n°2 because it was not link in an announcement on discord or something but randomly paste in the channel `#aoc-2023-side-quest`. It was pinned in the channel, but I didn't think about that, I found it by searching for `https://assets.tryhackme.com/additional/aoc2023/` since the other links had all the same base.

## Side Quest Challenge 1 - The Return of the Yeti

[The Return of the Yeti - Write-up - TryHackMe](https://blog.raw.pm/en/TryHackMe-The-Return-of-the-Yeti-write-up/)

## Second Side Quest Challenge Key

> This key will be hidden in one of the challenges of the main Advent of Cyber event between Day 2 and Day 8. Look for clues to find out which challenge to dig into!

On day 6, there is this last "question" giving a hint:

> Van Jolly still thinks the Ghost of Christmas Past is in the game. She says she has seen it with her own eyes! She thinks the Ghost is hiding in a glitch, whatever that means. What could she have seen?

If we take a look at the source code `view-source:http://10-10-141-183.p.thmlabs.com/`, we can see a JS script is sourced: `<script src="index.js"></script>`.

http://10-10-141-183.p.thmlabs.com/index.js (I won't paste it here, it's huge)

The script is minified so let's prettify it with whatever you want (I used VSCode plugin [prettier](https://github.com/prettier/prettier-vscode.git)).

We can see the script is loading additional data:

- `index.data.gz`
- `index.wasm.gz`

But we could see that in the network tab of the dev console as well without opening index.js.

We can try to extract the data.

```
➜ 7z x index.data.gz
➜ file index.data
index.data: TrueType Font data, digitally signed, 25 tables, 1st "DSIG", 58 names, Unicode, \251 2017 The Monotype Corporation. All Rights Reserved.
```

A virtual file system is created from the data file:

```js
Module["FS_createPath"]("/", "assets", true, true);
```

Then, there is a function mapping which file is at which index in the blob.

```js
  loadPackage({
    files: [
      {
        filename: "/assets/arial.ttf",
        start: 0,
        end: 1027192,
      },
      …
      {
        filename: "/assets/qr.map",
        start: 1656484,
        end: 1689911,
      },
      …
    remote_package_size: 1724107,
  });
```

We could have used `binwalk index.data`, but that would have been a black box approach. By reading the code we can go straight to the point.

As we are looking for a QR code, so `qr.map` looks nice to us.

We can extract the qr.map file with dd:

```
➜ dd if=index.data of=qr.map bs=1 skip=1656484 count=$(echo '1689911 - 1656484' | bc)
33427+0 records in
33427+0 records out
33427 bytes (33 kB, 33 KiB) copied, 0.190099 s, 176 kB/s
```

It's a text file full of `-1`, `50`, `30`, `0`.

```
➜ cat qr.map | tr ' ' "\n" | sort -u

-1
0
30
50
```

The image will be 79x144, (number of item per lines and number of lines) or maybe two images of 79x72 (because there is a blank line).

```ruby
irb(main):009:0> "-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0".split(' ').size
```

Let's put that back to a PNG with [PixelChart](https://github.com/noraj/PixelChart).

```
➜ pikaur -S pixelchart
```

The idea is that 0 and -1 are garbage, that 50 is a black pixel and 30 a white pixel. I achieve that with a quick Ruby script:

```ruby
require 'pixelchart' # https://noraj.github.io/PixelChart/

# Parse each line, remove trailing CRLF, split value on space, convert string to integer
data = File.read('qr.map').each_line(chomp: true).map { |l| l.split(' ').map(&:to_i) }
# 0 is black, -1, 50 or 30 will be 1 to be white
data = data.each do |row|
  row.map! { |x| x == 50 ? true : x } # 50 -> true
  row.map! { |x| x == 30 ? false : x } # 30 -> false
  row.map! { |x| x.is_a?(Integer) ? nil : x } # 0, -1 -> nil
  row.reject! {|x| x.nil?}
end
# remove empty rows
data.reject! { |x| x.empty? }
# sizes
w = data.first.size
h = data.size
# 2D -> 1D
data.flatten!
# Array to image
options = {
  colors: [[0,0,0], [255,255,255]],
  scale: 5
}
im = PixelChart.new(data, w, h, options) # 31 x 31
im.draw('qr.png')
```

Or for those who prefer CLI-fu one-liners:

```bash
# Same as the ruby script but bash style
sed 's/50/false/g' qr.map | sed 's/30/true/g' | dos2unix | awk NF | sed -r 's/[^true|false| ]//g' | dos2unix| awk NF | cut -d ' ' -f '49-' | tr ' ' ',' | tr "\n" ',' > qr.csv
# Calc the canva size
➜ pixelchart calc qr.csv
Possible dimensions: width x height or height x width
1 x 961
31 x 31
# 1D CSV to image
pixelchart draw qr.csv qr.png -w 31 -h 31 -s 5

# So we can one-line as
pixelchart draw =(sed 's/50/false/g' qr.map | sed 's/30/true/g' | dos2unix | awk NF | sed -r 's/[^true|false| ]//g' | dos2unix| awk NF | cut -d ' ' -f '49-' | tr ' ' ',' | tr "\n" ',') qr.png -w 31 -h 31 -s 5
```

Then I used [QtQr](https://archlinux.org/packages/extra/any/qtqr/) to read the QR code from the file: https://tryhackme.com/room/armageddon2r

I think there was another way to solve this by doing some reverse engineering on the WASM binary to find a backdoor in the game.

## Side Quest Challenge 2 - Snowy ARMageddon

[Snowy ARMageddon - Write-up - TryHackMe](https://blog.raw.pm/en/TryHackMe-Snowy-ARMageddon-write-up/)

## Third Side Quest Challenge Key

> This key will be hidden in one of the challenges of the main Advent of Cyber event between Day 9 and Day 16. Look for clues to find out which challenge to dig into!

On day 11, there is this last "question" giving a hint:

> Van Sprinkles left some stuff around the DC. It's like a secret message waiting to be unravelled!

That's not really in the AD, but where the flag is stored, there are extra chat logs.

```
*Evil-WinRM* PS C:\Users\Administrator\Desktop> ls


    Directory: C:\Users\Administrator\Desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----       11/22/2023  10:56 AM                chatlog_files
-a----       11/22/2023  10:29 AM          11620 chatlog.html
-a----       10/16/2023   7:33 AM             17 flag.txt
```

We can easily download them within our `evil-winrm` session.

```
*Evil-WinRM* PS C:\Users\Administrator\Desktop> download "C:/Users/Administrator/Desktop/chatlog.html" /tmp/chatlog.html

Info: Downloading C:/Users/Administrator/Desktop/chatlog.html to /tmp/chatlog.html

Info: Download successful!

*Evil-WinRM* PS C:\Users\Administrator\Desktop> download "C:/Users/Administrator/Desktop/chatlog.html" /tmp/chatlog.html

Info: Downloading C:/Users/Administrator/Desktop/chatlog.html to /tmp/chatlog.html

Info: Download successful!
```

In the conversation, there was a screenshot leaking a key, but it was deleted, however there was a new cropped screenshot made with Microsoft Windows Snip & Sketch tool.

We can see extra data is appended to the file.

```
➜ exiftool Screenshot\ 2023-11-22\ 034941.png
ExifTool Version Number         : 12.70
File Name                       : Screenshot 2023-11-22 034941.png
Directory                       : .
File Size                       : 882 kB
File Modification Date/Time     : 2023:12:19 12:54:10+01:00
File Access Date/Time           : 2023:12:19 13:11:12+01:00
File Inode Change Date/Time     : 2023:12:19 12:54:10+01:00
File Permissions                : -rw-r--r--
File Type                       : PNG
File Type Extension             : png
MIME Type                       : image/png
Image Width                     : 844
Image Height                    : 452
Bit Depth                       : 8
Color Type                      : RGB with Alpha
Compression                     : Deflate/Inflate
Filter                          : Adaptive
Interlace                       : Noninterlaced
SRGB Rendering                  : Perceptual
Gamma                           : 2.2
Pixels Per Unit X               : 3779
Pixels Per Unit Y               : 3779
Pixel Units                     : meters
Warning                         : [minor] Trailer data after PNG IEND chunk
Image Size                      : 844x452
Megapixels                      : 0.381

➜ binwalk Screenshot\ 2023-11-22\ 034941.png

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 844 x 452, 8-bit/color RGBA, non-interlaced
91            0x5B            Zlib compressed data, compressed
```

Indeed, _Snip & Sketch_ is vulnerable to _acropalypse_ vulnerability.

See references:

- [The Verge - Oops, Windows’ screenshot tool may be saving stuff you cropped out, too](https://www.theverge.com/2023/3/21/23650657/windows-snipping-tool-crop-screenshots-vulnerability)
- [Tenable - Windows Snip & Sketch/ Snipping Tool CVE-2023-28303 (Acropalypse)](https://www.tenable.com/plugins/nessus/177217)
- [MSRC - Windows Snipping Tool Information Disclosure Vulnerability CVE-2023-28303](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2023-28303)
- [CVE.org - CVE-2023-28303](https://www.cve.org/CVERecord?id=CVE-2023-28303)
- [NVD - CVE-2023-28303](https://nvd.nist.gov/vuln/detail/CVE-2023-28303)
- [Acropalypse Restoration and Detection Multi-Tool](https://github.com/frankthetank-music/Acropalypse-Multi-Tool)

Let's recover this with the GitHub tool mentioned above.

```
➜ git clone https://github.com/frankthetank-music/Acropalypse-Multi-Tool
➜ cd Acropalypse-Multi-Tool
➜ python -m venv venv
➜ source venv/bin/activate
➜ pip install -r requirements.txt
➜ python gui.py
```

As we can see on the other full screen screenshot, the original resolution is 2560x1080.

```
➜ exiftool Screenshot\ 2023-11-22\ 034711.png
…
Image Width                     : 2560
Image Height                    : 1080
…
```

We can then select the `Custom RGBA` with the custom dimensions to recover the uncropped image.

There is a QR code on the original image.

Then I used [QtQr](https://archlinux.org/packages/extra/any/qtqr/) to read the QR code from the file: https://tryhackme.com/jr/busyvimfrosteau.

## Side Quest Challenge 3 - Frosteau Busy with Vim

[Frosteau Busy with Vim - Write-up - TryHackMe](https://blog.raw.pm/en/TryHackMe-Frosteau-Busy-with-Vim-write-up/)

## Fourth Side Quest Challenge Key

> This key will be hidden in one of the challenges of the main Advent of Cyber event between Day 17 and Day 24. Look for clues to find out which challenge to dig into!

On day 20, there is this last "question" giving a hint:

> Detective Frosteau believes it was an account takeover based on the activity. However, Tracy might have left some crumbs.

Once the original website is restored, the web app is available on port 9080 : http://10.10.235.255:9080/

On the day 20 of the calendar there is a QR code with the following content: https://tryhackme.com/jr/surfingyetiiscomingtotown

That's it.

## Side Quest Challenge 4 - The Bandit Surfer

[The Bandit Surfer - Write-up - TryHackMe](https://blog.raw.pm/en/TryHackMe-The-Bandit-Surfer-write-up/)
