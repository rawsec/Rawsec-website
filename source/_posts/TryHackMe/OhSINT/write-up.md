---
layout: post
title: "OhSINT - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - osint
  - thm
date: 2020/08/30
thumbnail: /images/TryHackMe/OhSINT.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** OhSINT
- **Profile:** [tryhackme.com](https://tryhackme.com/room/ohsint)
- **Difficulty:** Easy
- **OS:** None
- **Description**: Are you able to use open source intelligence to solve this challenge?

![ohsint](/images/TryHackMe/OhSINT.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pacman -S perl-image-exiftool
```

## [Task 1] OhSINT

### #1

> What is this users avatar of?

Answer: {% spoiler cat %}

With [ExifTool][exiftool] we can list metadata of the image.
We can find GPS coordinates, the author of the image, etc.

```
$ exiftool WindowsXP.jpg
ExifTool Version Number         : 12.00
File Name                       : WindowsXP.jpg
Directory                       : .
File Size                       : 229 kB
File Modification Date/Time     : 2020:08:21 22:52:59+02:00
File Access Date/Time           : 2020:08:21 22:53:31+02:00
File Inode Change Date/Time     : 2020:08:21 22:52:59+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
XMP Toolkit                     : Image::ExifTool 11.27
GPS Latitude                    : 54 deg 17' 41.27" N
GPS Longitude                   : 2 deg 15' 1.33" W
Copyright                       : OWoodflint
Image Width                     : 1920
Image Height                    : 1080
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 1920x1080
Megapixels                      : 2.1
GPS Latitude Ref                : North
GPS Longitude Ref               : West
GPS Position                    : 54 deg 17' 41.27" N, 2 deg 15' 1.33" W
```

The author pseudo is `OWoodflint`.

We can find several resources that he may own:

- Blog: https://oliverwoodflint.wordpress.com/
- Github: https://github.com/OWoodfl1nt
- Twitter: https://twitter.com/owoodflint

On tweeter we can see the profile picture of OWoodflint.

![](https://i.imgur.com/YWPgF7I.png)

### #2

> What city is this person in?

Answer: {% spoiler London %}

https://twitter.com/OWoodflint/status/1102220421091463168

> From my house I can get free wifi ;D
>
> Bssid: B4:5D:50:AA:86:41 - Go nuts!

He twitted it's WIFI BSSID, we can try to find the location from this.

`mylnikov.org` is a solution but didn't find a result.

```
https://api.mylnikov.org/geolocation/wifi?v=1.1&data=open&bssid=B4:5D:50:AA:86:41
```

A more famous solution is to use `wigle.net` but this require to register.

```
https://wigle.net/search?netid=B4%3A5D%3A50%3AAA%3A86%3A41
```

By opening the map we can see it's the location.

PS: We can use the GPS coordinates found earlier in the metadata of the image.

```
GPS Latitude                    : 54 deg 17' 41.27" N
GPS Longitude                   : 2 deg 15' 1.33" W
GPS Latitude Ref                : North
GPS Longitude Ref               : West
GPS Position                    : 54 deg 17' 41.27" N, 2 deg 15' 1.33" W
```

But that gave me a very weird GPS position.

### #3

> Whats the SSID of the WAP he connected to?

Answer: {% spoiler UnileverWiFi %}

We can read the SSID in wigle too.

### #4

> What is his personal email address?

Answer: {% spoiler OWoodflint@gmail.com %}

The email address is written in one of the github repository he owns:
https://github.com/OWoodfl1nt/people_finder

### #5

> What site did you find his email address on?

Answer: {% spoiler Github %}

### #6

> Where has he gone on holiday?

Answer: {% spoiler New York %}

On https://oliverwoodflint.wordpress.com/

> Im in New York right now, so I will update this site right away with new photos!

### #7

> What is this persons password?

Answer: {% spoiler pennYDr0pper.! %}

By selecting all the text you can see a password is written in white on white.

![](https://i.imgur.com/995LmqP.png)

[exiftool]:https://inventory.rawsec.ml/tools.html#ExifTool
