---
layout: post
title: "The Hacker Methodology - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - pentest
date: 2021/01/14 23:01:00
thumbnail: /images/TryHackMe/hackermethodology.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** The Hacker Methodology
- **Profile:** [tryhackme.com](https://tryhackme.com/room/hackermethodology)
- **Difficulty:** Easy
- **Description**: Introduction to the Hacker Methodology

![The Hacker Methodology](/images/TryHackMe/hackermethodology.jpeg)

# Write-up

## Methodology Outline

> What is the first phase of the Hacker Methodology?

Answer: {% spoiler Reconnaissance %}

Just read the course material.

## Reconnaissance Overview

> Who is the CEO of SpaceX?

Answer: {% spoiler Elon Musk %}

Check on [Wikipédia](https://fr.wikipedia.org/wiki/SpaceX).

> Do some research into the tool: sublist3r, what does it list?

Answer: {% spoiler subdomains %}

Check the [README](https://github.com/aboul3la/Sublist3r).

> What is it called when you use Google to look for specific vulnerabilities or to research a specific topic of interest?

Answer: {% spoiler Google Dorking %}

Read the course material.

## Enumeration and Scanning Overview

> What does enumeration help to determine about the target?

Answer: {% spoiler attack surface %}

Read the course material.

> Do some reconnaissance about the tool: Metasploit, what company developed it?

Answer: {% spoiler rapid7 %}

Search it on a search engine.

> What company developed the technology behind the tool Burp Suite?

Answer: {% spoiler portswigger %}

Search it on a search engine.

## Exploitation

> What is one of the primary exploitation tools that pentester(s) use?

Answer: {% spoiler metasploit %}

Read the course material.

## Privilege Escalation

> In Windows what is usually the other target account besides Administrator?

Answer: {% spoiler system %}

Read the course material.

> What thing related to SSH could allow you to login to another machine (even without knowing the username or password)?

Answer: {% spoiler keys %}

Read the course material.

## Reporting

> What would be the type of reporting that involves a full documentation of all findings within a formal document?

Answer: {% spoiler full formal report %}

Read the course material.

> What is the other thing that a pentester should provide in a report beyond: the finding name, the finding description, the finding criticality

Answer: {% spoiler Remediation Recommendation %}

Read the course material.
