---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E01 - Rattachage DNS en une fraction de seconde"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - dns
date: 2023/12/21 16:07:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 1

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/0jcmYD16ri2OQuuO6XwJsQ)
- [Deezer](https://www.deezer.com/fr/episode/586982552)
- [Youtube](https://youtu.be/L5fp5cehqJ8)
- [Youtube Music](https://music.youtube.com/watch?v=L5fp5cehqJ8)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/20c4e442-4049-4f5c-810b-7fa68b9ec47a/service-hacktion-s01e01---rattachage-dns-en-une-fraction-de-seconde)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/ZGZiNGNkMTctNDZkMS00MDk0LWJmNDctNjgzM2E4ODU2NDY4)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/service-hacktion/id1725734663?i=1000641633637)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=17532274392)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e01-rattachage-dns-en-une-fraction-de-seconde)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e01-rattachage-dns-en-une-f-195562708)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e01-rattachage-dns-en-une-fraction-de-seconde/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e01-rattachage-dns-en-une-fraction-de-seconde)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-dfb4cd17-46d1-4094-bf47-6833a8856468/s01e01-rattachage-dns-en-une-fraction-de-seconde/)
- [Spreaker](https://www.spreaker.com/episode/s01e01-rattachage-dns-en-une-fraction-de-seconde--58700365)

## Introduction

Balado (_podcast_)

- Revue d'actualité
- Creuser plus en profondeur un sujet
- Entrevues (_Interview_)
- etc.

## Le nom

_Service Hacktion_, pourquoi un tel nom ?

- Je voulais **hack** dans le nom
- En référence à la DGSE
  - "Le service Action (SA) est une unité militaire secrète française placée sous le commandement opérationnel de la direction des opérations (DO) de la direction générale de la Sécurité extérieure (DGSE)."
- Les classiques _hacktualité_ ou _hackademie_ ont déjà été faits 40 fois
- Je ne voulais pas reprendre tous les jeux de mots qui ont déjà été faits pour les conférences de sécurité ou les noms d'équipe de CTF

## Le sujet - Rattachage DNS

⚠️ Niveau avancé, sujet complexe ⚠️

- [BlackHat Europe 2023 - New Techniques for Split-Second DNS Rebinding](https://blackhat.com/eu-23/briefings/schedule/index.html#new-techniques-for-split-second-dns-rebinding-35619article)

- 🇬🇧 _New Techniques for Split-Second DNS Rebinding_
  - 🇫🇷 Nouvelles techniques pour le rattachage DNS en une fraction de seconde

**Daniel Thatcher** ([@_danielthatcher](https://twitter.com/_danielthatcher)) qui travaille pour l'entreprise [**Intruder**](https://www.intruder.io/).

C'est un chercheur en sécurité et un ancien ingénieur en test d'intrusion. Il partage son temps entre la recherche de nouvelles techniques de piratage d'applications web et le développement de produits (scanneur de vulnérabilité).

## Article 1

- [Intruder - We Hacked Ourselves With DNS Rebinding](https://www.intruder.io/research/we-hacked-ourselves-with-dns-rebinding)

- 🇬🇧 _We Hacked Ourselves With DNS Rebinding_
  - 🇫🇷 Nous nous sommes piratés nous-mêmes avec le rattachage DNS

### Contexte

- Ils ont des serveurs de prise de captures d'écran des sites web de leurs clients (gowitness)
- Ils ont fait des TI sur les plateformes
- gowitness suit les redirections HTTP pour prendre les captures d'écran
- gowitness hébergé sur AWS, machine virtuelle EC2
- gowitness avait accès au service de métadonnées interne à l'instance EC2 et donc avait la possibilité de récupéré les authentifiants AWS des différents rôles disponibles sur l'instance
- Pour exploiter cela, il suffit de créer un site web qui redirige vers `http://169.254.169.254/latest/meta-data/iam/security-credentials/`
- Remédiations :
  - Restriction réseau pour l'accès au service de métadonnées
  - Utilisation d'[IMDSv2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/configuring-instance-metadata-service.html) (_Instance Metadata Service Version 2_)
    - Protection contre les SSRF (voir [AWS Security Blog - Add defense in depth against open firewalls, reverse proxies, and SSRF vulnerabilities with enhancements to the EC2 Instance Metadata Service](https://aws.amazon.com/blogs/security/defense-in-depth-open-firewalls-reverse-proxies-ssrf-vulnerabilities-ec2-instance-metadata-service/))
    - Méthode `PUT` pour récupérer un jeton de session, puis l'utiliser dans l'en-tête `X-aws-ec2-metadata-token`
    - Accès au service de métadonnées impossible, car la SSRF peut faire un `GET` mais pas un `PUT` ou définir un en-tête

### Rappels : rattachage DNS

- Manipulation de nom de domaine (NDD)
- À ne pas confondre avec l'usurpation de nom DNS (_DNS spoofing_) souvent utiliser pour contourner les restrictions de domaine (ex : hôte local)
  - Exemple : dans la limite, affichage flux RSS, SSRF
- Depuis JavaScript impossible d'attaquer des machines sur le réseau autre que la machine qui héberge le script grâce à la SOP (Same Origin Policy)
- TTL DNS très court pour empêcher la mise en cache
- 1ière résolution : adresse IP du serveur hébergeant le script
- Quand le script exécuté, une nouvelle requête DNS vers le NDD est faite
- 2ième résolution : adresse IP interne ou ailleurs sur le réseau
- Valide la SOP donc peut lire la réponse
- Prérequis :
  - avec navigateur piloté
  - le serveur le valide pas l'en-tête `Host`
  - HTTP (pas HTTPS)
- Exemple : hors bande, API, SSRF

### Le rattachage DNS permet de contourner l'IMDSv2 d'AWS

- Exemple d'implémentation de l'exploitation et de l'exfiltration sur ZAP Proxy (Firefox piloté) (au lieu de gowitness (Chrome piloté))
  - Pour l'exfiltration, on doit attendre que la réponse DNS expire afin de pouvoir requêter vers l'IP publique
- ZAP Proxy au lieu de gowitness à cause des faibles délais d'expiration et de la restriction contre les requêtes vers les réseaux privés (voir article 2)

## Article 2

- [Intruder - Tricks for Reliable Split-Second DNS Rebinding in Chrome and Safari](https://www.intruder.io/research/split-second-dns-rebinding-in-chrome-and-safari)

- 🇬🇧 _Tricks for Reliable Split-Second DNS Rebinding in Chrome and Safari_
  - 🇫🇷 Astuces pour un rattachage DNS fiable en une fraction de seconde dans Chrome et Safari

### Intro

- Quand IPv6 est disponible
- Technique pour contourner la restriction sur les réseaux locaux appliquée à `fetch()` dans Chromium

### Caches lents

- Technique classique d'inondation DNS : vider le cache du navigateur - en générant un grand nombre de recherches DNS pour remplir l'espace disponible dans le cache et en faisant en sorte que les entrées plus anciennes soient écartées avant d'avoir expiré - afin que le navigateur effectue une deuxième recherche du même nom d'hôte plus tôt.
- Inconvénients de l'inondation DNS :
  - ~ 10 sec
  - Caches intermédiaires plus difficiles à vider que le cache du navigateur, ex : ~ 5 minutes pour `systemd-resolved`, ~ 30 minutes pour certains VPN
  - Difficile de forcer l'utilisateur à garder la page ouverte longtemps
- Autre technique : multiples enregistrements `A` dans la même réponse pour le même domaine
  - Présentée à la BlackHat USA 2010 par Craig Heffner
  - Exploitée par Gerald Doussot and Roger Meyer dans [Github - nccgroup/singularity](https://github.com/nccgroup/singularity) en 2019
  - Les 2 enregistrements sont : une adresse IP publique contrôlée par l'attaquant et une adresse IP du serveur cible
  - Aléatoire, car ne fonctionne que si le navigateur communique avec le serveur public en premier
  - Le serveur web de l'attaquant doit bloquer le trafic provenant du navigateur de la victime, ce qui a pour effet de ramener le navigateur à envoyer toutes les futures demandes au serveur cible. Dans ce cas, JavaScript dans la page de l'attaquant pourra envoyer des requêtes à l'adresse IP cible sous la même origine
  - Contourne le problème de cache car 1 seule requête DNS mais fonctionne rarement en pratique car la majorité des navigateurs essayent de contacter l'adresse privée en premier

Dans Wireshark, l'auteur a remarqué que lors du chargement de sites web dans les navigateurs modernes, une requête A et une requête AAAA sont toutes deux envoyées, voyons voir ça ⬇️

### Attaquer Safari : Retarder les réponses DNS

- Si accès à internet via IPv6 et utilisation de Safari
  - Safari envoi req. DNS A (IPv4) et AAAA (IPv6)
  - Safari priorise les adresses IP privées sur les publiques quand il y en a plusieurs
- Si la réponse A ou AAAA est retardée
  - Safari n'attend pas de recevoir toutes les réponses
  - Safari envoi des req. HTTP dès la 1ière réponse DNS
  - Quand la réponse DNS différée est reçue, les adresses IP de cette réponse sont ajoutées au lot d'adresses IP que Safari peut utiliser pour les futures requêtes vers ce domaine
  - Donc si la première réponse reçue est publique que celle délayée est privée, Safari enverra la 1ière req. HTTP vers l'adresse IP publique jusqu'à ce que la réponse DNS délayé soit reçue, à ce moment-là, il enverra les req. vers l'IP privée
  - Pour l'exfiltration, on doit attendre que la réponse DNS expire afin de pouvoir requêter vers l'IP publique

Cf. scénario schéma

### Attaquer Chrome : Utiliser la priorisation AAAA

- Comme Safari, Chrome priorise les adresses IP privées sur celles publiques
- Mais Chrome priorise aussi les adresses IPv6 sur les IPv4 (plus de poids)
- Priorité (+ haute vers + faible) :
  - IPv6 privée
  - IPv6 publique 💡
  - IPv4 privée 💡
  - IPv4 publique
- IPv6 publique avant IPv4 privée
- S'il y a une réinitialisation, de connexion, Chrome essaye une autre IP

Cf. scénario schéma

- Mais Chrome bloque !
  - Contexte non sécurisé (HTTP) et accès une adresse IP privée
  - Implémentation partielle de PNA ([Private Network Access](https://wicg.github.io/private-network-access/)) (brouillon pour un standard W3C)
    - 🇫🇷 ARP (Accès aux Réseaux Privés)

### Contournement de l'Accès aux Réseaux Privés

- Les protections PNA empêchent les pages chargées par HTTP à partir de l'internet public d'adresser des requêtes aux réseaux privés
- Dans Chrome, ces protections sont mises en œuvre pour les requêtes `fetch()`, mais pas encore pour les `iFrames`
- On garde tout pareil sauf qu'on essaye d'accéder au secret via une iFrame au lieu d'un req. `fetch()`
- La page dans l'iFrame a la même origine que la page racine, donc la page racine peut contrôler la DOM de la page dans l'iFrame
- La page racine peut donc injecter un script qui fait une req. `fetch()` dans l'iFrame pour accéder à la cible et exfiltrer la donnée

Cf. scénario schéma

- Astuce : si la cible est un navigateur piloté, introduire du délai de chargement pour l'iFrame pour que le navigateur considère que la page n'est pas complètement chargée jusqu'à ce que l'iFrame ait fini de charger afin de s'assurer que l'attaque ait le temps de s'exécuter
