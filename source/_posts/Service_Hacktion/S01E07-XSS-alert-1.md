---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E07 - Pourquoi il ne faut pas utiliser alert(1) pour les XSS"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - xss
date: 2024/02/20 23:20:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 7

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/2U4JBwqmarqnhdQewOtFdF)
- [Deezer](https://www.deezer.com/fr/episode/606596682)
- [Youtube](https://youtu.be/35oBwOwLu9Q)
- [Youtube Music](https://music.youtube.com/watch?v=35oBwOwLu9Q)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/e543c867-a65d-443b-9ae7-99a1e45c2437/service-hacktion-s01e07---pourquoi-il-ne-faut-pas-utiliser-alert-1-pour-les-xss)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/NjFiNTY0ZTUtNGM0Yy00YjY3LWJkYjEtNTkzZmRhNTY0MTNh)
- [Apple Podcast](https://podcasts.apple.com/fr/podcast/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-pour-les-xss/id1725734663?i=1000646429520)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=19454098140)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-pour-les-xss)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e07-pourquoi-il-ne-faut-pas-201528024)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-po/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-po)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-61b564e5-4c4c-4b67-bdb1-593fda56413a/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-pour-les-xss/)
- [Spreaker](https://www.spreaker.com/episode/s01e07-pourquoi-il-ne-faut-pas-utiliser-alert-1-pour-les-xss--58894164)

## Notes

Avantages `alert()` :

- Facilement visible

Pas le `1` :

- Ça ne sert à rien
- On ne sait pas d'où ça vient, si on a un formulaire avec 10 champs et qu'on a 5 alertes avec 1 qui s'affichent, quels étaient les champs vulnérables ?
    - Plutôt utiliser le nom de champ d'origine et la page
    - Une chaine de caractère unique pour facilement chercher
- Dans le cas d'un domaine bac à sable, on ne sait pas de quel domaine provient la XSS. Iframe bac à sable ?
    - Plutôt utiliser `document.domain` ou `window.origin`
    - Hors périmètre (prime aux bogues) ?
    - Pas d'impact (cookie, SOP) ?

Pas le `alert()` :

- Quand la charge utile est réfléchie 89 fois, il va falloir cliquer 89 fois sur ok
- Si la XSS est stockée et qu'on a pas les droits pour la supprimer ?
- Si on est en env. de prod. et que les clients la voient ?
- Souvent sur liste noire d'un WAF
- Privilégier `console.log()` pour répondre à ces problématiques

La part d'ombre, déclenchement en aveugle ?

- Formulaire de contact
- Ticket de support
- Interface d'administration
- En-tête `Referer` ou `User-Agent` ➡️ Analyse de contenu
- Réfléchie dans une autre application qui utilise la même BDD ou l'API ou application mobile
- [XSS Hunter Express](https://github.com/mandatoryprogrammer/xsshunter-express), app. web, pas maintenu
- [bXSS](https://github.com/LewisArdern/bXSS), notifications (courriel, chats), peu maintenu
- [ezXSS](https://github.com/ssl/ezXSS), app. web + notifications, maintenu

Ressources :

- [PayloadsAllTheThings - XSS - Identify an XSS endpoint](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection#identify-an-xss-endpoint)
- [PayloadsAllTheThings - XSS - Blind XSS](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection#blind-xss)
- [LiveOverflow - DO NOT USE alert(1) for XSS (vidéo)](https://www.youtube.com/watch?v=KHwVjzWei1c)
- [LiveOverflow - DO NOT USE alert(1) for XSS (article)](https://liveoverflow.com/do-not-use-alert-1-in-xss/)
