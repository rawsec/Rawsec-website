---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E08 - Nomenclature du test d'intrusion"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - "test d'intrusion"
  - pentest
date: 2024/03/03 23:59:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 8

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/7v0IaP3RKzG1yU8l4fmM2I)
- [Deezer](https://www.deezer.com/fr/episode/612172972)
- [Youtube](https://youtu.be/OD8MdQBcgdI)
- [Youtube Music](https://music.youtube.com/watch?v=OD8MdQBcgdI)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/3d5df7ae-104e-4ff0-811b-6d4185972316/service-hacktion-s01e08---nomenclature-du-test-d'intrusion)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/OGE5YmMwZDEtZGQyOS00ODBlLWI1M2MtYTM5MDM5OGQyNTQx)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/s01e08-nomenclature-du-test-dintrusion/id1725734663?i=1000647864328)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=19782857238)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e08-nomenclature-du-test-dintrusion)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e08-nomenclature-du-test-di-202475996)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e08-nomenclature-du-test-d-intrusion/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e08-nomenclature-du-test-d-intrusion)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-8a9bc0d1-dd29-480e-b53c-a390398d2541/s01e08-nomenclature-du-test-d-intrusion/)
- [Spreaker](https://www.spreaker.com/episode/s01e08-nomenclature-du-test-d-intrusion--58911632)

## Notes

On entend souvent :

- TI interne ➡️ infra
- TI externe ➡️ web

Amalgame :

- interne / sur site
- externe / à distance

En anglais : infrastructure penetration test, web application penetration test

Absurdités :

- TI infra. à distance ➡️ TI interne externe ?
- TI infra sur site ➡️ TI interne interne ?
- TI web sur site ➡️ TI externe interne ?
- TI web à distance ➡️ TI externe externe ?

Externe ou interne qualifie la nature de l'exposition du réseau, pas l'emplacement des auditeurs vis-à-vis de celui-ci.

Alors qu'on devrait parler de :

- TI infra. interne
- TI infra. externe
- TI (app.) web

Si dans 95% des cas le TI web se fait à distance et dans 80% des cas le TI infra. se fait sur site, on peut abréger l'implicite de la manière suivante :

- TI infra
- TI web
