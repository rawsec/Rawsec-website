---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E02 - Revue d'actualité n°1"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - whatsapp
  - hameçonnage
  - clickjacking
  - phishing
  - rce
  - dns
  - outlook
  - ssrf
  - php
date: 2024/01/08 01:00:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 2

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/02YO88BG0wkBJ6HbOgpGO2)
- [Deezer](https://www.deezer.com/fr/episode/592966322)
- [Youtube](https://youtu.be/ZuFG0mGH_4U)
- [Youtube Music](https://music.youtube.com/watch?v=ZuFG0mGH_4U)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/df2d7b76-0ce1-435e-b18c-7d4a1dcc3fb5/service-hacktion-s01e02---revue-d'actualit%C3%A9-n%C2%B01)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/NzU5NTc2ZTgtZDYwNS00ZTVjLTk5YjctOGQ3MTA4NzI4Mjg4)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/service-hacktion/id1725734663?i=1000641633638)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=18023247145)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e02-revue-dactualite-n-degres-1)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e02-revue-dactualite-n1-197051675)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e02-revue-d-actualite-n-1/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e02-revue-d-actualite-n-1)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-759576e8-d605-4e5c-99b7-8d7108728288/s01e02-revue-d-actualite-n-1/)
- [Spreaker](https://www.spreaker.com/episode/s01e02-revue-d-actualite-n-1--58700363)

## Articles

Extrait du [magazine en ligne de sécurité applicative (_Appsec Ezine_)](https://pathonproject.com/appsecezine/) n°515

- 🇬🇧 [Security Is Broken - _Don't Believe Your Eyes - A WhatsApp Clickjacking Vulnerability_](https://00xbyte.github.io/posts/Don't-Believe-Your-Eyes-A-WhatsApp-Clickjacking-Vulnerability/)
  - 🇫🇷 Vous n'en croirez pas vos yeux - Une vulnérabilité de détournement de clics sur WhatsApp

L'auteur, Zuki, a trouvé une vulnérabilité de détournement de clic dans WhatsApp permettant de faire des attaques d'hameçonnage.
Un cybercriminel peut envoyer à n'importe qui un message élaboré avec un lien qui semble mener à un site web légitime, mais qui mène sur un site arbitraire contrôlé par l'attaquant.

Zuki voulait chercher dans la fonctionnalité de prévisualisation de lien. La prévisualisation n'est pas générée par la victime, mais par l'attaquant lors de l'envoi du message. Il est possible de générer une prévisualisation qui ne correspond pas au lien.

Comme le trafic est chiffré, Zuki a utilisé le débogueur JS pour injecter un point d'interruption juste avant le chiffrement.

Propriétés :

- `text` : Le texte du message
- `canonicalURL` : Le domaine qui apparaît au bas de l'aperçu
- `matchedText` : Semble être comparé à `canonicalURL`, et a également testé que sa valeur apparaît dans `text`

Zuki a découvert qu'en supprimant `matchedText`, on pouvait avoir deux URLs qui ne correspondent pas.

Pour aller plus loin, l'auteur souhait que le lien malveillant original ne soit pas visible dans le message.

Il a donc injecté un caractère Unicode U+202E (_Right-To-Left Override_ (RtLO)) pour modifier l'affichage de droite a gauche. Il a donc réservé le nom de domaine `moc.margatsni.nl` qui une fois renversé donne `ln.instagram.com`. `https://moc.margatsni.nl//:sttph` apparaitra donc comme `https://ln.instagram.com//:sptth`. WhatsApp prend en charge l'Unicode et en particulier l'affichage des caractères BiDi / RTL.

Le lien malveillant pointe vers le site de l'attaquant ressemble visuellement à celui d'Instragram et la prévisualisation est celle du vrai site Instragram. Parfait pour piéger la victime.

Pas de correction :

- générer la prévisualisation depuis le client est impossible pour des questions évidentes de vie privée
- depuis les serveurs de WhatsApp impossible car 1) en théorie la conversation chiffrée de bout en bout 2) le cas échéant risque de SSRF
- filtrer les caractères BiDi empêcherait l'affichage correct en Hébreu ou en Arabe.

La vulnérabilité est bien la non-correspondance entre l'URL du texte et celle de la prévisualisation. La meilleure solution, qui resterait imparfaite, serait de vérifier que l'URL de prévisualisation est contenu dans le texte et de ne pas afficher la prévisualisation le cas échéant.

Note : l'auteur pense avoir trouvé une vulnérabilité similaire sur Facebook, Android Message, Google Keep, Google Photo. Mais il n'en est rien. C'est simplement la norme Unicode normale et il n'y a pas de prévisualisation sur ces services.

- 🇬🇧 [solid-snail - _npm search RCE? - Escape Sequence Injection_](https://blog.solidsnail.com/posts/npm-esc-seq)
  - 🇫🇷 Éxécution de commande à distance (ECD) dans `npm search` ? - Injection de séquence d'échappement

C'est une histoire de vulnérabilité corrigée / pas corrigée.

Aussi dans Radare2 et Github CLI.

Dans l'interface en ligne de commande (ILC) de `npm`, une expression régulière contournable.

Solution = filtrage de certains caractères / séquences d'échappement.

- 🇬🇧 [Vin01 - _SSH ProxyCommand == unexpected code execution (CVE-2023-51385)_](https://vin01.github.io/piptagole/ssh/security/openssh/libssh/remote-code-execution/2023/12/20/openssh-proxycommand-libssh-rce.html)
  - 🇫🇷 SSH ProxyCommand == exécution de code inattendue (CVE-2023-51385)

Dans `ProxyCommand`, injection via `%h` ou `%p`.

Exemple de preuve de concept avec les sous-modules git, avec une URL SSH et une commande entourée de guillemets simples inverses à la place du domaine.

- 🇬🇧 [ProjectDiscovery - _A Guide to DNS Takeovers: The Misunderstood Cousin of Subdomain Takeovers_](https://blog.projectdiscovery.io/guide-to-dns-takeovers/)
  - 🇫🇷 Un guide sur les prises de contrôle de DNS : Le cousin incompris des prises de contrôle de sous-domaines

Rappel sur la prise de contrôle de sous-domaine.

Prise de contrôle de DNS : compromission d'un serveur DNS dans la chaine de résolution.

Différence entre prise de contrôle de sous-domaine et de DNS.

Improbable de compromettre un serveur DNS donc comment faire en pratique ?

Beaucoup de fournisseurs infonuagiques proposent de gérer des zones DNS.
On peut désigner le fournisseur DNS infonuagique comme serveur de nom faisant autorité.

Scénario (en vrai, c'est improbable) :

- La victime achète un domaine
- La victime établit une zone avec le fournisseur DNS
- La victime définit la zone comme serveur de noms faisant autorité pour son nouveau domaine
- À un moment donné, la victime a décidé qu'elle ne voulait plus rien héberger sur ce domaine
- La victime supprime la zone dans le fournisseur DNS, mais ne modifie jamais les serveurs de noms faisant autorité assignés au domaine
- Le domaine n'est pas expiré, juste inutilisé

Cela crée une situation où les enregistrements DNS d'un domaine sont contrôlés par des serveurs DNS que tout le monde peut utiliser, mais personne ne revendique le domaine. En tant qu'attaquant, on peut créer une zone qui utilise l'un des mêmes serveurs de noms, on peut ajouter des enregistrements DNS pour ce domaine.

La plupart des fournisseurs de DNS ne vous permettent pas de choisir les serveurs de noms qui vous sont attribués. Nous pouvons y remédier en créant de nombreuses zones jusqu'à ce que nous en obtenions une avec un serveur DNS correspondant.

Liste des fournisseurs vulnérables : [Can I Take Over DNS?](https://github.com/indianajson/can-i-take-over-dns?ref=blog.projectdiscovery.io)

Détection : Le domaine renvoie `SERVFAIL` (ou parfois `REFUSED`) lorsqu'on essaye de le résoudre.

Il existe un modèle Nuclei : `servfail-refused-hosts`.

- 🇬🇧 [CheckPoint - _The Obvious, the Normal, and the Advanced: A Comprehensive Analysis of Outlook Attack Vectors_](https://research.checkpoint.com/2023/the-obvious-the-normal-and-the-advanced-a-comprehensive-analysis-of-outlook-attack-vectors/)
  - 🇫🇷 L'évident, le normal et l'avancé : Une analyse complète des vecteurs d'attaque Outlook

Détails de tous les cas de figure :

1. L'évident : le vecteur d'attaque par hyperlien
    1. Un simple clic sur le lien, pas de confirmation
    2. Liens autres que web (hors sujet)
2. Le normal : le vecteur d'attaque par pièce jointe
    1. Double clic sur la P.J.
        1. Aucun clic, l'extension de la P.J. est marquée comme un type de fichier non sûr ➡️ impossible de l'ouvrir
        2. Un double-clic et un simple-clic : l'extension de la P.J. n'est pas marquée comme non sûre et n'est pas non plus marqué comme sûre
            - Dans ce scénario, une boîte de dialogue s'affiche pour demander à l'utilisateur de confirmer l'ouverture du fichier
            - Pour les devs d'app Windows, il faut honorer le Mark-of-the-Web (MotW)
        3. Un double-clic : l'extension de la P.J. est marqué comme un type de fichier sûre.
    2. Simple clic : prévisualisation de la P.J.
        1. Pas de prévisualisation : l'extension de la P.J. est marquée comme un type de fichier non sûre
        2. Pas de prévisualisation : il n'y a pas d'application de prévisualisation enregistrée pour l'extension
        3. Deux simples clics : L'application de prévisualisation est enregistrée, mais a besoin d'une confirmation supplémentaire pour prévisualiser le contenu.
        4. Un clic : l'application de prévisualisation est enregistrée et marquée comme sûre.
3. L'avancé : le vecteur d'attaque par lecture des courriels et des objets spéciaux
    1. Le vecteur d'attaque de la lecture des courriels
        - Exemple : panneau de prévisualisation / lecture
       - 3 formats de courriels : texte, HTML, TNEF (spécifique Outlook)
    2. Le vecteur d'attaque d'objets spéciaux Outlook
        - Juste recevoir le courriel dans le client Outlook est suffisant, pas besoin de le lire
        - Vulnérabilité de jour zéro [CVE-2023-23397](https://www.cert.ssi.gouv.fr/alerte/CERTFR-2023-ALE-002/) permettait à un Windows local de divulguer des informations d'identification (Net)NTLM au serveur contrôlé par l'attaquant à cause des objets de rappel, [démo par MDsec](https://www.mdsec.co.uk/2023/03/exploiting-cve-2023-23397-microsoft-outlook-elevation-of-privilege-vulnerability/)

- 🇬🇧 [Ambionics - _Introducing wrapwrap: using PHP filters to wrap a file with a prefix and suffix_](https://www.ambionics.io/blog/wrapwrap-php-filters-suffix)
  - 🇫🇷 Présentation de wrapwrap : utilisation de filtres PHP pour encapsuler un fichier avec un préfixe et un suffixe

De base, SSRF en aveugle sur une appli PHP avec un analyseur syntaxique XML.

Le contenu est quand même retourné si les balises ne sont pas fermées.

Il est possible de récupérer le contenu avec la technique de [chaîne de filtre PHP publié par Synacktiv](https://www.synacktiv.com/en/publications/php-filters-chain-what-is-it-and-how-to-use-it) (ajout de caractères base64 à un contenu pré-existant en jouant sur des conversions des jeux de caractères / encodage). En général, utilisé pour générer un interpréteur de commande web. Il est possible de détourner la technique pour ajouter les balises XML ouvrantes devant la ressource à extraire.

Jusqu'ici, rien de nouveau.

Mais comment faire s'il avait fallu avoir une syntaxe XML valide (balises fermantes) ou si la SSRF était sur du JSON ?
Cette fois si un préfixe ne suffit pas, il faut aussi un suffixe.

Comment ajouter un suffixe ? Assez complexe, je vous laisse lire.

## Outils

Extrait de [l'inventaire de Cybersécurité de Rawsec (_Rawsec's Cybersecurity Inventory_)](https://inventory.raw.pm/)

🛠️ [Geolocation Estimation](https://labs.tib.eu/geoestimation/) par [@TIBHannover](https://twitter.com/TIBHannover)

Renseignement géographique (trouver l'emplacement d'une image) automatique en utilisant l'apprentissage approfondi.

🛠️ [Echidna](https://github.com/Echidna-Pentest/Echidna) par `tyeurada` & [@shiracamus](https://twitter.com/shiracamus)

Plate-forme collaborative de tests d'intrusion ; partage de terminaux, extraction d'informations sur les cibles, suggestion de commandes, recherche de code d'exploitation, chat, visualisation de graphiques.

🛠️ [PyWSUS](https://github.com/GoSecure/pywsus) par [@e2cda98eb051178](https://twitter.com/e2cda98eb051178) à [@GoSecure_Inc](https://twitter.com/GoSecure_Inc)

Serveur WSUS conçu pour envoyer des réponses malveillantes aux clients

🛠️ [NTLM to password](https://ntlm.pw/) par [@lkarlslund](https://twitter.com/@lkarlslund)

Table de consultation des empreintes NTLM, des milliards de mots de passe indexés

🛠️ [NetExec](https://github.com/Pennyw0rth/NetExec) par [@MJHallenbeck](https://twitter.com/MJHallenbeck), [@mpgn_x64](https://twitter.com/mpgn_x64), [@al3x_n3ff](https://twitter.com/al3x_n3ff), [@Memory_before](https://twitter.com/Memory_before), [@_zblurx](https://twitter.com/_zblurx) et d'autres

Test d'intrusion des environnements Windows / Active Directory ; fourche de CrackMapExec. Regarder ma vidéo [Point de vue - CrackMapExec vs NetExec](https://youtu.be/k77CDwyryFc).

🛠️ [GooFuzz](https://github.com/m3n0sd0n4ld/GooFuzz) par [@David_Uton](https://twitter.com/David_Uton)

Reconnaissance passive énumérant des répertoires, des fichiers, des sous-domaines ou des paramètres à l'aide de dorks Google.

🛠️ [Whisker](https://github.com/eladshamir/Whisker) par [@elad_shamir](https://twitter.com/elad_shamir) and [@podalirius_](https://twitter.com/podalirius_)

Prendre le contrôle de comptes d'utilisateurs et d'ordinateurs Active Directory en manipulant leur attribut `msDS-KeyCredentialLink`, ce qui a pour effet d'ajouter des informations d'identification fictives au compte cible.

🛠️ [FTK Imager](https://www.exterro.com/ftk-imager) par [@Exterro](https://twitter.com/Exterro)

Enquête numérique sur les appareils électroniques ; fonctions d'imagerie de disque complet : prévisualisation et imagerie des disques durs des ordinateurs Windows et Linux, des CD, des DVD, des clés USB et autres ; montage d'images judiciaires : montage d'une image pour une vue en lecture seule qui tire parti de l'explorateur de fichiers ; prévisualisation des données ; capture de la RAM.
