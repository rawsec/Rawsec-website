---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E05 - Erreurs volontaires dans les Preuves de Concept et les outils offensifs"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - web
  - pdc
  - poc
  - outils
date: 2024/02/09 00:53:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 5

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/5G1lszJ7ya0aNbzz1CBLns)
- [Deezer](https://www.deezer.com/fr/episode/601840152)
- [Youtube](https://youtu.be/smgvkZYCz98)
- [Youtube Music](https://music.youtube.com/watch?v=smgvkZYCz98)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/e8d3cc6d-4b1a-494e-be71-56d3ba23faea/service-hacktion-s01e05---erreurs-volontaires-dans-les-preuves-de-concept-et-les-outils-offensifs)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/ZGRhOTBjY2ItZWY4Mi00NjBlLWFiYjQtM2ZiZWRhM2VmODEw)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/s01e05-erreurs-volontaires-dans-les-preuves-de/id1725734663?i=1000644791650)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=19034423119)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e05-erreurs-volontaires-dans-les-preuves-de-concept-et-les-outils-offensifs)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e05-erreurs-volontaires-dan-200271051)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e05-erreurs-volontaires-dans-les-preuves-de-con/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e05-erreurs-volontaires-dans-les-preuves-de-con)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-dda90ccb-ef82-460e-abb4-3fbeda3ef810/s01e05-erreurs-volontaires-dans-les-preuves-de-concept-et-les-outils-offensifs/)
- [Spreaker](https://www.spreaker.com/episode/s01e05-erreurs-volontaires-dans-les-preuves-de-concept-et-les-outils-offensifs--58700362)

## Lexique

- **PdC** - _Preuves de Concept_ - Production d'un code source permettant de prouver la présence ou l'exploitabilité d'une vulnérabilité dans un système informatique.
- **IdC** - _Indicateur de Compromission_ - Artefact observé sur un système informatique indiquant une intrusion informatique avec un haut niveau de certitude.
- **Piratin** - Pirate amateur à la recherche d'une intrusion facile, souvent irresponsable et sans éthique, qui utilise des scripts déjà existants, disponibles gratuitement sur le Net, pour effectuer ses attaques malveillantes. ([source](https://vitrinelinguistique.oqlf.gouv.qc.ca/fiche-gdt/fiche/8872869/pirate-adolescent))

## Exemple n°1 - Rubeus, nom du processus de connexion

`User32LogonProcesss` (avec 3 `s`) avec un commentaire surprenant :

```cs
        public static IntPtr LsaRegisterLogonProcessHelper()
        {
            // helper that establishes a connection to the LSA server and verifies that the caller is a logon application
            //  used for Kerberos ticket enumeration for ALL users

            var logonProcessName = "User32LogonProcesss"; // yes I know this is "weird" ;)
```

Visible dans les journaux d'évènement Windows.

Supprimé quelques heures après avoir été exposé dans un tweet, avec un message de transaction lui aussi évocateur :

```
 Removed call to LsaRegisterLogonProcess

-Removed call to `LsaRegisterLogonProcess` as it was ultimately unnecessary.

So long User32LogonProcesss IOC, we hardly knew thee :)
```

Référence :

- [Twitter - mise en avant](https://twitter.com/_RastaMouse/status/1747636529613197757), 2024-01-17 16:06
- [Github - transaction de suppression](https://github.com/GhostPack/Rubeus/commit/b303d1c9d8076bc4b5f97d8151fe3c39c42e50d6), 2024-01-17 19:42
- [Github - code avant modification](https://github.com/GhostPack/Rubeus/blob/4a2fc8a3d99eb45f884486d86f9fd4552b5544d0/Rubeus/lib/LSA.cs#L62)
- [SpecterOps - mise en avant lors d'une étude AD CS](https://posts.specterops.io/hunting-in-active-directory-unconstrained-delegation-forests-trusts-71f2b33688e1)
- [Fortinet - Règle de détection](https://help.fortinet.com/fsiem/Public_Resource_Access/7_1_0/rules/PH_RULE_Register_new_Logon_Process_by_Rubeus.htm)
- [detection.fyi - Règle de détection](https://detection.fyi/sigmahq/sigma/windows/builtin/security/win_security_register_new_logon_process_by_rubeus/)
- [FireEye - Règle de détection Snort](https://github.com/search?q=repo%3Amandiant%2Fred_team_tool_countermeasures%20User32LogonProcesss&type=code)
- [SecurityBreak - Règle de détection Yara](https://blog.securitybreak.io/100daysofyara-challenge-04c966eab1ae)

## Exemple n°2 - mimikatz, domaine de connexion

```c
  KIWI_NEVERTIME(&validationInfo.PasswordMustChange);
  RtlInitUnicodeString(&validationInfo.LogonDomainName, L"<3 eo.oe ~ ANSSI E>");

  validationInfo.EffectiveName		= ticket.ClientName->Names[0];
```

- [Twitter - mise en avant](https://twitter.com/cnotin/status/639791642170556416/photo/1), 2015-09-04 23:31
- [Github - mise en avant](https://github.com/gentilkiwi/mimikatz/issues/16), 2015-10-22
- [Github - code](https://github.com/gentilkiwi/mimikatz/blob/42993f51027a0c47ac60e6483b6fd08ff09e9f47/mimikatz/modules/kerberos/kuhl_m_kerberos.c#L589)
- [ANSSI - Bulletin d'actualité CERTFR-2015-ACT-025](https://web.archive.org/web/20150702043928/https://cert.ssi.gouv.fr/site/CERTFR-2015-ACT-025/)
- [ADsecurity - mise en avant](https://adsecurity.org/?p=1515)

## Exemple n°3 - PdC EDB

- virgules en trop
- guillemet ou parenthèse manquante
- valeurs codées en dur (ex : `127.0.0.1`)
- indentation ou saut de ligne arbitraire

## Questions

- Doit-on placer ce genre d'Indicateur de Compromission dans les PdC ? Contre les _piratins_.
  - Efficacité ? Combien se font avoir ? Qui surveille ces IdC ?
  - Les piratins arrivent-ils réellement à utiliser ce genre d'outil ?
  - Les menaces persistantes avancées développent leurs propres outils, relisent et modifient les outils existant de base pour évader les signatures ?
  - Le haché du binaire ou les nombreuses chaines de caractères uniques ne sont-elles pas suffisantes ?
- Si oui, doit-on placer un indice pour le lecteur attentif ?
- Si l'on découvre ce genre d'IdC, doit-on en faire part publiquement ?
  - Aider les équipes défensives ? Vraiment ? Car ils auront déjà sans doute une règle dans leur outil de détection sans le savoir.
  - Risque d'aider les cybercriminels ?
- Quand code compilé, ne pas fournir de binaire pré-compilé suffit à repousser les _piratins_ ?
  - Ne prouve pas qu'on a lu le code et sait ce que l'on fait.
  - IdC dans le binaire pré-compilé, mais pas dans le code source ?
- Il paraît que des groupes soutenus par des états utilisent les binaires pré-compilés, fait exprès ou non ?
  - Simuler des attaquants non avancés ?
  - Attaque sous faux drapeau ?
- IdC trop connu ? Faux-positif ?
- Pour les outils, obliger de faire confiance à partir d'un certain moment ?
  - Pour un outil d'une taille très conséquente, difficile d'auditer l'ensemble du code ?
  - Pour un outil évolue souvent, difficile d'effectuer un suivi des modifications fréquent ?
  - Nombre d'outils ?
  - Variété des langages de programmation ?
  - Qui a déjà lu entièrement le code de nmap ? De netexec ?
  - Et les outils propriétaires ? Nessus ? Rétro-ingénieurie ?
  - Où situer le curseur de confiance ?
    - PdC -> systématiquement ?
    - Outils
      - Selon l'auteur ?
      - Selon le niveau d'adoption ?
      - Selon les distributions Linux qui empaquètent ?

