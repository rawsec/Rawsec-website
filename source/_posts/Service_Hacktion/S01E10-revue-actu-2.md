---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E10 - Revue d'actualité n°2, spéciale 1er avril"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
date: 2024/04/01 19:50:00
updated: 2024/05/15 02:26:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 10

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/0o16m8WtSB2Km9wqrpFq98)
- [Deezer](https://www.deezer.com/fr/episode/621252261)
- [Youtube](https://youtu.be/DbSx6woFGGk)
- [Youtube Music](https://music.youtube.com/podcast/DbSx6woFGGk)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/e8bb7efd-e27c-4608-a955-ed4693d88673/service-hacktion-s01e10---revue-d'actualit%C3%A9-n%C2%B02-sp%C3%A9ciale-1er-avril)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/NWViZTcyZmMtNzdhNy00MWUwLWE4NTEtZGRjODE3MmQzM2Fj?sa=X&ved=0CAUQkfYCahcKEwjQosLByqGFAxUAAAAAHQAAAAAQAQ)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/s01e10-revue-dactualit%C3%A9-n-2-sp%C3%A9ciale-1er-avril/id1725734663?i=1000651099978)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=20841262965)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e10-revue-dactualite-n-degres-2-speciale-1er-avril)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e10-revue-dactualite-n2-spe-205280590)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e10-revue-d-actualite-n-2-speciale-1er-avril/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e10-revue-d-actualite-n-2-speciale-1er-avril)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-5ebe72fc-77a7-41e0-a851-ddc8172d33ac/s01e10-revue-d-actualite-n-2-speciale-1er-avril/)
- [Spreaker](https://www.spreaker.com/episode/s01e10-revue-d-actualite-n-2-speciale-1er-avril--59251870)

## Notes

1. **Bug Booty**
  - HackerOne lance un partenariat avec OnlyFans
  - De nouveaux programmes où les bogues seront payés non plus en argent, mais en "nudes"
  - Ces programmes seront appelés Bug Booty
2. **xz**
  - Un étudiant à l'origine de la porte dérobée xz
  - Il aurait voulu déployer un rançongiciel pour se payer des "skins" fortnite
3. **BreizhCTF**
  - Cette année les "rumps" du BreizhCTF seront obligatoirement en breton
4. **STHACK**
  - Sthack annulé
  - Un membre du staff assassiné pour avoir commandé des pains au chocolat pour le petit déjeuner après le CTF au lieu de chocolatines
5. **Debian**
  - Debian fait partenariat avec ArchLinux
  - Pour préparer Debian 14 ("stones") qui passera en "rolling release cutting edge"
6. **Linux**
  - Le code du noyau Linux deviendra privé
  - Cela fait échos aux récents évènements de porte dérobée dans les sources de xz et nft
7. **NetExec**
  - NetExec supprime le support de Microsoft Active Directory pour se concentrer sur une autre implémentation LDAP : NetIQ eDirectory
8. `Q2V0IMOpcGlzb2RlIGVzdCBiaWVuIHPDu3IgdW4gcG9pc3NvbiBkJ0F2cmlsCg==`
