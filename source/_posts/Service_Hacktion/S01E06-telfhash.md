---
layout: post
title: "Service Hacktion - Notes attachées au balado S01E06 - Identifier les familles de maliciel avec Telfhash (Trend Micro ELF Hash)"
lang: fr
categories:
  - security
tags:
  - podcast
  - balado
  - menace
  - maliciel
  - hachage
date: 2024/02/18 18:02:00
thumbnail: /images/service-hacktion-podcast.jpg
authorId: noraj
toc: true
---
Saison | Épisode
--- | ---
1 | 6

<iframe title="Spotify Embed: Service Hacktion" style="border-radius: 12px" width="624" height="351" frameborder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" src="https://open.spotify.com/embed/show/2cvqgxJWv3mrwIWvR39Kyk/video?utm_source=oembed"></iframe>

- [Spotify](https://open.spotify.com/episode/0fy8gnY96cWtuA7JkF9FMF)
- [Deezer](https://www.deezer.com/fr/episode/606596692)
- [Youtube](https://youtu.be/Qzi4fV2UD9o)
- [Youtube Music](https://music.youtube.com/watch?v=Qzi4fV2UD9o)
- [Amazon Music](https://music.amazon.fr/podcasts/985ea8b9-30bf-4145-a08a-1ecdc021f695/episodes/65ff16d9-b2aa-4d93-ab37-82370843ce97/service-hacktion-s01e06---identifier-les-familles-de-maliciel-avec-telfhash-trend-micro-elf-hash)
- [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lZjcwNWIzMC9wb2RjYXN0L3Jzcw/episode/MmM2NGEwOWEtMGNjMS00NmY5LWEzMjQtZWU5NjVkMGU3ODhj)
- [Apple Podcast](https://podcasts.apple.com/us/podcast/s01e06-identifier-les-familles-de-maliciel-avec/id1725734663?i=1000645769374)
- [Podcast Index](https://podcastindex.org/podcast/6727486?episode=19313874182)
- [podCloud](https://podcloud.fr/podcast/service-hacktion/episode/s01e06-identifier-les-familles-de-maliciel-avec-telfhash-trend-micro-elf-hash)
- [Podchaser](https://www.podchaser.com/podcasts/service-hacktion-5564666/episodes/s01e06-identifier-les-familles-201061376)
- [podtail](https://podtail.com/podcast/service-hacktion/s01e06-identifier-les-familles-de-maliciel-avec-te/)
- [Podcasts Français](https://podcasts-francais.fr/podcast/service-hacktion/s01e06-identifier-les-familles-de-maliciel-avec-te)
- [Vodio](https://www.vodio.fr/vodiotheque/i/rss-959-2c64a09a-0cc1-46f9-a324-ee965d0e788c/s01e06-identifier-les-familles-de-maliciel-avec-telfhash-trend-micro-elf-hash/)
- [Spreaker](https://www.spreaker.com/episode/s01e06-identifier-les-familles-de-maliciel-avec-telfhash-trend-micro-elf-hash--58894163)

## Lexique

- **algorithme de hachage** - Fonction mathématique qui permet la création d'un haché ou code de hachage en transformant un groupe de données de taille variable en un code unique de taille fixe.
- **haché** ou **code de hachage** - Résultat d'une fonction ou d'un algorithme de hachage cryptographique.

## Introduction

**telfhash** pour _Trend Micro ELF Hash_ est un haché de symboles pour les fichiers ELF, tout comme `imphash` est un haché d'importations pour les fichiers PE. Avec telfhash, on peut regrouper les fichiers ELF par similarité sur la base des symboles. Cela est particulièrement utile pour regrouper des échantillons malveillants. Si un échantillon n'a pas de symboles, telfhash utilise ses destinations d'adresses d'appel pour émuler une liste de symboles.

telfhash utilise **TLSH** (_Trend Micro Locality Sensitive Hash_) pour générer le code de hachage.

_Source : [trendmicro/telfhash](https://github.com/trendmicro/telfhash)_

TLSH est une bibliothèque de correspondance approximative (_fuzzy matching_ en anglais). À partir d'un flux d'octets d'une longueur minimale de 50 octets, TLSH génère un haché qui peut être utilisée pour des comparaisons de similarité. Des objets similaires auront des hachés similaires, ce qui permet de détecter des objets similaires en comparant leurs hachés. Il convient de noter que le flux d'octets doit être suffisamment complexe. Par exemple, un flux d'octets identiques ne générera pas de haché.

_Source : [trendmicro/tlsh](https://github.com/trendmicro/tlsh)_

## Details

La différence avec un algorithme de hachage classique, pour lequel des objets similaires auront des hachés très différents, avec telfhash des objets similaires auront des hachés similaires.

Même si les créateurs de logiciels malveillants ajoutent de nouvelles fonctionnalités à leurs échantillons malveillants en ajoutant ou en important de nouvelles fonctions de bibliothèque, le haché telfhash restera proche de l'original et permettra toujours de déterminer si les échantillons de logiciels malveillants appartiennent à la même famille.

Actuellement, telfhash prend en charge les architectures x86, x86-64, ARM et MIPS, qui couvrent la majorité des échantillons de logiciels malveillants IoT et le haché est agnostique de l'architecture.

Le haché telfhash est affiché sur VirusTotal et les utilisateurs de VirusTotal Intelligence cliquer dessus pour trouver les autres binaires ELF similaires.

## Ressources

- [Historique et justifications](https://www.trendmicro.com/en_us/research/20/d/grouping-linux-iot-malware-samples-with-trend-micro-elf-hash.html)
- [Détails techniques](https://documents.trendmicro.com/assets/pdf/TB_Telfhash-%20An%20Algorithm%20That%20Finds%20Similar%20Malicious%20ELF%20Files%20Used%20in%20Linux%20IoT%20Malware.pdf)
- [Support VirusTotal](https://www.trendmicro.com/en_us/research/20/j/virustotal-now-supports-trend-micro-elf-hash.html)
